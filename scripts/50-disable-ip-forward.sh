#!/bin/sh

sysctl_conf=/etc/sysctl.d/10-default.conf

sed -i 's|net.ipv4.ip_forward=1|net.ipv4.ip_forward=0|g' ${sysctl_conf}
sed -i 's|net.ipv6.conf.all.forwarding=1|net.ipv6.conf.all.forwarding=0|g' ${sysctl_conf}

exit 0