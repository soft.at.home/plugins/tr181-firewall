#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-firewall"
datamodel_root="Firewall"

FW_DEFAULTS=/etc/amx/tr181-firewall/iptables.d/firewall.defaults
FW6_DEFAULTS=/etc/amx/tr181-firewall/iptables.d/firewall_ipv6.defaults

firewall_load_defaults(){
    iptables-restore < ${FW_DEFAULTS}
    ip6tables-restore < ${FW6_DEFAULTS}
    if [ -d /etc/amx/tr181-firewall/rules4 ]; then
        for f in /etc/amx/tr181-firewall/rules4/* ; do
            iptables-restore -n < $f
        done
    fi
    if [ -d /etc/amx/tr181-firewall/rules6 ]; then
        for f in /etc/amx/tr181-firewall/rules6/* ; do
            ip6tables-restore -n < $f
        done
    fi
}

firewall_clear(){
    iptables -P INPUT ACCEPT
    iptables -P FORWARD ACCEPT
    iptables -F
    iptables -X
    iptables -t nat -F
    iptables -t nat -X

    ip6tables -P INPUT ACCEPT
    ip6tables -P FORWARD ACCEPT
    ip6tables -F
    ip6tables -X
}

case $1 in
    boot)
        firewall_load_defaults
        process_boot ${name} -D
        echo "1" > /proc/sys/net/ipv4/ip_forward
        echo "1" > /proc/sys/net/ipv6/conf/all/forwarding
        ;;
    start)
        firewall_load_defaults
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name} 20
        firewall_clear
        ;;
    shutdown)
        process_shutdown ${name}
        firewall_clear
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        process_debug_info "NAT"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
