/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "protocols.h"
#include "firewall_utilities.h" // for STRING_EMPTY
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>
#include <netdb.h>

#define ME "firewall"

bool protocol_from_string(amxc_var_t* protocols, const char* csv_protocols) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t var_list;
    amxc_string_t var_string;
    bool retval = false;

    amxc_var_set_type(protocols, AMXC_VAR_ID_LIST);

    amxc_var_init(&var_list);
    amxc_string_init(&var_string, 0);

    when_null(protocols, out);
    when_str_empty(csv_protocols, out);

    when_false(amxc_string_set(&var_string, csv_protocols) > 0, out);
    when_false(amxc_string_to_lower(&var_string) == 0, out); // musl libc getprotobyname only accepts lower case names
    when_false(amxc_string_csv_to_var(&var_string, &var_list, NULL) == AMXC_STRING_SPLIT_OK, out);

    amxc_var_for_each(var, (&var_list)) {
        int32_t protocol = amxc_var_dyncast(int32_t, var);
        const char* protocol_name = amxc_var_constcast(cstring_t, var);
        struct protoent* ent = NULL;

        if((protocol == -1) || STRING_EMPTY(protocol_name) || (strcmp(protocol_name, "0") == 0)) {
            continue;
        }

        if(protocol > 0) {
            ent = getprotobynumber(protocol);
        } else if(STRING_EMPTY(protocol_name) == false) {
            ent = getprotobyname(protocol_name);
        }

        when_null_trace(ent, out, ERROR, "Failed to get protocol number of %s", protocol_name);

        amxc_var_add(uint32_t, protocols, ent->p_proto);
    }

    retval = true;

out:
    if(!retval) {
        amxc_var_set_type(protocols, AMXC_VAR_ID_LIST);
    }
    amxc_var_clean(&var_list);
    amxc_string_clean(&var_string);
    FW_PROFILING_OUT();
    return retval;
}

bool protocol_in_protocols(amxc_var_t* protocols, uint32_t protocol) {
    bool found = false;

    amxc_var_for_each(var, protocols) {
        uint32_t a = GET_UINT32(var, NULL);
        if(a == protocol) {
            found = true;
            break;
        }
    }

    return found;
}

bool protocol_compare(amxc_var_t* a, amxc_var_t* b) {
    bool res = false;

    amxc_var_for_each(var, a) {
        if(protocol_in_protocols(b, GET_UINT32(var, NULL))) {
            res = true;
            break;
        }
    }

    return res;
}
