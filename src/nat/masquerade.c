/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "nat/masquerade.h"
#include "nat/portmapping.h"
#include "fwrule_helpers.h"
#include "firewall.h"       // for fw_get_chain
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <netmodel/client.h> // for netmodel_get_amxb_bus
#include <amxd/amxd_object_expression.h>
#include <string.h>

#define ME "nat"

firewall_interface_t* waniface_get_addresses(amxd_object_t* object) {
    natiface_wan_t* waniface = NULL;

    when_null(object->priv, exit);
    waniface = (natiface_wan_t*) object->priv;

exit:
    return waniface == NULL ? NULL : &waniface->address_fwiface;
}

static void status_update(natiface_wan_t* waniface) {
    status_t status = FIREWALL_ERROR;

    switch(waniface->nat.status) {
    case NAT_ACTIVE:
        status = FIREWALL_ENABLED;
        break;
    case NAT_INACTIVE:
        status = FIREWALL_DISABLED;
        break;
    case NAT_INIT:
        status = FIREWALL_DISABLED;
        break;
    case NAT_ERROR_MISCONF:
        status = FIREWALL_ERROR_MISCONFIG;
        break;
    case NAT_ERROR:
        // the default status is Error
        break;
    }

    set_status(waniface->object, status);
}

static void ft_init(ft_t* ft, bool enable, const char* table, const char* chain) {
    ft->enable = enable;
    ft->status = NAT_INIT;
    fw_folder_new(&ft->folder);
    ft->table = table;
    ft->chain = chain;
}

static void ft_deinit(ft_t* ft, const char* msg) {
    if(msg != NULL) {
        SAH_TRACEZ_WARNING(ME, "[%s] failed, rollback changes", msg);
    }
    fw_folder_delete_rules(ft->folder);
    fw_commit(fw_rule_callback);
    ft->status = NAT_INACTIVE;
}

static void ft_delete(ft_t* ft) {
    if(ft->folder != NULL) {
        fw_folder_delete(&ft->folder);
        fw_commit(fw_rule_callback);
    }
}

static void ipv4_netdevname_changed_from(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    natiface_wan_t* waniface = (natiface_wan_t*) fwiface->query_priv;

    if(!STRING_EMPTY(waniface->fwiface.default_netdevname)) {
        toggle_nat_flags_nm(waniface, "nat nat-up", false);
        FREE(waniface->nm_intf_path); // todo maybe use amxc_string_t for its resize property
    }

    FW_PROFILING_OUT();
}

static void ipv4_netdevname_changed_to(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    natiface_wan_t* waniface = (natiface_wan_t*) fwiface->query_priv;

    if(!STRING_EMPTY(waniface->fwiface.default_netdevname)) {
        free(waniface->nm_intf_path);
        waniface->nm_intf_path = get_nm_intf_by_netdev_name(waniface->fwiface.default_netdevname);
    }

    toggle_nat_flags_nm(waniface, NULL, true);

    when_true(waniface->flags != 0, out); // already being configured

    waniface->flags = FW_MODIFIED;
    interface_implement_changes(waniface);

out:
    FW_PROFILING_OUT();
}

void lan_interface_delete(natiface_lan_t* laniface) {
    when_null(laniface, out);

    clean_firewall_interface(&laniface->fwiface);

    if(laniface->ipv4address_subscription != NULL) {
        amxb_subscription_delete(&laniface->ipv4address_subscription);
    }

    amxc_llist_it_take(&laniface->it);
    free(laniface);

out:
    return;
}

static void _lan_interface_delete(amxc_llist_it_t* it) {
    natiface_lan_t* lan_interface = amxc_container_of(it, natiface_lan_t, it);
    lan_interface_delete(lan_interface);
}

natiface_wan_t* interface_create(amxd_object_t* object) {
    natiface_wan_t* waniface = NULL;
    const char* chain = NULL;

    waniface = (natiface_wan_t*) calloc(1, sizeof(natiface_wan_t));
    when_null(waniface, out);

    waniface->object = object;

    waniface->flags = FW_NEW;
    chain = fw_get_chain("masquerade", "POSTROUTING_DEFAULT");
    ft_init(&waniface->nat, false, TABLE_NAT, chain);
    amxc_llist_init(&waniface->lanifaces);
    init_firewall_interface(&waniface->fwiface);
    waniface->fwiface.query_result_pre_change = ipv4_netdevname_changed_from;
    waniface->fwiface.query_result_changed = ipv4_netdevname_changed_to;
    waniface->fwiface.query_priv = waniface;

    init_firewall_interface(&waniface->address_fwiface);
    waniface->address_fwiface.query_result_changed = portmap_hairpin_address_changed;
    waniface->address_fwiface.query_priv = waniface;

    SAH_TRACEZ_INFO(ME, "Create new interface[%s]", OBJECT_NAMED);
    object->priv = waniface;

out:
    return waniface;
}

static bool interface_delete(natiface_wan_t** waniface) {
    when_null(waniface, out);
    when_null(*waniface, out);

    ft_delete(&(*waniface)->nat);

    clean_firewall_interface(&(*waniface)->fwiface);
    clean_firewall_interface(&(*waniface)->address_fwiface);

    amxc_llist_clean(&(*waniface)->lanifaces, _lan_interface_delete);

    (*waniface)->object->priv = NULL;

    toggle_nat_flags_nm(*waniface, "nat nat-up", false);
    free((*waniface)->nm_intf_path);
    free(*waniface);
    *waniface = NULL;

out:
    return true;
}

static void lan_interface_address_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    natiface_lan_t* laniface = (natiface_lan_t*) fwiface->query_priv;
    natiface_wan_t* waniface = laniface->waniface;
    amxd_object_t* object = laniface->object;

    SAH_TRACEZ_INFO(ME, "NatIfSetting[%s] addresses changed", OBJECT_NAMED);

    waniface->flags = FW_MODIFIED;
    interface_implement_changes(waniface);

    FW_PROFILING_OUT();
    return;
}

natiface_lan_t* lan_interface_create(amxd_object_t* object, natiface_wan_t* waniface) {
    natiface_lan_t* laniface = NULL;

    laniface = (natiface_lan_t*) calloc(1, sizeof(natiface_lan_t));
    when_null(laniface, out);

    amxc_llist_append(&waniface->lanifaces, &laniface->it);

    init_firewall_interface(&laniface->fwiface);
    laniface->fwiface.query_priv = laniface;
    laniface->fwiface.query_result_changed = lan_interface_address_changed;

    laniface->waniface = waniface;
    laniface->object = object;
    object->priv = laniface;

out:
    return laniface;
}

static uint32_t nr_of_lans(amxc_llist_t* lanifaces) {
    uint32_t count = 0;

    amxc_llist_for_each(it, lanifaces) {
        natiface_lan_t* laniface = amxc_container_of(it, natiface_lan_t, it);

        if(address_has_ipversion(&laniface->fwiface.address, IPv4)) {
            count++;
            break; // 1 is enough to continue
        }
    }

    return count;
}

static void interface_activate(natiface_wan_t* waniface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    amxd_object_t* object = NULL;
    nat_status_t new_status = NAT_ERROR;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_null(waniface, out);

    object = waniface->object;

    if(STRING_EMPTY(waniface->fwiface.default_netdevname)) {
        SAH_TRACEZ_WARNING(ME, "NAT.InterfaceSetting[%s] is misconfigured because parameter Interface is empty", OBJECT_NAMED);
        new_status = NAT_ERROR_MISCONF;
        goto cleanup;
    }

    if(amxc_llist_is_empty(&waniface->lanifaces)) {
        fw_folder_unset_feature(waniface->nat.folder, FW_FEATURE_SOURCE);
    } else {
        fw_folder_set_feature(waniface->nat.folder, FW_FEATURE_SOURCE);

        if(nr_of_lans(&waniface->lanifaces) == 0) {
            SAH_TRACEZ_INFO(ME, "NatWANIfSetting[%s] is waiting for lan interfaces", OBJECT_NAMED);
            new_status = NAT_ERROR_MISCONF;
            goto cleanup;
        }
    }

    r = fw_folder_fetch_default_rule(waniface->nat.folder);
    when_null_trace(r, cleanup, ERROR, "NatWANIfSetting[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_chain(r, waniface->nat.chain);
    fw_rule_set_table(r, waniface->nat.table);
    fw_rule_set_out_interface(r, waniface->fwiface.default_netdevname);
    fw_rule_set_target_policy(r, FW_RULE_POLICY_MASQUERADE);
    fw_rule_set_ipv4(r, true);
    fw_folder_push_rule(waniface->nat.folder, r);

    amxc_llist_for_each(it, &waniface->lanifaces) {
        natiface_lan_t* laniface = amxc_container_of(it, natiface_lan_t, it);
        const bool is_ipv4 = true;
        const bool is_source = true;
        const bool dont_exclude = false;

        if(!address_has_ipversion(&laniface->fwiface.address, IPv4)) {
            continue;
        }

        when_false(folder_feature_set_address(waniface->nat.folder,
                                              &laniface->fwiface.address,
                                              is_ipv4,
                                              is_source,
                                              dont_exclude), cleanup);
    }

    fw_folder_set_enabled(waniface->nat.folder, true);

    fw_commit(fw_rule_callback);

    new_status = NAT_ACTIVE;

cleanup:
    if(new_status != waniface->nat.status) {
        waniface->nat.status = new_status;
        status_update(waniface);
    }
out:
    FW_PROFILING_OUT();
    return;
}

static void interface_deactivate(natiface_wan_t* waniface) {
    if(waniface != NULL) {
        SAH_TRACEZ_INFO(ME, "Deactivate ipv4Interface");
        if(waniface->nat.status == NAT_ACTIVE) {
            ft_deinit(&waniface->nat, NULL);
        }
    }
}

bool interface_implement_changes(natiface_wan_t* waniface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = waniface->object;
    (void) object; // when tracing is disabled, variable is not used anymore

    if((waniface->flags & FW_NEW) || (waniface->flags & FW_MODIFIED)) {
        waniface->flags = 0;
        if(waniface->nat.status != NAT_INACTIVE) {
            SAH_TRACEZ_INFO(ME, "Deactivate IPv4 interface[%s] (NAT)", OBJECT_NAMED);
            interface_deactivate(waniface);
            status_update(waniface);
        }
        if(waniface->nat.enable && (waniface->nat.status != NAT_ACTIVE)) {
            interface_activate(waniface);
            status_update(waniface);
        }
    }

    if(waniface->flags & FW_DELETED) {
        bool notify = waniface->nat.enable;
        interface_deactivate(waniface);
        status_update(waniface);
        interface_delete(&waniface);
        if(notify) {
            portmap_hairpin_address_changed(NULL);
        }
    }

    fw_apply();
    FW_PROFILING_OUT();

    return true;
}

bool is_nat_enabled_interface(const char* interface) {
    amxd_object_t* interface_setting_template = amxd_dm_findf(fw_get_dm(), "NAT.InterfaceSetting");
    amxp_expr_t expression;
    amxc_string_t expression_str;
    bool nat_enabled = false;

    if(STRING_EMPTY(interface)) {
        goto exit;
    }

    amxc_string_init(&expression_str, 0);
    amxc_string_setf(&expression_str, "Interface==\"%s\" && Enable==True", interface);
    amxp_expr_init(&expression, amxc_string_get(&expression_str, 0));

    if(amxd_object_find_instance(interface_setting_template, &expression) != NULL) {
        nat_enabled = true;
    }

    amxc_string_clean(&expression_str);
    amxp_expr_clean(&expression);
exit:
    return nat_enabled;
}

char* get_nm_intf_by_netdev_name(const char* netdev_name) {
    char* intf = NULL;
    amxc_string_t str;
    int ret = -1;
    amxc_var_t data;
    const char* alias = NULL;

    amxc_string_init(&str, 0);
    amxc_var_init(&data);
    when_str_empty_trace(netdev_name, exit, ERROR, "NetDev name is empty");

    amxc_string_setf(&str, "NetModel.Intf.[NetDevName == '%s'].", netdev_name);
    ret = amxb_get(netmodel_get_amxb_bus(), amxc_string_get(&str, 0), 0, &data, 5);
    when_failed_trace(ret, exit, ERROR, "Failed to get NetModel interface with NetDevName == %s", netdev_name);

    alias = GETP_CHAR(&data, "0.0.Alias");
    when_str_empty_trace(alias, exit, ERROR, "Failed to get NetModel interface with NetDevName == %s", netdev_name);

    amxc_string_setf(&str, "NetModel.Intf.%s.", alias);
    intf = amxc_string_take_buffer(&str);

exit:
    amxc_string_clean(&str);
    amxc_var_clean(&data);
    return intf;
}

int toggle_nat_flags_nm(natiface_wan_t* waniface, const char* flags, bool set) {
    int ret = -1;
    amxc_string_t str;

    amxc_string_init(&str, 0);
    when_null_trace(waniface, exit, ERROR, "Interface can not be NULL");
    when_str_empty_trace(waniface->nm_intf_path, exit, WARNING, "No NetModel Intf path stored");

    if(flags == NULL) {
        amxc_var_t params;
        amxc_var_init(&params);
        const char* status = NULL;
        bool enable = false;

        amxd_object_get_params(waniface->object, &params, amxd_dm_access_private);
        status = GET_CHAR(&params, "Status");
        if(status == NULL) {
            amxc_var_clean(&params);
            goto exit;
        }
        enable = GET_BOOL(&params, "Enable");

        if(!enable) {
            flags = "nat nat-up";
            set = false;
        } else if(status_is_enabled(status)) {
            flags = "nat nat-up";
            set = true;
        } else {
            flags = "nat";
            set = true;
        }

        amxc_var_clean(&params);
    }

    SAH_TRACEZ_INFO(ME, "%s flags \"%s\" for interface \"%s\"", set ? "Setting" : "Clearing", flags, waniface->nm_intf_path);

    if(set) {
        netmodel_setFlag(waniface->nm_intf_path, flags, NULL, netmodel_traverse_this);
    } else {
        netmodel_clearFlag(waniface->nm_intf_path, flags, NULL, netmodel_traverse_this);
    }
    ret = 0;

exit:
    amxc_string_clean(&str);
    return ret;
}
