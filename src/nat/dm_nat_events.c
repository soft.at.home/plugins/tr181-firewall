/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "nat/dm_nat.h"
#include "rules.h"
#include "nat/masquerade.h" // for natiface_wan_t
#include "nat/portmapping.h"
#include "protocols.h"
#ifdef WITH_GMAP
#include "devices_query.h"
#endif
#include "logs/logs.h"
#include "firewall.h"       // for fw_get_dm
#include "init_cleanup.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_path.h>
#include <ipat/ipat.h>
#include <string.h>

#define ME "nat"

typedef enum {
    ADDED,
    CHANGED,
    REMOVED,
    UNKNOWN
} event_type_t;

static bool auto_all_interfaces = false;

static event_type_t type_of_event(const char* const event) {
    event_type_t type = UNKNOWN;

    when_str_empty_trace(event, exit, ERROR, "Missing event, using default %d", type);

    if(strcmp(event, "dm:instance-added") == 0) {
        type = ADDED;
    } else if(strcmp(event, "dm:object-changed") == 0) {
        type = CHANGED;
    } else if(strcmp(event, "dm:instance-removed") == 0) {
        type = REMOVED;
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown event '%s', using default %d", event, type);
    }

exit:
    return type;
}

static const char* skip_prefix(const char* path) {
    const char* prefix = "Device.";

    if(strstr(path, prefix) == &path[0]) {
        path = &path[strlen(prefix)];
    }

    return path;
}

static bool is_event_for_me(event_type_t type, natiface_lan_t* laniface, const amxc_var_t* const data) {
    const char* path = GET_CHAR(data, "path");
    const char* path2 = object_da_string(laniface->object, "Interface");
    int index = GET_INT32(data, "index");
    bool rv = false;
    when_null_trace(path2, exit, ERROR, "Can't get interface path");

    path2 = skip_prefix(path2);

    switch(type) {
    case ADDED:
    // fall through
    case REMOVED: {
        amxc_string_t buf;
        amxc_string_init(&buf, 0);
        amxc_string_setf(&buf, "%s%d.", path, index);
        rv = (strcmp(amxc_string_get(&buf, 0), path2) == 0);
        amxc_string_clean(&buf);
    }
    break;
    case CHANGED:
        rv = (strcmp(path, path2) == 0);
        break;
    default:
        SAH_TRACEZ_WARNING(ME, "Ignored unknown event[%s]", GET_CHAR(data, "notification"));
    }

exit:
    return rv;
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    rule_t* rule = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    rule = (rule_t*) object->priv;
    when_null_trace(rule, exit, ERROR, "PortMapping[%s] is NULL", OBJECT_NAMED);

    if(update_rule_logs(NULL, object, NULL, data, log, false)) {
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

/**
   @brief
   Translate hostname or MAC address to IP and adds to the rule . Or keeps the IP.

   @param[in] rule Rule structure.
   @param[in] address Address (IP, MAC Address, or Hostname)

   @return
   0 if the function is succesfully executed.
 */
static int translate_hostname_to_ip(rule_t* rule, const char* address) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    int rv = -1;
#ifdef WITH_GMAP
    int ret_value = -1;

    stop_gmap_queries(rule);
#endif

    if(ipat_text_valid(address)) {
        rule_set_destination(rule, address);
        rv = 0;
        goto exit;
    }

#ifdef WITH_GMAP
    ret_value = query_hostname_address(rule, address);
    when_failed_status(ret_value, exit, rv = 0);
#endif

exit:
    FW_PROFILING_OUT();
    return rv;
}

static void schedule_updated(schedule_status_t new_status, void* userdata) {
    rule_t* rule = NULL;

    when_null(userdata, exit);
    rule = (rule_t*) userdata;
    when_null(rule, exit);

    when_true(rule->common.status == FIREWALL_ENABLED && new_status == SCHEDULE_STATUS_ACTIVE, exit);
    when_true(rule->common.status == FIREWALL_DISABLED && (new_status == SCHEDULE_STATUS_DISABLED || new_status == SCHEDULE_STATUS_STACK_DISABLED), exit);

    if(rule->common.schedule_status != new_status) {
        rule_set_enable(rule, true);              // Enable Rule for now, will be disabled again when needed
        rule->common.schedule_status = new_status;
        when_true(rule->common.flags != 0, exit); // already being configured
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    return;
}

static bool nat_portmap_listen_schedule_updates(rule_t* rule, const char* path) {
    bool enable = false;
    bool res = false;
    amxd_object_t* object = NULL;

    when_null_trace(rule, exit, ERROR, "rule is NULL");

    object = rule->common.object;
    when_null_trace(object, exit, ERROR, "object is NULL");
    enable = amxd_object_get_value(bool, object, "Enable", NULL);
    when_false(enable, exit);

    schedule_close(&(rule->common.schedule));
    rule->common.schedule = NULL;

    if(enable && !STRING_EMPTY(path)) {
        rule->common.schedule = schedule_open(path, schedule_updated, (void*) rule);
        when_null_trace(rule->common.schedule, exit, ERROR, "Rule[%s] Could not start listening for Schedule updates", OBJECT_NAMED);
    } else if(enable && STRING_EMPTY(path) && (rule->common.schedule_status != SCHEDULE_STATUS_EMPTY)) {
        rule_set_enable(rule, true);
        rule->common.schedule_status = SCHEDULE_STATUS_EMPTY;
        when_true(rule->common.flags != 0, exit); // already being configured
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    return res;
}

static bool _fw_validate_port_mapping(amxd_object_t* object, bool enable, bool hairpin_nat, bool all_interfaces,
                                      const char* interface,
                                      const char* internal_client, const char* target_interface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool validated = false;

    when_null(object, exit);
    when_false_status(enable, exit, validated = true);

    if(hairpin_nat && STRING_EMPTY(target_interface)) {
        SAH_TRACEZ_ERROR(ME, "PortMapping[%s] is missing TargetInterface", OBJECT_NAMED);
        goto exit;
    }
    if(hairpin_nat && STRING_EMPTY(interface) && !all_interfaces) {
        SAH_TRACEZ_ERROR(ME, "PortMapping[%s] is missing Interface", OBJECT_NAMED);
        goto exit;
    }
    if(!hairpin_nat && STRING_EMPTY(internal_client)) {
        SAH_TRACEZ_ERROR(ME, "PortMapping[%s] is missing InternalClient", OBJECT_NAMED);
        goto exit;
    }

    validated = true;

exit:
    FW_PROFILING_OUT();
    return validated;
}

static bool evaluate_and_set_status(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool rv = false;
    rule_t* rule = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    rule = (rule_t*) object->priv;

    rv = _fw_validate_port_mapping(object, amxd_object_get_value(bool, object, "Enable", NULL),
                                   amxd_object_get_value(bool, object, "HairpinNAT", NULL),
                                   amxd_object_get_value(bool, object, "AllInterfaces", NULL),
                                   object_da_string(object, "Interface"),
                                   object_da_string(object, "InternalClient"), object_da_string(object, "TargetInterface"));
    if(!rv) {
        rule->common.flags = 0; // to avoid rule being activated (can happen when instance is removed)
        common_set_status(&rule->common, FIREWALL_ERROR_MISCONFIG);
    }
exit:
    FW_PROFILING_OUT();
    return rv;
}

static void check_and_change_all_interfaces(amxd_object_t* object) {
    const char* interface = NULL;

    when_false(auto_all_interfaces, exit);

    interface = object_da_string(object, "Interface");
    amxd_object_set_value(bool, object, "AllInterfaces", STRING_EMPTY(interface));

exit:
    return;
}

static rule_t* nat_portmap_init(amxd_object_t* object) {
    rule_t* rule = NULL;
    firewall_interface_t* src_fwiface = NULL;
    firewall_interface_t* dest_fwiface = NULL;
    const char* interface = NULL;
    const char* target_interface = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");

    check_and_change_all_interfaces(object);

    rule = portmapping_create(object);
    when_null(rule, exit);

    src_fwiface = &rule->common.src_fwiface;
    dest_fwiface = &rule->common.dest_fwiface;

    protocol_from_string(&rule->common.protocols, object_da_string(object, "Protocol"));
    rule_set_src_port(rule, amxd_object_get_int32_t(object, "ExternalPort", NULL), amxd_object_get_int32_t(object, "ExternalPortEndRange", NULL));
    rule_set_dst_port(rule, amxd_object_get_int32_t(object, "InternalPort", NULL), 0);
    rule_set_source(rule, object_da_string(object, "RemoteHost"));
    rule_set_protocols(rule, object_da_string(object, "Protocol"));
    rule_set_enable(rule, object_get_enable(object));

    interface = object_da_string(object, "Interface");
    if(!STRING_EMPTY(interface)) {
        open_ip_address_query(interface, IPv4, "ipv4", src_fwiface);
    } else {
        init_firewall_interface(src_fwiface);
    }

    target_interface = object_da_string(object, "TargetInterface");
    if(!STRING_EMPTY(target_interface)) {
        open_ip_address_query(target_interface, IPv4, "ipv4", dest_fwiface);
    } else {
        init_firewall_interface(dest_fwiface);
    }

    translate_hostname_to_ip(rule, object_da_string(object, "InternalClient"));

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    init_rule_logs(object, rule, src_fwiface, dest_fwiface, false, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

    nat_portmap_listen_schedule_updates(rule, object_da_string(object, "ScheduleRef"));

exit:
    return rule;
}

void _nat_portmap_added(UNUSED const char* const sig_name,
                        const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* portmappings = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(portmappings, NULL, GET_UINT32(event_data, "index"));
    rule_t* rule = NULL;

    when_null(object, exit);
    rule = nat_portmap_init(object);
    when_null(rule, exit);
    when_false(evaluate_and_set_status(object), exit);

    rule_update(rule);

exit:
    FW_PROFILING_OUT();
    return;
}

void _nat_portmap_changed_allowed_origins(UNUSED const char* const sig_name,
                                          const amxc_var_t* const event_data,
                                          UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* portmapping_object = NULL;

    portmapping_object = amxd_dm_findf(fw_get_dm(), "%s", "NAT.PortMapping.");
    when_null(portmapping_object, exit);

    rules_allowed_origins_changed(GETP_ARG(event_data, "parameters.PortMappingAllowedOrigins.from"),
                                  GETP_ARG(event_data, "parameters.PortMappingAllowedOrigins.to"),
                                  portmapping_object);

exit:
    FW_PROFILING_OUT();
    return;
}

void _nat_portmap_changed(UNUSED const char* const sig_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    rule_t* rule = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    bool update_schedule = false;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);

    check_and_change_all_interfaces(object);

    rule = (rule_t*) object->priv;
    rule->common.flags = FW_MODIFIED;

    var = GET_ARG(params, "ExternalPort");
    var2 = GET_ARG(params, "ExternalPortEndRange");
    if((var != NULL) || (var2 != NULL)) {
        int external_port = (var != NULL) ? GET_INT32(var, "to") : amxd_object_get_value(int32_t, object, "ExternalPort", NULL);
        int external_port_end = (var2 != NULL) ? GET_INT32(var2, "to") : amxd_object_get_value(int32_t, object, "ExternalPortEndRange", NULL);
        rule_set_src_port(rule, external_port, external_port_end);
    }

    var = GET_ARG(params, "InternalPort");
    if(var != NULL) {
        rule_set_dst_port(rule, GET_INT32(var, "to"), 0);
    }

    var = GET_ARG(params, "InternalClient");
    if(var != NULL) {
        translate_hostname_to_ip(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "RemoteHost");
    if(var != NULL) {
        rule_set_source(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Protocol");
    if(var != NULL) {
        rule_set_protocols(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        update_schedule = GET_BOOL(var, "to");
        rule_set_enable(rule, GET_BOOL(var, "to"));
    }

    var = GET_ARG(params, "Interface");
    if(var != NULL) {
        const char* interface = GET_CHAR(var, "to");
        firewall_interface_t* fwiface = &rule->common.src_fwiface;
        if(!STRING_EMPTY(interface)) {
            open_ip_address_query(interface, IPv4, "ipv4", fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }

    var = GET_ARG(params, "TargetInterface");
    if(var != NULL) {
        const char* target_interface = GET_CHAR(var, "to");
        firewall_interface_t* fwiface = &rule->common.dest_fwiface;
        if(!STRING_EMPTY(target_interface)) {
            open_ip_address_query(target_interface, IPv4, "ipv4", fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }

    if(update_schedule) {
        char* schedule_ref = amxd_object_get_value(cstring_t, object, "ScheduleRef", NULL);
        nat_portmap_listen_schedule_updates(rule, schedule_ref);
        free(schedule_ref);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_rule_logs(rule, NULL, event_data, NULL, NULL, false);
#endif //FIREWALL_LOGS
    when_false(evaluate_and_set_status(object), exit);
    rule_update(rule);

exit:
    FW_PROFILING_OUT();
    return;
}

void _nat_portmap_schedule_changed(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), data);
    amxc_var_t* params = NULL;
    amxc_var_t* var = NULL;
    rule_t* rule = NULL;
    const char* new_schedule_ref = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);
    when_false(amxd_object_get_value(bool, object, "Enable", NULL), exit); // Not applicable if PortMap is disabled

    params = GET_ARG(data, "parameters");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    rule = (rule_t*) object->priv;
    // don't set rule->common.flags so nat_portmap_listen_schedule_updates can decide to (de)activate the rule

    var = GET_ARG(params, "ScheduleRef");
    new_schedule_ref = GET_CHAR(var, "to");

    SAH_TRACEZ_INFO(ME, "Changing PortMapping ScheduleRef from '%s' to '%s'", GET_CHAR(var, "from"), new_schedule_ref);

    nat_portmap_listen_schedule_updates(rule, new_schedule_ref);
exit:
    FW_PROFILING_OUT();
    return;
}

void _nat_changed(UNUSED const char* const sig_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_string_t buf;

    amxc_string_init(&buf, 0);

    when_null(object, exit);

    amxc_string_setf(&buf, "%sMaxPortMappingNumberOfEntries", fw_get_vendor_prefix());
    var = GET_ARG(params, amxc_string_get(&buf, 0));
    if(var != NULL) {
        uint32_t max = GET_UINT32(var, "to");
        amxd_object_t* portmapping = amxd_object_get_child(object, "PortMapping");
        SAH_TRACEZ_NOTICE(ME, "Set new max instance count to [%d] for portmapping object", max);
        amxd_object_set_max_instances(portmapping, max);
    }

    amxc_string_reset(&buf);
    amxc_string_setf(&buf, "%sMaxPortTriggerNumberOfEntries", fw_get_vendor_prefix());
    var = GET_ARG(params, amxc_string_get(&buf, 0));
    if(var != NULL) {
        uint32_t max = GET_UINT32(var, "to");
        amxd_object_t* PortTrigger = amxd_object_get_child(object, "PortTrigger");
        SAH_TRACEZ_NOTICE(ME, "Set new max instance count to [%d] for PortTrigger object", max);
        amxd_object_set_max_instances(PortTrigger, max);
    }

exit:
    amxc_string_clean(&buf);
    FW_PROFILING_OUT();
    return;
}

static void ipv4_interface_on_write(natiface_wan_t* waniface, amxd_object_t* object, bool changed) {
    const char* iface_path = NULL;
    waniface->flags = FW_MODIFIED;
    waniface->nat.enable = amxd_object_get_value(bool, object, "Enable", NULL);

    SAH_TRACEZ_INFO(ME, "NAT enabled[%d]", waniface->nat.enable);

    if(waniface->nat.enable) {
        iface_path = object_da_string(object, "Interface");

        waniface->fwiface.query_flags = "netdev-bound && !dslite-up && ipv4";
        to_linux_interface(iface_path, IPv4, &waniface->fwiface);

        open_ip_address_query(iface_path, IPv4, "ipv4", &waniface->address_fwiface);
    } else if(changed) {
        init_firewall_interface(&waniface->fwiface);
        init_firewall_interface(&waniface->address_fwiface);

        portmap_hairpin_address_changed(NULL);
    }

    interface_implement_changes(waniface);
}

static int interface_setting_init(amxd_object_t* object) {
    natiface_wan_t* waniface = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    waniface = interface_create(object);
    when_null(waniface, exit);

    ipv4_interface_on_write(waniface, object, false);
exit:
    return 1;
}

void _interface_setting_added(UNUSED const char* const sig_name,
                              const amxc_var_t* const event_data,
                              UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));

    when_null(object, exit);
    interface_setting_init(object);
exit:
    FW_PROFILING_OUT();
    return;
}

void _interface_setting_changed(UNUSED const char* const sig_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* object = amxd_dm_signal_get_object(dm, event_data);
    amxc_var_t* status = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_htable_t* hparams = (amxc_htable_t*) amxc_var_constcast(amxc_htable_t, params);
    natiface_wan_t* waniface = NULL;

    when_null(dm, exit);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null_trace(object->priv, exit, ERROR, "Object[%s] has no private data", OBJECT_NAMED);
    waniface = (natiface_wan_t*) object->priv;

    status = GET_ARG(params, "Status");
    if(status != NULL) {
        if(status_is_enabled(GET_CHAR(status, "to"))) {
            toggle_nat_flags_nm(waniface, "nat-up", true);
        } else {
            toggle_nat_flags_nm(waniface, "nat-up", false);
        }
    }
    amxc_var_take_it(status);
    if(amxc_htable_is_empty(hparams)) {
        goto exit; // ignore status changed events
    }

    if((GET_ARG(params, "Interface") != NULL) ||
       (GET_ARG(params, "Enable") != NULL)) {
        ipv4_interface_on_write(waniface, object, (GET_ARG(params, "Enable") != NULL));
    }

    if(GET_ARG(params, "Enable") != NULL) {
        toggle_nat_flags_nm(waniface, NULL, true);
    }

exit:
    amxc_var_set_key(params, "Status", status, AMXC_VAR_FLAG_DEFAULT); // restore Status
    FW_PROFILING_OUT();
    return;
}

static bool get_current_ipaddress(const char* instance, char** ipaddress, int* prefixlen, char** status) {
    amxc_var_t ret;
    amxc_var_t* var = NULL;
    char* free_me = NULL;
    bool rv = false;
    int amxb_rv = 0;

    amxc_var_init(&ret);

    instance = skip_prefix(instance);

    SAH_TRACEZ_INFO(ME, "Fetch parameters of instance %s", instance);

    amxb_rv = amxb_get(amxb_be_who_has("IP."), instance, 0, &ret, 5); // todo should we use IP datamodel or netmodel?
    when_failed_trace(amxb_rv, exit, ERROR, "Failed to get parameters of '%s': %d", instance, amxb_rv);

    var = GETP_ARG(&ret, "0.0.PrefixLen");
    when_null_trace(var, exit, ERROR, "Missing PrefixLen");
    *prefixlen = GET_INT32(var, NULL);

    var = GETP_ARG(&ret, "0.0.IPAddress");
    when_null_trace(var, exit, ERROR, "Missing IPAddress");
    free_me = amxc_var_take(cstring_t, var);

    var = GETP_ARG(&ret, "0.0.Status");
    when_null_trace(var, exit, ERROR, "Missing Status");
    *status = amxc_var_take(cstring_t, var);

    *ipaddress = free_me;
    free_me = NULL;

    rv = true;

exit:
    free(free_me);
    amxc_var_clean(&ret);
    return rv;
}

static void set_address(firewall_interface_t* fwiface, const char* ipaddress, int prefixlen) {
    amxc_string_t cidr;

    amxc_string_init(&cidr, 0);

    if(!STRING_EMPTY(ipaddress)) {
        amxc_string_setf(&cidr, "%s/%d", ipaddress, prefixlen);
        address_from_string(amxc_string_get(&cidr, 0), &fwiface->address);
    } else {
        address_from_string(NULL, &fwiface->address);
    }

    amxc_string_clean(&cidr);
}

static void ipaddress_added(natiface_lan_t* laniface, const amxc_var_t* const data) {
    amxc_var_t* params = GET_ARG(data, "parameters");
    int prefixlen = GET_INT32(params, "PrefixLen");
    const char* status = GET_CHAR(params, "Status");
    const char* ipaddress = GET_CHAR(params, "IPAddress");
    natiface_wan_t* waniface = laniface->waniface;

    when_str_empty_trace(ipaddress, exit, NOTICE, "Ignore event because of missing IPAddress");
    when_false_trace(prefixlen > 0, exit, NOTICE, "Ignore event because of missing PrefixLen");
    when_null_trace(status, exit, NOTICE, "Ignore event because of missing Status");
    when_false_trace(status_is_enabled(status), exit, NOTICE, "Ignore event because of Status[%s]", status);

    set_address(&laniface->fwiface, ipaddress, prefixlen);

    waniface->flags = FW_MODIFIED;
    interface_implement_changes(waniface);

exit:
    return;
}

static void ipaddress_removed(natiface_lan_t* laniface, UNUSED const amxc_var_t* const data) {
    natiface_wan_t* waniface = laniface->waniface;
    set_address(&laniface->fwiface, NULL, -1);

    waniface->flags = FW_MODIFIED;
    interface_implement_changes(waniface);
}

static void ipaddress_changed(natiface_lan_t* laniface, const amxc_var_t* const data) {
    amxc_var_t* params = GET_ARG(data, "parameters");
    amxc_var_t* ipaddress_var = GET_ARG(params, "IPAddress");
    amxc_var_t* prefixlen_var = GET_ARG(params, "PrefixLen");
    amxc_var_t* status_var = GET_ARG(params, "Status");
    int prefixlen_to = GET_INT32(prefixlen_var, "to");
    const char* ipaddress_to = GET_CHAR(ipaddress_var, "to");
    const char* status_to = GET_CHAR(status_var, "to");
    const char* status_from = GET_CHAR(status_var, "from");
    char* free_me = NULL;
    char* free_me2 = NULL;
    const char* instance = object_da_string(laniface->object, "Interface");
    bool ipaddress_changed = (NULL != ipaddress_var);
    bool prefixlen_changed = (NULL != prefixlen_var);
    bool status_changed = (NULL != status_var);
    natiface_wan_t* waniface = laniface->waniface;

    when_null_trace(instance, exit, ERROR, "Can't get Interface object");

    when_false_trace(ipaddress_changed || prefixlen_changed || status_changed, exit, NOTICE, "Ignore event that misses 1 of [%s, %s, %s]", ipaddress_changed ? "" : "IPAddress", prefixlen_changed ? "" : "PrefixLen", status_changed ? "" : "Status");

    when_false(get_current_ipaddress(instance, &free_me, &prefixlen_to, &free_me2), exit);
    SAH_TRACEZ_INFO(ME, "ipaddress[%s] prefixlen[%d] status[%s]", free_me, prefixlen_to, free_me2);

    if(!ipaddress_changed) {
        ipaddress_to = free_me;
    }

    if(!status_changed) {
        status_to = free_me2;
        status_from = free_me2;
    }

    if(status_changed) {
        if(status_is_enabled(status_to)) {
            set_address(&laniface->fwiface, ipaddress_to, prefixlen_to);
        } else {
            set_address(&laniface->fwiface, NULL, -1);
        }
    } else if(status_is_enabled(status_from)) {
        set_address(&laniface->fwiface, ipaddress_to, prefixlen_to);
    }

    waniface->flags = FW_MODIFIED;
    interface_implement_changes(waniface);

exit:
    free(free_me);
    free(free_me2);
}

static void ipaddress_subscription_handler(UNUSED const char* const sig_name,
                                           const amxc_var_t* const data,
                                           void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    natiface_lan_t* laniface = (natiface_lan_t*) priv;
    event_type_t type = type_of_event(GET_CHAR(data, "notification"));

    when_false(is_event_for_me(type, laniface, data), exit);

    switch(type) {
    case ADDED:
        ipaddress_added(laniface, data);
        break;
    case CHANGED:
        ipaddress_changed(laniface, data);
        break;
    case REMOVED:
        ipaddress_removed(laniface, data);
        break;
    default:
        SAH_TRACEZ_WARNING(ME, "Ignored unknown event[%s] %s", GET_CHAR(data, "notification"), sig_name);
    }

exit:
    FW_PROFILING_OUT();
    return;
}

static bool subscribe_on_address_change_events(const char* instance, natiface_lan_t* laniface) {
    amxb_subscription_t** subscr = &laniface->ipv4address_subscription;
    amxd_path_t path;
    amxd_object_t* object = laniface->object;
    amxb_bus_ctx_t* bus = NULL;
    const char* filter = "notification in ['dm:object-changed', 'dm:instance-added', 'dm:instance-removed']";
    bool rv = false;

    bus = amxb_be_who_has("IP.");
    when_null_trace(bus, exit, ERROR, "NATLanIfSetting[%s] failed to subscribe on IP. because of no bus", OBJECT_NAMED);

    amxd_path_init(&path, skip_prefix(instance));
    free(amxd_path_get_last(&path, true)); // remove index to receive events dm:instance-added and dm:instance-removed
    instance = amxd_path_get(&path, AMXD_OBJECT_TERMINATE);

    if(AMXB_STATUS_OK != amxb_subscription_new(subscr, bus, instance, filter, ipaddress_subscription_handler, laniface)) {
        SAH_TRACEZ_ERROR(ME, "Failed to subscribe to %s", instance);
        goto exit_clean;
    }

    rv = true;
exit_clean:
    amxd_path_clean(&path);
exit:
    return rv;
}

static bool set_current_network(const char* instance, natiface_lan_t* laniface) {
    char* ipaddress = NULL;
    char* status = NULL;
    int prefixlen = 0;
    bool rv = false;

    when_false(get_current_ipaddress(instance, &ipaddress, &prefixlen, &status), exit);
    when_false_trace(status_is_enabled(status), exit, NOTICE, "Ignore event because of Status[%s]", status);
    set_address(&laniface->fwiface, ipaddress, prefixlen);

    rv = true;
exit:
    free(ipaddress);
    free(status);
    return rv;
}

static void natinterface_on_write(natiface_lan_t* laniface) {
    natiface_wan_t* waniface = laniface->waniface;
    amxd_object_t* object = laniface->object;
    const char* netmodel_interface = object_da_string(laniface->object, "Interface");

    if((laniface->fwiface.query == NULL) &&
       (laniface->ipv4address_subscription == NULL) &&
       !STRING_EMPTY(netmodel_interface)) {
        laniface->fwiface.addr_required = true;
        if(strstr(netmodel_interface, ".IPv4Address.") != NULL) {
            SAH_TRACEZ_INFO(ME, "NatIfSetting[%s] subscribe on %s", OBJECT_NAMED, netmodel_interface);

            when_false(subscribe_on_address_change_events(netmodel_interface, laniface), exit);
            when_false(set_current_network(netmodel_interface, laniface), exit);

            waniface->flags = FW_MODIFIED;
            interface_implement_changes(waniface);
        } else {
            SAH_TRACEZ_INFO(ME, "NatIfSetting[%s] open query on %s", OBJECT_NAMED, netmodel_interface);
            open_ip_address_query(netmodel_interface, IPv4, "ipv4", &laniface->fwiface);
        }
    } else {
        waniface->flags = FW_MODIFIED;
        interface_implement_changes(waniface);
    }
exit:
    return;
}

void _natinterface_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));
    amxd_object_t* ifsetting_object = NULL;
    natiface_wan_t* waniface = NULL;
    natiface_lan_t* laniface = NULL;

    when_null(object, exit);

    ifsetting_object = amxd_object_findf(object, "^.^.");
    when_null(ifsetting_object, exit);

    when_null_trace(ifsetting_object->priv, exit, ERROR, "InterfaceSetting[%s] has no priv data", amxd_object_get_name(ifsetting_object, AMXD_OBJECT_NAMED));
    waniface = (natiface_wan_t*) ifsetting_object->priv;

    laniface = lan_interface_create(object, waniface);
    when_null_trace(laniface, exit, ERROR, "NATInterface[%s] failed to create priv data", OBJECT_NAMED);

    natinterface_on_write(laniface);

exit:
    FW_PROFILING_OUT();
    return;
}

void _natinterface_changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const event_data,
                           UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    natiface_lan_t* laniface = NULL;

    when_null(object, exit);

    laniface = (natiface_lan_t*) object->priv;
    when_null_trace(laniface, exit, ERROR, "NATInterface[%s] has no private data", OBJECT_NAMED);

    if(GET_ARG(params, "Interface") != NULL) {
        init_firewall_interface(&laniface->fwiface);
        if(laniface->ipv4address_subscription != NULL) {
            amxb_subscription_delete(&laniface->ipv4address_subscription);
        }
    }

    natinterface_on_write(laniface);

exit:
    FW_PROFILING_OUT();
    return;
}

static int dm_natinterface_init(UNUSED amxd_object_t* templ, amxd_object_t* object, void* priv) {
    natiface_wan_t* waniface = (natiface_wan_t*) priv;
    natiface_lan_t* laniface = NULL;

    laniface = lan_interface_create(object, waniface);
    when_null_trace(laniface, exit, ERROR, "NATInterface[%s] failed to create priv data", OBJECT_NAMED);

    natinterface_on_write(laniface);
exit:
    return amxd_status_ok;
}

static int nat_ifsetting_init(UNUSED amxd_object_t* templ, amxd_object_t* object, void* priv) {
    const char* path = (const char*) priv;

    interface_setting_init(object);
    when_null(object->priv, exit);

    amxd_object_for_all(object, path, dm_natinterface_init, object->priv);
exit:
    return amxd_status_ok;
}

static int dm_portmapping_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    rule_t* rule = NULL;
    rule = nat_portmap_init(object);
    when_null(rule, exit);
    rule_activate(rule);
exit:
    return amxd_status_ok;
}

void dm_nat_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* nat = amxd_dm_findf(fw_get_dm(), "NAT.");
    amxd_object_t* portmapping = amxd_dm_findf(fw_get_dm(), "NAT.PortMapping.");
    amxc_string_t buf;
    uint32_t max_number = 0;

    auto_all_interfaces = GET_BOOL(amxo_parser_get_config(fw_get_parser(), "fw-auto-all-interfaces"), NULL);

    amxc_string_init(&buf, 0);
    amxc_string_setf(&buf, ".%sNATInterface.*.", fw_get_vendor_prefix());
    amxd_object_for_all(nat, ".InterfaceSetting.*.", nat_ifsetting_init, (void*) amxc_string_get(&buf, 0));

    amxc_string_reset(&buf);
    amxc_string_setf(&buf, "%sMaxPortMappingNumberOfEntries", fw_get_vendor_prefix());
    max_number = amxc_var_constcast(uint32_t, amxd_object_get_param_value(nat, amxc_string_get(&buf, 0)));
    amxc_string_clean(&buf);
    amxd_object_set_max_instances(portmapping, max_number);
    amxd_object_for_all(nat, ".PortMapping.*.", dm_portmapping_init, NULL);

    FW_PROFILING_OUT();
}
