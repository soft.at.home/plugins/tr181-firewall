/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "nat/staticnat.h"
#include "fwrule_helpers.h"
#include "ports.h"
#include "protocols.h"
#include "init_cleanup.h"
#include "firewall.h" // for fw_get_vendor_prefix
#include "firewall_utilities.h"
#include "logs/logs.h"
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "nat"

typedef enum {
    NOT_VALIDATED,
    VALIDATED,
    MISCONFIGURED
} statnat_validated_t;

enum folders {
    DNAT,
    SNAT,
    NUM_OF_FOLDERS
};

enum folder_containers {
    FORWARD,
    REVERSE,
    NUM_OF_CONTAINERS
};

typedef enum {
    PUB_ADDRESS,
    PRIV_ADDRESS,
    NUM_OF_ADDRESSES
} statnat_address_t;

typedef struct {
    statnat_validated_t validated;
    address_t address[NUM_OF_ADDRESSES];
    amxc_llist_t ports;
    amxc_var_t protocols;
    fw_folder_t* folder[NUM_OF_FOLDERS];
    folder_container_t folder_container[NUM_OF_CONTAINERS];
    const char* name;
    amxd_object_t* obj;
    bool is_host;
    bool teardown;
} statnat_portmap_t;

static void update_all_portmappings(statnat_portmap_t* host);
static void update_host_portmapping(statnat_portmap_t* host);
static bool portmap_enable(statnat_portmap_t* portmap);

static statnat_portmap_t* get_host(statnat_portmap_t* portmap) {
    statnat_portmap_t* host = NULL;
    amxd_object_t* obj = NULL;

    obj = amxd_object_findf(portmap->obj, ".^.^");
    when_null(obj, exit);
    when_null(obj->priv, exit);
    host = (statnat_portmap_t*) obj->priv;

exit:
    return host;
}

static void count_portmappings(statnat_portmap_t* host, uint32_t* validated, uint32_t* misconfigured) {
    when_false_trace(host->is_host, exit, ERROR, "Expected host but got StatNatPM[%s]", host->name);

    amxd_object_for_each(instance, it, amxd_object_findf(host->obj, "%s", ".PortMapping.")) { // using amxd_object_findf with [Enable==true] doesn't work if there are >1 instances that match
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        statnat_portmap_t* portmap = NULL;

        if(obj->priv == NULL) {
            continue;
        }
        portmap = (statnat_portmap_t*) obj->priv;

        if((validated != NULL) && (portmap->validated == VALIDATED)) {
            (*validated)++;
        } else if((misconfigured != NULL) && (portmap->validated == MISCONFIGURED)) {
            (*misconfigured)++;
        }
    }

exit:
    return;
}

static bool portmap_is_active(statnat_portmap_t* portmap) {
    return fw_folder_get_enabled(portmap->folder[0]);
}

static void portmap_reset_folders(statnat_portmap_t* portmap) {
    for(int i = 0; i < NUM_OF_FOLDERS; i++) {
        if(!fw_folder_get_enabled(portmap->folder[i])) {
            continue;
        }
        fw_folder_delete_rules(portmap->folder[i]);
        fw_commit(fw_rule_callback);
        fw_folder_set_enabled(portmap->folder[i], false);
    }

    for(int i = 0; i < NUM_OF_CONTAINERS; i++) {
        folder_container_delete_rules(&portmap->folder_container[i], IPv4, true);
    }

    fw_apply();
}

static void portmap_disable(statnat_portmap_t* portmap) {
    if(!portmap->is_host) {
        portmap->validated = NOT_VALIDATED;
        set_status(portmap->obj, FIREWALL_DISABLED);
    } // else don't un-validate host

    portmap_reset_folders(portmap);
}

static void host_disable(statnat_portmap_t* host) {
    when_false_trace(host->is_host, exit, ERROR, "Expected host but got StatNatPM[%s]", host->name);

    host->validated = NOT_VALIDATED;
    set_status(host->obj, FIREWALL_DISABLED);

exit:
    return;
}

static void portmap_destroy(statnat_portmap_t** portmap) {
    when_null(portmap, exit);
    when_null(*portmap, exit);

    if((*portmap)->is_host) {
        (*portmap)->teardown = true;
        host_disable(*portmap);
    } else {
        portmap_disable(*portmap);
        update_host_portmapping(*portmap);
    }

    for(int i = 0; i < NUM_OF_ADDRESSES; i++) {
        address_clean(&(*portmap)->address[i]);
    }

    amxc_var_clean(&(*portmap)->protocols);
    port_range_clean(&(*portmap)->ports);

    for(int i = 0; i < NUM_OF_FOLDERS; i++) {
        fw_folder_delete(&(*portmap)->folder[i]);
    }

    for(int i = 0; i < NUM_OF_CONTAINERS; i++) {
        folder_container_clean(&(*portmap)->folder_container[i]);
    }

    FREE(*portmap);

exit:
    return;
}

static statnat_portmap_t* _portmap_new(amxd_object_t* obj, bool is_host) {
    statnat_portmap_t* portmap = NULL;
    bool init_failed = false;

    when_not_null(obj->priv, exit);

    init_failed = true;

    portmap = (statnat_portmap_t*) calloc(1, sizeof(statnat_portmap_t));
    when_null(portmap, exit);

    amxc_var_init(&portmap->protocols);
    amxc_llist_init(&portmap->ports);

    for(int i = 0; i < NUM_OF_FOLDERS; i++) {
        fw_folder_new(&portmap->folder[i]);
        when_null(portmap->folder[i], exit);
    }

    for(int i = 0; i < NUM_OF_CONTAINERS; i++) {
        folder_container_init(&portmap->folder_container[i], NULL);
        when_false(is_folders_initialized(&portmap->folder_container[i]), exit);
    }

    for(int i = 0; i < NUM_OF_ADDRESSES; i++) {
        address_init(&portmap->address[i]);
    }

    portmap->name = amxd_object_get_name(obj, AMXD_OBJECT_NAMED);
    portmap->is_host = is_host;
    portmap->obj = obj;
    obj->priv = portmap;

    init_failed = false;

exit:
    if(init_failed) {
        portmap_destroy(&portmap);
        obj->priv = NULL;
    }
    return portmap;
}

static statnat_portmap_t* portmap_new(amxd_object_t* obj) {
    return _portmap_new(obj, false);
}

static statnat_portmap_t* host_new(amxd_object_t* obj) {
    return _portmap_new(obj, true);
}

static address_t* get_host_address(statnat_portmap_t* host, statnat_address_t addr_type) {
    address_t* address = NULL;

    when_null_trace(host, exit, ERROR, "missing argument");

    if(!host->is_host) {
        const char* name = host->name;
        host = get_host(host);
        when_null_trace(host, exit, ERROR, "StatNatPM[%s] can't find %s address", name, addr_type == PUB_ADDRESS ? "public" : "private");
    }

    address = &host->address[addr_type];
exit:
    return address;
}

static address_t* get_public_address(statnat_portmap_t* portmap) {
    return get_host_address(portmap, PUB_ADDRESS);
}

static address_t* get_private_address(statnat_portmap_t* portmap) {
    return get_host_address(portmap, PRIV_ADDRESS);
}

static bool has_host_address(statnat_portmap_t* portmap, statnat_address_t addr_type) {
    return !STRING_EMPTY(address_first_ipv4_char(get_host_address(portmap, addr_type)));
}

static bool has_public_address(statnat_portmap_t* portmap) {
    return has_host_address(portmap, PUB_ADDRESS);
}

static bool has_private_address(statnat_portmap_t* portmap) {
    return has_host_address(portmap, PRIV_ADDRESS);
}

static bool host_is_validated(statnat_portmap_t* host) {
    bool validated = false;

    if(!host->is_host) {
        const char* name = host->name;
        host = get_host(host);
        when_null_trace(host, exit, ERROR, "StatNatPM[%s] can't find host", name);
    }

    validated = host->validated == VALIDATED;
exit:
    return validated;
}

static bool enable_dnat_rule(statnat_portmap_t* portmap) {
    bool rv = false;
    dnat_args_t args = {0};
    status_t status = FIREWALL_ERROR;
    fw_folder_t* folder = portmap->folder[DNAT];

    args.chain = fw_get_chain("staticnat_dnat", "PREROUTING_StaticNAT");
    args.match.protocols = &portmap->protocols;
    args.match.dest_ports = &portmap->ports;
    args.match.dest_address = get_public_address(portmap);
    args.target.address = get_private_address(portmap);

    when_false(fill_folder_dnat_rule(folder, &args), exit);

    rv = enable_rule(NULL, folder, &status, false);
exit:
    return rv;
}

static bool enable_filter_rule(statnat_portmap_t* portmap, bool reverse) {
    bool rv = false;
    filter_args_t args = {0};
    status_t status = FIREWALL_ERROR;
    folder_container_t* folder_container = &portmap->folder_container[reverse ? REVERSE : FORWARD];
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = log_get_enable(&folder_container->log) ? folder_container->log_folders.folder : NULL;
#else
    fw_folder_t* log_folder = NULL;
#endif

    args.chain = fw_get_chain("staticnat", "FORWARD_StaticNAT");
    args.ipversion = IPv4;
    args.reverse = reverse;
    args.match.dest_address = get_private_address(portmap);
    args.match.protocols = &portmap->protocols;
    args.match.dest_ports = &portmap->ports;
    args.target.verdict = TARGET_ACCEPT;

    when_false(fill_folder_container_filter_rule(folder_container, &args), exit);

    rv = enable_rule(log_folder, folder_container->rule_folders.folder, &status, false);
exit:
    return rv;
}

static bool enable_forward_filter_rule(statnat_portmap_t* portmap) {
    return enable_filter_rule(portmap, false);
}

static bool enable_reverse_filter_rule(statnat_portmap_t* portmap) {
    return enable_filter_rule(portmap, true);
}

static bool enable_snat_rule(statnat_portmap_t* portmap) { // todo portforw seems to have SNAT maybe make generic func?
    fw_rule_t* r = NULL;
    bool rv = false;
    status_t status = FIREWALL_ERROR;
    fw_folder_t* folder = portmap->folder[SNAT];
    const char* source = NULL;

    fw_folder_set_feature(folder, FW_FEATURE_SNAT);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "Failed to fetch default rule");

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, fw_get_chain("staticnat_snat", "POSTROUTING_StaticNAT"));
    fw_rule_set_ipv4(r, true);

    source = address_first_ipv4_char(get_private_address(portmap));
    when_false(folder_default_set_source(r, source, NULL, false), out);

    fw_folder_push_rule(folder, r);

    when_false(folder_feature_set_protocol(folder, &portmap->protocols), out);
    when_false(folder_feature_set_source_port(folder, &portmap->ports, false), out);
    when_false(folder_feature_set_target_snat(folder, get_public_address(portmap), 0, 0), out);

    rv = enable_rule(NULL, folder, &status, false);
out:
    return rv;
}

static bool portmap_activate(statnat_portmap_t* portmap) {
    bool rv = true;

    when_false_trace(has_public_address(portmap), skip_apply, ERROR, "%s[%s] no address", !portmap->is_host ? "StatNatPM" : "StatNatH", portmap->name);
    when_false_trace(has_private_address(portmap), skip_apply, ERROR, "%s[%s] no address", !portmap->is_host ? "StatNatPM" : "StatNatH", portmap->name);

    rv &= enable_dnat_rule(portmap); // ingress NAT
    when_false(rv, stop);
    rv &= enable_forward_filter_rule(portmap);
    when_false(rv, stop);
    rv &= enable_reverse_filter_rule(portmap);
    when_false(rv, stop);
    rv &= enable_snat_rule(portmap); // egress NAT
    when_false(rv, stop);

stop:
    if(!rv || (fw_commit(fw_rule_callback) != 0)) {
        if(rv) {
            SAH_TRACEZ_ERROR(ME, "%s[%s] fw_commit failed", !portmap->is_host ? "StatNatPM" : "StatNatH", portmap->name);
            rv = false;
        }
        portmap_reset_folders(portmap);
    }

    fw_apply();
skip_apply:
    return rv;
}

static bool portmap_can_enable(statnat_portmap_t* portmap) {
    bool rv = false;
    int32_t start = 0;
    int32_t end = 0;
    const char* protocol = NULL;

    when_true_status(portmap->validated == VALIDATED, exit, rv = true);

    if(!amxd_object_get_value(bool, portmap->obj, "Enable", NULL)) {
        set_status(portmap->obj, FIREWALL_DISABLED);
        // can't enable but not misconfigured
        goto exit;
    }

    protocol = object_da_string(portmap->obj, "Protocol");
    if(STRING_EMPTY(protocol) || !protocol_from_string(&portmap->protocols, protocol)) {
        set_status(portmap->obj, FIREWALL_ERROR);
        portmap->validated = MISCONFIGURED;
        goto exit;
    }

    start = amxd_object_get_value(int32_t, portmap->obj, "DestPort", NULL);
    end = amxd_object_get_value(int32_t, portmap->obj, "DestPortRangeMax", NULL);
    if(!port_range_from_int(&portmap->ports, start, end)) {
        set_status(portmap->obj, FIREWALL_ERROR);
        portmap->validated = MISCONFIGURED;
        goto exit;
    }

    portmap->validated = VALIDATED;
    rv = true; // not returning portmap->validated in case portmap is a host and the host is validated
exit:
    return rv;
}

static bool portmap_enable(statnat_portmap_t* portmap) {
    bool rv = false;

    when_true_trace(portmap_is_active(portmap), skip_status, INFO, "%s[%s] already enabled", !portmap->is_host ? "StatNatPM" : "StatNatH", portmap->name);

    when_false(portmap_can_enable(portmap), skip_status); // portmap_can_enable sets the status if it returned false

    when_false_trace(host_is_validated(portmap), exit, INFO, "%s[%s] is waiting for Host", !portmap->is_host ? "StatNatPM" : "StatNatH", portmap->name);

    rv = portmap_activate(portmap);
exit:
    set_status(portmap->obj, rv ? FIREWALL_ENABLED : FIREWALL_ERROR);
skip_status:
    return rv;
}

static void portmap_changed(amxd_object_t* obj) {
    statnat_portmap_t* portmap = NULL;

    if(obj->priv == NULL) {
        portmap = portmap_new(obj);
        when_null(portmap, exit);
    } else {
        portmap = (statnat_portmap_t*) obj->priv;
        portmap_disable(portmap);
    }

    portmap_enable(portmap);

exit:
    if(portmap != NULL) {
        update_host_portmapping(portmap);
    }
}

void _statnat_portmap_added(UNUSED const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    amxd_object_t* obj = amxd_object_get_instance(amxd_dm_signal_get_object(fw_get_dm(), data), NULL, GET_UINT32(data, "index"));
    when_null(obj, exit);
    portmap_changed(obj);
exit:
    return;
}

void _statnat_portmap_changed(UNUSED const char* const sig_name,
                              const amxc_var_t* const data,
                              UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(fw_get_dm(), data);
    when_null(obj, exit);
    portmap_changed(obj);
exit:
    return;
}

static void update_host_portmapping(statnat_portmap_t* host) {
    uint32_t validated = 0;
    uint32_t misconfigured = 0;

    if(!host->is_host) {
        const char* name = host->name;
        host = get_host(host);
        when_null_trace(host, exit, ERROR, "StatNatPM[%s] can't find host", name);
    }

    count_portmappings(host, &validated, &misconfigured);

    if(host->teardown ||
       !host_is_validated(host) ||
       (misconfigured > 0) ||
       (validated >= 1)) {
        portmap_disable(host);
    } else if(validated == 0) {
        portmap_enable(host);
    }

exit:
    return;
}

static void update_all_portmappings(statnat_portmap_t* host) {
    amxd_object_for_each(instance, it, amxd_object_findf(host->obj, ".PortMapping")) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        portmap_changed(obj);
    }

    update_host_portmapping(host);
}

static bool host_can_enable(statnat_portmap_t* host) {
    struct {
        address_t* address;
        const char* param;
    } addresses[] = {
        {get_host_address(host, PUB_ADDRESS), "PublicIPAddress"},
        {get_host_address(host, PRIV_ADDRESS), "LANDevice"},
        {0}
    };
    bool rv = false;

    when_false_trace(host->is_host, exit, ERROR, "Expected host but got StatNatPM[%s]", host->name);
    when_true_status(host_is_validated(host), exit, rv = true);

    if(!amxd_object_get_value(bool, host->obj, "Enable", NULL)) {
        set_status(host->obj, FIREWALL_DISABLED);
        goto exit;
    }

    for(int i = 0; addresses[i].param != NULL; i++) {
        const char* value = object_da_string(host->obj, addresses[i].param);
        if(STRING_EMPTY(value)) {
            SAH_TRACEZ_ERROR(ME, "StatNatH[%s] misconfigured because %s is empty", host->name, addresses[i].param);
            set_status(host->obj, FIREWALL_ERROR);
            goto exit;
        }

        address_from_string(value, addresses[i].address); // the result is a list with 1 address
    }

    host->validated = VALIDATED;
    rv = true; // not returning host->validated in case host is a portmap and the portmap is validated

exit:
    return rv;
}

static bool host_enable(statnat_portmap_t* host) {
    bool rv = false;

    when_false_trace(host->is_host, exit, ERROR, "Expected host but got StatNatPM[%s]", host->name);

    when_false(host_can_enable(host), skip_status); // host_can_enable sets the status if it returned false

    rv = true;

exit:
    set_status(host->obj, rv ? FIREWALL_ENABLED : FIREWALL_ERROR);
skip_status:
    return rv;
}

static bool host_changed(amxd_object_t* obj) {
    statnat_portmap_t* host = NULL;
    bool rv = false;

    if(obj->priv == NULL) {
        host = host_new(obj);
        when_null(host, exit);
    } else {
        host = (statnat_portmap_t*) obj->priv;
        host_disable(host);
    }

    when_false(host_enable(host), skip_status); // host_enable sets the status

    rv = true;

exit:
    if(!rv) {
        set_status(obj, FIREWALL_ERROR);
    }
skip_status:
    if(host != NULL) {
        update_all_portmappings(host);
    }
    return rv;
}

void _statnat_host_added(UNUSED const char* const sig_name,
                         const amxc_var_t* const data,
                         UNUSED void* const priv) {
    amxd_object_t* obj = amxd_object_get_instance(amxd_dm_signal_get_object(fw_get_dm(), data), NULL, GET_UINT32(data, "index"));
    when_null(obj, exit);
    host_changed(obj);
exit:
    return;
}

void _statnat_host_changed(UNUSED const char* const sig_name,
                           const amxc_var_t* const data,
                           UNUSED void* const priv) {
    amxd_object_t* obj = amxd_dm_signal_get_object(fw_get_dm(), data);
    when_null(obj, exit);
    host_changed(obj);
exit:
    return;
}

amxd_status_t _statnat_host_destroy(amxd_object_t* obj,
                                    UNUSED amxd_param_t* param,
                                    amxd_action_t reason,
                                    UNUSED const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    return _statnat_portmap_destroy(obj, NULL, reason, NULL, NULL, NULL);
}

amxd_status_t _statnat_portmap_destroy(amxd_object_t* obj,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    statnat_portmap_t* portmap = NULL;

    when_true_status(reason != action_object_destroy,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_null_status(obj->priv, exit, status = amxd_status_ok);

    portmap = (statnat_portmap_t*) obj->priv;
    portmap_destroy(&portmap);
    obj->priv = NULL;

    status = amxd_status_ok;
exit:
    return status;
}

void init_static_nat(void) {
    amxd_object_for_each(instance, it, amxd_dm_findf(fw_get_dm(), "NAT.%sStaticNAT.Host", fw_get_vendor_prefix())) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        host_changed(obj);
    }
}