/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dmz/dm_dmz.h"
#include "dmz/dmz.h"
#include "nat/masquerade.h" // for is_nat_enabled_interface
#include "firewall.h"       // for fw_get_dm
#include "init_cleanup.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include "nat/masquerade.h"
#include "logs/logs.h"

#define ME "dmz"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    dmz_t* dmz = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    dmz = (dmz_t*) object->priv;
    when_null_trace(dmz, exit, ERROR, "Rule[%s] is NULL", OBJECT_NAMED);

    if(update_dmz_logs(NULL, object, NULL, data)) {
        dmz->common.flags = FW_MODIFIED;
        dmz_implement_changes();
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

const char* dmz_get_lan_address(void) {
    amxd_object_t* obj = amxd_dm_findf(fw_get_dm(), "Firewall.DMZ.");
    firewall_interface_t* fwiface = (firewall_interface_t*) obj->priv;
    const char* address = NULL;

    when_null_trace(fwiface, exit, ERROR, "DMZ template has no private data");

    address = address_first_ipv4_char(&fwiface->address);

exit:
    return address;
}

static void set_interface(dmz_t* dmz, const char* interface) {
    firewall_interface_t* fwiface = &dmz->common.src_fwiface;
    if(!STRING_EMPTY(interface)) {
        if(!is_nat_enabled_interface(interface)) {
            init_firewall_interface(fwiface);
            fwiface->netdev_required = true;
            fwiface->addr_required = true;
            common_set_status(&dmz->common, FIREWALL_ERROR);
            SAH_TRACEZ_WARNING(ME, "Interface needs to have nat enabled");
            return;
        }
        open_ip_address_query(interface, IPv4, "ipv4", fwiface);
    } else {
        init_firewall_interface(fwiface);
        fwiface->netdev_required = true;
        fwiface->addr_required = true;
    }
}

static void dmz_lan_interface_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    amxd_object_t* templ = amxd_dm_findf(fw_get_dm(), "Firewall.DMZ.");
    (void) fwiface; // when tracing is disabled, variable is not used anymore

    amxd_object_for_each(instance, it, templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        common_t* common = instance->priv;
        if(common == NULL) {
            continue;
        }
        common->flags = FW_MODIFIED;
    }

    dmz_implement_changes();
    FW_PROFILING_OUT();
}

static void dmz_template_init(void) {
    amxd_object_t* object = amxd_dm_findf(fw_get_dm(), "Firewall.DMZ.");
    firewall_interface_t* fwiface = NULL;
    const char* lan_interface = NULL;
    bool ret = false;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    lan_interface = GET_CHAR(amxo_parser_get_config(fw_get_parser(), "ip_intf_lan"), NULL);
    when_str_empty_trace(lan_interface, exit, ERROR, "DMZ[%s] LAN interface not defined", OBJECT_NAMED);

    fwiface = calloc(1, sizeof(firewall_interface_t));
    when_null(fwiface, exit);

    init_firewall_interface(fwiface);

    fwiface->query_result_changed = dmz_lan_interface_changed;
    ret = open_ip_address_query(lan_interface, IPv4, "ipv4", fwiface);
    when_false(ret, exit);

    object->priv = fwiface;
    ret = true;

exit:
    if(!ret && (fwiface != NULL)) {
        clean_firewall_interface(fwiface);
        free(fwiface);
    }
    return;
}

static bool dmz_init(amxd_object_t* object) {
    dmz_t* dmz = NULL;
    bool res = false;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    dmz = dmz_create(object);
    when_null(dmz, exit);

    set_interface(dmz, object_da_string(object, "Interface"));

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_init(object, &dmz->common.folders.log, &dmz->common.src_fwiface, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             log_get_dm_enable_param(object, NULL, NULL), log_get_dm_controller_param(object, NULL, NULL), TARGET_ACCEPT, update_rule_log_info_cb);

    //Initialize reverse log without subscribing
    log_init(object, &dmz->reverse.log, NULL, !EXCLUDE_DST_INTERFACE, &dmz->common.src_fwiface, !EXCLUDE_SRC_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             log_get_dm_enable_param(object, NULL, NULL), log_get_dm_controller_param(object, NULL, NULL), TARGET_ACCEPT, NULL);
#endif //FIREWALL_LOGS

    res = true;

exit:
    return res;
}

void _dmz_added(UNUSED const char* const sig_name,
                const amxc_var_t* const event_data,
                UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));

    when_null(object, exit);

    when_false(dmz_init(object), exit);
    dmz_implement_changes();
exit:
    FW_PROFILING_OUT();
    return;
}

void _dmz_changed(UNUSED const char* const sig_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    dmz_t* dmz = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    dmz = (dmz_t*) object->priv;

    dmz->common.flags = FW_MODIFIED;

    var = GET_ARG(params, "Interface");
    if(var != NULL) {
        set_interface(dmz, GET_CHAR(var, "to"));
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_dmz_logs(dmz, NULL, event_data, NULL);
#endif //FIREWALL_LOGS
    dmz_implement_changes();

exit:
    FW_PROFILING_OUT();
    return;
}

static int _dm_dmz_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    dmz_init(object);
    return amxd_status_ok;
}

void dm_dmz_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    dmz_template_init();

    amxd_object_for_all(amxd_dm_findf(fw_get_dm(), "Firewall"), ".DMZ.*.", _dm_dmz_init, NULL);
    dmz_implement_changes();
    FW_PROFILING_OUT();
}
