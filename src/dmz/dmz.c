/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "dmz/dmz.h"
#include "rules.h"    // for fw_callback_lease_timer
#include "firewall.h" // for fw_get_dm
#include "firewall_utilities.h"
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "dmz"

static amxc_llist_t* dmz_list = NULL;

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    common_t* common = (common_t*) fwiface->query_priv;

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_dmz_logs(NULL, common->object, NULL, NULL);
#endif //FIREWALL_LOGS

    dmz_implement_changes();

exit:
    FW_PROFILING_OUT();
}

void _dmz_lease_time_changed(UNUSED const char* const sig_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    dmz_t* dmz = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);

    dmz = (dmz_t*) object->priv;
    set_lease_duration(&object, dmz->lease_timer, &dmz->lease_duration, true);
    if(object == NULL) {
        dmz = NULL; // handle dangling pointers
        goto exit;
    }

exit:
    FW_PROFILING_OUT();
    return;
}

static bool dmz_delete(dmz_t** dmz) {
    when_null(*dmz, exit);

    SAH_TRACEZ_INFO(ME, "Delete Firewall DMZ");

    amxc_llist_it_take(&(*dmz)->it);

    fw_folder_delete(&(*dmz)->dnat_folder);
    fw_folder_delete(&(*dmz)->dnat_lan_folder);
    fw_folder_delete(&(*dmz)->snat_folder);
    folder_container_clean(&(*dmz)->reverse);
    common_clean(&(*dmz)->common);
    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_commit failed");
    }

    amxp_timer_delete(&((*dmz)->lease_timer));

    free(*dmz);
    *dmz = NULL;

    if(amxc_llist_is_empty(dmz_list)) {
        amxc_llist_delete(&dmz_list, NULL);
    }
exit:
    return true;
}

dmz_t* dmz_create(amxd_object_t* object) {
    dmz_t* dmz = NULL;
    int rv = 0;

    if(dmz_list == NULL) {
        amxc_llist_new(&dmz_list);
        when_null(dmz_list, out);
    }

    dmz = (dmz_t*) calloc(1, sizeof(dmz_t));
    when_null(dmz, out);

    amxc_llist_append(dmz_list, &dmz->it);

    common_init(&dmz->common, object);
    folder_container_init(&dmz->reverse, &dmz->common);

    rv |= fw_folder_new(&dmz->dnat_folder);
    rv |= fw_folder_new(&dmz->dnat_lan_folder);
    rv |= fw_folder_new(&dmz->snat_folder);
    if((rv != 0) || (!is_folders_initialized(&dmz->common.folders)) || (!is_folders_initialized(&dmz->reverse))) {
        SAH_TRACEZ_ERROR(ME, "DMZ[%s] failed to create folders", OBJECT_NAMED);
        dmz_delete(&dmz);
        goto out;
    }

    dmz->common.src_fwiface.query_priv = &dmz->common;
    dmz->common.src_fwiface.query_result_changed = query_result_changed;
    dmz->lease_timer = NULL;
    object->priv = dmz;

    amxp_timer_new(&(dmz->lease_timer), fw_callback_lease_timer, object);
    set_lease_duration(&object, dmz->lease_timer, &dmz->lease_duration, false);
    if(object == NULL) {
        dmz = NULL; // handle dangling pointers
        goto out;
    }

out:
    return dmz;
}

static void dmz_reset_folder(dmz_t* dmz) {
    amxd_object_t* object = NULL;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_null(dmz, out);

    object = dmz->common.object;

    folder_container_delete_rules(&dmz->common.folders, IPvAll, false);
    folder_container_delete_rules(&dmz->reverse, IPvAll, false);
    if(dmz->dnat_folder != NULL) {
        fw_folder_delete_rules(dmz->dnat_folder);
    }
    if(dmz->dnat_lan_folder != NULL) {
        fw_folder_delete_rules(dmz->dnat_lan_folder);
    }
    if(dmz->snat_folder != NULL) {
        fw_folder_delete_rules(dmz->snat_folder);
    }
    when_false_trace(fw_commit(fw_rule_callback) == 0, out, WARNING, "DMZ[%s] fw_commit failed", OBJECT_NAMED);
    folder_container_disable_folders(&dmz->common.folders, IPvAll);
    folder_container_disable_folders(&dmz->reverse, IPvAll);

out:
    return;
}

/**
 * Enable the iptables filter part of the dmz rule.
 * if reverse is set to true, enable a rule from LAN to WAN (FW level 'Custom' and 'High')
 * return true if the rule is successfully applied, false otherwise.
 */
static bool enable_dmz_filter(dmz_t* dmz, folder_container_t* folder_container, bool reverse) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    fw_folder_t* folder = folder_container->rule_folders.folder;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = folder_container->log_folders.folder;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&folder_container->log) ? 0 : 1;
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    amxd_object_t* object = NULL;
    firewall_interface_t* fwiface = NULL;
    bool retval = false;
    const char* sourceip = NULL;
    const char* destip = NULL;
    const char* chain_name = fw_get_chain("dmz", "FORWARD_DMZ");

    when_null(dmz, out);
    when_null(folder, out);

    object = dmz->common.object;
    fwiface = &dmz->common.src_fwiface;

    SAH_TRACEZ_NOTICE(ME, "DMZ[%s] about to enable filter rule", OBJECT_NAMED);

    if(fwiface->netdev_required && STRING_EMPTY(fwiface->default_netdevname)) {
        SAH_TRACEZ_INFO(ME, "DMZ[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));
        return false;
    }
    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        if(folders[i] != log_folder) {
            fw_folder_set_feature(folders[i], FW_FEATURE_TARGET);
        }

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_chain(r, chain_name);

        if(!reverse) {
            fw_rule_set_in_interface(r, fwiface->default_netdevname);
        } else {
            fw_rule_set_out_interface(r, fwiface->default_netdevname);
        }
        fw_rule_set_ipv4(r, true);

        sourceip = object_da_string(object, "SourcePrefix");
        if(!STRING_EMPTY(sourceip)) {
            if(!reverse) {
                fw_rule_set_source(r, sourceip);
            } else {
                fw_rule_set_destination(r, sourceip);
            }
        }

        destip = object_da_string(object, "DestIP");
        if(!STRING_EMPTY(destip)) {
            if(!reverse) {
                fw_rule_set_destination(r, destip);
            } else {
                fw_rule_set_source(r, destip);
            }
        }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &folder_container->log);
        }
#endif //FIREWALL_LOGS

        fw_folder_push_rule(folders[i], r);

        if(folders[i] != log_folder) {
            s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_TARGET);
            when_null_trace(s, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

            if(folders[i] != log_folder) {
                fw_rule_set_target_policy(s, FW_RULE_POLICY_ACCEPT);
            }
            fw_folder_push_rule(folders[i], s);
        }
    }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    retval = enable_rule(log_get_enable(&folder_container->log) ? log_folder : NULL, folder, &(dmz->common.status), false);
#else //FIREWALL_LOGS
    retval = enable_rule(NULL, folder, &(dmz->common.status), false);
#endif //FIREWALL_LOGS

out:
    FW_PROFILING_OUT();
    return retval;
}

/**
 * Enable the iptables DNAT part of the dmz rule.
 * return true if the rule is successfully applied, false otherwise.
 */
static bool enable_dmz_nat(dmz_t* dmz, fw_folder_t* folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    firewall_interface_t* fwiface = NULL;
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    bool retval = false;
    bool waiting = false;
    const char* source_prefix = NULL;
    const char* destip = NULL;

    when_null(dmz, out);
    when_null(folder, out);

    object = dmz->common.object;
    fwiface = &dmz->common.src_fwiface;

    SAH_TRACEZ_INFO(ME, "DMZ[%s] about to enable DNAT rule", OBJECT_NAMED);

    waiting = (fwiface->addr_required && !address_has_ipversion(&fwiface->address, IPv4));
    when_true_trace(waiting, out, INFO, "DMZ[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    destip = object_da_string(object, "DestIP");
    when_str_empty_trace(destip, out, ERROR, "DMZ[%s] requires dest IP address", OBJECT_NAMED);

    fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, fw_get_chain("dmz_dnat", "PREROUTING_DMZ"));
    fw_rule_set_destination(r, address_first_ipv4_host_address(&fwiface->address));
    fw_rule_set_ipv4(r, true);

    source_prefix = object_da_string(object, "SourcePrefix");
    if(!STRING_EMPTY(source_prefix)) {
        fw_rule_set_source(r, source_prefix);
    }
    fw_folder_push_rule(folder, r);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    when_null_trace(s, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_target_dnat(s, destip, 0, 0);
    fw_folder_push_rule(folder, s);

    retval = enable_rule(NULL, folder, &(dmz->common.status), true);
out:
    FW_PROFILING_OUT();
    return retval;
}

/**
 * Enable the iptables LAN DNAT part of the dmz rule.
 * return true if the rule is successfully applied, false otherwise.
 * This rule is used to allow DMZ for LAN hosts, whenever a lan clients uses the WAN IPv4 address
 * and DMZ is enable, it should always be redirected to the DMZ host, regardless the local services.
 */
static bool enable_dmz_lan_nat(dmz_t* dmz, fw_folder_t* folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    firewall_interface_t* fwiface = NULL;
    amxd_object_t* object = NULL;
    bool retval = false;
    bool waiting = false;

    when_null(dmz, out);
    when_null(folder, out);

    fwiface = &dmz->common.src_fwiface;
    object = dmz->common.object;
    (void) object; // when tracing is disabled, variable is not used anymore

    waiting = (fwiface->addr_required && !address_has_ipversion(&fwiface->address, IPv4));
    when_true_trace(waiting, out, INFO, "DMZ[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    SAH_TRACEZ_INFO(ME, "DMZ[%s] about to enable LAN NAT rule(s)", OBJECT_NAMED);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, fw_get_chain("dmz_dnat_lan", "PREROUTING_LAN_DMZ"));
    fw_rule_set_target_chain(r, fw_get_chain("dmz_dnat", "PREROUTING_DMZ"));
    fw_rule_set_destination(r, address_first_ipv4_host_address(&fwiface->address));
    fw_rule_set_ipv4(r, true);
    fw_folder_push_rule(folder, r);

    retval = enable_rule(NULL, folder, &(dmz->common.status), true);
out:
    FW_PROFILING_OUT();
    return retval;
}

/**
 * Enable the iptables SNAT part of the dmz rule.
 * return true if the rule is successfully applied, false otherwise.
 */
static bool enable_dmz_snat(dmz_t* dmz, fw_folder_t* folder) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    amxd_object_t* object = NULL;
    firewall_interface_t* fwiface = NULL;
    bool retval = false;
    bool waiting = false;
    const char* destip = NULL;
    const char* lan_address = dmz_get_lan_address();

    when_null(dmz, out);
    when_null(folder, out);

    object = dmz->common.object;
    fwiface = &dmz->common.src_fwiface;

    when_str_empty_trace(lan_address, out, INFO, "DMZ[%s] is waiting for LAN address", OBJECT_NAMED);

    waiting = (fwiface->addr_required && !address_has_ipversion(&fwiface->address, IPv4));
    when_true_trace(waiting, out, INFO, "DMZ[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    destip = object_da_string(object, "DestIP");
    when_str_empty_trace(destip, out, ERROR, "DMZ[%s] is missing dest ip address", OBJECT_NAMED);

    SAH_TRACEZ_INFO(ME, "DMZ[%s] enable SNAT rule(s)", OBJECT_NAMED);
    fw_folder_set_feature(folder, FW_FEATURE_SNAT);
    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_table(r, TABLE_NAT);
    fw_rule_set_chain(r, fw_get_chain("dmz_snat", "POSTROUTING_DMZ"));
    fw_rule_set_source(r, lan_address);
    fw_rule_set_destination(r, destip);
    fw_rule_set_ipv4(r, true);
    fw_folder_push_rule(folder, r);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SNAT);
    when_null_trace(s, out, ERROR, "DMZ[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_target_snat(s, address_first_ipv4_host_address(&fwiface->address), 0, 0);
    fw_folder_push_rule(folder, s);

    retval = enable_rule(NULL, folder, &(dmz->common.status), true);
out:
    FW_PROFILING_OUT();
    return retval;
}

static bool dmz_activate(dmz_t* dmz) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = dmz->common.object;
    bool retval = false;
    bool enable = amxd_object_get_value(bool, object, "Enable", NULL);
    bool dmz_is_active = (dmz->common.status == FIREWALL_ENABLED);
    const char* interface = object_da_string(object, "Interface");
    const char* ip_address = object_da_string(object, "DestIP");
    (void) object; // when tracing is disabled, variable is not used anymore

    if(!enable) {
        SAH_TRACEZ_INFO(ME, "DMZ[%s] is not enabled", OBJECT_NAMED);
        retval = true;
        goto skip_status;
    }

    if(dmz_is_active) {
        SAH_TRACEZ_NOTICE(ME, "DMZ[%s] is already active", OBJECT_NAMED);
        retval = true;
        goto skip_status;
    }

    when_str_empty_trace(interface, out, WARNING, "DMZ[%s] requires Interface", OBJECT_NAMED);
    when_str_empty_trace(ip_address, out, WARNING, "DMZ[%s] requires DestIP", OBJECT_NAMED);

    if(enable_dmz_filter(dmz, &dmz->common.folders, false) == false) {
        SAH_TRACEZ_WARNING(ME, "DMZ[%s] failed to enable filter rule", OBJECT_NAMED);
        goto out;
    }

    if(enable_dmz_filter(dmz, &dmz->reverse, true) == false) {
        SAH_TRACEZ_WARNING(ME, "DMZ[%s] failed to enable reverse filter rule", OBJECT_NAMED);
        folder_container_disable_folders(&dmz->reverse, IPv4);
        goto out;
    }
    when_false_trace(commit_filter_rules(&dmz->common, &dmz->reverse, true), out, ERROR, "DMZ[%s] failed to to commit filter rules", OBJECT_NAMED);

    retval = enable_dmz_nat(dmz, dmz->dnat_folder);
    when_false_trace(retval, out, WARNING, "DMZ[%s] failed to enable nat rule", OBJECT_NAMED);

    retval = enable_dmz_lan_nat(dmz, dmz->dnat_lan_folder);
    when_false_trace(retval, out, WARNING, "DMZ[%s] failed to enable LAN nat rule", OBJECT_NAMED);

    retval = enable_dmz_snat(dmz, dmz->snat_folder);
    when_false_trace(retval, out, WARNING, "DMZ[%s] failed to enable snat rule", OBJECT_NAMED);

    retval = true;
out:
    common_set_status(&dmz->common, retval ? FIREWALL_ENABLED : FIREWALL_ERROR);
skip_status:
    FW_PROFILING_OUT();
    return retval;
}

static bool dmz_deactivate(dmz_t* dmz) {
    amxd_object_t* object = dmz->common.object;
    bool dmz_is_active = (dmz->common.status != FIREWALL_DISABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(dmz_is_active, out, NOTICE, "DMZ[%s] is not active", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "DMZ[%s] deactivate", OBJECT_NAMED);
    dmz_reset_folder(dmz);
    common_set_status(&dmz->common, FIREWALL_DISABLED);

out:
    return true;
}

bool dmz_implement_changes(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_llist_for_each(it, dmz_list) {
        dmz_t* dmz = amxc_container_of(it, dmz_t, it);

        if(dmz->common.flags & FW_DELETED) {
            dmz_deactivate(dmz);
            dmz_delete(&dmz);
            continue;
        }
        if((dmz->common.flags & FW_NEW) || (dmz->common.flags & FW_MODIFIED)) {
            if(dmz->common.flags & FW_MODIFIED) {
                dmz_deactivate(dmz);
            }
            dmz_activate(dmz);
            dmz->common.flags = 0;
        }
    }
    if(fw_apply() != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_apply failed");
    }
    FW_PROFILING_OUT();
    return true;
}

amxd_status_t _RemainingLeaseTime_dmz_lease_onread(amxd_object_t* const object,
                                                   AMXB_UNUSED amxd_param_t* const param,
                                                   amxd_action_t reason,
                                                   AMXB_UNUSED const amxc_var_t* const args,
                                                   amxc_var_t* const retval,
                                                   AMXB_UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    dmz_t* dmz = NULL;
    uint32_t remaining_time = 0;
    amxd_object_type_t obj_type = amxd_object_invalid;

    when_null(object, exit);
    obj_type = amxd_object_get_type(object);
    when_false(obj_type == amxd_object_instance, exit);
    dmz = (dmz_t*) object->priv;
    when_null(dmz, exit);

    when_null(retval, exit);
    if(reason != action_param_read) {
        SAH_TRACEZ_ERROR(ME, "Wrong reason");
        status = amxd_status_invalid_action;
        goto exit;
    }

    if(dmz->lease_duration > 0) {
        amxp_timers_calculate();
        remaining_time = (uint32_t) amxp_timer_remaining_time(dmz->lease_timer) / 1000;
    }

    //Set the retval to the value we found
    if(amxc_var_set_uint32_t(retval, remaining_time) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set parameter RemainingLeaseTime");
        goto exit;
    }

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
bool update_dmz_logs(dmz_t* p_dmz, amxd_object_t* object, const amxc_var_t* const data_rule, const amxc_var_t* const data_log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool update = false;
    dmz_t* dmz = NULL;

    if(p_dmz == NULL) {
        when_null(object, exit);
        dmz = (dmz_t*) object->priv;
        when_null(dmz, exit);
    } else {
        dmz = p_dmz;
    }

    update = log_update(&dmz->common.folders.log,
                        log_get_dm_enable_param(dmz->common.object, data_rule, NULL),
                        log_get_dm_controller_param(dmz->common.object, data_rule, NULL),
                        !EXCLUDE_SRC_INTERFACE,
                        !EXCLUDE_DST_INTERFACE,
                        data_log);


    update |= log_update(&dmz->reverse.log,
                         log_get_dm_enable_param(dmz->common.object, data_rule, NULL),
                         log_get_dm_controller_param(dmz->common.object, data_rule, NULL),
                         !EXCLUDE_SRC_INTERFACE,
                         !EXCLUDE_DST_INTERFACE,
                         data_log);

exit:
    FW_PROFILING_OUT();
    return update;
}
#endif //FIREWALL_LOGS