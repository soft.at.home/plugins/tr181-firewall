/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "validator.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <ipat/ipat.h>

#define ME "firewall"

// the libamxd "check_range" validator will check only the length of csv strings and not the integer values
amxd_status_t _fw_list_check_range(UNUSED amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* data = (amxc_var_t*) priv;
    amxc_var_t var_list;
    int min = 0;
    int max = 0;

    amxc_var_init(&var_list);

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_failed_trace(amxc_var_convert(&var_list, args, AMXC_VAR_ID_CSV_STRING), exit, ERROR, "conversion failed: %s", GET_CHAR(args, NULL));
    when_failed_trace(amxc_var_cast(&var_list, AMXC_VAR_ID_LIST), exit, ERROR, "conversion failed: %s", GET_CHAR(args, NULL));

    min = amxc_var_dyncast(int32_t, GET_ARG(data, "min"));
    max = amxc_var_dyncast(int32_t, GET_ARG(data, "max"));

    amxc_var_for_each(var, (&var_list)) {
        char* endptr = NULL;
        int value = strtol(GET_CHAR(var, NULL), &endptr, 10); // can't use amxc_var_dyncast(int32, ...) here because it returns 0 for non numerical values and that might be in range
        if((endptr == NULL) || (*endptr != '\0') || (value < min) || (value > max)) {
            status = amxd_status_invalid_value;
            goto exit;
        }
    }

    status = amxd_status_ok;
exit:
    amxc_var_clean(&var_list);
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _max_instances_check(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   amxd_action_t reason,
                                   const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;
    uint32_t max_instances = GET_UINT32(args, NULL);
    const char* path = GET_CHAR((amxc_var_t*) priv, NULL);
    amxd_object_t* parent = NULL;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);
    when_null_status(path, exit, status = amxd_status_invalid_arg);
    when_null_status(fw_get_dm(), exit, status = amxd_status_ok); // entry point hasn't been called (amxd_object_findf will fail)

    parent = amxd_object_findf(object, "%s", path);
    when_false_status(amxd_object_template == amxd_object_get_type(parent), exit, status = amxd_status_invalid_arg);
    when_false_trace(max_instances >= amxd_object_get_instance_count(parent), exit, ERROR, "Too many instances");

    status = amxd_status_ok;

exit:
    FW_PROFILING_OUT();
    return status;
}

static bool valid_prefix(bool ipv4, const amxc_var_t* const args) {
    amxc_var_t csv;
    bool valid = false;

    amxc_var_init(&csv);

    when_str_empty_status(GET_CHAR(args, NULL), exit, valid = true);

    amxc_var_convert(&csv, args, AMXC_VAR_ID_CSV_STRING);
    amxc_var_cast(&csv, AMXC_VAR_ID_LIST);

    amxc_var_for_each(prefix, &csv) {
        ipat_t ip = IPAT_INITIALIZE;
        when_false(ipat_pton(&ip, ipv4 ? AF_INET : AF_INET_ANY, GET_CHAR(prefix, NULL)), exit);
        when_false(ipat_valid(&ip), exit);
    }

    valid = true;
exit:
    amxc_var_clean(&csv);
    return valid;
}

amxd_status_t _fw_is_valid_prefix(UNUSED amxd_object_t* object,
                                  UNUSED amxd_param_t* param,
                                  amxd_action_t reason,
                                  const amxc_var_t* const args,
                                  UNUSED amxc_var_t* const retval,
                                  UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_false_status(valid_prefix(false, args), exit, status = amxd_status_invalid_arg);

    status = amxd_status_ok;

exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _fw_is_valid_ipv4_prefix(UNUSED amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       amxd_action_t reason,
                                       const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_unknown_error;

    when_true_status(reason != action_param_validate,
                     exit,
                     status = amxd_status_function_not_implemented);

    when_false_status(valid_prefix(true, args), exit, status = amxd_status_invalid_arg);

    status = amxd_status_ok;

exit:
    FW_PROFILING_OUT();
    return status;
}