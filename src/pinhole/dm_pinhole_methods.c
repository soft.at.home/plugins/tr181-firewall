/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "pinhole/dm_pinhole.h"
#include "rules.h"
#include "firewall_utilities.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>

#define ME "pinhole"

static bool add_pinhole_parameters(amxd_trans_t* trans, amxc_var_t* args) {
    const char* origin = GET_CHAR(args, "origin");
    const char* interface = GET_CHAR(args, "interface");
    const char* internal_client = GET_CHAR(args, "internalClient");
    const char* source_prefix = GET_CHAR(args, "sourcePrefix");
    const char* description = GET_CHAR(args, "description");
    // amxc_var_dyncast sourcePort, destinationPort and protocol in case the user gives a uint32, otherwise GET_CHAR returns NULL
    char* source_port = amxc_var_dyncast(cstring_t, GET_ARG(args, "sourcePort"));
    char* destination_port = amxc_var_dyncast(cstring_t, GET_ARG(args, "destinationPort"));
    char* protocol = amxc_var_dyncast(cstring_t, GET_ARG(args, "protocol"));
    char* lease_duration = amxc_var_dyncast(cstring_t, GET_ARG(args, "leaseDuration"));
    amxc_var_t* enable_arg = GET_ARG(args, "enable");
    bool retval = false;

    trans_add_optional_string(trans, "Origin", origin);
    trans_add_optional_string(trans, "Description", description);
    trans_add_optional_string(trans, "Interface", interface);
    trans_add_optional_string(trans, "Protocol", protocol);
    trans_add_optional_string(trans, "SourcePort", source_port);
    trans_add_optional_string(trans, "DestPort", destination_port);
    trans_add_optional_string(trans, "SourcePrefixes", source_prefix);
    trans_add_optional_string(trans, "DestIP", internal_client);
    trans_add_optional_string(trans, "LeaseDuration", lease_duration);
    if(enable_arg != NULL) {
        amxd_trans_set_value(bool, trans, "Enable", GET_BOOL(enable_arg, NULL));
    }

    retval = true;

    free(source_port);
    free(destination_port);
    free(protocol);
    free(lease_duration);

    return retval;
}

amxd_status_t _setPinhole(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* templ = amxd_dm_findf(fw_get_dm(), "Firewall.Pinhole.");
    status = set_and_return_params(templ, args, ret, add_pinhole_parameters);
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _updatePinhole(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* pinhole = NULL;
    uint32_t lease_duration;

    when_null(GET_ARG(args, "alias"), exit);
    when_null(GET_ARG(args, "leaseDuration"), exit);
    when_null(GET_ARG(args, "origin"), exit);
    pinhole = amxd_dm_findf(fw_get_dm(), "Firewall.Pinhole.%s.", GET_CHAR(args, "alias"));
    when_null(pinhole, exit);
    lease_duration = GET_UINT32(args, "leaseDuration");
    if(GET_UINT32(amxd_object_get_param_value(pinhole, "LeaseDuration"), NULL) == lease_duration) {
        rule_t* rule = pinhole->priv;

        status = amxp_timer_start(rule->lease_timer, rule->lease_duration * 1000);
    } else {
        status = _setPinhole(NULL, NULL, args, ret);
    }

exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _deletePinhole(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* templ = amxd_dm_findf(fw_get_dm(), "Firewall.Pinhole.");
    status = delete_by_alias(templ, args);
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _getPinhole(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* templ = amxd_dm_findf(fw_get_dm(), "Firewall.Pinhole.");
    status = list_by_alias(templ, args, ret);
    FW_PROFILING_OUT();
    return status;
}
