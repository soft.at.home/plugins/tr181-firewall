/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/interfacesetting.h"
#include "interfacesetting/icmp_echo_request.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <string.h>

#define ME "firewall"

#define ICMP_PROTOCOL_V4    "1"
#define ICMP_TYPE_V4        8
#define ICMP_PROTOCOL_V6    "58"
#define ICMP_TYPE_V6        128

#define ICMPV4_ECHO_STATUS        "ICMPv4EchoRequestStatus"
#define ICMPV6_ECHO_STATUS        "ICMPv6EchoRequestStatus"
#define IFSETTING_SERVICE_ALIAS   "ifsetting_icmpv%d_%s"

/**
   @brief
   Set the status of the ICMPv4EchoRequestStatus or the ICMPv6EchoRequestStatus.

   @param[in] common Common structure.
   @param[in] status Status to be set on the data model.
   @param[in] ipv4 Whether is IPv4 or not.
 */
static void set_ifsetting_icmp_status(common_t* common, const char* status, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_status_t ret = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_object_t* object = common->object;
    const cstring_t key = ipv4 ? ICMPV4_ECHO_STATUS : ICMPV6_ECHO_STATUS;

    when_null(amxd_object_get_param_def(object, key), out);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, object);

    amxd_trans_set_value(cstring_t, &trans, key, status);

    ret = amxd_trans_apply(&trans, fw_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(ret, out, ERROR, "InterfaceSetting[%s] failed to set '%s' to '%s'", OBJECT_NAMED, key, status);

out:
    FW_PROFILING_OUT();
    return;
}

static status_t read_status_from_string(const char* status) {
    status_t value = FIREWALL_ENABLED;

    when_str_empty_trace(status, exit, ERROR, "Missing status, using default %d", value);

    if(strcmp(status, "Disabled") == 0) {
        value = FIREWALL_DISABLED;
    } else if(strcmp(status, "Error") == 0) {
        value = FIREWALL_ERROR;
    } else if(strcmp(status, "Error_Misconfigured") == 0) {
        value = FIREWALL_ERROR_MISCONFIG;
    } else if(strcmp(status, "Pending") == 0) {
        value = FIREWALL_PENDING;
    } else if(strcmp(status, "Enabled") != 0) {
        SAH_TRACEZ_ERROR(ME, "Unknown status '%s', using default %d", status, value);
    }

exit:
    return value;
}

/**
   @brief
   Callback function that checks the status of the Firewall.Service instance
   that was created by the InterfaceSetting instance.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the AcceptICMPEchoRequest internal structure.
 */
static void ifsetting_check_icmp_status_cb(const char* const sig_name,
                                           const amxc_var_t* const data,
                                           void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    icmp_echo_request_t* icmp_echo_request = (icmp_echo_request_t*) priv;
    amxd_object_t* service_instance_icmp = NULL;
    const amxc_var_t* var = NULL;
    amxc_var_t* params = GET_ARG(data, "parameters");
    status_t service_status = FIREWALL_ERROR;
    bool ipv4 = true;
    bool ret = false;
    const cstring_t path = GET_CHAR(data, "path");
    const cstring_t action = NULL;
    ipversion_t ipversion = IPvAll;
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null_trace(icmp_echo_request, error, ERROR, "Interface Setting structure does not exist.");
    when_str_empty_trace(path, error, ERROR, "Path does not exist.");

    service_instance_icmp = amxd_dm_findf(fw_get_dm(), "%s", path);
    when_null_trace(icmp_echo_request, exit, ERROR, "ICMP Service instance could not be retrieved.");

    var = amxd_object_get_param_value(service_instance_icmp, "IPVersion");
    when_null_trace(var, exit, ERROR, "Failed to get the IPVersion parameter.");

    ipversion = int_to_ipversion(amxc_var_dyncast(int32_t, var));
    if(ipversion == IPv6) {
        ipv4 = false;
    }

    var = GET_ARG(params, "Status");
    if(STRING_EMPTY(GET_CHAR(var, NULL))) {
        var = amxd_object_get_param_value(service_instance_icmp, "Status");
    }
    when_str_empty_trace(GET_CHAR(var, NULL), exit, ERROR, "Failed to get the Status parameter from the Firewall.Service instance");

    service_status = read_status_from_string(GET_CHAR(var, NULL));
    when_false(service_status == FIREWALL_ENABLED, exit);

    var = GET_ARG(params, "Action");
    if(STRING_EMPTY(GET_CHAR(var, NULL))) {
        var = amxd_object_get_param_value(service_instance_icmp, "Action");
    }
    action = GET_CHAR(var, NULL);
    when_str_empty_trace(action, exit, ERROR, "Failed to get the Action parameter from the Firewall.Service instance");

    if(strcmp(action, "Accept") == 0) {
        set_ifsetting_icmp_status(&(icmp_echo_request->common), "Allow", ipv4);
    } else if(strcmp(action, "Drop") == 0) {
        set_ifsetting_icmp_status(&(icmp_echo_request->common), "Reject", ipv4);
    } else {
        set_ifsetting_icmp_status(&(icmp_echo_request->common), "Error", ipv4);
    }

    ret = true;
exit:
    if(!ret) {
        set_ifsetting_icmp_status(&(icmp_echo_request->common), "Error", ipv4);
    }
error:
    FW_PROFILING_OUT();
    return;
}

/**
   @brief
   Deffered function that deletes the Firewall.Service instance.

   @param[in] data the data that is passed to the function.
   @param[in] priv some pointer to private data
 */
static void deferred_function_delete_icmp_service(UNUSED const amxc_var_t* data, void* priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* services = NULL;
    amxc_var_t* var_alias = (amxc_var_t*) priv;
    amxd_trans_t trans;
    const cstring_t alias = GET_CHAR(var_alias, NULL);

    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    when_null_trace(services, exit, ERROR, "Failed to fetch the Firewall.Service. object");

    status = amxd_trans_init(&trans);
    when_failed_trace(status, exit, ERROR, "Transaction initialisation failed.");

    status = amxd_trans_select_object(&trans, services);
    when_failed_trace(status, clean, ERROR, "Failed to select the object for the transaction.");

    status = amxd_trans_del_inst(&trans, 0, alias);
    when_failed_trace(status, clean, ERROR, "Failed to delete the instance %s.", alias);

    status = amxd_trans_apply(&trans, fw_get_dm());
    when_failed_trace(status, clean, ERROR, "Failed to apply the transaction.");

clean:
    amxd_trans_clean(&trans);
exit:
    amxc_var_delete(&var_alias);
    FW_PROFILING_OUT();
}

/**
   @brief
   Subscribe to the Service instance that sets the ICMP rule.

   Used to update the ICMPv4EchoRequestStatus or ICMPv6EchoRequestStatus

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.
   @param[in] obj Object containing the Service instance.
   @param[in] ipv4 Whether is IPv4 or not.
 */
static void ifsetting_subscribe_to_service(icmp_echo_request_t* icmp_echo_request, amxd_object_t* object, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool* subscribed = ipv4 ? &icmp_echo_request->service_icmpv4_subscription : &icmp_echo_request->service_icmpv6_subscription;
    int* index = ipv4 ? &icmp_echo_request->icmpv4_index : &icmp_echo_request->icmpv6_index;

    if(!*subscribed) {
        cstring_t path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);
        int rv = -1;
        rv = amxb_subscribe(amxb_be_who_has("Firewall."),
                            path,
                            "notification in ['dm:instance-removed', 'dm:object-changed'] && path matches 'Firewall.Service.[0-9]+.$'",
                            ifsetting_check_icmp_status_cb,
                            icmp_echo_request);

        if(rv != 0) {
            SAH_TRACEZ_ERROR(ME, "AcceptICMPEchoRequest[%s] Failed to subscribe to ICMPv%d Service.", OBJECT_NAMED, ipv4 ? 4 : 6);
        } else {
            *subscribed = true;
            *index = amxd_object_get_index(object);
        }
        free(path);
    }
    FW_PROFILING_OUT();
}

/**
   @brief
   Fetch the Firewall.Service instance that sets the ICMP rule.

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   Object containing the object, or NULL if it fails.
 */
amxd_object_t* ifsetting_get_icmp_service(icmp_echo_request_t* icmp_echo_request, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance = NULL;
    amxc_string_t alias;

    amxc_string_init(&alias, 0);
    amxc_string_setf(&alias, IFSETTING_SERVICE_ALIAS, ipv4 ? 4 : 6, object_da_string(icmp_echo_request->common.object, "Alias"));

    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    when_null(services, out);

    service_instance = amxd_object_get_instance(services, amxc_string_get(&alias, 0), 0);
    when_null(service_instance, out);

out:
    amxc_string_clean(&alias);
    FW_PROFILING_OUT();
    return service_instance;
}

/**
   @brief
   Add an instance on the Firewall.Service to set the ICMP rules.

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.
   @param[in] ipv4 Whether is IPv4 or not.
   @param[in] accept Accept or not the ICMP echo requests.

   @return
   True if success, false if failed.
 */
static bool ifsetting_add_icmp(icmp_echo_request_t* icmp_echo_request, bool ipv4, bool accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance = NULL;
    amxd_trans_t trans;
    const amxc_var_t* param = NULL;
    const cstring_t interface = NULL;
    ipversion_t ipversion = ipv4 ? IPv4 : IPv6;
    amxc_string_t alias;
    bool ret = false;

    amxc_string_init(&alias, 0);
    amxc_string_setf(&alias, IFSETTING_SERVICE_ALIAS, ipversion, object_da_string(icmp_echo_request->common.object, "Alias"));

    param = amxd_object_get_param_value(icmp_echo_request->common.object, "Interface");
    when_null_trace(param, out, ERROR, "Failed to acquire the Interface from the Datamodel");
    interface = GET_CHAR(param, NULL);

    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    when_null_trace(services, out, ERROR, "Failed to fetch the Firewall.Service. object");

    status = amxd_trans_init(&trans);
    when_failed_trace(status, out, ERROR, "Transaction initialisation failed.");

    status = amxd_trans_select_object(&trans, services);
    when_failed_trace(status, clean, ERROR, "Failed to select the object for the transaction.");

    status = amxd_trans_add_inst(&trans, 0, amxc_string_get(&alias, 0));
    when_failed_trace(status, clean, ERROR, "Failed to add Service instance.");

    if(accept) {
        amxd_trans_set_value(cstring_t, &trans, "Action", "Accept");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Action", "Drop");
    }
    amxd_trans_set_value(cstring_t, &trans, "Interface", interface);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", ipv4 ? ICMP_PROTOCOL_V4 : ICMP_PROTOCOL_V6);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", ipversion);
    amxd_trans_set_value(int32_t, &trans, "ICMPType", ipv4 ? ICMP_TYPE_V4 : ICMP_TYPE_V6);
    amxd_trans_set_value(bool, &trans, "Enable", true);

    status = amxd_trans_apply(&trans, fw_get_dm());
    when_failed_trace(status, clean, ERROR, "Failed to apply the transaction.");

    service_instance = amxd_object_get_instance(services, amxc_string_get(&alias, 0), 0);
    when_null_trace(service_instance, clean, ERROR, "Failed to fetch the Service instance");

    set_ifsetting_icmp_status(&(icmp_echo_request->common), "Error", ipv4);
    ifsetting_subscribe_to_service(icmp_echo_request, service_instance, ipv4);

    ret = true;
clean:
    amxd_trans_clean(&trans);
out:
    amxc_string_clean(&alias);
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Modify the instance on the Firewall.Service that sets the ICMP rules.

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.
   @param[in] ipv4 Whether is IPv4 or not.
   @param[in] accept Accept or not the ICMP echo requests.

   @return
   True if success, false if failed.
 */
static bool ifsetting_modify_icmp(icmp_echo_request_t* icmp_echo_request, amxd_object_t* service_instance, bool ipv4, bool accept) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    const amxc_var_t* param = NULL;
    const cstring_t interface;
    bool ret = false;

    param = amxd_object_get_param_value(icmp_echo_request->common.object, "Interface");
    when_null_trace(param, out, ERROR, "Failed to acquire the Interface from the Datamodel");
    interface = GET_CHAR(param, NULL);

    status = amxd_trans_init(&trans);
    when_failed_trace(status, out, ERROR, "Transaction initialisation failed.");

    status = amxd_trans_select_object(&trans, service_instance);
    when_failed_trace(status, clean, ERROR, "Transaction select failed.");

    amxd_trans_set_value(cstring_t, &trans, "Interface", interface);
    if(accept) {
        amxd_trans_set_value(cstring_t, &trans, "Action", "Accept");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Action", "Drop");
    }
    amxd_trans_set_value(bool, &trans, "Enable", true);

    status = amxd_trans_apply(&trans, fw_get_dm());
    when_failed_trace(status, clean, ERROR, "Failed to apply the transaction.");

    ifsetting_subscribe_to_service(icmp_echo_request, service_instance, ipv4);

    ret = true;
clean:
    amxd_trans_clean(&trans);
out:
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Delete the instance on the Firewall.Service that sets the ICMP rules.

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_delete_icmp(icmp_echo_request_t* icmp_echo_request, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* services = NULL;
    amxd_object_t* service_instance = NULL;
    amxc_string_t alias;
    amxc_string_t indexed_path;
    amxc_var_t* var_alias = NULL;
    bool ret = false;
    cstring_t path = NULL;
    bool* subscribed = NULL;
    int* index = NULL;

    amxc_string_init(&alias, 0);
    amxc_string_init(&indexed_path, 0);

    when_null_trace(icmp_echo_request, out, ERROR, "FATAL: Internal struct is NULL");
    subscribed = ipv4 ? &icmp_echo_request->service_icmpv4_subscription : &icmp_echo_request->service_icmpv6_subscription;
    index = ipv4 ? &icmp_echo_request->icmpv4_index : &icmp_echo_request->icmpv6_index;

    amxc_string_setf(&alias, IFSETTING_SERVICE_ALIAS, ipv4 ? 4 : 6, object_da_string(icmp_echo_request->common.object, "Alias"));

    services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    when_null(services, out); // in case amx first destroys the Firewall.Service template

    service_instance = amxd_object_get_instance(services, amxc_string_get(&alias, 0), 0);
    if(service_instance == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to fetch the Firewall.Service.%s. instance, it was deleted by another process or user.", amxc_string_get(&alias, 0));
        when_false_trace(*index > 0, out, ERROR, "The index saved in the struct is < 0. Aborting ICMPEchoRequest delete.");
        amxc_string_setf(&indexed_path, "Firewall.Service.%d.", *index);
        path = amxc_string_dup(&indexed_path, 0, UINT32_MAX);
    } else {
        path = amxd_object_get_path(service_instance, AMXD_OBJECT_INDEXED);
        amxc_var_new(&var_alias);
        amxc_var_set(cstring_t, var_alias, amxc_string_get(&alias, 0));
        //As we can't delete the instance insite an action callback, we should create a deferred function to handle the Service cleanup.
        amxp_sigmngr_deferred_call(NULL, deferred_function_delete_icmp_service, NULL, var_alias);
    }

    if(*subscribed) {
        amxb_unsubscribe(amxb_be_who_has("Firewall."),
                         path,
                         ifsetting_check_icmp_status_cb,
                         icmp_echo_request);
        *subscribed = false;
    }
    free(path);
    path = NULL;

    ret = true;
out:
    amxc_string_clean(&alias);
    amxc_string_clean(&indexed_path);
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Activate the ICMPv4 and ICMPv6 rules.

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_icmp(icmp_echo_request_t* icmp_echo_request) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool ret = false;
    amxd_object_t* service_instance_icmpv4 = NULL;
    amxd_object_t* service_instance_icmpv6 = NULL;
    bool ipv4_status = (icmp_echo_request->accept_icmp & 0x1);
    bool ipv6_status = (icmp_echo_request->accept_icmp & 0x2) >> 1;

    service_instance_icmpv4 = ifsetting_get_icmp_service(icmp_echo_request, true);
    if(service_instance_icmpv4 != NULL) {
        ret = ifsetting_modify_icmp(icmp_echo_request, service_instance_icmpv4, true, ipv4_status);
    } else {
        ret = ifsetting_add_icmp(icmp_echo_request, true, ipv4_status);
    }
    when_false_trace(ret, out, ERROR, "Failed to activate rule for ICMPv4.");

    service_instance_icmpv6 = ifsetting_get_icmp_service(icmp_echo_request, false);
    if(service_instance_icmpv6 != NULL) {
        ret = ifsetting_modify_icmp(icmp_echo_request, service_instance_icmpv6, false, ipv6_status);
    } else {
        ret = ifsetting_add_icmp(icmp_echo_request, false, ipv6_status);
    }
    when_false_trace(ret, out, ERROR, "Failed to activate rule for ICMPv6.");

    ret = true;
out:
    FW_PROFILING_OUT();
    return ret;
}

/**
   @brief
   Delete/clean the AcceptICMPEchoRequest internal structure.

   @param[in] icmp_echo_request AcceptICMPEchoRequest internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_icmp_echo_request(icmp_echo_request_t** icmp_echo_request) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*icmp_echo_request, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete AcceptICMPEchoRequest");

    ifsetting_delete_icmp(*icmp_echo_request, true);
    ifsetting_delete_icmp(*icmp_echo_request, false);

    amxc_llist_it_take(&(*icmp_echo_request)->it);

    common_clean(&(*icmp_echo_request)->common);
    fw_commit(fw_rule_callback);

    free(*icmp_echo_request);
    *icmp_echo_request = NULL;
exit:
    FW_PROFILING_OUT();
    return true;
}

bool icmp_echo_request_implement_changes(icmp_echo_request_t* icmp_echo_request) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object;     // when tracing is disabled, variable is not used anymore

    when_null_trace(icmp_echo_request, exit, ERROR, "AcceptICMPEchoRequest internal structure is NULL.");

    object = icmp_echo_request->common.object;

    if(icmp_echo_request->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "AcceptICMPEchoRequest[%s] about to be deleted", OBJECT_NAMED);
        delete_fw_icmp_echo_request(&icmp_echo_request);
    } else if(icmp_echo_request->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "AcceptICMPEchoRequest[%s] about to be add", OBJECT_NAMED);
        ifsetting_activate_icmp(icmp_echo_request);
        icmp_echo_request->common.flags = 0;
    } else if(icmp_echo_request->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "AcceptICMPEchoRequest[%s] deactivate, activate", OBJECT_NAMED);
        ifsetting_activate_icmp(icmp_echo_request);
        icmp_echo_request->common.flags = 0;
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a AcceptICMPEchoRequest
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the AcceptICMPEchoRequest internal structure.
 */
icmp_echo_request_t* create_fw_icmp_echo_request(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    icmp_echo_request_t* icmp_echo_request = NULL;

    SAH_TRACEZ_NOTICE(ME, "AcceptICMPEchoRequest[%s] is new", OBJECT_NAMED);

    icmp_echo_request = (icmp_echo_request_t*) calloc(1, sizeof(icmp_echo_request_t));
    when_null(icmp_echo_request, exit);

    common_init(&icmp_echo_request->common, object);

    icmp_echo_request->service_icmpv4_subscription = false;
    icmp_echo_request->service_icmpv6_subscription = false;
    icmp_echo_request->icmpv4_index = 0;
    icmp_echo_request->icmpv6_index = 0;
exit:
    FW_PROFILING_OUT();
    return icmp_echo_request;
}