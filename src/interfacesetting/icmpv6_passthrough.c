/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/interfacesetting.h"
#include "interfacesetting/icmpv6_passthrough.h"
#include "firewall.h" // for fw_get_dm
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "firewall"

#define PASSTHROUGH_ICMPV6_STATUS "PassthroughICMPv6Status"

static common_t common_lan;
static bool initialized_lan_intf = false;

/**
   @brief
   Delete/clean the PassthroughICMPv6EchoRequest internal structure.

   @param[in] icmpv6_passthrough PassthroughICMPv6EchoRequest internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_icmpv6_passthrough(icmpv6_passthrough_t** icmpv6_passthrough) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*icmpv6_passthrough, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete PassthroughICMPv6EchoRequest");

    amxc_llist_it_take(&(*icmpv6_passthrough)->it);

    common_clean(&(*icmpv6_passthrough)->common);
    free(*icmpv6_passthrough);

    *icmpv6_passthrough = NULL;
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Activate the ICMPv6 Passthrough mode (Accept or Drop)

   @param[in] icmpv6_passthrough PassthroughICMPv6EchoRequest internal structure.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_icmpv6_passthrough(icmpv6_passthrough_t* icmpv6_passthrough) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = icmpv6_passthrough->common.object;
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    fw_folder_t* folder = icmpv6_passthrough->common.folders.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = icmpv6_passthrough->common.folders.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&icmpv6_passthrough->common.folders.log) ? 0 : 1;
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* fwiface = &icmpv6_passthrough->common.dest_fwiface;
    bool res = false;
    const char* chain = fw_get_chain("forward_ifsetting6", "FORWARD6_InterfaceSettings");
    (void) object; // when tracing is disabled, variable is not used anymore

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "PassthroughICMPv6EchoRequest[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));
    when_false_trace(initialized_lan_intf, out, ERROR, "LAN interface query is not initialized.");
    when_str_empty_trace(common_lan.dest_fwiface.default_netdevname, out, INFO, "PassthroughICMPv6EchoRequest[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&common_lan.dest_fwiface.netmodel_interface, 0));

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        fw_folder_set_feature(folders[i], FW_FEATURE_PROTOCOL);

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "PassthroughICMPv6EchoRequest[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_chain(r, chain);
        fw_rule_set_ipv4(r, false);
        fw_rule_set_in_interface(r, fwiface->default_netdevname);
        fw_rule_set_out_interface(r, common_lan.dest_fwiface.default_netdevname);
        fw_rule_set_icmpv6_type(r, 128);
        if(folders[i] != log_folder) {
            if(icmpv6_passthrough->target == TARGET_ACCEPT) {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT);
            } else if(icmpv6_passthrough->target == TARGET_REJECT) {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_REJECT);
            } else {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP);
            }
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        else {
            set_log_rule_specific_settings(r, &icmpv6_passthrough->common.folders.log);
        }
#endif //FIREWALL_LOGS

        fw_rule_set_table(r, TABLE_FILTER);
        fw_folder_push_rule(folders[i], r);

        s = fw_folder_fetch_feature_rule(folders[i], FW_FEATURE_PROTOCOL);
        when_null_trace(s, out, ERROR, "Service[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_protocol(s, 58);
        fw_folder_push_rule(folders[i], s);

        fw_folder_set_enabled(folders[i], true);
    }
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

static void unfold_fw_setting_icmpv6_passthrough(icmpv6_passthrough_t* icmpv6_passthrough) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(icmpv6_passthrough, out);

    folder_container_delete_rules(&icmpv6_passthrough->common.folders, IPv6, true);

out:
    FW_PROFILING_OUT();
    return;
}

static bool deactivate_icmpv6_passthrough(icmpv6_passthrough_t* icmpv6_passthrough) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = icmpv6_passthrough->common.object;
    bool is_active = (icmpv6_passthrough->status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "PassthroughICMPv6EchoRequest[%s] PassthroughICMPv6EchoRequest is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "PassthroughICMPv6EchoRequest[%s] deactivate PassthroughICMPv6EchoRequest", OBJECT_NAMED);
    /* do not commit yet, wait for the activate/delete to commit it. */
    unfold_fw_setting_icmpv6_passthrough(icmpv6_passthrough);
    _set_status(icmpv6_passthrough->common.object, PASSTHROUGH_ICMPV6_STATUS, FIREWALL_ERROR);
    icmpv6_passthrough->status = FIREWALL_DISABLED;
out:
    FW_PROFILING_OUT();
    return true;
}

static bool activate_icmpv6_passthrough(icmpv6_passthrough_t* icmpv6_passthrough) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = icmpv6_passthrough->common.object;
    bool is_active = (icmpv6_passthrough->status == FIREWALL_ENABLED);
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(is_active) {
        res = true;
        SAH_TRACEZ_NOTICE(ME, "PassthroughICMPv6EchoRequest[%s] PassthroughICMPv6EchoRequest already active", OBJECT_NAMED);
        goto out;
    }

    res = ifsetting_activate_icmpv6_passthrough(icmpv6_passthrough);
    when_false(res, out);

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_ERROR(ME, "PassthroughICMPv6EchoRequest[%s] fw_commit failed", OBJECT_NAMED);
        deactivate_icmpv6_passthrough(icmpv6_passthrough);
        goto out;
    }

    icmpv6_passthrough->status = FIREWALL_ENABLED;
    if(icmpv6_passthrough->target == TARGET_DROP) {
        _set_status(icmpv6_passthrough->common.object, PASSTHROUGH_ICMPV6_STATUS, FIREWALL_DISABLED);
    } else {
        _set_status(icmpv6_passthrough->common.object, PASSTHROUGH_ICMPV6_STATUS, FIREWALL_ENABLED);
    }

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}


bool icmpv6_passthrough_implement_changes(icmpv6_passthrough_t* icmpv6_passthrough) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_null_trace(icmpv6_passthrough, exit, ERROR, "PassthroughICMPv6EchoRequest internal structure is NULL.");

    object = icmpv6_passthrough->common.object;

    if(icmpv6_passthrough->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "PassthroughICMPv6EchoRequest[%s] about to be deleted", OBJECT_NAMED);
        deactivate_icmpv6_passthrough(icmpv6_passthrough);
        delete_fw_icmpv6_passthrough(&icmpv6_passthrough);
    } else if(icmpv6_passthrough->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "PassthroughICMPv6EchoRequest[%s] about to be add", OBJECT_NAMED);
        activate_icmpv6_passthrough(icmpv6_passthrough);
        icmpv6_passthrough->common.flags = 0;
    } else if(icmpv6_passthrough->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "PassthroughICMPv6EchoRequest[%s] deactivate, activate", OBJECT_NAMED);
        deactivate_icmpv6_passthrough(icmpv6_passthrough);
        activate_icmpv6_passthrough(icmpv6_passthrough);
        icmpv6_passthrough->common.flags = 0;
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

static int _lan_change_for_each_interface(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;
    icmpv6_passthrough_t* icmpv6_passthrough = NULL;

    when_null(object, exit);
    //Must be on an instance object
    when_false(amxd_object_instance == amxd_object_get_type(object), exit);
    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);
    icmpv6_passthrough = ifsetting->icmpv6_passthrough;
    when_null(icmpv6_passthrough, exit);
    icmpv6_passthrough->common.flags = FW_MODIFIED;
    icmpv6_passthrough_implement_changes(icmpv6_passthrough);

exit:
    FW_PROFILING_OUT();
    return 0;
}

static void lan_query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    common_t* common = (common_t*) fwiface->query_priv;

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

    amxd_object_for_all(fw_ifsetting_template(fw_get_dm()), ".*.", _lan_change_for_each_interface, NULL);
    common_lan.flags = 0;
exit:
    FW_PROFILING_OUT();
}

static void create_common_lan(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const cstring_t lan_interface = NULL;

    //Get the lan interface
    if(!initialized_lan_intf) {
        common_init(&common_lan, NULL);
        common_lan.dest_fwiface.query_priv = &common_lan;
        common_lan.dest_fwiface.query_result_changed = lan_query_result_changed;
        common_lan.flags = 0;
        lan_interface = GET_CHAR(amxo_parser_get_config(fw_get_parser(), "ip_intf_lan"), NULL);
        if(!STRING_EMPTY(lan_interface)) {
            to_linux_interface(lan_interface, 0, &common_lan.dest_fwiface);
        } else {
            init_firewall_interface(&common_lan.dest_fwiface);
        }
        initialized_lan_intf = true;
    }
    FW_PROFILING_OUT();
}

/**
   @brief
   Allocate the memory and set default values of a PassthroughICMPv6EchoRequest
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the PassthroughICMPv6EchoRequest internal structure.
 */
icmpv6_passthrough_t* create_fw_icmpv6_passthrough(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    icmpv6_passthrough_t* icmpv6_passthrough = NULL;

    SAH_TRACEZ_NOTICE(ME, "PassthroughICMPv6EchoRequest[%s] is new", OBJECT_NAMED);

    icmpv6_passthrough = (icmpv6_passthrough_t*) calloc(1, sizeof(icmpv6_passthrough_t));
    when_null(icmpv6_passthrough, exit);

    common_init(&icmpv6_passthrough->common, object);

    //Get the lan interface
    create_common_lan();

    icmpv6_passthrough->status = FIREWALL_DISABLED;
    _set_status(icmpv6_passthrough->common.object, PASSTHROUGH_ICMPV6_STATUS, FIREWALL_ERROR);
    icmpv6_passthrough->target = TARGET_DROP;

exit:
    FW_PROFILING_OUT();
    return icmpv6_passthrough;
}

void clean_icmpv6_passthrough_common(void) {
    clean_firewall_interface(&common_lan.dest_fwiface);
    common_clean(&common_lan);
}
