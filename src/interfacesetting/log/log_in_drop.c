/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/log_in_drop.h"
#include "firewall.h" // for fw_get_chain
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "firewall"

#define LOG_DROPPED_CONN_STATUS    "LOGDroppedIncomingConnectionStatus"

/**
   @brief
   Delete/clean the LOGDroppedIncomingConnection internal structure.

   @param[in] log_in_drop LOGDroppedIncomingConnection internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_log_in_drop(log_in_drop_t** log_in_drop) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*log_in_drop, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete LOGDroppedIncomingConnection");

    amxc_llist_it_take(&(*log_in_drop)->it);

    folder_wrapper_clean(&(*log_in_drop)->folders_forward);
    common_clean(&(*log_in_drop)->common);

    free(*log_in_drop);
    *log_in_drop = NULL;
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Activate the LOG all denied inbound mode (Reject or Drop) that comes in the INPUT chain on the filter table

   @param[in] log_in_drop LOGDroppedIncomingConnection internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_log_in_drop_input(log_in_drop_t* log_in_drop, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_in_drop->common.object;
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? log_in_drop->common.folders.rule_folders.folder : log_in_drop->common.folders.rule_folders.folder6;
    firewall_interface_t* fwiface = &log_in_drop->common.dest_fwiface;
    bool res = false;
    const char* chain = ipv4 ? fw_get_chain("log_input", "INPUT_Last_Log") : fw_get_chain("log_input6", "INPUT6_Last_Log");
    (void) object;  // when tracing is disabled, variable is not used anymore

    when_false_status(log_get_enable(&log_in_drop->common.folders.log), out, res = true);

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "LOGDroppedIncomingConnection[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "LOGDroppedIncomingConnection[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_chain(r, chain);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_in_interface(r, fwiface->default_netdevname);
    fw_rule_set_table(r, TABLE_FILTER);
    set_log_rule_specific_settings(r, &(log_in_drop->common.folders.log));
    fw_folder_push_rule(folder, r);

    fw_folder_set_enabled(folder, true);
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

/**
   @brief
   Activate the LOG all denied inbound mode (Reject or Drop) that comes in the FORWARD chain on the filter table

   @param[in] log_in_drop LOGDroppedIncomingConnection internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_log_in_drop_forward(log_in_drop_t* log_in_drop, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_in_drop->common.object;
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? log_in_drop->folders_forward.folder : log_in_drop->folders_forward.folder6;
    firewall_interface_t* fwiface = &log_in_drop->common.dest_fwiface;
    bool res = false;
    const char* chain = ipv4 ? fw_get_chain("log_forward", "FORWARD_Last_Log") : fw_get_chain("log_forward6", "FORWARD6_Last_Log");
    (void) object;  // when tracing is disabled, variable is not used anymore

    when_false_status(log_get_enable(&log_in_drop->common.folders.log), out, res = true);

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "Inbound interface is an empty string.");

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "LOGDroppedIncomingConnection[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_chain(r, chain);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_in_interface(r, fwiface->default_netdevname);
    fw_rule_set_table(r, TABLE_FILTER);
    set_log_rule_specific_settings(r, &(log_in_drop->common.folders.log));
    fw_folder_push_rule(folder, r);

    fw_folder_set_enabled(folder, true);
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

static void unfold_fw_setting_log_in_drop(log_in_drop_t* log_in_drop) {
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(log_in_drop, out);

    folder_container_delete_rules(&log_in_drop->common.folders, IPvAll, true);
    folder_wrapper_delete_rules(&log_in_drop->folders_forward, IPvAll, true);

out:
    FW_PROFILING_OUT();
    return;
}

static bool deactivate_log_in_drop(log_in_drop_t* log_in_drop) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_in_drop->common.object;
    bool is_active = (log_in_drop->status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "LOGDroppedIncomingConnection[%s] LOGDroppedIncomingConnection is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "LOGDroppedIncomingConnection[%s] deactivate LOGDroppedIncomingConnection", OBJECT_NAMED);
    /* do not commit yet, wait for the activate/delete to commit it. */
    unfold_fw_setting_log_in_drop(log_in_drop);
    log_in_drop->status = FIREWALL_DISABLED;
out:
    FW_PROFILING_OUT();
    return true;
}

static bool activate_log_in_drop(log_in_drop_t* log_in_drop) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = log_in_drop->common.object;
    bool is_active = (log_in_drop->status == FIREWALL_ENABLED);
    bool res = false;
    bool do_commit = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(is_active) {
        res = true;
        SAH_TRACEZ_NOTICE(ME, "LOGDroppedIncomingConnection[%s] LOGDroppedIncomingConnection already active", OBJECT_NAMED);
        goto out;
    }

    do_commit |= ifsetting_activate_log_in_drop_input(log_in_drop, true);
    do_commit |= ifsetting_activate_log_in_drop_input(log_in_drop, false);
    do_commit |= ifsetting_activate_log_in_drop_forward(log_in_drop, true);
    do_commit |= ifsetting_activate_log_in_drop_forward(log_in_drop, false);

    if(do_commit) {
        if(fw_commit(fw_rule_callback) != 0) {
            deactivate_log_in_drop(log_in_drop);
            goto out;
        } else {
            log_in_drop->status = FIREWALL_ENABLED;
            if(!log_get_enable(&log_in_drop->common.folders.log)) {
                _set_status(log_in_drop->common.object, LOG_DROPPED_CONN_STATUS, FIREWALL_DISABLED);
            } else {
                _set_status(log_in_drop->common.object, LOG_DROPPED_CONN_STATUS, FIREWALL_ENABLED);
            }
        }
    }

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}


bool log_in_drop_implement_changes(log_in_drop_t* log_in_drop) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object;     // when tracing is disabled, variable is not used anymore

    when_null_trace(log_in_drop, exit, ERROR, "LOGDroppedIncomingConnection internal structure is NULL.");

    object = log_in_drop->common.object;

    if(log_in_drop->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "LOGDroppedIncomingConnection[%s] about to be deleted", OBJECT_NAMED);
        deactivate_log_in_drop(log_in_drop);
        delete_fw_log_in_drop(&log_in_drop);
    } else if(log_in_drop->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "LOGDroppedIncomingConnection[%s] about to be add", OBJECT_NAMED);
        if(!activate_log_in_drop(log_in_drop)) {
            _set_status(object, LOG_DROPPED_CONN_STATUS, FIREWALL_ERROR);
            SAH_TRACEZ_ERROR(ME, "LOGDroppedIncomingConnection[%s] activation failed", OBJECT_NAMED);
        }
        log_in_drop->common.flags = 0;
    } else if(log_in_drop->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "LOGDroppedIncomingConnection[%s] deactivate, activate", OBJECT_NAMED);
        deactivate_log_in_drop(log_in_drop);
        if(!activate_log_in_drop(log_in_drop)) {
            _set_status(object, LOG_DROPPED_CONN_STATUS, FIREWALL_ERROR);
            SAH_TRACEZ_ERROR(ME, "LOGDroppedIncomingConnection[%s] activation failed", OBJECT_NAMED);
        }
        log_in_drop->common.flags = 0;
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a LOGDroppedIncomingConnection
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the LOGDroppedIncomingConnection internal structure.
 */
log_in_drop_t* create_log_in_drop(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    log_in_drop_t* log_in_drop = NULL;

    SAH_TRACEZ_NOTICE(ME, "LOGDroppedIncomingConnection[%s] is new", OBJECT_NAMED);

    log_in_drop = (log_in_drop_t*) calloc(1, sizeof(log_in_drop_t));
    when_null(log_in_drop, exit);

    common_init(&log_in_drop->common, object);
    folder_wrapper_init(&log_in_drop->folders_forward);

    log_in_drop->status = FIREWALL_DISABLED;
    _set_status(log_in_drop->common.object, LOG_DROPPED_CONN_STATUS, FIREWALL_ERROR);
exit:
    FW_PROFILING_OUT();
    return log_in_drop;
}