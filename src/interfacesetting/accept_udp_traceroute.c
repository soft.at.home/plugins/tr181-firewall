/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/accept_udp_traceroute.h"
#include "firewall.h" // for fw_get_chain
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "firewall"

#define ACCEPT_UDP_TRACEROUTE_STATUS       "AcceptUDPTracerouteStatus"
#define PROTOCOL_UDP 17

/**
   @brief
   Delete/clean the AcceptUDPTraceroute internal structure.

   @param[in] accept_udp_traceroute AcceptUDPTraceroute internal structure.

   @return
   True if success, false if failed.
 */
static bool delete_fw_accept_udp_traceroute(accept_udp_traceroute_t** accept_udp_traceroute) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*accept_udp_traceroute, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete AcceptUDPTraceroute");

    amxc_llist_it_take(&(*accept_udp_traceroute)->it);

    common_clean(&(*accept_udp_traceroute)->common);

    FREE(*accept_udp_traceroute);
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Activate the accept_udp_traceroute mode (Accept or Drop)

   @param[in] accept_udp_traceroute AcceptUDPTraceroute internal structure.
   @param[in] ipv4 Whether is IPv4 or not.

   @return
   True if success, false if failed.
 */
static bool ifsetting_activate_accept_udp_traceroute(accept_udp_traceroute_t* accept_udp_traceroute, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = accept_udp_traceroute->common.object;
    fw_rule_t* r = NULL;
    fw_folder_t* folder = ipv4 ? accept_udp_traceroute->common.folders.rule_folders.folder : accept_udp_traceroute->common.folders.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? accept_udp_traceroute->common.folders.log_folders.folder : accept_udp_traceroute->common.folders.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder};
    size_t loop_start = log_get_enable(&accept_udp_traceroute->common.folders.log) ? 0 : 1;
#else //FIREWALL_LOGS
    fw_folder_t* log_folder = NULL;
    fw_folder_t* folders[] = {folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    firewall_interface_t* fwiface = &accept_udp_traceroute->common.dest_fwiface;
    bool res = false;
    const char* chain = ipv4 ? fw_get_chain("interfacesetting", "INPUT_InterfaceSettings") : fw_get_chain("interfacesetting6", "INPUT6_InterfaceSettings");
    (void) object; // when tracing is disabled, variable is not used anymore

    when_str_empty_trace(fwiface->default_netdevname, out, INFO, "AcceptUDPTraceroute[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "AcceptUDPTraceroute[%s] failed to fetch rule", OBJECT_NAMED);

        fw_rule_set_chain(r, chain);
        fw_rule_set_ipv4(r, ipv4);
        fw_rule_set_in_interface(r, fwiface->default_netdevname);
        if(folders[i] != log_folder) {
            if(accept_udp_traceroute->target == TARGET_ACCEPT) {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT);
            } else {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP);
            }
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        else {
            set_log_rule_specific_settings(r, &accept_udp_traceroute->common.folders.log);
        }
#endif //FIREWALL_LOGS

        fw_rule_set_table(r, TABLE_FILTER);
        fw_rule_set_protocol(r, PROTOCOL_UDP);
        fw_rule_set_destination_port(r, 33434);
        fw_rule_set_destination_port_range_max(r, 33523);
        fw_folder_push_rule(folders[i], r);

        fw_folder_set_enabled(folders[i], true);
    }
    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}

static bool deactivate_accept_udp_traceroute(accept_udp_traceroute_t* accept_udp_traceroute) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = accept_udp_traceroute->common.object;
    bool is_active = (accept_udp_traceroute->status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "AcceptUDPTraceroute[%s] AcceptUDPTraceroute is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "AcceptUDPTraceroute[%s] deactivate AcceptUDPTraceroute", OBJECT_NAMED);
    folder_container_delete_rules(&accept_udp_traceroute->common.folders, IPvAll, true);
    _set_status(accept_udp_traceroute->common.object, ACCEPT_UDP_TRACEROUTE_STATUS, FIREWALL_ERROR);
    accept_udp_traceroute->status = FIREWALL_DISABLED;
out:
    FW_PROFILING_OUT();
    return true;
}

static bool activate_accept_udp_traceroute(accept_udp_traceroute_t* accept_udp_traceroute) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = accept_udp_traceroute->common.object;
    bool is_active = (accept_udp_traceroute->status == FIREWALL_ENABLED);
    bool res = false;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(is_active) {
        res = true;
        SAH_TRACEZ_NOTICE(ME, "AcceptUDPTraceroute[%s] AcceptUDPTraceroute already active", OBJECT_NAMED);
        goto out;
    }

    res = ifsetting_activate_accept_udp_traceroute(accept_udp_traceroute, true);
    when_false(res, out);

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_ERROR(ME, "AcceptUDPTraceroute[%s] fw_commit failed", OBJECT_NAMED);
        folder_container_delete_rules(&accept_udp_traceroute->common.folders, IPvAll, true);
        goto out;
    }

    res = ifsetting_activate_accept_udp_traceroute(accept_udp_traceroute, false);
    when_false(res, out);

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_ERROR(ME, "AcceptUDPTraceroute[%s] fw_commit failed", OBJECT_NAMED);
        folder_container_delete_rules(&accept_udp_traceroute->common.folders, IPvAll, true);
        goto out;
    }

    if(accept_udp_traceroute->target == TARGET_DROP) {
        _set_status(accept_udp_traceroute->common.object, ACCEPT_UDP_TRACEROUTE_STATUS, FIREWALL_DISABLED);
    } else {
        _set_status(accept_udp_traceroute->common.object, ACCEPT_UDP_TRACEROUTE_STATUS, FIREWALL_ENABLED);
    }
    accept_udp_traceroute->status = FIREWALL_ENABLED;

    res = true;
out:
    FW_PROFILING_OUT();
    return res;
}


bool accept_udp_traceroute_implement_changes(accept_udp_traceroute_t* accept_udp_traceroute) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = NULL;
    (void) object;     // when tracing is disabled, variable is not used anymore

    when_null_trace(accept_udp_traceroute, exit, ERROR, "AcceptUDPTraceroute internal structure is NULL.");

    object = accept_udp_traceroute->common.object;

    if(accept_udp_traceroute->common.flags & FW_DELETED) {
        SAH_TRACEZ_NOTICE(ME, "AcceptUDPTraceroute[%s] about to be deleted", OBJECT_NAMED);
        deactivate_accept_udp_traceroute(accept_udp_traceroute);
        delete_fw_accept_udp_traceroute(&accept_udp_traceroute);
    } else if(accept_udp_traceroute->common.flags & FW_NEW) {
        SAH_TRACEZ_NOTICE(ME, "AcceptUDPTraceroute[%s] about to be add", OBJECT_NAMED);
        activate_accept_udp_traceroute(accept_udp_traceroute);
        accept_udp_traceroute->common.flags = 0;
    } else if(accept_udp_traceroute->common.flags & FW_MODIFIED) {
        SAH_TRACEZ_NOTICE(ME, "AcceptUDPTraceroute[%s] deactivate, activate", OBJECT_NAMED);
        deactivate_accept_udp_traceroute(accept_udp_traceroute);
        activate_accept_udp_traceroute(accept_udp_traceroute);
        accept_udp_traceroute->common.flags = 0;
    }
    fw_apply();
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a AcceptUDPTraceroute
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the AcceptUDPTraceroute internal structure.
 */
accept_udp_traceroute_t* create_fw_accept_udp_traceroute(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    accept_udp_traceroute_t* accept_udp_traceroute = NULL;

    SAH_TRACEZ_NOTICE(ME, "AcceptUDPTraceroute[%s] is new", OBJECT_NAMED);

    accept_udp_traceroute = (accept_udp_traceroute_t*) calloc(1, sizeof(accept_udp_traceroute_t));
    when_null(accept_udp_traceroute, exit);

    common_init(&accept_udp_traceroute->common, object);

    accept_udp_traceroute->status = FIREWALL_DISABLED;
    _set_status(accept_udp_traceroute->common.object, ACCEPT_UDP_TRACEROUTE_STATUS, FIREWALL_ERROR);
    accept_udp_traceroute->target = TARGET_DROP;
exit:
    FW_PROFILING_OUT();
    return accept_udp_traceroute;
}