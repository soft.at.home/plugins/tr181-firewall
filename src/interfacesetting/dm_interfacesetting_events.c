/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/dm_spoofing_protection.h"
#include "interfacesetting/spoofing_protection.h"
#include "interfacesetting/dm_icmpv6_passthrough.h"
#include "interfacesetting/icmpv6_passthrough.h"
#include "interfacesetting/dm_icmp_echo_request.h"
#include "interfacesetting/icmp_echo_request.h"
#include "interfacesetting/dm_stealth_mode.h"
#include "interfacesetting/stealth_mode.h"
#include "interfacesetting/dm_accept_udp_traceroute.h"
#include "interfacesetting/accept_udp_traceroute.h"
#include "interfacesetting/dm_log_in_drop.h"
#include "interfacesetting/log_in_drop.h"
#include "interfacesetting/dm_log_out_drop.h"
#include "interfacesetting/log_out_drop.h"
#include "interfacesetting/dm_log_out_accept.h"
#include "interfacesetting/log_out_accept.h"
#include "service/dm_service.h"
#include "protocols.h"
#include "logs/logs.h"
#include "init_cleanup.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "firewall"

/**
   @brief
   Get the InterfaceSetting object from the Data Model.

   @param[in] dm Pointer to datamodel.

   @return
   Object or NULL if the InterfaceSetting is not available.
 */
amxd_object_t* fw_ifsetting_template(amxd_dm_t* dm) {
    amxd_object_t* templ = NULL;
    templ = amxd_dm_findf(dm, "Firewall.%sInterfaceSetting.", fw_get_vendor_prefix());
    if(templ == NULL) {
        templ = amxd_dm_findf(dm, "Firewall.InterfaceSetting.");
    }
    return templ;
}

static void netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    amxd_object_t* object = (amxd_object_t*) userdata;
    ifsetting_t* ifsetting = NULL;
    const char* netdevname = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);
    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);

    if((netdevname = GET_CHAR(result, NULL)) == NULL) {
        netdevname = "";
    }

    SAH_TRACEZ_INFO(ME, "%s -> %s", ifsetting->fwiface.default_netdevname, netdevname);

    when_true(strncmp(ifsetting->fwiface.default_netdevname, netdevname, INTERFACE_LEN - 1) == 0, exit);
    snprintf(ifsetting->fwiface.default_netdevname, INTERFACE_LEN, "%s", netdevname);

    if(ifsetting->fwiface.query != NULL) {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        log_update(&ifsetting->isolate->folders.log,
                   ENABLE_LOG,
                   log_get_dm_controller_param(object, NULL, NULL),
                   !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                   NULL);
#endif //FIREWALL_LOGS

        isolate_iface_changed(ifsetting);
        // maybe other features can also use this query
    } // else called from netmodel_openQuery

exit:
    FW_PROFILING_OUT();
    return;
}

/**
   @brief
   Initialise the InterfaceSetting internal structure.

   @param[in] instance Object with the InterfaceSetting's instance.

   @return
   0 if success, 1 if failed.
 */
static int fw_ifsetting_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;
    int ret_value = 1;
    const char* iface = NULL;

    when_null(object, exit);

    ifsetting = (ifsetting_t*) object->priv;
    if(ifsetting == NULL) {
        ifsetting = create_fw_ifsetting(object);
        when_null(ifsetting, exit);
        object->priv = ifsetting;
    }

    ifsetting->spoofing = NULL;
    ifsetting->icmpv6_passthrough = NULL;
    ifsetting->icmp_echo_request = NULL;
    ifsetting->stealth_mode = NULL;
    ifsetting->accept_udp_traceroute = NULL;
    ifsetting->isolate = NULL;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    ifsetting->log_in_drop = NULL;
    ifsetting->log_out_drop = NULL;
    ifsetting->log_out_accept = NULL;
#endif //FIREWALL_LOGS

    iface = object_da_string(object, "Interface");
    if(!STRING_EMPTY(iface)) {
        // assuming netdevname for ipv4 and ipv6 is the same (only using 1 query)
        ifsetting->fwiface.query = netmodel_openQuery_getFirstParameter(iface, FW_QUERY_SUBSCR, "NetDevName", "ipv4", netmodel_traverse_down, netdevname_changed, object);
        when_null_trace(ifsetting->fwiface.query, exit, ERROR, "%s failed to open query", ifsetting->name);
    }

    ret_value = 0;
exit:
    FW_PROFILING_OUT();
    return ret_value;
}

static int _dm_ifsetting_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;

    fw_ifsetting_init(object);
    fw_spoofing_init(object);
    fw_icmpv6_passthrough_init(object);
    fw_icmp_echo_request_init(object);
    fw_stealth_mode_init(object);
    fw_accept_udp_traceroute_init(object);
    isolate_iface_init(object);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_in_drop_init(object);
    log_out_drop_init(object);
    log_out_accept_init(object);
#endif //FIREWALL_LOGS

    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);

    if(ifsetting->spoofing != NULL) {
        spoofing_implement_changes(ifsetting->spoofing, true, true);
    }

    if(ifsetting->icmpv6_passthrough != NULL) {
        icmpv6_passthrough_implement_changes(ifsetting->icmpv6_passthrough);
    }

    if(ifsetting->icmp_echo_request != NULL) {
        icmp_echo_request_implement_changes(ifsetting->icmp_echo_request);
    }

    if(ifsetting->stealth_mode != NULL) {
        stealth_mode_implement_changes(ifsetting->stealth_mode);
    }

    if(ifsetting->accept_udp_traceroute != NULL) {
        accept_udp_traceroute_implement_changes(ifsetting->accept_udp_traceroute);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    if(ifsetting->log_in_drop != NULL) {
        log_in_drop_implement_changes(ifsetting->log_in_drop);
    }

    if(ifsetting->log_out_drop != NULL) {
        log_out_drop_implement_changes(ifsetting->log_out_drop);
    }

    if(ifsetting->log_out_accept != NULL) {
        log_out_accept_implement_changes(ifsetting->log_out_accept);
    }
#endif //FIREWALL_LOGS

exit:
    FW_PROFILING_OUT();
    return amxd_status_ok;
}

void dm_ifsetting_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_for_all(fw_ifsetting_template(fw_get_dm()), ".*.", _dm_ifsetting_init, NULL);
    FW_PROFILING_OUT();
}

void _ifsetting_added(const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ_settings = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* obj_ifsetting = amxd_object_get_instance(templ_settings, NULL, GET_UINT32(event_data, "index"));
    ifsetting_t* ifsetting = NULL;
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_INFO(ME, "Received event: %s", sig_name);

    when_null(obj_ifsetting, exit);

    fw_ifsetting_init(obj_ifsetting);
    fw_spoofing_init(obj_ifsetting);
    fw_icmpv6_passthrough_init(obj_ifsetting);
    fw_icmp_echo_request_init(obj_ifsetting);
    fw_stealth_mode_init(obj_ifsetting);
    fw_accept_udp_traceroute_init(obj_ifsetting);
    isolate_iface_init(obj_ifsetting);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_in_drop_init(obj_ifsetting);
    log_out_drop_init(obj_ifsetting);
    log_out_accept_init(obj_ifsetting);
#endif //FIREWALL_LOGS

    ifsetting = (ifsetting_t*) obj_ifsetting->priv;
    when_null(ifsetting, exit);

    if(ifsetting->spoofing != NULL) {
        spoofing_implement_changes(ifsetting->spoofing, true, true);
    }

    if(ifsetting->icmpv6_passthrough != NULL) {
        icmpv6_passthrough_implement_changes(ifsetting->icmpv6_passthrough);
    }

    if(ifsetting->icmp_echo_request != NULL) {
        icmp_echo_request_implement_changes(ifsetting->icmp_echo_request);
    }

    if(ifsetting->stealth_mode != NULL) {
        stealth_mode_implement_changes(ifsetting->stealth_mode);
    }

    if(ifsetting->accept_udp_traceroute != NULL) {
        accept_udp_traceroute_implement_changes(ifsetting->accept_udp_traceroute);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    if(ifsetting->log_in_drop != NULL) {
        log_in_drop_implement_changes(ifsetting->log_in_drop);
    }

    if(ifsetting->log_out_drop != NULL) {
        log_out_drop_implement_changes(ifsetting->log_out_drop);
    }

    if(ifsetting->log_out_accept != NULL) {
        log_out_accept_implement_changes(ifsetting->log_out_accept);
    }
#endif //FIREWALL_LOGS

exit:
    FW_PROFILING_OUT();
    return;
}

void ifsettings_cleanup(void) {
    clean_icmpv6_passthrough_common();
}
