/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/interfacesetting.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define ME "firewall"

/**
   @brief
   Delete/clean the InterfaceSetting internal structure.

   @param[in] ifsetting InterfaceSetting internal structure.

   @return
   True if success, false if failed.
 */
bool delete_fw_ifsetting(ifsetting_t** ifsetting) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*ifsetting, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete Interface Setting");

    amxc_llist_it_take(&(*ifsetting)->it);

    clean_firewall_interface(&(*ifsetting)->fwiface);
    FREE(*ifsetting);
exit:
    FW_PROFILING_OUT();
    return true;
}

/**
   @brief
   Allocate the memory and set default values of a InterfaceSetting
   internal structure.

   @param[in] obj_ifsetting Instance of InterfaceSetting.

   @return
   Pointer to the InterfaceSetting internal structure.
 */
ifsetting_t* create_fw_ifsetting(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    ifsetting_t* ifsetting = NULL;

    SAH_TRACEZ_NOTICE(ME, "IfSet[%s] is new", OBJECT_NAMED);

    ifsetting = (ifsetting_t*) calloc(1, sizeof(ifsetting_t));
    when_null(ifsetting, exit);

    ifsetting->object = object;
    ifsetting->name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
    init_firewall_interface(&ifsetting->fwiface);
exit:
    FW_PROFILING_OUT();
    return ifsetting;
}