/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/dm_spoofing_protection.h"
#include "interfacesetting/interfacesetting.h"
#include "interfacesetting/spoofing_protection.h"
#include "firewall.h" // for fw_get_dm
#include "logs/logs.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "firewall"

//This Delay can be changed in the future, probably after the Service re-work, or any other firewall improvement
//It was tried 1 second and 100 ms, and both caused the WanManager.Reset() to fail
#define DELAY_TO_IMPLEMENT_CHANGES 5000 //5 seconds to implement the accumulated changes in a batch.

static void fw_callback_delayed_implement_changes(UNUSED amxp_timer_t* timer, void* data) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    spoofing_protection_t* spoofing = (spoofing_protection_t*) data;
    if(!spoofing->initialized) {
        spoofing->common.flags = FW_NEW;
        spoofing->delayed_change_ipv4 = true;
        spoofing->delayed_change_ipv6 = true;
    } else {
        spoofing->common.flags = FW_MODIFIED;
    }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_update(&spoofing->common.folders.log,
               ENABLE_LOG,
               log_get_dm_controller_param(spoofing->common.object, NULL, "LogControllerSpoofingProtection"),
               !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
               NULL);
#endif //FIREWALL_LOGS
    spoofing_implement_changes(spoofing, spoofing->delayed_change_ipv4, spoofing->delayed_change_ipv6);
    spoofing->delayed_change_ipv4 = false;
    spoofing->delayed_change_ipv6 = false;
    amxp_timer_stop(spoofing->timer);
    FW_PROFILING_OUT();
}

static void fill_addresses_data_cb(const amxc_var_t* result, spoofing_protection_t* spoofing, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t* intf_var = NULL;
    amxc_var_t* new_intf_var = NULL;
    amxc_var_t* new_ip_version_var = NULL;
    const cstring_t ip_version = ipv4 ? "ipv4" : "ipv6";
    uint32_t new_num_of_addrs = 0;
    int cmp_result = 0;

    when_null_trace(spoofing, exit, ERROR, "Spoofing internal structure is empty");

    intf_var = ipv4 ? &(spoofing->addresses_v4) : &(spoofing->addresses_v6);

    amxc_var_new(&new_intf_var);
    amxc_var_set_type(new_intf_var, AMXC_VAR_ID_HTABLE);

    new_ip_version_var = amxc_var_add_key(amxc_htable_t, new_intf_var, ip_version, NULL);

    amxc_var_for_each(var, result) {
        const char* ipaddress = GET_CHAR(var, "Address");
        const char* family = GET_CHAR(var, "Family");
        amxc_var_t* new_ip_params = NULL;

        if(STRING_EMPTY(ipaddress) || STRING_EMPTY(family)) {
            continue;
        }
        if(strcmp(family, ip_version) == 0) {
            amxc_var_t* new_address_var = GET_ARG(new_ip_version_var, ipaddress);
            if(new_address_var == NULL) {
                new_ip_params = amxc_var_add_key(amxc_htable_t, new_ip_version_var, ipaddress, NULL);
                amxc_var_add_key(cstring_t, new_ip_params, "Address", ipaddress);
                amxc_var_add_key(cstring_t, new_ip_params, "Family", family);
                amxc_var_add_key(cstring_t, new_ip_params, "Scope", GET_CHAR(var, "Scope"));
                amxc_var_add_key(uint32_t, new_ip_params, "PrefixLen", GET_UINT32(var, "PrefixLen"));
                new_num_of_addrs++;
            }
        }
    }
    amxc_var_add_key(uint32_t, new_intf_var, "NumberOfAddress", new_num_of_addrs);

    amxc_var_compare(intf_var, new_intf_var, &cmp_result);

    if(cmp_result != 0) {
        amxc_var_move(intf_var, new_intf_var);
        if(spoofing->timer == NULL) {
            amxp_timer_new(&spoofing->timer, fw_callback_delayed_implement_changes, spoofing);
        }
        amxp_timer_start(spoofing->timer, DELAY_TO_IMPLEMENT_CHANGES);
        if(ipv4) {
            spoofing->delayed_change_ipv4 = true;
        } else {
            spoofing->delayed_change_ipv6 = true;
        }
        spoofing->common.flags = 0;
    }
exit:
    amxc_var_delete(&new_intf_var);
    FW_PROFILING_OUT();
    return;
}

static void fill_addresses_v4_data_cb(const char* sig_name UNUSED, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    spoofing_protection_t* spoofing = (spoofing_protection_t*) userdata;

    when_null_trace(spoofing, exit, ERROR, "Userdata is empty");

    fill_addresses_data_cb(result, spoofing, true);
exit:
    FW_PROFILING_OUT();
    return;
}

static void fill_addresses_v6_data_cb(const char* sig_name UNUSED, const amxc_var_t* result, void* userdata) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    spoofing_protection_t* spoofing = (spoofing_protection_t*) userdata;

    when_null_trace(spoofing, exit, ERROR, "Userdata is empty");

    fill_addresses_data_cb(result, spoofing, false);
exit:
    FW_PROFILING_OUT();
    return;
}

/**
   @brief
   Open the getAddrs query.

   @param[in] spoofing SpoofingProtection internal structure.
   @param[in] interface NetModel interface.
 */
static void query_addresses(spoofing_protection_t* spoofing, const char* interface, bool ipv4) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const cstring_t flags = ipv4 ? "ipv4" : "ipv6";
    netmodel_query_t** query = ipv4 ? &(spoofing->query_addresses_v4) : &(spoofing->query_addresses_v6);
    netmodel_callback_t cb = ipv4 ? fill_addresses_v4_data_cb : fill_addresses_v6_data_cb;
    _netmodel_closeQuery(query);
    *query = netmodel_openQuery_getAddrs(interface, FW_QUERY_SUBSCR, flags, netmodel_traverse_down, cb, spoofing);
    FW_PROFILING_OUT();
}

/**
   @brief
   Fetch the list of IP address excluding the interface.

   @param[in] spoofing SpoofingProtection internal structure.
   @param[in] interface NetModel interface.
 */
static void fetch_source_addresses(spoofing_protection_t* spoofing, const char* interface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_set_type(&spoofing->addresses_v4, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&spoofing->addresses_v6, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &spoofing->addresses_v4, "NumberOfAddress", 0);
    amxc_var_add_key(uint32_t, &spoofing->addresses_v6, "NumberOfAddress", 0);
    query_addresses(spoofing, interface, true);
    query_addresses(spoofing, interface, false);
    FW_PROFILING_OUT();
}

/**
   @brief
   Translate the Netmodel interface and add to the common structure.

   @param[in] spoofing SpoofingProtection internal structure.
   @param[in] interface NetModel interface.
 */
static void set_interface(spoofing_protection_t* spoofing, const char* interface) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    firewall_interface_t* fwiface = &spoofing->common.dest_fwiface;
    if(!STRING_EMPTY(interface)) {
        to_linux_interface(interface, 0, fwiface);
    } else {
        init_firewall_interface(fwiface);
    }
    fetch_source_addresses(spoofing, interface);
    FW_PROFILING_OUT();
}

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    common_t* common = (common_t*) fwiface->query_priv;
    when_null_trace(common, exit, ERROR, "Common object is NULL.");
    spoofing_protection_t* spoofing = (spoofing_protection_t*) common->priv_data;
    when_null_trace(common, exit, ERROR, "SpoofingProtection object is NULL.");
    amxp_timer_start(spoofing->timer, DELAY_TO_IMPLEMENT_CHANGES);
    spoofing->delayed_change_ipv4 = true;
    spoofing->delayed_change_ipv6 = true;
    spoofing->common.flags = 0;
exit:
    FW_PROFILING_OUT();
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    UNUSED const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    ifsetting_t* ifsetting = NULL;
    spoofing_protection_t* spoofing = NULL;
    bool update = false;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    ifsetting = (ifsetting_t*) object->priv;
    when_null_trace(ifsetting, exit, ERROR, "IterfaceSetting[%s] is NULL", OBJECT_NAMED);

    spoofing = ifsetting->spoofing;
    when_null(spoofing, exit);

    update = log_update(log,
                        ENABLE_LOG,
                        log_get_dm_controller_param(object, NULL, "LogControllerSpoofingProtection"),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        data);
    if(update) {
        spoofing->common.flags = FW_MODIFIED;
        spoofing_implement_changes(spoofing, true, true);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

/**
   @brief
   Initialise the SpoofingProtection internal structure.

   @param[in] instance Object with the InterfaceSetting's instance.

   @return
   0 if success, 1 if failed.
 */
int fw_spoofing_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const cstring_t spoofing_protection = NULL;
    ifsetting_t* ifsetting = NULL;
    spoofing_protection_t* spoofing = NULL;
    int ret_value = 1;

    when_null(object, exit);
    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);

    spoofing = ifsetting->spoofing;
    if(spoofing == NULL) {
        spoofing = create_fw_spoofing(object);
        when_null(spoofing, exit);
        ifsetting->spoofing = spoofing;
    }

    spoofing->common.dest_fwiface.query_priv = &spoofing->common;
    spoofing->common.dest_fwiface.query_result_changed = query_result_changed;
    spoofing->common.priv_data = spoofing;

    spoofing_protection = object_da_string(object, "SpoofingProtection"); // won't return NULL

    if(strcmp(spoofing_protection, "ipv4") == 0) {
        spoofing->target = TARGET_DROP;
        spoofing->target6 = TARGET_ACCEPT;
    } else if(strcmp(spoofing_protection, "ipv6") == 0) {
        spoofing->target = TARGET_ACCEPT;
        spoofing->target6 = TARGET_DROP;
    } else if(strcmp(spoofing_protection, "both") == 0) {
        spoofing->target = TARGET_DROP;
        spoofing->target6 = TARGET_DROP;
    } else {
        spoofing->target = TARGET_ACCEPT;
        spoofing->target6 = TARGET_ACCEPT;
        if(strcmp(spoofing_protection, "none") != 0) {
            SAH_TRACEZ_ERROR(ME, "Unknown SpoofingProtection '%s', using IPv4:%d IPv6:%d", spoofing_protection, spoofing->target, spoofing->target6);
        }
    }

    set_interface(spoofing, object_da_string(object, "Interface"));

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_init(object, &spoofing->common.folders.log, &spoofing->common.dest_fwiface, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             spoofing->target == TARGET_DROP || spoofing->target6 == TARGET_DROP, log_get_dm_controller_param(object, NULL, "LogControllerSpoofingProtection"), TARGET_DROP, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

    spoofing->common.flags = FW_NEW;

    ret_value = 0;
exit:
    FW_PROFILING_OUT();
    return ret_value;
}

/**
   @brief
   Set the flags to "delete" and update the SpoofingProtection.

   @param[in] ifsetting InterfaceSetting's struct

   @return
   0 if success, 1 if failed.
 */
int fw_spoofing_cleanup(ifsetting_t* ifsetting) {
    spoofing_protection_t* spoofing = NULL;
    bool res;
    int ret_value = 1;

    when_null(ifsetting, exit);

    spoofing = ifsetting->spoofing;
    if(spoofing != NULL) {
        spoofing->common.flags = FW_DELETED;
        res = spoofing_implement_changes(spoofing, true, true);
        when_false(res, exit);
    }

    ret_value = 0;
exit:
    return ret_value;
}

void _spoofing_changed(const char* const sig_name,
                       const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    ifsetting_t* ifsetting = NULL;
    spoofing_protection_t* spoofing = NULL;
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* obj_setting = amxd_dm_signal_get_object(dm, event_data);
    amxc_var_t* var = NULL;
    const cstring_t spoofing_protection = NULL;
    const cstring_t old_spoofing_protection = NULL;
    bool change_ipv4 = true;
    bool change_ipv6 = true;
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null(dm, exit);
    when_null(obj_setting, exit);
    ifsetting = (ifsetting_t*) obj_setting->priv;
    when_null(ifsetting, exit);
    spoofing = ifsetting->spoofing;
    when_null(spoofing, exit);

    spoofing->common.flags = FW_MODIFIED;

    var = GETP_ARG(event_data, "parameters.SpoofingProtection");
    if(var != NULL) {
        spoofing_protection = GET_CHAR(var, "to");
        old_spoofing_protection = GET_CHAR(var, "from");

        if(strcmp(spoofing_protection, "ipv4") == 0) {
            spoofing->target = TARGET_DROP;
            spoofing->target6 = TARGET_ACCEPT;
            if(strcmp(old_spoofing_protection, "both") == 0) {
                change_ipv4 = false;
            } else if(strcmp(old_spoofing_protection, "none") == 0) {
                change_ipv6 = false;
            }
        } else if(strcmp(spoofing_protection, "ipv6") == 0) {
            spoofing->target = TARGET_ACCEPT;
            spoofing->target6 = TARGET_DROP;
            if(strcmp(old_spoofing_protection, "none") == 0) {
                change_ipv4 = false;
            } else if(strcmp(old_spoofing_protection, "both") == 0) {
                change_ipv6 = false;
            }
        } else if(strcmp(spoofing_protection, "both") == 0) {
            spoofing->target = TARGET_DROP;
            spoofing->target6 = TARGET_DROP;
            if(strcmp(old_spoofing_protection, "ipv4") == 0) {
                change_ipv4 = false;
            } else if(strcmp(old_spoofing_protection, "ipv6") == 0) {
                change_ipv6 = false;
            }
        } else {
            spoofing->target = TARGET_ACCEPT;
            spoofing->target6 = TARGET_ACCEPT;
            if(strcmp(old_spoofing_protection, "ipv4") == 0) {
                change_ipv6 = false;
            } else if(strcmp(old_spoofing_protection, "ipv6") == 0) {
                change_ipv4 = false;
            }
        }
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_update(&spoofing->common.folders.log,
               ENABLE_LOG,
               log_get_dm_controller_param(obj_setting, event_data, "LogControllerSpoofingProtection"),
               !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
               event_data);
#endif //FIREWALL_LOGS
    spoofing_implement_changes(spoofing, change_ipv4, change_ipv6);
exit:
    FW_PROFILING_OUT();
    return;
}
