/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "interfacesetting/dm_icmp_echo_request.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "firewall"

/**
   @brief
   Initialise the ICMPvxEchoRequest internal structure.

   @param[in] instance Object with the InterfaceSetting's instance.

   @return
   0 if success, 1 if failed.
 */
int fw_icmp_echo_request_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const cstring_t accept_icmp_req = NULL;
    ifsetting_t* ifsetting = NULL;
    icmp_echo_request_t* icmp_echo_request = NULL;
    int ret_value = 1;

    when_null(object, exit);
    ifsetting = (ifsetting_t*) object->priv;
    when_null(ifsetting, exit);

    icmp_echo_request = ifsetting->icmp_echo_request;
    if(icmp_echo_request == NULL) {
        icmp_echo_request = create_fw_icmp_echo_request(object);
        when_null(icmp_echo_request, exit);
        ifsetting->icmp_echo_request = icmp_echo_request;
    }

    accept_icmp_req = object_da_string(object, "AcceptICMPEchoRequest");
    when_null_trace(accept_icmp_req, exit, ERROR, "Missing AcceptICMPEchoRequest");
    if(strcmp(accept_icmp_req, "allow_ipv4") == 0) {
        icmp_echo_request->accept_icmp = accept_icmp_ipv4;
    } else if(strcmp(accept_icmp_req, "allow_ipv6") == 0) {
        icmp_echo_request->accept_icmp = accept_icmp_ipv6;
    } else if(strcmp(accept_icmp_req, "allow_both") == 0) {
        icmp_echo_request->accept_icmp = accept_icmp_both;
    } else {
        icmp_echo_request->accept_icmp = accept_icmp_disable;
    }

    ret_value = 0;
exit:
    FW_PROFILING_OUT();
    return ret_value;
}

/**
   @brief
   Set the flags to "delete" and update the ICMPvxEchoRequest.

   @param[in] ifsetting InterfaceSetting's struct

   @return
   0 if success, 1 if failed.
 */
int fw_icmp_echo_request_cleanup(ifsetting_t* ifsetting) {
    icmp_echo_request_t* icmp_echo_request = NULL;
    bool res;
    int ret_value = 1;

    when_null(ifsetting, exit);

    icmp_echo_request = ifsetting->icmp_echo_request;
    if(icmp_echo_request != NULL) {
        icmp_echo_request->common.flags = FW_DELETED;
        res = icmp_echo_request_implement_changes(icmp_echo_request);
        when_false(res, exit);
    }

    ret_value = 0;
exit:
    return ret_value;
}

void _icmp_echo_request_changed(const char* const sig_name,
                                const amxc_var_t* const event_data,
                                UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    ifsetting_t* ifsetting = NULL;
    icmp_echo_request_t* icmp_echo_request = NULL;
    amxd_dm_t* dm = fw_get_dm();
    amxd_object_t* obj_setting = amxd_dm_signal_get_object(dm, event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    const cstring_t accept_icmp_req = NULL;
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null(dm, exit);
    when_null(obj_setting, exit);
    ifsetting = (ifsetting_t*) obj_setting->priv;
    when_null(ifsetting, exit);
    icmp_echo_request = ifsetting->icmp_echo_request;
    when_null(icmp_echo_request, exit);

    icmp_echo_request->common.flags = FW_MODIFIED;

    var = GET_ARG(params, "AcceptICMPEchoRequest");
    if(var != NULL) {
        accept_icmp_req = GET_CHAR(var, "to");
        when_null_trace(accept_icmp_req, exit, ERROR, "Missing AcceptICMPEchoRequest");
        if(strcmp(accept_icmp_req, "allow_ipv4") == 0) {
            icmp_echo_request->accept_icmp = accept_icmp_ipv4;
        } else if(strcmp(accept_icmp_req, "allow_ipv6") == 0) {
            icmp_echo_request->accept_icmp = accept_icmp_ipv6;
        } else if(strcmp(accept_icmp_req, "allow_both") == 0) {
            icmp_echo_request->accept_icmp = accept_icmp_both;
        } else {
            icmp_echo_request->accept_icmp = accept_icmp_disable;
        }
    }

    icmp_echo_request_implement_changes(icmp_echo_request);
exit:
    FW_PROFILING_OUT();
    return;
}
