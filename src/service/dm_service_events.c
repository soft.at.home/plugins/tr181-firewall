/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "service/dm_service.h"
#include "service/service.h"
#include "fwrule_helpers.h"
#include "ports.h"
#include "protocols.h"
#include "logs/logs.h"
#include "init_cleanup.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>

#define ME "service"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    service_t* service = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    service = (service_t*) object->priv;
    when_null_trace(service, exit, ERROR, "Service[%s] is NULL", OBJECT_NAMED);

    if(update_service_logs(NULL, object, NULL, data)) {
        service->common.flags = FW_MODIFIED;
        services_implement_changes();
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

static void set_interface(service_t* service, const char* interface, ipversion_t ipversion) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    firewall_interface_t* fwiface = &service->common.dest_fwiface;
    if(!STRING_EMPTY(interface)) {
        to_linux_interface(interface, ipversion, fwiface);
    } else {
        init_firewall_interface(fwiface);
    }
    FW_PROFILING_OUT();
}

static bool service_init(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    service_t* service = NULL;
    const char* protocol = NULL;
    const char* ports = NULL;
    const char* prefix = NULL;
    ipversion_t ipversion = IPvUnknown;
    bool persistent = amxd_object_get_value(bool, object, "Persistent", NULL);

    when_null_trace(object, exit, ERROR, "Object is NULL"); // in case event is handled after object is removed?
    when_false(object->priv == NULL, exit);

    service = create_firewall_service(object);
    when_null(service, exit);

    if(!persistent) {
        // Dynamically added services (by other plugins) must not be persistent. Services loaded from ODL must be persistent.
        amxd_object_set_attr(object, amxd_oattr_persistent, false);
    }

    service->common.enable = amxd_object_get_value(bool, object, "Enable", NULL);

    protocol = object_da_string(object, "Protocol");
    if(!STRING_EMPTY(protocol)) {
        bool res = protocol_from_string(&service->common.protocols, protocol);
        if(!res) {
            SAH_TRACEZ_WARNING(ME, "Service[%s] parsing protocols failed[%s]", OBJECT_NAMED, protocol);
        }
    }

    ports = object_da_string(object, "DestPort");
    if(!STRING_EMPTY(ports)) {
        port_range_from_string(&service->ports, ports);
    }

    prefix = object_da_string(object, "SourcePrefixes");
    ipversion = object_get_ipversion(object);
    if(!address_from_string(prefix, &service->source_prefix)) {
        SAH_TRACEZ_WARNING(ME, "Service[%s] parsing source prefix failed[%s]", OBJECT_NAMED, prefix);
    }

    service->target = string_to_target(object_da_string(object, "Action"));

    set_interface(service, object_da_string(object, "Interface"), ipversion);

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_init(object, &service->common.folders.log, &service->common.dest_fwiface, !EXCLUDE_SRC_INTERFACE, NULL, !EXCLUDE_DST_INTERFACE, !USE_LOG_CONTROLLER_INTERFACE,
             log_get_dm_enable_param(object, NULL, NULL), log_get_dm_controller_param(object, NULL, NULL), service->target, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

exit:
    FW_PROFILING_OUT();
    return true;
}

static int dm_service_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    service_init(object);
    return amxd_status_ok;
}

void dm_services_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* firewall = amxd_dm_findf(fw_get_dm(), "Firewall.");
    amxd_object_for_all(firewall, ".Service.*.", dm_service_init, NULL);
    services_implement_changes();
    FW_PROFILING_OUT();
}

void _service_added(UNUSED const char* const sig_name,
                    const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));

    when_null(object, exit);
    service_init(object);
    services_implement_changes();
exit:
    FW_PROFILING_OUT();
    return;
}

void _service_changed(const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    service_t* service = NULL;
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    (void) sig_name; // when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);
    service = (service_t*) object->priv;

    service->common.flags = FW_MODIFIED;

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        bool enable = GET_BOOL(var, "to");
        SAH_TRACEZ_INFO(ME, "Service[%s] configure enable[%s]", OBJECT_NAMED, enable ? "true" : "false");
        service->common.enable = enable;
    }

    var = GET_ARG(params, "DestPort");
    if(var != NULL) {
        const char* ports = GET_CHAR(var, "to");
        port_range_clean(&service->ports);
        if(ports != NULL) {
            SAH_TRACEZ_INFO(ME, "Service[%s] configure destination port[%s]", OBJECT_NAMED, ports);
            port_range_from_string(&service->ports, ports);
        }
    }

    var = GET_ARG(params, "Protocol");
    if(var != NULL) {
        const char* protocol = GET_CHAR(var, "to");
        if(protocol != NULL) {
            bool res = protocol_from_string(&service->common.protocols, protocol);
            SAH_TRACEZ_INFO(ME, "Service[%s] configure protocol[%s]", OBJECT_NAMED, protocol);
            if(res == false) {
                SAH_TRACEZ_WARNING(ME, "Service[%s] parsing protocols failed[%s]", OBJECT_NAMED, protocol);
            }
        }
    }

    var = GET_ARG(params, "SourcePrefixes");
    if(var != NULL) {
        const char* prefix = GET_CHAR(var, "to");
        if(!address_from_string(prefix, &service->source_prefix)) {
            SAH_TRACEZ_WARNING(ME, "Service[%s] parsing source prefix failed[%s]", OBJECT_NAMED, prefix);
        }
    }

    var = GET_ARG(params, "Action");
    if(var != NULL) {
        const char* action = GET_CHAR(var, "to");
        service->target = (action != NULL) ? string_to_target(action) : TARGET_ACCEPT;
    }

    var = GET_ARG(params, "Interface");
    var2 = GET_ARG(params, "IPVersion");
    if((var != NULL) || (var2 != NULL)) {
        const char* interface = (var != NULL) ? GET_CHAR(var, "to") : object_da_string(object, "Interface");
        ipversion_t ipversion = (var2 != NULL) ? int_to_ipversion(GET_INT32(var2, "to")) : object_get_ipversion(object);
        set_interface(service, interface, ipversion);
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_service_logs(service, NULL, event_data, NULL);
#endif //FIREWALL_LOGS
    services_implement_changes();

exit:
    FW_PROFILING_OUT();
    return;
}
