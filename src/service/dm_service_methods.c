/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "service/dm_service.h"
#include "firewall.h" // for fw_get_dm
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_dm.h>

#define ME "service"

static bool add_service_parameters(amxd_trans_t* trans, amxc_var_t* args) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const char* interface = GET_CHAR(args, "interface");
    // amxc_var_dyncast destinationPort and protocol in case the user gives a uint32, otherwise GETP_CHAR returns NULL
    // Alternatively %strict could be used in the ODL.
    char* destination_port = amxc_var_dyncast(cstring_t, GET_ARG(args, "destinationPort"));
    char* protocol = amxc_var_dyncast(cstring_t, GET_ARG(args, "protocol"));
    const char* source_prefix = GET_CHAR(args, "sourcePrefix");
    const char* action = GET_CHAR(args, "_action");
    amxc_var_t* enable_arg = GET_ARG(args, "enable");
    amxc_var_t* icmp_type_arg = GET_ARG(args, "icmpType");
    bool retval = false;

    trans_add_optional_string(trans, "Interface", interface);
    trans_add_optional_string(trans, "DestPort", destination_port);
    amxd_trans_set_value(cstring_t, trans, "Protocol", protocol);
    trans_add_optional_string(trans, "SourcePrefixes", source_prefix);
    trans_add_optional_string(trans, "Action", action);
    if(amxc_var_get_key(args, "ipversion", AMXC_VAR_FLAG_DEFAULT) != NULL) {
        amxd_trans_set_value(int32_t, trans, "IPVersion", amxc_var_dyncast(int32_t, GET_ARG(args, "ipversion")));
    }
    if(enable_arg != NULL) {
        amxd_trans_set_value(bool, trans, "Enable", amxc_var_dyncast(bool, enable_arg));
    }
    if(icmp_type_arg != NULL) {
        amxd_trans_set_value(uint32_t, trans, "ICMPType", amxc_var_dyncast(uint32_t, icmp_type_arg));
    }

    retval = true;

    free(destination_port);
    free(protocol);
    FW_PROFILING_OUT();
    return retval;
}

amxd_status_t _setService(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");

    /* Checked with validators: <overlapping rule>, <overlapping service>, <conflicting range>
       The transaction should fail. */
    status = set_and_return_params(services, args, ret, add_service_parameters);
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _deleteService(UNUSED amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    status = delete_by_alias(services, args);
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _getService(UNUSED amxd_object_t* object,
                          UNUSED amxd_function_t* func,
                          amxc_var_t* args,
                          amxc_var_t* ret) {
    FW_PROFILING_IN(TRACE_DM_RPC);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* services = amxd_dm_findf(fw_get_dm(), "Firewall.Service.");
    status = list_by_alias(services, args, ret);
    FW_PROFILING_OUT();
    return status;
}
