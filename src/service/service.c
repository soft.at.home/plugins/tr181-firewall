/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "service/service.h"
#include "nat/portmapping.h" // for portmapping_suspend
#include "fwrule_helpers.h"  // for set_log_rule_specific_settings
#include "ports.h"
#include "logs/logs.h"
#include "firewall.h"        // for fw_get_chain
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#define ME "service"
#define PROTOCOL_TCP 6
#define PROTOCOL_UDP 17

static amxc_llist_t service_llist;

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    common_t* common = (common_t*) fwiface->query_priv;

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_service_logs(NULL, common->object, NULL, NULL);
#endif //FIREWALL_LOGS
    services_implement_changes();

exit:
    FW_PROFILING_OUT();
}

service_t* create_firewall_service(amxd_object_t* object) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    service_t* service = NULL;

    SAH_TRACEZ_NOTICE(ME, "Service[%s] is new", OBJECT_NAMED);

    service = (service_t*) calloc(1, sizeof(service_t));
    when_null(service, exit);


    common_init(&service->common, object);

    fw_folder_new(&service->nat_folder);

    service->target = TARGET_ACCEPT;

    service->common.src_fwiface.query_priv = &service->common;
    service->common.src_fwiface.query_result_changed = query_result_changed;
    service->common.dest_fwiface.query_priv = &service->common;
    service->common.dest_fwiface.query_result_changed = query_result_changed;

    address_init(&service->source_prefix);

    object->priv = service;

    amxc_llist_append(&service_llist, &service->it);

exit:
    FW_PROFILING_OUT();
    return service;
}

static bool delete_firewall_service(service_t** service) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(*service, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete Firewall Service");

    fw_folder_delete(&(*service)->nat_folder);

    port_range_clean(&(*service)->ports);

    amxc_llist_it_take(&(*service)->it);

    common_clean(&(*service)->common);

    address_clean(&(*service)->source_prefix);

    FREE(*service);
exit:
    FW_PROFILING_OUT();
    return true;
}

static bool service_activate(service_t* service, ipversion_t ipversion) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_rule_t* r = NULL;
    bool ipv4 = ipversion == IPv4;
    fw_folder_t* folder = ipv4 ? service->common.folders.rule_folders.folder : service->common.folders.rule_folders.folder6;
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    fw_folder_t* log_folder = ipv4 ? service->common.folders.log_folders.folder : service->common.folders.log_folders.folder6;
    fw_folder_t* folders[] = {log_folder, folder, service->nat_folder};
    size_t loop_start = log_get_enable(&service->common.folders.log) ? 0 : 1;
#else
    fw_folder_t* folders[] = {folder, service->nat_folder};
    size_t loop_start = 0;
#endif //FIREWALL_LOGS
    amxd_object_t* object = service->common.object;
    firewall_interface_t* fwiface = &service->common.dest_fwiface;
    bool res = false;
    bool waiting = false;
    bool need_nat = false;
    int32_t icmp_type = -1;
    const char* chain = NULL;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_null_trace(amxc_var_get_first(&service->common.protocols), out, INFO, "Service[%s] requires protocols", OBJECT_NAMED);

    waiting = (fwiface->netdev_required && STRING_EMPTY(fwiface->default_netdevname));
    when_true_trace(waiting, out, INFO, "Service[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    waiting = address_is_required(&service->source_prefix) &&
        !address_has_ipversion(&service->source_prefix, ipversion);
    if(waiting) {
        SAH_TRACEZ_INFO(ME, "Service[%s] is waiting for IPv%d source addresses", OBJECT_NAMED, ipversion);
        res = true;
        goto out;
    }

    // Do not unfold the rule if protocol is ICMP. The NO_ICMP chain is not enabled in nat table.
    need_nat = ipv4 && (GET_INT32(amxc_var_get_first(&service->common.protocols), NULL) != 1);
    chain = ipv4 ? fw_get_chain("service", "INPUT_Services") : fw_get_chain("service6", "INPUT6_Services");
    icmp_type = amxd_object_get_value(int32_t, service->common.object, "ICMPType", NULL);

    for(size_t i = loop_start; i < sizeof(folders) / sizeof(folders[0]); i++) {
        SAH_TRACEZ_INFO(ME, "Processing[%s] folder %d", OBJECT_NAMED, (int) i);
        if(((folders[i] == service->nat_folder) && !need_nat) || ((folders[i] == service->nat_folder) && !ipv4)) {
            SAH_TRACEZ_INFO(ME, "Skipping[%s] NAT folder", OBJECT_NAMED);
            continue;
        }

        when_false_trace(folder_require_source_feature(folders[i], &service->source_prefix), out, INFO, "Service[%s] waiting for source address", OBJECT_NAMED);

        fw_folder_set_feature(folders[i], FW_FEATURE_PROTOCOL);

        r = fw_folder_fetch_default_rule(folders[i]);
        when_null_trace(r, out, ERROR, "Service[%s] failed to fetch %d rule", OBJECT_NAMED, (int) i);

        fw_rule_set_chain(r, chain);
        fw_rule_set_ipv4(r, ipv4);
        fw_rule_set_in_interface(r, fwiface->default_netdevname);

        // only used when protocol == 1 / 58
        if(icmp_type >= 0) {
            if(ipv4) {
                fw_rule_set_icmp_type(r, icmp_type);
            } else {
                fw_rule_set_icmpv6_type(r, icmp_type);
            }
        }

        if(folders[i] == service->nat_folder) {
            fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT); // table nat can only have target ACCEPT, this is the reason folder 'service->nat_folder' exists instead of working with FW_FEATURE_TABLE
        } else if(folders[i] == folder) {
            if(service->target == TARGET_ACCEPT) {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_ACCEPT);
            } else if(service->target == TARGET_REJECT) {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_REJECT);
            } else {
                fw_rule_set_target_policy(r, FW_RULE_POLICY_DROP);
            }
        }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
        else if(folders[i] == log_folder) {
            set_log_rule_specific_settings(r, &service->common.folders.log);
        }
#endif //FIREWALL_LOGS

        fw_rule_set_table(r, folders[i] == service->nat_folder ? TABLE_NAT : TABLE_FILTER);

        fw_folder_push_rule(folders[i], r);

        when_false(folder_feature_set_protocol(folders[i], &service->common.protocols), out);
        when_false(folder_feature_set_address(folders[i], &service->source_prefix, ipv4, true, false), out);
        when_false(folder_feature_set_destination_port(folders[i], &service->ports, false), out);
        fw_folder_set_enabled(folders[i], true);
    }

    res = true;

out:
    FW_PROFILING_OUT();
    return res;
}

static void unfold_firewall_service(service_t* service, bool do_commit) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    when_null(service, out);

    folder_container_delete_rules(&service->common.folders, IPvAll, false);
    fw_folder_delete_rules(service->nat_folder);
    fw_commit(fw_rule_callback);
    folder_container_disable_folders(&service->common.folders, IPvAll);
    if(do_commit) {
        fw_apply();
    }

out:
    FW_PROFILING_OUT();
    return;
}

static void suspend_portmapping(service_t* service, bool suspend) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    const char* interface = object_da_string(service->common.object, "Interface");
    const char* ports = object_da_string(service->common.object, "DestPort");
    const char* protocols = object_da_string(service->common.object, "Protocol");
    ipversion_t ipversion = object_get_ipversion(service->common.object);

    if(ipversion != IPv4) {
        FW_PROFILING_OUT();
        return;
    }

    if(suspend) {
        portmapping_suspend(interface, ports, protocols);
    } else {
        portmapping_resume(interface, ports, protocols);
    }
    FW_PROFILING_OUT();
}

static bool activate_firewall_service(service_t* service) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = service->common.object;
    bool enable = object_get_enable(service->common.object);
    ipversion_t ipversion = object_get_ipversion(service->common.object);
    bool res = true;
    bool is_inactive = (service->common.status != FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(enable, out, INFO, "Service[%s] is not enabled", OBJECT_NAMED);
    when_false_trace(is_inactive, out, NOTICE, "Service[%s] is already active", OBJECT_NAMED);

    if(ipversion != IPv6) {
        res = service_activate(service, IPv4);
        when_false(res, out);
    }
    if(ipversion != IPv4) {
        res = service_activate(service, IPv6);
        when_false(res, out);
    }

    folder_container_reorder(&service->common.folders);

    res = (fw_commit(fw_rule_callback) == 0);
    when_false_trace(res, out, ERROR, "Service[%s] fw_commit failed", OBJECT_NAMED);

    common_set_status(&service->common, FIREWALL_ENABLED);
    suspend_portmapping(service, true);

out:
    if(!res) {
        SAH_TRACEZ_INFO(ME, "Service[%s] Clean Firewall rules.", OBJECT_NAMED);
        unfold_firewall_service(service, true);
        common_set_status(&service->common, FIREWALL_ERROR);
    }
    FW_PROFILING_OUT();
    return res;
}

static bool deactivate_firewall_service(service_t* service) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = service->common.object;
    bool is_active = (service->common.status == FIREWALL_ENABLED);
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(is_active, out, INFO, "Service[%s] is already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "Service[%s] is about to be deactivated", OBJECT_NAMED);
    /* do not commit yet, wait for the activate/delete to commit it. */
    unfold_firewall_service(service, false);
    common_set_status(&service->common, FIREWALL_DISABLED);
    suspend_portmapping(service, false);

out:
    FW_PROFILING_OUT();
    return true;
}

bool services_implement_changes(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_llist_for_each(it, &service_llist) {
        service_t* service = amxc_llist_it_get_data(it, service_t, it);
        amxd_object_t* object = NULL;
        (void) object; // when tracing is disabled, variable is not used anymore

        if(service == NULL) {
            continue;
        }

        object = service->common.object;

        if(service->common.flags & FW_DELETED) {
            SAH_TRACEZ_INFO(ME, "Service[%s] will be deleted", OBJECT_NAMED);
            deactivate_firewall_service(service);
            delete_firewall_service(&service);
        } else if(service->common.flags & FW_NEW) {
            SAH_TRACEZ_INFO(ME, "Service[%s] will be activated", OBJECT_NAMED);
            activate_firewall_service(service);
            service->common.flags = 0;
        } else if(service->common.flags & FW_MODIFIED) {
            SAH_TRACEZ_INFO(ME, "Service[%s] deactivate, activate", OBJECT_NAMED);
            deactivate_firewall_service(service);
            activate_firewall_service(service);
            service->common.flags = 0;
        }
        fw_apply();
    }
    FW_PROFILING_OUT();
    return true;
}

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
bool update_service_logs(service_t* p_service, amxd_object_t* object, const amxc_var_t* const data_rule, const amxc_var_t* const data_log) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool update = false;
    service_t* service = NULL;

    if(p_service == NULL) {
        when_null(object, exit);
        service = (service_t*) object->priv;
        when_null(service, exit);
    } else {
        service = p_service;
    }

    log_set_policy(&service->common.folders.log, service->target);
    update = log_update(&service->common.folders.log,
                        log_get_dm_enable_param(service->common.object, data_rule, NULL),
                        log_get_dm_controller_param(service->common.object, data_rule, NULL),
                        !EXCLUDE_SRC_INTERFACE, !EXCLUDE_DST_INTERFACE,
                        data_log);
exit:
    FW_PROFILING_OUT();
    return update;
}
#endif //FIREWALL_LOGS
