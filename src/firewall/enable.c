/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall/enable.h"
#include "fwrule_helpers.h"
#include "firewall.h" // for fw_get_chain
#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "firewall"

#define IPV4 1
#define IPV6 0

static fw_folder_t* fw_enable_input = NULL;
static fw_folder_t* fw_enable_input6 = NULL;
static fw_folder_t* fw_enable_forward = NULL;
static fw_folder_t* fw_enable_forward6 = NULL;
static fw_folder_t* fw_enable_prerouting = NULL;
static fw_folder_t* fw_enable_prerouting6 = NULL;
static fw_folder_t* fw_enable_prerouting_nat = NULL;
static fw_folder_t* fw_enable_postrouting_nat = NULL;

typedef enum {
    INPUT_ENABLE,
    FORWARD_ENABLE,
    PREROUTING_ENABLE,
    PREROUTING_ENABLE_NAT,
    POSTROUTING_ENABLE,
} enable_chain_t;

static bool fw_enable_activate(bool ipv4, enable_chain_t chain_type) {
    fw_rule_t* r = NULL;
    fw_rule_t* s = NULL;
    fw_folder_t* folder = NULL;
    bool res = false;
    const char* chain = NULL;
    const char* table = NULL;

    switch(chain_type) {
    case INPUT_ENABLE:
        folder = (ipv4) ? fw_enable_input : fw_enable_input6;
        chain = ipv4 ? fw_get_chain("input_fw_enable", "INPUT_Firewall_Enable") : fw_get_chain("input_fw_enable6", "INPUT6_Firewall_Enable");
        table = TABLE_FILTER;
        break;
    case FORWARD_ENABLE:
        folder = (ipv4) ? fw_enable_forward : fw_enable_forward6;
        chain = ipv4 ? fw_get_chain("forward_fw_enable", "FORWARD_Firewall_Enable") : fw_get_chain("forward_fw_enable6", "FORWARD6_Firewall_Enable");
        table = TABLE_FILTER;
        break;
    case PREROUTING_ENABLE:
        folder = (ipv4) ? fw_enable_prerouting : fw_enable_prerouting6;
        chain = ipv4 ? fw_get_chain("prerouting_fw_enable", "PREROUTING_Firewall_Enable") : fw_get_chain("prerouting_fw_enable6", "PREROUTING6_Firewall_Enable");
        table = TABLE_RAW;
        break;
    case PREROUTING_ENABLE_NAT:
        when_false_status(ipv4, out, res = true);
        folder = fw_enable_prerouting_nat;
        chain = fw_get_chain("prerouting_fw_enable", "PREROUTING_Firewall_Enable");
        table = TABLE_NAT;
        break;
    case POSTROUTING_ENABLE:
        when_false_status(ipv4, out, res = true);
        folder = fw_enable_postrouting_nat;
        chain = fw_get_chain("postrouting_fw_enable", "POSTROUTING_Firewall_Enable");
        table = TABLE_NAT;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Wrong chain type");
        goto out;
        break;
    }

    fw_folder_set_feature(folder, FW_FEATURE_POLICY);

    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "Failed to fetch rule for the Firewall.Enable");

    fw_rule_set_chain(r, chain);
    fw_rule_set_ipv4(r, ipv4);
    fw_rule_set_table(r, table);
    fw_folder_push_rule(folder, r);

    s = fw_folder_fetch_feature_rule(folder, FW_FEATURE_POLICY);
    when_null_trace(s, out, ERROR, "Failed to fetch rule");

    fw_rule_set_target_policy(s, FW_RULE_POLICY_ACCEPT);
    fw_folder_push_rule(folder, s);

    fw_folder_set_enabled(folder, true);
    res = true;
out:
    return res;
}

static void unfold_fw_enable(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    fw_folder_delete_rules(fw_enable_input);
    fw_folder_delete_rules(fw_enable_input6);
    fw_folder_delete_rules(fw_enable_forward);
    fw_folder_delete_rules(fw_enable_forward6);
    fw_folder_delete_rules(fw_enable_prerouting);
    fw_folder_delete_rules(fw_enable_prerouting6);
    fw_folder_delete_rules(fw_enable_prerouting_nat);
    fw_folder_delete_rules(fw_enable_postrouting_nat);
    fw_commit(fw_rule_callback);
    fw_apply();
    FW_PROFILING_OUT();
}

void enable_firewall(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    unfold_fw_enable();

    fw_folder_delete(&fw_enable_input);
    fw_folder_delete(&fw_enable_input6);
    fw_folder_delete(&fw_enable_forward);
    fw_folder_delete(&fw_enable_forward6);
    fw_folder_delete(&fw_enable_prerouting);
    fw_folder_delete(&fw_enable_prerouting6);
    fw_folder_delete(&fw_enable_prerouting_nat);
    fw_folder_delete(&fw_enable_postrouting_nat);
    fw_commit(fw_rule_callback);
    fw_apply();
    FW_PROFILING_OUT();
}

bool disable_firewall(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    bool status = false;

    when_not_null(fw_enable_input, exit);
    when_not_null(fw_enable_input6, exit);
    when_not_null(fw_enable_forward, exit);
    when_not_null(fw_enable_forward6, exit);
    when_not_null(fw_enable_prerouting, exit);
    when_not_null(fw_enable_prerouting6, exit);
    when_not_null(fw_enable_prerouting_nat, exit);
    when_not_null(fw_enable_postrouting_nat, exit);

    fw_folder_new(&fw_enable_input);
    fw_folder_new(&fw_enable_input6);
    fw_folder_new(&fw_enable_forward);
    fw_folder_new(&fw_enable_forward6);
    fw_folder_new(&fw_enable_prerouting);
    fw_folder_new(&fw_enable_prerouting6);
    fw_folder_new(&fw_enable_prerouting_nat);
    fw_folder_new(&fw_enable_postrouting_nat);

    when_false(fw_enable_activate(IPV4, INPUT_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV6, INPUT_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV4, FORWARD_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV6, FORWARD_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV4, PREROUTING_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV6, PREROUTING_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV4, PREROUTING_ENABLE_NAT), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_false(fw_enable_activate(IPV4, POSTROUTING_ENABLE), exit);
    when_failed_trace(fw_commit(fw_rule_callback), exit, ERROR, "fw_commit failed for the Firewall.Enable");
    when_failed_trace(fw_apply(), exit, ERROR, "fw_apply failed for the Firewall.Enable");

    status = true;

exit:
    if(!status) {
        enable_firewall();
    }
    FW_PROFILING_OUT();
    return status;
}