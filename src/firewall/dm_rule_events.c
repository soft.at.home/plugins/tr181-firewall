/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall/dm_firewall.h"
#include "rules.h"
#include "ipset/dm_ipset.h"
#include "logs/logs.h"
#ifdef WITH_GMAP
#include "devices_query.h"
#endif
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwinterface/interface.h> // for fw_apply

#define ME "firewall"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    rule_t* rule = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    rule = (rule_t*) object->priv;
    when_null_trace(rule, exit, ERROR, "Rule[%s] is NULL", OBJECT_NAMED);

    if(update_rule_logs(NULL, object, NULL, data, log, true)) {
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

static void set_interface(rule_t* rule, const char* src_interface, const char* dest_interface, ipversion_t ipversion) {
    amxd_object_t* object = rule->common.object;
    (void) object; // when tracing is disabled, variable is not used anymore

    if(dest_interface != NULL) {
        firewall_interface_t* fwiface = &rule->common.dest_fwiface;

        if(!STRING_EMPTY(dest_interface)) {
            SAH_TRACEZ_INFO(ME, "Rule[%s] Configure Destination Interface[%s]", OBJECT_NAMED, dest_interface);
            to_linux_interface(dest_interface, ipversion, fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }

    if(src_interface != NULL) {
        firewall_interface_t* fwiface = &rule->common.src_fwiface;

        if(!STRING_EMPTY(src_interface)) {
            SAH_TRACEZ_INFO(ME, "Rule[%s] Configure Source Interface[%s]", OBJECT_NAMED, src_interface);
            to_linux_interface(src_interface, ipversion, fwiface);
        } else {
            init_firewall_interface(fwiface);
        }
    }
}

static void mask_or_ip_changed(bool changed_event, amxc_var_t* params, amxd_object_t* obj, const char* param_mask, const char* param_ip, void (* apply_ip)(rule_t*, const char*), rule_t* rule) {
    amxc_var_t* var = NULL;
    const char* ip = NULL;
    const char* to = changed_event ? "to" : NULL;

    var = GET_ARG(params, param_mask);
    if(var != NULL) {
        ip = GET_CHAR(var, to);
        if(!STRING_EMPTY(ip)) {
            apply_ip(rule, ip);
            goto exit;
        }
    } else {
        ip = object_da_string(obj, param_mask);
        if(!STRING_EMPTY(ip)) {
            goto exit; // already in use
        }
    }

    // at this point we know Mask is empty
    var = GET_ARG(params, param_ip);
    if(var != NULL) {
        ip = GET_CHAR(var, to);
    } else {
        ip = object_da_string(obj, param_ip);
    }
    apply_ip(rule, ip);

exit:
    return;
}

rule_t* rule_init(amxd_object_t* object) {
    amxd_trans_t trans;
    amxc_ts_t now;
    amxc_var_t params;
    rule_t* rule = NULL;
    int start = 0;
    int end = 0;

    amxc_var_init(&params);

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    rule = rule_create(object, false);
    when_null(rule, exit);

    rule->chain = amxd_object_get_parent(amxd_object_get_parent(object));

    amxd_object_get_params(object, &params, amxd_dm_access_protected);

    rule_set_target(rule, GET_CHAR(&params, "Target"));
    rule_set_log_target(rule, GET_CHAR(&params, "LogTarget"));
    rule_set_protocol(rule, GET_INT32(&params, "Protocol"));
    mask_or_ip_changed(false, &params, object, "SourceMask", "SourceIP", rule_set_source, rule);
    mask_or_ip_changed(false, &params, object, "DestMask", "DestIP", rule_set_destination, rule);
    increment_set_ref(GET_CHAR(&params, "SourceMatchSet"));
    increment_set_ref(GET_CHAR(&params, "DestMatchSet"));
    increment_set_ref(GET_CHAR(&params, "SourceMatchSetExclude"));
    increment_set_ref(GET_CHAR(&params, "DestMatchSetExclude"));
    rule_set_enable(rule, GET_BOOL(&params, "Enable"));

    start = GET_INT32(&params, "SourcePort");
    end = GET_INT32(&params, "SourcePortRangeMax");
    rule_set_src_port(rule, start, end);

    start = GET_INT32(&params, "DestPort");
    end = GET_INT32(&params, "DestPortRangeMax");
    rule_set_dst_port(rule, start, end);

    set_interface(rule, GET_CHAR(&params, "SourceInterface"), GET_CHAR(&params, "DestInterface"), GET_INT32(&params, "IPVersion"));

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    init_rule_logs(object, rule, &rule->common.src_fwiface, &rule->common.dest_fwiface, true, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

#ifdef WITH_GMAP
    rule_query_vendor_class_id(rule);
#endif

    amxc_ts_now(&now);
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(amxc_ts_t, &trans, "CreationDate", &now);
    amxd_trans_apply(&trans, fw_get_dm());
    amxd_trans_clean(&trans);

    rule_activate(rule);

exit:
    amxc_var_clean(&params);
    return rule;
}

void _firewall_rule_added(UNUSED const char* const sig_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));
    rule_t* rule = NULL;

    when_null(object, exit);

    rule = rule_init(object);
    when_null(rule, exit);

    fw_apply();
exit:
    FW_PROFILING_OUT();
    return;
}

void _firewall_rule_changed(UNUSED const char* const sig_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    rule_t* rule = NULL;
    const char* src_interface = NULL;
    const char* dest_interface = NULL;
    ipversion_t ipversion = IPvAll;
    bool ipversion_changed = false;

    when_null(object, exit);
    when_null(object->priv, exit);
    rule = (rule_t*) object->priv;

    rule->common.flags = FW_MODIFIED;
    rule->common.validated = false;

    var = GET_ARG(params, "Target");
    if(var != NULL) {
        rule_set_target(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "LogTarget");
    if(var != NULL) {
        rule_set_log_target(rule, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Protocol");
    if(var != NULL) {
        rule_set_protocol(rule, GET_INT32(var, "to"));
    }

    var = GET_ARG(params, "DestPort");
    var2 = GET_ARG(params, "DestPortRangeMax");
    if((var != NULL) || (var2 != NULL)) {
        int start = 0;
        int end = 0;
        if(var != NULL) {
            start = GET_INT32(var, "to");
        } else {
            start = amxd_object_get_value(int32_t, object, "DestPort", NULL);
        }
        if(var2 != NULL) {
            end = GET_INT32(var2, "to");
        } else {
            end = amxd_object_get_value(int32_t, object, "DestPortRangeMax", NULL);
        }
        rule_set_dst_port(rule, start, end);
    }

    var = GET_ARG(params, "SourcePort");
    var2 = GET_ARG(params, "SourcePortRangeMax");
    if((var != NULL) || (var2 != NULL)) {
        int start = 0;
        int end = 0;
        if(var != NULL) {
            start = GET_INT32(var, "to");
        } else {
            start = amxd_object_get_value(int32_t, object, "SourcePort", NULL);
        }
        if(var2 != NULL) {
            end = GET_INT32(var2, "to");
        } else {
            end = amxd_object_get_value(int32_t, object, "SourcePortRangeMax", NULL);
        }
        rule_set_src_port(rule, start, end);
    }

    mask_or_ip_changed(true, params, object, "SourceMask", "SourceIP", rule_set_source, rule);
    mask_or_ip_changed(true, params, object, "DestMask", "DestIP", rule_set_destination, rule);

    var = GET_ARG(params, "SourceMatchSet");
    if(var != NULL) {
        decrement_set_ref(GET_CHAR(var, "from"));
        increment_set_ref(GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "DestMatchSet");
    if(var != NULL) {
        decrement_set_ref(GET_CHAR(var, "from"));
        increment_set_ref(GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "SourceMatchSetExclude");
    if(var != NULL) {
        decrement_set_ref(GET_CHAR(var, "from"));
        increment_set_ref(GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "DestMatchSetExclude");
    if(var != NULL) {
        decrement_set_ref(GET_CHAR(var, "from"));
        increment_set_ref(GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        rule_set_enable(rule, GET_BOOL(var, "to"));
    }

    var = GET_ARG(params, "SourceInterface");
    if(var != NULL) {
        src_interface = GET_CHAR(var, "to");
    }

    var = GET_ARG(params, "DestInterface");
    if(var != NULL) {
        dest_interface = GET_CHAR(var, "to");
    }

    var = GET_ARG(params, "IPVersion");
    if(var != NULL) {
        ipversion = int_to_ipversion(GET_INT32(var, "to"));
        ipversion_changed = true;
    }

    if((src_interface != NULL) || (dest_interface != NULL) || ipversion_changed) {
        if(ipversion_changed) {
            if(src_interface == NULL) {
                src_interface = object_da_string(object, "SourceInterface");
            }
            if(dest_interface == NULL) {
                dest_interface = object_da_string(object, "DestInterface");
            }
        } else {
            ipversion = object_get_ipversion(object);
        }
        set_interface(rule, src_interface, dest_interface, ipversion);
    }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_rule_logs(rule, NULL, event_data, NULL, NULL, true);
#endif //FIREWALL_LOGS

#ifdef WITH_GMAP
    var = GET_ARG(params, param_vendor_class_id());
    if(var != NULL) {
        rule_query_vendor_class_id(rule);
    }
#endif

    rule_update(rule);

exit:
    FW_PROFILING_OUT();
    return;
}
