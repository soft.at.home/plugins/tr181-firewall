/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall/level.h"
#include "policy/policy.h" // for extendedpolicy_t
#include "firewall.h"      // for fw_get_dm
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <string.h>

#define ME "firewall"

static void update_policies(amxd_object_t* level, bool enable) {
    amxc_var_t policies;
    amxd_trans_t trans;
    amxd_object_t* object = level;
    uint32_t count = 0;
    (void) object; // when tracing is disabled, variable is not used anymore
    char* char_policies = amxd_object_get_value(cstring_t, level, "Policies", NULL);
    extendedpolicy_t* priv_data = NULL;
    priv_data = object->priv;
    when_null(priv_data, exit);
    if(enable) {
        free(priv_data->policies);
        if(!STRING_EMPTY(char_policies)) {
            priv_data->policies = strdup(char_policies);
        } else {
            priv_data->policies = NULL;
        }
    }
    amxd_trans_init(&trans);
    amxc_var_init(&policies);
    amxc_var_set(csv_string_t, &policies, priv_data->policies);

    amxc_var_cast(&policies, AMXC_VAR_ID_LIST);
    amxc_var_for_each(var, &policies) {
        const char* path = GET_CHAR(var, NULL);
        amxd_object_t* policy_object = amxd_dm_findf(fw_get_dm(), "%s", path);
        if(policy_object == NULL) { // maybe policy was already deleted
            SAH_TRACEZ_WARNING(ME, "Level[%s] did not find Policy[%s]", OBJECT_NAMED, path);
        } else {
            SAH_TRACEZ_INFO(ME, "Level[%s] %s policy[%s]", OBJECT_NAMED, enable ? "enable" : "disable", path);
            amxd_trans_select_pathf(&trans, "%s", path);
            amxd_trans_set_value(bool, &trans, "Enable", enable);
            count++;
        }
    }

    if(count == 0) {
        SAH_TRACEZ_WARNING(ME, "Level[%s] without policies", OBJECT_NAMED);
    } else if(amxd_status_ok != amxd_trans_apply(&trans, fw_get_dm())) {
        SAH_TRACEZ_ERROR(ME, "Transaction failed");
    }
    if(!enable) {
        FREE(priv_data->policies);
    }
    free(char_policies);
    amxd_trans_clean(&trans);
    amxc_var_clean(&policies);

exit:
    return;
}

void level_activate_policy(amxd_object_t* level) {
    update_policies(level, true);
}
void level_activate_not_policy(amxd_object_t* level) {
    policy_activate_object(level);
}

void level_desactivate_policy(amxd_object_t* level) {
    update_policies(level, false);
}

void level_desactivate_not_policy(amxd_object_t* level) {
    policy_deactivate_object(level);
}
