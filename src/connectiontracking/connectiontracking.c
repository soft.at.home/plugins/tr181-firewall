/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "connectiontracking/connectiontracking.h"
#include "connectiontracking/sip.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>            // for fw_commit
#include <fwinterface/interface.h> // for fw_apply

#define ME "connectiontracking"

static amxc_llist_t connectiontracking_llist;

static bool deactivate_firewall_connectiontracking(connectiontracking_t* c);

connectiontracking_t* create_firewall_connectiontracking(amxd_object_t* object) {
    connectiontracking_t* c = NULL;

    SAH_TRACEZ_NOTICE(ME, "ConnectionTracking[%s] is new", OBJECT_NAMED);

    c = (connectiontracking_t*) calloc(1, sizeof(connectiontracking_t));
    when_null(c, exit);

    c->target = TARGET_ACCEPT;
    c->object = object;
    object->priv = c;

    amxc_llist_append(&connectiontracking_llist, &c->it);

exit:
    return c;
}

bool delete_firewall_connectiontracking(connectiontracking_t** c) {
    when_null(*c, exit);
    SAH_TRACEZ_NOTICE(ME, "Delete Firewall ConnectionTracking");

    deactivate_firewall_connectiontracking(*c);
    amxc_llist_it_take(&(*c)->it);

    FREE(*c);
exit:
    return true;
}

static bool connectiontracking_activate(connectiontracking_t* c, bool enable) {
    int res = false;

    switch(c->type) {
    case FW_CONNECTIONTRACKING_TYPE_SIP:
        configure_sip(c, enable);
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Unknown ConnectionTracking type");
        goto exit;
    }
    res = true;

exit:
    return res;
}

static bool activate_firewall_connectiontracking(connectiontracking_t* c) {
    return connectiontracking_activate(c, true);
}

static bool deactivate_firewall_connectiontracking(connectiontracking_t* c) {
    return connectiontracking_activate(c, false);
}

bool connectiontracking_implement_changes(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_llist_for_each(it, &connectiontracking_llist) {
        connectiontracking_t* c = amxc_llist_it_get_data(it, connectiontracking_t, it);
        amxd_object_t* object = NULL;
        bool is_active = false;

        if(c == NULL) {
            continue;
        }

        object = c->object;
        is_active = amxd_object_get_value(bool, object, "Enable", NULL);

        if(is_active) {
            SAH_TRACEZ_INFO(ME, "ConnectionTracking[%s] will be activated", OBJECT_NAMED);
            activate_firewall_connectiontracking(c);
        } else {
            SAH_TRACEZ_INFO(ME, "ConnectionTracking[%s] will be deactivated", OBJECT_NAMED);
            deactivate_firewall_connectiontracking(c);
        }
    }
    FW_PROFILING_OUT();
    return true;
}
