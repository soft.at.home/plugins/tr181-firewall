/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "connectiontracking/sip.h"
#include "kmods.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "sip"

#define NF_NAT_SIP "nf_nat_sip"
#define NF_CONNTRACK_SIP "nf_conntrack_sip"

int enable_sip_modules(bool enable, const char* parameters) {
    int rv = -1;

    rv = fw_unload_kernel_module(NF_NAT_SIP);
    when_true_trace(rv != 0 && !enable, exit, ERROR, "Failed unloading kernel module %s", NF_NAT_SIP);       // Only give error when explicitely asked to disable
    rv = fw_unload_kernel_module(NF_CONNTRACK_SIP);
    when_true_trace(rv != 0 && !enable, exit, ERROR, "Failed unloading kernel module %s", NF_CONNTRACK_SIP); // Only give error when explicitely asked to disable

    if(enable) {
        rv = fw_load_kernel_module(NF_CONNTRACK_SIP, parameters);
        when_failed_trace(rv, exit, ERROR, "Failed loading kernel module %s", NF_CONNTRACK_SIP);
        rv = fw_load_kernel_module(NF_NAT_SIP, "");
        when_failed_trace(rv, exit, ERROR, "Failed loading kernel module %s", NF_NAT_SIP);
    }

exit:
    return rv;
}

void configure_sip(connectiontracking_t* c, bool enable) {
    const char* ports = NULL;
    bool direct_media = false;
    bool direct_signaling = false;
    bool external_media = false;
    uint32_t timeout = 0;
    amxc_var_t params;
    amxc_string_t module_arguments;
    amxd_object_t* object = NULL;

    amxc_var_init(&params);
    amxc_string_init(&module_arguments, 0);

    object = (amxd_object_t*) c->object;
    when_null_trace(object, exit, ERROR, "Object is NULL");

    amxd_object_get_params(object, &params, amxd_dm_access_protected);
    ports = GET_CHAR(&params, "Ports");
    direct_media = GET_BOOL(&params, "DirectMedia");
    direct_signaling = GET_BOOL(&params, "DirectSignaling");
    external_media = GET_BOOL(&params, "ExternalMedia");
    timeout = GET_UINT32(&params, "TimeOut");

    amxc_string_setf(&module_arguments, "ports=%s sip_direct_signalling=%d sip_direct_media=%d sip_external_media=%d sip_timeout=%d", ports, direct_signaling, direct_media, external_media, timeout);
    enable_sip_modules(enable, amxc_string_get(&module_arguments, 0));

exit:
    amxc_string_clean(&module_arguments);
    amxc_var_clean(&params);
}