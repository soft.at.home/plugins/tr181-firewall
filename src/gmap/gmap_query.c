/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "devices_query.h"
#include "firewall_utilities.h"
#include "firewall.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <gmap/gmap.h>
#include <ipat/ipat.h>

#define ME "firewall"
#define INVALID_IP "not_valid"
#define GMAP_ROOT_OBJECT "Devices.Device."

#define CLOSE_GMAP_QUERY(Q) do { \
        if(Q != NULL) { \
            gmap_query_close(Q); \
            Q = NULL; \
        } \
} while(0)

typedef enum {
    FWGMAP_UNINITIALIZED,
    FWGMAP_INITIALIZING,
    FWGMAP_INITIALIZED
} fwgmap_state_t;

static fwgmap_state_t fwgmap_state = FWGMAP_UNINITIALIZED;
static amxp_timer_t* timer_retry_start = NULL;
static amxc_llist_t waiting_list_rules;

typedef struct gmap_rule gmap_rule_t;

struct gmap_rule {
    rule_t* rule;
    address_t* address;
    gmap_query_t* query;
    amxc_llist_it_t it;
    char id[8 + 1 + 10 + 1]; // big enough to hold "firewall-4294967295"
    bool (* open_query)(gmap_rule_t* gmap_rule);
};

const char* param_vendor_class_id(void) {
    return VENDOR_PREFIXED("VendorClassID");
}

static void set_id_for_query(gmap_rule_t* gmap_rule) {
    static uint32_t count = 0; // gmap query needs unique name (used as alias) or open query will fail
    snprintf(gmap_rule->id, sizeof(gmap_rule->id) - 1, "firewall-%u", count);
    count++;
}

static int try_open_query(gmap_rule_t* gmap_rule) {
    int rv = -1;

    when_null_trace(gmap_rule->open_query, exit, ERROR, "Missing callback function");

    if(fwgmap_state == FWGMAP_UNINITIALIZED) {
        SAH_TRACEZ_ERROR(ME, "Gmap maybe not initialized");
        fwgmap_init();
    }

    if(fwgmap_state == FWGMAP_INITIALIZING) {
        amxc_llist_append(&waiting_list_rules, &gmap_rule->it);
    } else if(fwgmap_state == FWGMAP_INITIALIZED) {
        when_false_trace(gmap_rule->open_query(gmap_rule), exit, ERROR, "Failed to open query");
    } else {
        SAH_TRACEZ_ERROR(ME, "Unknown state %d", fwgmap_state);
        goto exit;
    }

    rv = 0;
exit:
    return rv;
}

static void remove_from_waiting_list(gmap_rule_t* gmap_rule) {
    amxc_llist_it_take(&gmap_rule->it);
}

// todo can this also be replaced with device_address_cb?
static void rule_query_callback(gmap_query_t* lquery, UNUSED const cstring_t key, amxc_var_t* device, gmap_query_action_t action) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    SAH_TRACEZ_IN(ME);
    gmap_rule_t* gmap_rule = NULL;
    rule_t* rule = NULL;

    when_null(lquery, exit);
    when_null(device, exit);

    gmap_rule = (gmap_rule_t*) lquery->data;
    when_null_trace(gmap_rule, exit, ERROR, "Gmap rule for gmap callback is NULL.");

    rule = gmap_rule->rule;
    when_null_trace(rule, exit, ERROR, "Rule for gmap callback is NULL.");

    gmap_rule->rule->common.flags = FW_MODIFIED;
    if(action == gmap_query_expression_stop_matching) {
        rule_set_destination(gmap_rule->rule, INVALID_IP); // todo don't set invalid ip because it will be parsed and fail wasting cpu cycles
    } else {
        rule_set_destination(gmap_rule->rule, GET_CHAR(device, "IPAddress"));
    }
    rule_update(gmap_rule->rule);

exit:
    SAH_TRACEZ_OUT(ME);
    FW_PROFILING_OUT();
    return;
}

static bool query_address_from_gmap(gmap_rule_t* gmap_rule) {
    bool status = false;
    rule_t* rule = NULL;
    amxd_object_t* object = NULL;
    amxc_string_t expression;
    const char* address = NULL;

    amxc_string_init(&expression, 0);

    when_null(gmap_rule, exit);
    rule = gmap_rule->rule;
    when_null_trace(rule, exit, ERROR, "The firewall rule is NULL.");
    object = rule->common.object;

    address = object_da_string(object, "InternalClient");
    when_str_empty_trace(address, exit, ERROR, "Rule[%s] InternalClient is empty", OBJECT_NAMED);

    if(ipat_mac_text_valid(address)) {
        amxc_string_t mac;
        amxc_string_init(&mac, 0);
        amxc_string_set(&mac, address);
        amxc_string_to_upper(&mac); // gmap uses upper case strings
        amxc_string_setf(&expression, "not self and physical and .PhysAddress matches \"%s\"", amxc_string_get(&mac, 0));
        amxc_string_clean(&mac);
    } else {
        amxc_string_setf(&expression, "not self and physical and .Name matches \"%s\"", address);
    }

    CLOSE_GMAP_QUERY(gmap_rule->query);

    if(STRING_EMPTY(gmap_rule->id)) {
        set_id_for_query(gmap_rule);
    }

    gmap_rule->query = gmap_query_open_ext(amxc_string_get(&expression, 0), gmap_rule->id, rule_query_callback, gmap_rule);
    if(gmap_rule->query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open Gmap query with expression: %s", amxc_string_get(&expression, 0));
        rule_set_destination(rule, INVALID_IP); // todo don't set invalid ip because it will be parsed and fail wasting cpu cycles
        goto exit;
    }

    status = true;
exit:
    amxc_string_clean(&expression);
    return status;
}

static void create_queries_from_waiting_list(void) {
    amxc_llist_for_each(it, &waiting_list_rules) {
        gmap_rule_t* gmap_rule = amxc_container_of(it, gmap_rule_t, it);
        // gmap_rule->open_query will not be NULL for items in the waiting list
        if(!gmap_rule->open_query(gmap_rule)) {
            SAH_TRACEZ_ERROR(ME, "Failed to open gmap query");
        }
        remove_from_waiting_list(gmap_rule);
    }
}

static gmap_rule_t* gmap_rule_create(rule_t* rule, gmap_rule_t** gmap_rule) {
    when_null_trace(rule, out, ERROR, "Rule is NULL");
    when_null_trace(gmap_rule, out, ERROR, "Gmap Rule is NULL");

    *gmap_rule = (gmap_rule_t*) calloc(1, sizeof(gmap_rule_t));
    when_null(*gmap_rule, out);

    (*gmap_rule)->rule = rule;

out:
    return *gmap_rule;
}

static bool gmap_rule_clean(gmap_rule_t* gmap_rule) {
    when_null(gmap_rule, exit);
    remove_from_waiting_list(gmap_rule);
    CLOSE_GMAP_QUERY(gmap_rule->query);
    if(gmap_rule->rule != NULL) {
        address_clean_devices(&gmap_rule->rule->source);
    }
    gmap_rule->open_query = NULL;
exit:
    return true;
}

static int init_gmap_client(void) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* gmap_ctx = amxb_be_who_has(GMAP_ROOT_OBJECT);

    gmap_client_init(gmap_ctx);
    fwgmap_state = FWGMAP_INITIALIZED;
    create_queries_from_waiting_list();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static void continue_gmap_initialization(UNUSED amxp_timer_t* timer, UNUSED void* data) {
    SAH_TRACEZ_IN(ME);
    int ret_value = -1;
    amxb_bus_ctx_t* gmap_ctx = NULL;

    gmap_ctx = amxb_be_who_has(GMAP_ROOT_OBJECT);
    //Sometimes it hangs (5% of the times when booting up), even with a signal from amxb. This is a workaround and must be removed in the future.
    if((gmap_ctx == NULL) && (timer_retry_start == NULL)) {
        SAH_TRACEZ_ERROR(ME, "No Gmap bus context found - retrying again in 3 seconds.");
        ret_value = amxp_timer_new(&timer_retry_start, continue_gmap_initialization, NULL);
        when_failed_trace(ret_value, exit, ERROR, "Failed to initialize the timer, the GMAP query won't be initialized.");
        ret_value = amxp_timer_start(timer_retry_start, 3000);
        when_failed_trace(ret_value, exit, ERROR, "Failed to start the timer, the GMAP query won't be initialized.");
        goto rearm;
    }
    when_null_trace(gmap_ctx, exit, ERROR, "No Gmap bus context found. The GMAP query won't be initialized.");

    ret_value = init_gmap_client();
    when_failed(ret_value, exit);

exit:
    amxp_timer_delete(&timer_retry_start);
rearm:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void wait_for_gmap_cb(UNUSED const char* signame,
                             UNUSED const amxc_var_t* const data,
                             UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    SAH_TRACEZ_IN(ME);
    continue_gmap_initialization(NULL, NULL);
    SAH_TRACEZ_OUT(ME);
    FW_PROFILING_OUT();
    return;
}

bool fwgmap_init(void) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    int ret_value = -1;
    amxb_bus_ctx_t* gmap_ctx = NULL;

    when_true(fwgmap_state != FWGMAP_UNINITIALIZED, exit);

    fwgmap_state = FWGMAP_INITIALIZING;

    amxc_llist_init(&waiting_list_rules);

    gmap_ctx = amxb_be_who_has(GMAP_ROOT_OBJECT);
    if(gmap_ctx == NULL) {
        amxb_wait_for_object(GMAP_ROOT_OBJECT);
        ret_value = amxp_slot_connect_filtered(NULL, "^wait:Devices\\.Device\\.$", NULL, wait_for_gmap_cb, NULL);
        when_failed_trace(ret_value, exit, ERROR, "Failed to wait for Gmap to be available");
        SAH_TRACEZ_WARNING(ME, "Waiting for Gmap");
        rv = true;
        goto exit;
    }

    ret_value = init_gmap_client();
    when_failed(ret_value, exit);

    rv = true;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void fwgmap_cleanup(void) {
    amxc_llist_clean(&waiting_list_rules, NULL);
}

int query_hostname_address(rule_t* rule, const char* address) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    gmap_rule_t* gmap_rule = NULL;

    when_null(rule, exit);
    when_str_empty(address, exit);

    gmap_rule = (gmap_rule_t*) rule->destination_query;
    if(gmap_rule == NULL) {
        gmap_rule = gmap_rule_create(rule, (gmap_rule_t**) &rule->destination_query);
        gmap_rule->open_query = query_address_from_gmap;
    }

    rv = try_open_query(gmap_rule);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int stop_gmap_queries(rule_t* rule) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    gmap_rule_t* gmap_rule = NULL;

    when_null_status(rule, exit, rv = 0);

    gmap_rule = (gmap_rule_t*) rule->destination_query;
    if(gmap_rule != NULL) {
        gmap_rule_clean(gmap_rule);
        FREE(rule->destination_query);
    }

    gmap_rule = (gmap_rule_t*) rule->source_query;
    if(gmap_rule != NULL) {
        gmap_rule_clean(gmap_rule);
        FREE(rule->source_query);
    }

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int rule_device_ip_addrs_add(address_t* address, const char* alias, amxc_var_t* device_params) {
    int retval = -1;

    when_str_empty(alias, exit);
    when_null(device_params, exit);

    if(address_add_device(address, alias, device_params)) {
        retval = 0;
    }

exit:
    return retval;
}

static int rule_device_ip_addrs_del(address_t* address, const char* alias) {
    int retval = -1;

    when_str_empty(alias, exit);

    if(address_delete_device(address, alias)) {
        retval = 0;
    }

exit:
    return retval;
}

static int rule_device_reset_ip_addrs(address_t* address, const char* alias, amxc_var_t* device_params) {
    int retval = -1;

    when_str_empty(alias, exit);

    // Delete IP addresses for current device
    retval = rule_device_ip_addrs_del(address, alias);
    when_failed(retval, exit);

    // Add IP addresses for current device
    // This is allowed to fail if there are no IPs to add
    (void) rule_device_ip_addrs_add(address, alias, device_params);

exit:
    return retval;
}

static void device_address_cb(gmap_query_t* query, const char* alias, amxc_var_t* device_params, gmap_query_action_t action) {
    gmap_rule_t* gmap_rule = NULL;
    rule_t* rule = NULL;
    amxd_object_t* object = NULL;
    address_t* address = NULL;
    int retval = -1;

    when_null(query, exit);

    when_null_trace(query->data, exit, ERROR, "Missing priv data");
    gmap_rule = (gmap_rule_t*) query->data;
    when_null_trace(gmap_rule->rule, exit, ERROR, "Missing rule");
    when_null_trace(gmap_rule->address, exit, ERROR, "Missing address");

    rule = gmap_rule->rule;
    object = rule->common.object;
    address = gmap_rule->address;

    switch(action) {
    case gmap_query_expression_start_matching:
        SAH_TRACEZ_INFO(ME, "Rule[%s] device %s start matching", OBJECT_NAMED, alias);
        (void) rule_device_ip_addrs_add(address, alias, device_params);
        retval = 0;
        break;
    case gmap_query_device_updated:
        SAH_TRACEZ_INFO(ME, "Rule[%s] device %s updated", OBJECT_NAMED, alias);
        retval = rule_device_reset_ip_addrs(address, alias, device_params);
        break;
    case gmap_query_expression_stop_matching:
        SAH_TRACEZ_INFO(ME, "Rule[%s] device %s stop matching", OBJECT_NAMED, alias);
        retval = rule_device_reset_ip_addrs(address, alias, NULL);
        break;
    case gmap_query_error:
        SAH_TRACEZ_ERROR(ME, "Rule[%s] gmap query error for device [%s]", OBJECT_NAMED, alias);
    default:
        break;
    }

    when_failed(retval, exit);

    when_true(rule->common.flags != 0, exit); // will be handled later
    rule->common.flags = FW_MODIFIED;
    rule_update(rule);

exit:
    return;
}

static bool open_query_vendor_class_id(gmap_rule_t* gmap_rule) {
    rule_t* rule = gmap_rule->rule;
    amxd_object_t* object = rule->common.object;
    const char* class_id = NULL;
    amxc_string_t expr;
    const char* param_name = NULL;
    bool rv = false;

    amxc_string_init(&expr, 0);

    CLOSE_GMAP_QUERY(gmap_rule->query);

    param_name = param_vendor_class_id();
    class_id = object_da_string(object, param_name);
    when_str_empty_trace(class_id, exit, ERROR, "Rule[%s] %s is empty", OBJECT_NAMED, param_name);

    amxc_string_setf(&expr, ".VendorClassID==\"%s\"", class_id);

    if(STRING_EMPTY(gmap_rule->id)) {
        set_id_for_query(gmap_rule);
    }

    gmap_rule->query = gmap_query_open_ext(amxc_string_get(&expr, 0), gmap_rule->id, device_address_cb, (void*) gmap_rule);
    when_null_trace(gmap_rule->query, exit, ERROR, "Rule[%s] failed to open query '%s'", OBJECT_NAMED, amxc_string_get(&expr, 0));

    rv = true;

exit:
    amxc_string_clean(&expr);
    return rv;
}

bool rule_query_vendor_class_id(rule_t* rule) {
    int rv = -1;
    gmap_rule_t* gmap_rule = NULL;
    amxd_object_t* object = rule->common.object;
    const char* class_id = NULL;
    const char* param_name = NULL;

    param_name = param_vendor_class_id();
    class_id = object_da_string(object, param_name);

    gmap_rule = (gmap_rule_t*) rule->source_query;

    if(STRING_EMPTY(class_id)) {
        gmap_rule_clean(gmap_rule);
        FREE(rule->source_query);
        require_address(false, &rule->source, REASON_ADDRESS_DEVICE);
        rv = 0;
        goto exit;
    }

    if(gmap_rule == NULL) {
        gmap_rule = gmap_rule_create(rule, (gmap_rule_t**) &rule->source_query);
    } else {
        gmap_rule_clean(gmap_rule);
    }

    gmap_rule->open_query = open_query_vendor_class_id;
    gmap_rule->address = &rule->source;
    require_address(true, gmap_rule->address, REASON_ADDRESS_DEVICE); // so rule won't be activated without source address

    rv = try_open_query(gmap_rule);

exit:
    return rv == 0;
}

static bool open_query_dest_mac(gmap_rule_t* gmap_rule) {
    rule_t* rule = gmap_rule->rule;
    amxd_object_t* object = rule->common.object;
    amxc_string_t expr;
    amxc_string_t mac;
    bool rv = false;

    amxc_string_init(&expr, 0);
    amxc_string_init(&mac, 0);

    CLOSE_GMAP_QUERY(gmap_rule->query);

    amxc_string_set(&mac, object_da_string(object, "DestMACAddress"));
    when_true_trace(amxc_string_is_empty(&mac), exit, ERROR, "Rule[%s] DestMACAddress is empty", OBJECT_NAMED);
    amxc_string_to_upper(&mac); // gmap uses upper case strings

    amxc_string_setf(&expr, "mac and (ipv4 || ipv6) and .PhysAddress==\"%s\"", amxc_string_get(&mac, 0));

    if(STRING_EMPTY(gmap_rule->id)) {
        set_id_for_query(gmap_rule);
    }

    gmap_rule->query = gmap_query_open_ext(amxc_string_get(&expr, 0), gmap_rule->id, device_address_cb, (void*) gmap_rule);
    when_null_trace(gmap_rule->query, exit, ERROR, "Rule[%s] failed to open query '%s'", OBJECT_NAMED, amxc_string_get(&expr, 0));

    rv = true;

exit:
    amxc_string_clean(&expr);
    amxc_string_clean(&mac);
    return rv;
}

bool rule_query_dest_address_by_mac(rule_t* rule, const char* mac) {
    int rv = -1;
    gmap_rule_t* gmap_rule = NULL;

    gmap_rule = (gmap_rule_t*) rule->destination_query;

    if(STRING_EMPTY(mac)) {
        gmap_rule_clean(gmap_rule);
        FREE(rule->destination_query);
        require_address(false, &rule->destination, REASON_ADDRESS_DEVICE);
        rv = 0;
        goto exit;
    }

    if(gmap_rule == NULL) {
        gmap_rule = gmap_rule_create(rule, (gmap_rule_t**) &rule->destination_query);
    } else {
        gmap_rule_clean(gmap_rule);
    }

    gmap_rule->open_query = open_query_dest_mac;
    gmap_rule->address = &rule->destination;
    require_address(true, gmap_rule->address, REASON_ADDRESS_DEVICE); // so rule won't be activated without addresses

    rv = try_open_query(gmap_rule);

exit:
    return rv == 0;
}