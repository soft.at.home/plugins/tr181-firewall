/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include "kmods.h"

#define ME "connectiontracking"

#define LOAD "loading"
#define UNLOAD "unloading"

#define LOAD_CMD "/sbin/modprobe"
#define UNLOAD_CMD "/sbin/rmmod"

static int run_manipulate_module_command(const char** command, const char* action) {
    int rv = -1;
    amxp_subproc_t* proc = NULL;

    rv = amxp_subproc_new(&proc);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    rv = amxp_subproc_vstart(proc, (char**) command);
    if(rv == 0) {
        int timeout = 1000;

        rv = amxp_subproc_wait(proc, timeout);
        if(rv == 0) {
            rv = amxp_subproc_get_exitstatus(proc);
            SAH_TRACEZ_INFO(ME, "%s finished, exitcode=%d", action, rv);
        } else if(rv == -1) {
            SAH_TRACEZ_ERROR(ME, "an error occured during %s", action);
        } else if(rv == 1) {
            SAH_TRACEZ_ERROR(ME, "%s timed-out after %dms", action, timeout);
        }
    } else {
        SAH_TRACEZ_ERROR(ME, "failed to %s: %d", action, rv);
    }

exit:
    amxp_subproc_delete(&proc);
    return rv;
}

int fw_load_kernel_module(const char* module_name, const char* options) {
    const char* cmd[] = {LOAD_CMD, module_name, options, NULL};
    return run_manipulate_module_command(cmd, LOAD);
}

int fw_unload_kernel_module(const char* module_name) {
    const char* cmd[] = {UNLOAD_CMD, module_name, NULL};
    return run_manipulate_module_command(cmd, UNLOAD);
}
