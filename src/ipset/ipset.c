/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ipset/dm_ipset.h"
#include "ipset/ipset.h"
#include "firewall.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>
#include <libipset/ipset.h>
#include <syslog.h>

#define ME "ipset"

static struct ipset* handle = NULL;

// Custom printf functions MUST be defined, otherwise the plugin crash when trying to use the default printf functions
static int ipset_printf_errorfn(UNUSED struct ipset* ipset, UNUSED void* p, UNUSED int status, UNUSED const char* msg, ...) {
    return 0;
}
static int ipset_printf_customfn(UNUSED struct ipset* ipset, UNUSED void* p) {
    return 0;
}
static int ipset_printf_outfn(UNUSED struct ipset_session* session, UNUSED void* p, UNUSED const char* msg, ...) {
    return 0;
}

void ipset_initialize(void) {
    when_not_null_trace(handle, exit, WARNING, "libipset already initialized");
    ipset_load_types();
    handle = ipset_init();
    when_null_trace(handle, exit, ERROR, "Cannot initialize libipset");
    ipset_custom_printf(handle, ipset_printf_errorfn, ipset_printf_customfn, ipset_printf_outfn, NULL);
exit:
    return;
}

static int ipset_destroy_all(void) {
    return ipset_destroy(" ");
}

void ipset_cleanup(void) {
    if(handle != NULL) {
        ipset_destroy_all();
        ipset_fini(handle);
        handle = NULL;
    }
}

static int ipset_exec(amxc_string_t* cmd) {
    int rv = -1;
    char* cmd_buf = NULL;

    when_null_trace(handle, exit, ERROR, "libipset is not initialized");

    cmd_buf = amxc_string_take_buffer(cmd);

    SAH_TRACEZ_INFO(ME, "Execute ipset command : %s", cmd_buf);
    rv = ipset_parse_line(handle, cmd_buf);
    when_failed_trace(rv, exit, ERROR, "ipset command '%s' failed with error code '%d'", cmd_buf, rv);
exit:
    free(cmd_buf);
    return rv;
}

int ipset_create(const char* set, set_type_t type) {
    int rv = -1;
    amxc_string_t* cmd = NULL;

    amxc_string_new(&cmd, 0);

    when_str_empty_trace(set, exit, ERROR, "Set name NULL or empty");

    amxc_string_setf(cmd, "create %s ", set);
    switch(type) {
    case SET_TYPE_HASH_IPV4:
        amxc_string_appendf(cmd, "hash:net family inet ");
        break;

    case SET_TYPE_HASH_IPV6:
        amxc_string_appendf(cmd, "hash:net family inet6 ");
        break;

    case SET_TYPE_HASH_MAC:
        amxc_string_appendf(cmd, "hash:mac ");
        break;

    case SET_TYPE_HASH_PORT:
        amxc_string_appendf(cmd, "bitmap:port range 0-65535 ");
        break;

    default:
        SAH_TRACEZ_ERROR(ME, "ipset type invalid");
        goto exit;
    }

    amxc_string_appendf(cmd, "-exist");

    rv = ipset_exec(cmd);

exit:
    amxc_string_delete(&cmd);
    return rv;
}


int ipset_destroy(const char* set) {
    int rv = -1;
    amxc_string_t* cmd = NULL;

    amxc_string_new(&cmd, 0);

    when_str_empty_trace(set, exit, ERROR, "Set name NULL or empty");

    amxc_string_setf(cmd, "destroy %s", set);
    rv = ipset_exec(cmd);

exit:
    amxc_string_delete(&cmd);
    return rv;
}

int ipset_add_entry(const char* set, const char* entry, bool nomatch) {
    int rv = -1;
    amxc_string_t* cmd = NULL;

    amxc_string_new(&cmd, 0);

    when_str_empty_trace(set, exit, ERROR, "Set name NULL or empty");
    when_str_empty_trace(entry, exit, ERROR, "Entry NULL or empty");

    amxc_string_setf(cmd, "add %s %s", set, entry);

    if(nomatch) {
        amxc_string_appendf(cmd, " nomatch");
    }

    amxc_string_appendf(cmd, " -exist");

    rv = ipset_exec(cmd);

exit:
    amxc_string_delete(&cmd);
    return rv;
}

int ipset_flush_set(const char* set) {
    int rv = -1;
    amxc_string_t* cmd = NULL;

    amxc_string_new(&cmd, 0);

    when_str_empty_trace(set, exit, ERROR, "Set name NULL or empty");

    amxc_string_setf(cmd, "flush %s", set);

    rv = ipset_exec(cmd);

exit:
    amxc_string_delete(&cmd);
    return rv;
}