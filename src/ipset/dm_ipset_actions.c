/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "ipset/dm_ipset.h"
#include "ipset/ipset.h"
#include "firewall.h"
#include "firewall_utilities.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>

#define ME "ipset"

amxd_status_t _set_instance_delete(amxd_object_t* object,
                                   UNUSED amxd_param_t* param,
                                   UNUSED amxd_action_t reason,
                                   const amxc_var_t* const args,
                                   UNUSED amxc_var_t* const retval,
                                   UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_invalid_action;
    set_t* set = NULL;
    amxd_object_t* instance = NULL;

    when_null_trace(args, exit, ERROR, "Cannot get the data of the Set to remove");
    when_null_trace(object, exit, ERROR, "Cannot get Firewall.Set object");

    instance = amxd_object_get_instance(object, GET_CHAR(args, "name"), GET_UINT32(args, "index"));
    when_null_trace(instance, exit, ERROR, "Cannot get Set instance to delete");

    set = (set_t*) instance->priv;
    when_true_trace((set != NULL) && (set->refcount > 0), exit, ERROR, "Set is referenced by %d rules", set->refcount);

    if(object_get_enable(instance)) {
        set_delete(instance);
    }

    status = amxd_status_ok;
exit:
    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _set_instance_cleanup(amxd_object_t* object,
                                    UNUSED amxd_param_t* param,
                                    UNUSED amxd_action_t reason,
                                    UNUSED const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_invalid_action;

    FREE(object->priv);

    status = amxd_status_ok;

    FW_PROFILING_OUT();
    return status;
}

amxd_status_t _set_rule_instance_delete(amxd_object_t* object,
                                        UNUSED amxd_param_t* param,
                                        UNUSED amxd_action_t reason,
                                        UNUSED const amxc_var_t* const args,
                                        UNUSED amxc_var_t* const retval,
                                        UNUSED void* priv) {
    FW_PROFILING_IN(TRACE_DM_ACTION);
    amxd_status_t status = amxd_status_invalid_action;
    amxd_object_t* instance = NULL;

    instance = amxd_object_get_instance(object, GET_CHAR(args, "name"), GET_UINT32(args, "index"));

    update_entries(instance, true);

    status = amxd_status_ok;

    FW_PROFILING_OUT();
    return status;
}