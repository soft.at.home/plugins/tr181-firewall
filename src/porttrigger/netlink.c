/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE   // for struct tcphdr on musl Linux 5.4
#include "porttrigger/porttrigger.h"
#include "firewall.h" // for fw_get_parser
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <amxo/amxo.h>
#include <errno.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <linux/netfilter.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libmnl/libmnl.h>
#include <string.h>
#include <arpa/inet.h>
#include <linux/if_ether.h>

#define ME "porttrigger"

static struct nfq_handle* h = NULL;
static struct nfq_q_handle* g_qh = NULL;

static int cb(struct nfq_q_handle* qh, UNUSED struct nfgenmsg* nfmsg, struct nfq_data* nfad, UNUSED void* data) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    struct nfqnl_msg_packet_hdr* header = NULL;
    uint16_t protocol = 0;
    struct iphdr* ip_header = NULL;
    char source[IP_LEN];
    uint32_t destination_port = 0;

    header = nfq_get_msg_packet_hdr(nfad);

    protocol = ntohs(header->hw_protocol);
    SAH_TRACEZ_INFO(ME, "received packet (hw=0x%04x)\n", protocol);
    if(protocol != ETH_P_IP) {
        SAH_TRACEZ_WARNING(ME, "Only handle ipv4 for now");
        goto exit;
    }

    if(nfq_get_payload(nfad, (unsigned char**) &ip_header) < 0) {
        SAH_TRACEZ_WARNING(ME, "Missing payload");
        goto exit;
    }

    memset(&source, 0, sizeof(source));
    if(inet_ntop(AF_INET, (void*) &ip_header->saddr, source, sizeof(source)) == NULL) {
        SAH_TRACEZ_WARNING(ME, "Failed to convert source IP address (inet_ntop %d: %s)", errno, strerror(errno));
        goto exit;
    }

    if(ip_header->protocol == IPPROTO_TCP) {
        struct tcphdr* tcp_header = (struct tcphdr*) ((uint32_t*) ip_header + ip_header->ihl);
        destination_port = ntohs(tcp_header->dest);
    } else if(ip_header->protocol == IPPROTO_UDP) {
        struct udphdr* udp_hdr = (struct udphdr*) ((uint32_t*) ip_header + ip_header->ihl);
        destination_port = ntohs(udp_hdr->dest);
    } else {
        SAH_TRACEZ_WARNING(ME, "Unknown protocol %d", ip_header->protocol);
        goto exit;
    }

    porttrigger_handler(source, ip_header->protocol, destination_port);

exit:
    FW_PROFILING_OUT();
    return nfq_set_verdict(qh, ntohl(header->packet_id), NF_ACCEPT, 0, NULL);
}

static void netlink_read_callback(UNUSED int fd, UNUSED void* data) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int rv = -1;

    rv = recv(fd, buf, sizeof(buf), 0);
    if(rv < 0) {
        SAH_TRACEZ_WARNING(ME, "recv[%d]: %s", errno, strerror(errno));
        goto exit;
    } else if(rv > 0) {
        nfq_handle_packet(h, buf, rv);
    }
exit:
    FW_PROFILING_OUT();
    return;
}

bool netlink_start(uint32_t qnum) {
    bool retval = false;

    h = nfq_open();
    if(h == NULL) {
        SAH_TRACEZ_ERROR(ME, "nfq_open");
        goto out;
    }

    if(nfq_unbind_pf(h, AF_INET) < 0) {
        SAH_TRACEZ_ERROR(ME, "nfq_unbind_pf");
        goto out;
    }

    if(nfq_bind_pf(h, AF_INET) < 0) {
        SAH_TRACEZ_ERROR(ME, "nfq_bind_pf");
        goto out;
    }

    g_qh = nfq_create_queue(h, qnum, &cb, NULL);
    if(g_qh == NULL) {
        SAH_TRACEZ_ERROR(ME, "nfq_create_queue()\n");
        goto out;
    }

    if(nfq_set_mode(g_qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to set packet_copy mode\n");
        goto out;
    }

    amxo_connection_add(fw_get_parser(), nfq_fd(h), netlink_read_callback, NULL, AMXO_CUSTOM, NULL);

    retval = true;
out:
    if(!retval) {
        if(g_qh != NULL) {
            nfq_destroy_queue(g_qh);
            g_qh = NULL;
        }
        if(h != NULL) {
            nfq_close(h);
            h = NULL;
        }
    }
    return retval;
}

void netlink_stop(void) {
    if(h != NULL) {
        amxo_connection_remove(fw_get_parser(), nfq_fd(h));
    }
    if(g_qh != NULL) {
        nfq_destroy_queue(g_qh);
        g_qh = NULL;
    }
    if(h != NULL) {
        nfq_close(h);
        h = NULL;
    }
}
