/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "porttrigger/dm_porttrigger.h"
#include "porttrigger/porttrigger.h"
#include "protocols.h"
#include "ports.h"
#include "firewall.h" // for fw_get_dm
#include "init_cleanup.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>
#include <string.h>

#define ME "porttrigger"
#define PORTTRIGGER_DEFAULT_NFQ_QNUM 1

static bool porttrigger_is_active(porttrigger_t* port_trigger) {
    return (port_trigger->common.status == FIREWALL_ENABLED);
}

static const char* porttrigger_get_ipaddress(porttrigger_t* port_trigger) {
    amxd_object_t* object = amxd_object_findf(port_trigger->common.object, "Stats");
    return object_da_string(object, "IPAddress");
}

static void disable_all_porttrigger_rules(porttrigger_t* port_trigger) {
    amxd_object_t* templ = amxd_object_findf(port_trigger->common.object, ".Rule");

    amxd_object_for_each(instance, it, templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        rule_t* ptrule = NULL;

        if(instance->priv == NULL) {
            continue;
        }

        ptrule = (rule_t*) instance->priv;

        rule_set_enable(ptrule, false);
        ptrule->common.flags = FW_MODIFIED;
        rule_deactivate(ptrule);
    }

    fw_apply();
}

static bool porttrigger_set_ipaddress(porttrigger_t* port_trigger, const char* address) {
    amxd_trans_t trans;
    amxd_object_t* stats_object = NULL;
    bool retval = false;
    bool is_reset = STRING_EMPTY(address);

    stats_object = amxd_object_findf(port_trigger->common.object, "Stats");
    when_null(stats_object, exit);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, stats_object);
    amxd_trans_set_value(cstring_t, &trans, "IPAddress", address);

    amxd_trans_select_object(&trans, port_trigger->common.object);
    if(!is_reset) {
        amxc_ts_t now;
        amxc_ts_now(&now);
        now.nsec = 0;
        amxd_trans_set_value(amxc_ts_t, &trans, "ActivationDate", &now);
    } else {
        amxd_trans_set_value(cstring_t, &trans, "ActivationDate", "0001-01-01T00:00:00Z");
    }

    if(amxd_trans_apply(&trans, fw_get_dm()) != amxd_status_ok) {
        amxd_object_t* object = port_trigger->common.object;
        (void) object; // when tracing is disabled, variable is not used anymore
        SAH_TRACEZ_ERROR(ME, "PortTrig[%s] failed to set IPAddress[%s]", OBJECT_NAMED, address);
        amxd_trans_clean(&trans);
        goto exit;
    }
    amxd_trans_clean(&trans);

    retval = true;

exit:
    return retval;
}

static void time_to_deactivate_rules(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    porttrigger_t* port_trigger = (porttrigger_t*) priv;

    when_null(port_trigger, exit);

    disable_all_porttrigger_rules(port_trigger);
    porttrigger_set_ipaddress(port_trigger, "");

exit:
    return;
}

static porttrigger_t* find_porttrigger_instance(uint32_t protocol, uint32_t port) {
    amxd_object_t* templ = amxd_dm_findf(fw_get_dm(), "NAT.PortTrigger.");
    port_range_t port_range = {0};
    porttrigger_t* port_trigger = NULL;

    port_range.start = port;

    amxd_object_for_each(instance, it, templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);

        if(instance == NULL) {
            continue;
        }

        port_trigger = (porttrigger_t*) instance->priv;
        if(port_trigger == NULL) {
            continue;
        }

        if(!protocol_in_protocols(&port_trigger->common.protocols, protocol)) {
            continue;
        }

        if(port_range_in_ports(&(port_trigger->dst_ports), &port_range)) {
            goto match_found;
        }
    }

    port_trigger = NULL;

match_found:
    return port_trigger;
}

static bool porttrigger_refresh_timer(porttrigger_t* port_trigger) {
    amxd_object_t* object = port_trigger->common.object;
    uint32_t duration = amxd_object_get_value(uint32_t, object, "AutoDisableDuration", NULL);

    when_false_trace(duration > 0, exit, NOTICE, "PortTrigger[%s] is actived without time limit", OBJECT_NAMED);

    if(amxp_timer_start(port_trigger->timer, duration * 1000) != 0) {
        SAH_TRACEZ_ERROR(ME, "PortTrigger[%s] failed to refresh timer", OBJECT_NAMED);
        return false;
    }
    SAH_TRACEZ_NOTICE(ME, "PortTrigger[%s] is actived for another %u seconds", OBJECT_NAMED, duration);
exit:
    return true;
}

// assume source IP address is validated by the caller
void porttrigger_handler(const char* source, uint8_t protocol, uint32_t port) {
    porttrigger_t* port_trigger = find_porttrigger_instance(protocol, port);
    const char* owner = NULL;

    when_null(port_trigger, exit);
    when_str_empty(source, exit);

    owner = porttrigger_get_ipaddress(port_trigger);

    if(STRING_EMPTY(owner)) {
        if(!porttrigger_refresh_timer(port_trigger)) {
            goto exit;
        }
        if(!porttrigger_set_ipaddress(port_trigger, source)) {
            amxp_timer_stop(port_trigger->timer);
            goto exit;
        }
        porttrigger_rule_update(port_trigger, NULL);
    } else if(strcmp(source, owner) != 0) {
        amxd_object_t* object = port_trigger->common.object;
        (void) object; // when tracing is disabled, variable is not used anymore
        SAH_TRACEZ_WARNING(ME, "PortTrig[%s] src[%s] != owner[%s]", OBJECT_NAMED, source, owner);
        goto exit;
    } else if(!porttrigger_refresh_timer(port_trigger)) {
        goto exit;
    }

exit:
    return;
}

void porttrigger_nfqueue_stop(void) {
    netlink_stop();
}

bool porttrigger_nfqueue_start(void) {
    return netlink_start(get_nfqueue_num(PORTTRIGGER_DEFAULT_NFQ_QNUM));
}

// assumes rule_t* is never NULL
static void _porttrigger_rule_update(rule_t* ptrule, const char* destination, bool enabled) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    rule_set_destination(ptrule, destination);
    rule_set_enable(ptrule, enabled);
    ptrule->common.flags = FW_MODIFIED;
    rule_update(ptrule);
    FW_PROFILING_OUT();
}

void porttrigger_rule_update(porttrigger_t* port_trigger, rule_t* ptrule) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* object = port_trigger->common.object;
    bool enabled = port_trigger->common.enable;
    const char* destination = NULL;

    when_false_trace(enabled, exit, INFO, "PortTrigger[%s] is inactive, skipping update", OBJECT_NAMED);

    destination = porttrigger_get_ipaddress(port_trigger);
    when_str_empty(destination, exit); // port trigger might not be active atm

    if(ptrule == NULL) {
        amxd_object_t* templ = amxd_object_findf(object, ".Rule");

        amxd_object_for_each(instance, it, templ) {
            amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);

            ptrule = (rule_t*) instance->priv;
            if(ptrule == NULL) {
                continue;
            }
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
            update_rule_logs(ptrule, instance, NULL, NULL, NULL, false);
#endif //FIREWALL_LOGS
            _porttrigger_rule_update(ptrule, destination, enabled);
        }
    } else {
        _porttrigger_rule_update(ptrule, destination, enabled);
    }

exit:
    FW_PROFILING_OUT();
    return;
}

rule_t* porttrigger_rule_create(amxd_object_t* object) {
    rule_t* ptrule = NULL;

    ptrule = rule_create(object, true);
    when_null(ptrule, out);

    ptrule->class_type = FIREWALL_PORTTRIGGER;
    ptrule->target = TARGET_ACCEPT;

out:
    return ptrule;
}

static bool porttrigger_delete(porttrigger_t** port_trigger) {
    when_null(*port_trigger, exit);
    SAH_TRACEZ_INFO(ME, "Delete PortTrigger");

    amxc_llist_it_take(&(*port_trigger)->it);

    fw_folder_delete(&(*port_trigger)->nfqueue_folder);
    (*port_trigger)->nfqueue_folder = NULL;

    if(fw_commit(fw_rule_callback) != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_commit failed");
    }

    port_range_clean(&(*port_trigger)->dst_ports);

    amxp_timer_delete(&(*port_trigger)->timer);

    common_clean(&(*port_trigger)->common);

    FREE(*port_trigger);
exit:
    return true;
}

static void query_result_changed(firewall_interface_t* fwiface) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    common_t* common = (common_t*) fwiface->query_priv;

    when_true(common->flags != 0, exit); // already being configured
    common->flags = FW_MODIFIED;

    porttrigger_implement_changes();

exit:
    FW_PROFILING_OUT();
}

porttrigger_t* porttrigger_create(amxd_object_t* object) {
    porttrigger_t* port_trigger = NULL;

    port_trigger = (porttrigger_t*) calloc(1, sizeof(porttrigger_t));
    when_null(port_trigger, out);

    common_init(&port_trigger->common, object);

    if(fw_folder_new(&(port_trigger->nfqueue_folder)) != 0) {
        SAH_TRACEZ_ERROR(ME, "PortTrig[%s] failed to create folders", OBJECT_NAMED);
        porttrigger_delete(&port_trigger);
        goto out;
    }

    if(amxp_timer_new(&(port_trigger->timer), time_to_deactivate_rules, port_trigger) != 0) {
        SAH_TRACEZ_ERROR(ME, "PortTrig[%s] failed to create timer", OBJECT_NAMED);
        porttrigger_delete(&port_trigger);
        goto out;
    }

    port_trigger->common.src_fwiface.query_priv = &port_trigger->common;
    port_trigger->common.src_fwiface.query_result_changed = query_result_changed;

    object->priv = port_trigger;

out:
    return port_trigger;
}

static void porttrigger_reset_folder(porttrigger_t* port_trigger) {
    if(port_trigger == NULL) {
        return;
    }

    if(port_trigger->nfqueue_folder != NULL) {
        fw_folder_delete_rules(port_trigger->nfqueue_folder);
    }

    if(fw_commit(fw_rule_callback) != 0) {
        amxd_object_t* object = port_trigger->common.object;
        (void) object; // when tracing is disabled, variable is not used anymore
        SAH_TRACEZ_WARNING(ME, "PortTrig[%s] fw_commit failed", OBJECT_NAMED);
    }
}

static bool enable_porttrigger_filter(porttrigger_t* port_trigger, fw_folder_t* folder) {
    fw_rule_t* r = NULL;
    bool retval = false;
    bool waiting = false;
    amxd_object_t* object = NULL;
    firewall_interface_t* fwiface = NULL;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_null(port_trigger, out);
    when_null(folder, out);

    object = port_trigger->common.object;
    fwiface = &port_trigger->common.src_fwiface;

    waiting = (fwiface->netdev_required && STRING_EMPTY(fwiface->default_netdevname));
    when_true_trace(waiting, out, INFO, "PortTrig[%s] is waiting for interface[%s]", OBJECT_NAMED, amxc_string_get(&fwiface->netmodel_interface, 0));

    SAH_TRACEZ_INFO(ME, "PortTrig[%s] enable NFQUEUE", OBJECT_NAMED);
    fw_folder_set_feature(folder, FW_FEATURE_DESTINATION_PORT);
    fw_folder_set_feature(folder, FW_FEATURE_PROTOCOL);
    r = fw_folder_fetch_default_rule(folder);
    when_null_trace(r, out, ERROR, "PortTrig[%s] failed to fetch rule", OBJECT_NAMED);

    fw_rule_set_table(r, TABLE_FILTER);
    fw_rule_set_chain(r, fw_get_chain("porttrigger", "FORWARD_PortTrigger"));
    fw_rule_set_ipv4(r, true);
    fw_rule_set_in_interface(r, fwiface->default_netdevname);
    fw_rule_set_target_nfqueue_options(r, get_nfqueue_num(PORTTRIGGER_DEFAULT_NFQ_QNUM), 1, 0);
    fw_folder_push_rule(folder, r);

    when_false(folder_feature_set_protocol(folder, &(port_trigger->common.protocols)), out);
    when_false(folder_feature_set_destination_port(folder, &(port_trigger->dst_ports), false), out);

    retval = enable_rule(NULL, folder, &(port_trigger->common.status), true);
out:
    return retval;
}

static bool porttrigger_activate(porttrigger_t* port_trigger) {
    amxd_object_t* object = port_trigger->common.object;
    bool retval = false;
    (void) object; // when tracing is disabled, variable is not used anymore
    schedule_status_t schedule_status = port_trigger->common.schedule_status;

    if((schedule_status != SCHEDULE_STATUS_EMPTY) && (schedule_status != SCHEDULE_STATUS_ACTIVE)) {
        if(schedule_status == SCHEDULE_STATUS_INACTIVE) {
            port_trigger->common.status = FIREWALL_INACTIVE;
        } else if((schedule_status == SCHEDULE_STATUS_DISABLED) || (schedule_status == SCHEDULE_STATUS_STACK_DISABLED)) {
            port_trigger->common.status = FIREWALL_DISABLED;
        } else {
            port_trigger->common.status = FIREWALL_ERROR;
        }
        port_trigger->common.enable = false;
        retval = true;
        goto out;
    }

    if(port_trigger->common.enable) {
        if(!porttrigger_is_active(port_trigger)) {
            when_null_trace(amxc_var_get_first(&port_trigger->common.protocols), out, WARNING, "PortTrigger[%s] is missing protocols", OBJECT_NAMED);
            when_true_trace(amxc_llist_size(&port_trigger->dst_ports) == 0, out, WARNING, "PortTrigger[%s] is missing destination ports", OBJECT_NAMED);

            retval = enable_porttrigger_filter(port_trigger, port_trigger->nfqueue_folder);
            if(!retval) {
                SAH_TRACEZ_WARNING(ME, "PortTrigger[%s] filter rule cannot be enabled", OBJECT_NAMED);
                porttrigger_reset_folder(port_trigger);
                goto out;
            }
        } else {
            SAH_TRACEZ_NOTICE(ME, "PortTrigger[%s] is already active", OBJECT_NAMED);
        }
    } else {
        SAH_TRACEZ_INFO(ME, "PortTrigger[%s] is not active", OBJECT_NAMED);
    }
    retval = true;
out:
    if(!retval) {
        port_trigger->common.status = FIREWALL_ERROR;
    }
    common_set_status(&port_trigger->common, port_trigger->common.status);
    return retval;
}

static bool porttrigger_deactivate(porttrigger_t* port_trigger) {
    amxd_object_t* object = port_trigger->common.object;
    (void) object; // when tracing is disabled, variable is not used anymore

    when_false_trace(porttrigger_is_active(port_trigger), out, NOTICE, "PortTrig[%s] already inactive", OBJECT_NAMED);

    SAH_TRACEZ_NOTICE(ME, "PortTrig[%s] is about to be deactivated", OBJECT_NAMED);

    disable_all_porttrigger_rules(port_trigger);
    porttrigger_reset_folder(port_trigger);
    porttrigger_set_ipaddress(port_trigger, "");
    common_set_status(&port_trigger->common, FIREWALL_DISABLED);
out:
    return true;
}

bool porttrigger_implement_changes(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* porttrigger_templ = amxd_dm_findf(fw_get_dm(), "NAT.PortTrigger.");

    amxd_object_for_each(instance, it, porttrigger_templ) {
        amxd_object_t* instance = amxc_llist_it_get_data(it, amxd_object_t, it);
        porttrigger_t* port_trigger = (instance != NULL) ? (porttrigger_t*) instance->priv : NULL;

        if(port_trigger == NULL) {
            continue;
        }

        if(port_trigger->common.flags & FW_DELETED) {
            porttrigger_deactivate(port_trigger);
            porttrigger_delete(&port_trigger);
            continue;
        }
        if((port_trigger->common.flags & FW_NEW) || (port_trigger->common.flags & FW_MODIFIED)) {
            if(port_trigger->common.flags & FW_MODIFIED) {
                porttrigger_deactivate(port_trigger);
            }
            porttrigger_activate(port_trigger);
            port_trigger->common.flags = 0;
        }
    }
    if(fw_apply() != 0) {
        SAH_TRACEZ_WARNING(ME, "fw_apply failed");
    }
    FW_PROFILING_OUT();
    return true;
}
