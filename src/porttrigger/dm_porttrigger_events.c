/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "porttrigger/dm_porttrigger.h"
#include "porttrigger/porttrigger.h"
#include "protocols.h"
#include "ports.h"
#include "logs/logs.h"
#include "init_cleanup.h"
#include "firewall.h"
#include "profiling/profiling.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxc/amxc_macros.h>
#include <string.h>

#define ME "porttrigger"

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
/**
   @brief
   Callback function to update the LOG.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Pointer to the common internal structure.
 */
static void update_rule_log_info_cb(UNUSED const char* const sig_name,
                                    const amxc_var_t* const data,
                                    void* const priv) {
    FW_PROFILING_IN(TRACE_CALLBACK);
    log_t* log = (log_t*) priv;
    amxd_object_t* object = NULL;
    rule_t* rule = NULL;

    when_null_trace(log, exit, ERROR, "Log struct is NULL");
    object = log_get_object(log);
    when_null_trace(object, exit, ERROR, "Object is NULL");
    rule = (rule_t*) object->priv;
    when_null_trace(rule, exit, ERROR, "PortTrigger.Rule[%s] is NULL", OBJECT_NAMED);

    if(update_rule_logs(NULL, object, NULL, data, log, false)) {
        rule->common.flags = FW_MODIFIED;
        rule_update(rule);
    }

exit:
    FW_PROFILING_OUT();
    return;
}
#endif //FIREWALL_LOGS

static void schedule_updated(schedule_status_t new_status, void* userdata) {
    porttrigger_t* port_trigger = NULL;

    when_null(userdata, exit);
    port_trigger = (porttrigger_t*) userdata;
    when_null(port_trigger, exit);

    when_true(port_trigger->common.status == FIREWALL_ENABLED && new_status == SCHEDULE_STATUS_ACTIVE, exit);
    when_true(port_trigger->common.status == FIREWALL_DISABLED && (new_status == SCHEDULE_STATUS_DISABLED || new_status == SCHEDULE_STATUS_STACK_DISABLED), exit);

    if(port_trigger->common.schedule_status != new_status) {
        port_trigger->common.enable = true;               // Enable Porttrigger for now, will be disabled again in porttrigger_implement_changes when needed
        port_trigger->common.schedule_status = new_status;
        when_true(port_trigger->common.flags != 0, exit); // already being configured
        port_trigger->common.flags = FW_MODIFIED;
        porttrigger_implement_changes();
    }

exit:
    return;
}

static bool porttrigger_listen_schedule_updates(porttrigger_t* port_trigger, const char* path) {
    bool enable = false;
    bool res = false;
    amxd_object_t* object = NULL;

    when_null_trace(port_trigger, exit, ERROR, "port_trigger is NULL");

    object = port_trigger->common.object;
    when_null_trace(object, exit, ERROR, "object is NULL");
    enable = amxd_object_get_value(bool, object, "Enable", NULL);
    when_false(enable, exit);

    schedule_close(&(port_trigger->common.schedule));
    port_trigger->common.schedule = NULL;

    if(enable && !STRING_EMPTY(path)) {
        port_trigger->common.schedule = schedule_open(path, schedule_updated, (void*) port_trigger);
        when_null_trace(port_trigger->common.schedule, exit, ERROR, "Rule[%s] Could not start listening for Schedule updates", OBJECT_NAMED);
    } else if(enable && STRING_EMPTY(path)) {
        port_trigger->common.enable = true;
        port_trigger->common.schedule_status = SCHEDULE_STATUS_EMPTY;
        when_true(port_trigger->common.flags != 0, exit); // already being configured
        port_trigger->common.flags = FW_MODIFIED;
        porttrigger_implement_changes();
    }

exit:
    return res;
}

static void set_interface(porttrigger_t* port_trigger, const char* interface) {
    firewall_interface_t* fwiface = &port_trigger->common.src_fwiface;
    if(!STRING_EMPTY(interface)) {
        open_ip_address_query(interface, IPv4, "ipv4", fwiface);
    } else {
        init_firewall_interface(fwiface);
    }
}

static bool porttrigger_init(amxd_object_t* object) {
    porttrigger_t* port_trigger = NULL;
    uint32_t start = 0;
    uint32_t end = 0;
    bool ret = false;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    port_trigger = porttrigger_create(object);
    when_null(port_trigger, exit);

    port_trigger->common.enable = amxd_object_get_value(bool, object, "Enable", NULL);
    protocol_from_string(&port_trigger->common.protocols, object_da_string(object, "Protocol"));
    start = amxd_object_get_value(uint32_t, object, "Port", NULL);
    end = amxd_object_get_value(uint32_t, object, "PortEndRange", NULL);
    port_range_from_int(&port_trigger->dst_ports, start, end);
    set_interface(port_trigger, object_da_string(object, "Interface"));

    porttrigger_listen_schedule_updates(port_trigger, object_da_string(object, "ScheduleRef"));

    ret = true;
exit:
    return ret;
}

void _porttrigger_added(UNUSED const char* const sig_name,
                        const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));

    when_null(object, exit);
    when_false(porttrigger_init(object), exit);

    porttrigger_implement_changes();
exit:
    FW_PROFILING_OUT();
    return;
}

void _porttrigger_changed(UNUSED const char* const sig_name,
                          const amxc_var_t* const event_data,
                          UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    porttrigger_t* port_trigger = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    bool update_schedule = false;

    when_null(object, exit);
    when_null(object->priv, exit);
    port_trigger = (porttrigger_t*) object->priv;

    port_trigger->common.flags = FW_MODIFIED;

    var = GET_ARG(params, "Interface");
    if(var != NULL) {
        set_interface(port_trigger, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Port");
    var2 = GET_ARG(params, "PortEndRange");
    if((var != NULL) || (var2 != NULL)) {
        uint32_t start = 0;
        uint32_t end = 0;
        start = (var != NULL) ? GET_UINT32(var, "to") : amxd_object_get_value(uint32_t, object, "Port", NULL);
        end = (var2 != NULL) ? GET_UINT32(var2, "to") : amxd_object_get_value(uint32_t, object, "PortEndRange", NULL);
        port_range_from_int(&port_trigger->dst_ports, start, end);
    }

    var = GET_ARG(params, "Protocol");
    if(var != NULL) {
        protocol_from_string(&port_trigger->common.protocols, GET_CHAR(var, "to"));
    }

    var = GET_ARG(params, "Enable");
    if(var != NULL) {
        update_schedule = GET_BOOL(var, "to");
        port_trigger->common.enable = GET_BOOL(var, "to");
    }

    if(update_schedule) {
        char* schedule_ref = amxd_object_get_value(cstring_t, object, "ScheduleRef", NULL);
        porttrigger_listen_schedule_updates(port_trigger, schedule_ref);
        free(schedule_ref);
    }

    porttrigger_implement_changes();

exit:
    FW_PROFILING_OUT();
    return;
}

void _porttrigger_schedule_changed(UNUSED const char* const sig_name,
                                   const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), data);
    amxc_var_t* params = NULL;
    amxc_var_t* var = NULL;
    porttrigger_t* port_trigger = NULL;
    const char* new_schedule_ref = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_null(object->priv, exit);
    when_false(amxd_object_get_value(bool, object, "Enable", NULL), exit); // Not applicable if PortTrigger is disabled

    params = GET_ARG(data, "parameters");
    when_null_trace(params, exit, ERROR, "Params is NULL");

    port_trigger = (porttrigger_t*) object->priv;
    // don't set rule->common.flags so porttrigger_listen_schedule_updates can decide to (de)activate the rule

    var = GET_ARG(params, "ScheduleRef");
    new_schedule_ref = GET_CHAR(var, "to");

    SAH_TRACEZ_INFO(ME, "Changing PortTrigger ScheduleRef from '%s' to '%s'", GET_CHAR(var, "from"), new_schedule_ref);

    porttrigger_listen_schedule_updates(port_trigger, new_schedule_ref);

exit:
    FW_PROFILING_OUT();
    return;
}

static bool porttrigger_rule_init(amxd_object_t* object, porttrigger_t* port_trigger) {
    rule_t* ptrule = NULL;
    uint32_t start = 0;
    uint32_t end = 0;
    bool ret = true;

    when_null_trace(object, exit, ERROR, "Object is NULL");
    when_false(object->priv == NULL, exit);

    ptrule = porttrigger_rule_create(object);
    when_null(ptrule, exit);

    start = amxd_object_get_value(uint32_t, object, "Port", NULL);
    end = amxd_object_get_value(uint32_t, object, "PortEndRange", NULL);
    rule_set_dst_port(ptrule, start, end);
    rule_set_protocols(ptrule, object_da_string(object, "Protocol"));

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    init_rule_logs(object, ptrule, &port_trigger->common.src_fwiface, NULL, false, update_rule_log_info_cb);
#endif //FIREWALL_LOGS

    porttrigger_rule_update(port_trigger, ptrule);
    ret = true;
exit:
    return ret;
}

void _porttrigger_rule_added(UNUSED const char* const sig_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* templ = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* object = amxd_object_get_instance(templ, NULL, GET_UINT32(event_data, "index"));
    amxd_object_t* porttrigger_obj = NULL;
    porttrigger_t* port_trigger = NULL;

    when_null_trace(object, exit, ERROR, "Object is NULL");

    porttrigger_obj = amxd_object_findf(templ, ".^");
    when_null(porttrigger_obj, exit);
    when_null(porttrigger_obj->priv, exit);
    port_trigger = (porttrigger_t*) porttrigger_obj->priv;

    porttrigger_rule_init(object, port_trigger);
exit:
    FW_PROFILING_OUT();
    return;
}

void _porttrigger_rule_changed(UNUSED const char* const sig_name,
                               const amxc_var_t* const event_data,
                               UNUSED void* const priv) {
    FW_PROFILING_IN(TRACE_DM_EVENT);
    amxd_object_t* object = amxd_dm_signal_get_object(fw_get_dm(), event_data);
    amxd_object_t* porttrigger_obj = NULL;
    amxc_var_t* params = GET_ARG(event_data, "parameters");
    amxc_var_t* var = NULL;
    amxc_var_t* var2 = NULL;
    porttrigger_t* port_trigger = NULL;
    rule_t* ptrule = NULL;

    when_null(object, exit);
    when_null(object->priv, exit);

    porttrigger_obj = amxd_object_findf(object, ".^.^");
    when_null(porttrigger_obj, exit);
    when_null(porttrigger_obj->priv, exit);

    port_trigger = (porttrigger_t*) porttrigger_obj->priv;
    ptrule = (rule_t*) object->priv;

    ptrule->common.flags = FW_MODIFIED;

    var = GET_ARG(params, "Port");
    var2 = GET_ARG(params, "PortEndRange");
    if((var != NULL) || (var2 != NULL)) {
        uint32_t start = 0;
        uint32_t end = 0;
        start = (var != NULL) ? GET_UINT32(var, "to") : amxd_object_get_value(uint32_t, object, "Port", NULL);
        end = (var2 != NULL) ? GET_UINT32(var2, "to") : amxd_object_get_value(uint32_t, object, "PortEndRange", NULL);
        rule_set_dst_port(ptrule, start, end);
    }

    var = GET_ARG(params, "Protocol");
    if(var != NULL) {
        rule_set_protocols(ptrule, GET_CHAR(var, "to"));
    }

#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    update_rule_logs(ptrule, NULL, event_data, NULL, NULL, false);
#endif //FIREWALL_LOGS
    porttrigger_rule_update(port_trigger, ptrule);
exit:
    FW_PROFILING_OUT();
    return;
}

static int dm_porttrigger_rule_init(UNUSED amxd_object_t* templ, amxd_object_t* object, void* priv) {
    porttrigger_t* port_trigger = (porttrigger_t*) priv;
    porttrigger_rule_init(object, port_trigger);
    return amxd_status_ok;
}

static int dm_porttrigger_init(UNUSED amxd_object_t* templ, amxd_object_t* object, UNUSED void* priv) {
    porttrigger_init(object);
    porttrigger_implement_changes();

    amxd_object_for_all(object, ".Rule.*.", dm_porttrigger_rule_init, object->priv);
    return amxd_status_ok;
}

void dm_porttriggers_init(void) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_object_t* nat = amxd_dm_findf(fw_get_dm(), "NAT.");
    amxd_object_t* porttrigger = amxd_object_findf(nat, ".PortTrigger.");
    uint32_t max_number = 0;
    amxc_string_t buf;
    amxc_string_init(&buf, 0);
    amxc_string_setf(&buf, "%sMaxPortTriggerNumberOfEntries", fw_get_vendor_prefix());
    max_number = amxc_var_constcast(uint32_t, amxd_object_get_param_value(nat, amxc_string_get(&buf, 0)));
    amxc_string_clean(&buf);
    amxd_object_set_max_instances(porttrigger, max_number);
    amxd_object_for_all(nat, ".PortTrigger.*.", dm_porttrigger_init, NULL);
    FW_PROFILING_OUT();
}
