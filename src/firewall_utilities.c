/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "firewall_utilities.h"
#include "firewall.h" // for fw_get_dm
#include "profiling/profiling.h"
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <regex.h>
#include <string.h>

#define ME "firewall"

void trans_add_optional_string(amxd_trans_t* trans, const char* key, const char* value) {
    when_str_empty(value, skip);
    amxd_trans_set_value(cstring_t, trans, key, value);
skip:
    return;
}

static amxd_object_t* find_instance_by_alias(amxd_object_t* templ, const char* alias) {
    amxd_object_t* instance = NULL;
    when_str_empty(alias, exit);
    instance = amxd_object_findf(templ, "[Alias=='%s'].", alias);
exit:
    return instance;
}

amxd_status_t delete_by_alias(amxd_object_t* templ, amxc_var_t* args) {
    amxd_dm_t* dm = fw_get_dm();
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    const char* alias = GET_CHAR(args, "alias");

    if(find_instance_by_alias(templ, alias) != NULL) {
        amxd_trans_init(&trans);
        amxd_trans_select_object(&trans, templ);
        amxd_trans_del_inst(&trans, 0, alias);
        if(amxd_trans_apply(&trans, dm) != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to delete instance with alias %s", alias);
            goto exit;
        }
        amxd_trans_clean(&trans);
    }

    status = amxd_status_ok;
exit:
    return status;
}

static int add_to_list(UNUSED amxd_object_t* templ, amxd_object_t* service, UNUSED void* priv) {
    amxc_var_t* list = (amxc_var_t*) priv;
    amxc_var_t* list_item = NULL;
    when_null(priv, exit);
    list_item = amxc_var_add_new(list);
    when_null(list_item, exit);
    amxd_object_get_params(service, list_item, amxd_dm_access_public);
exit:
    return 1;
}

amxd_status_t list_by_alias(amxd_object_t* templ, amxc_var_t* args, amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    const char* alias = GET_CHAR(args, "alias");

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    if((alias != NULL) && (*alias != 0)) {
        amxd_object_t* instance = amxd_object_findf(templ, "[Alias=='%s'].", alias);
        if(instance == NULL) {
            status = amxd_status_object_not_found;
            goto exit;
        }
        if(add_to_list(templ, instance, (void*) ret) != 1) {
            goto exit;
        }
    } else {
        amxd_object_for_all(templ, "*", add_to_list, ret);
    }

    status = amxd_status_ok;
exit:
    return status;
}

amxd_status_t set_and_return_params(amxd_object_t* templ, amxc_var_t* args, amxc_var_t* ret, set_and_return_params_cb set_params) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxd_dm_t* dm = fw_get_dm();
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* instance = NULL;
    const char* alias = GET_CHAR(args, "alias");

    amxd_trans_init(&trans);
    instance = find_instance_by_alias(templ, alias);
    if(instance != NULL) {
        amxd_trans_select_object(&trans, instance);
    } else {
        amxd_trans_select_object(&trans, templ);
        amxd_trans_add_inst(&trans, 0, NULL);
        trans_add_optional_string(&trans, "Alias", alias);
    }

    if(set_params(&trans, args) == false) {
        goto exit;
    }

    if(amxd_trans_apply(&trans, dm) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to set instance with alias %s", alias);
        status = amxd_status_invalid_function_argument;
        goto exit;
    }

    if(instance == NULL) {
        // in case alias was not given as function argument
        amxc_var_for_each(var, (&trans.retvals)) {
            alias = GET_CHAR(var, "name");
            instance = find_instance_by_alias(templ, alias);
            if(instance != NULL) {
                break;
            }
        }
    }

    if(instance == NULL) {
        SAH_TRACEZ_ERROR(ME, "Instance not found");
        amxc_var_set_type(ret, AMXC_VAR_ID_HTABLE);  // not reachable if transaction is ok, set type anyway
        goto exit;
    }

    amxd_object_get_params(instance, ret, amxd_dm_access_public);
    status = amxd_status_ok;
exit:
    amxd_trans_clean(&trans);
    FW_PROFILING_OUT();
    return status;
}

void _set_status(amxd_object_t* object, const char* key, status_t status) {
    amxd_trans_t trans;
    const char* status_str = NULL;
    FW_PROFILING_IN(TRACE_INTERNAL);

    when_null(amxd_object_get_param_def(object, key), out);

    if(status == FIREWALL_DISABLED) {
        status_str = "Disabled";
    } else if(status == FIREWALL_ENABLED) {
        status_str = "Enabled";
    } else if(status == FIREWALL_INACTIVE) {
        status_str = "Inactive";
    } else if(status == FIREWALL_ERROR) {
        status_str = "Error";
    } else if(status == FIREWALL_ERROR_MISCONFIG) {
        status_str = "Error_Misconfigured";
    } else if(status == FIREWALL_PENDING) {
        status_str = "Pending";
    }
    when_null(status_str, out);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(cstring_t, &trans, key, status_str);
    if(amxd_trans_apply(&trans, fw_get_dm()) != 0) {
        SAH_TRACEZ_NOTICE(ME, "Transaction failed");
    }

    amxd_trans_clean(&trans);
out:
    FW_PROFILING_OUT();
    return;
}

void set_status(amxd_object_t* object, status_t status) {
    _set_status(object, "Status", status);
}

bool status_is_enabled(const char* status) {
    return (status != NULL) && (strcmp(status, "Enabled") == 0);
}

const char* object_da_string(amxd_object_t* const object, const char* name) {
    const char* res = NULL;
    res = GET_CHAR(amxd_object_get_param_value(object, name), NULL);
    return res == NULL ? "" : res;
}

bool object_get_enable(amxd_object_t* object) {
    return amxd_object_get_value(bool, object, "Enable", NULL);
}

bool is_var_equal(const amxc_var_t* const var1, const amxc_var_t* const var2) {
    int cmp_result = -1;
    int rv = -1;
    rv = amxc_var_compare(var1, var2, &cmp_result);
    return (rv == 0) && (cmp_result == 0);
}

ipversion_t object_get_ipversion(amxd_object_t* object) {
    ipversion_t version = IPvUnknown;
    const amxc_var_t* value = NULL;

    value = amxd_object_get_param_value(object, "IPVersion");
    when_null_trace(value, exit, ERROR, "%s has no parameter IPVersion", amxd_object_get_name(object, AMXD_OBJECT_NAMED));

    version = int_to_ipversion(GET_INT32(value, NULL));

exit:
    return version;
}

bool validate_regexp(const char* regexp_str, const char* value) {
    bool status = false;
    regex_t regexp;

    int rv = regcomp(&regexp, regexp_str, REG_EXTENDED | REG_NOSUB);
    when_true_status(rv != 0, exit, status = amxd_status_invalid_arg);

    if(regexec(&regexp, value, 0, NULL, 0) == 0) {
        status = true;
    }

    regfree(&regexp);

exit:
    return status;
}

const char* int_to_char(uint32_t num) {
    static char buffer[64] = {0};
    snprintf(buffer, sizeof(buffer) - 1, "%u", num);
    return buffer;
}

bool is_string_in_amxc_var(const char* string, const amxc_var_t* var) {
    bool present = false;
    amxc_var_t list;

    amxc_var_init(&list);

    when_null(var, exit);
    when_failed(amxc_var_convert(&list, var, AMXC_VAR_ID_LIST), exit);

    amxc_var_for_each(value, &list) {
        if(strcmp(string, GET_CHAR(value, NULL)) == 0) {
            present = true;
            break;
        }
    }

exit:
    amxc_var_clean(&list);
    return present;
}

static void set_endtime(amxd_object_t* object, bool force_update_endtime) {
    amxc_var_t param;
    uint32_t duration = 0;
    const amxc_ts_t* endtime = NULL;
    amxc_ts_t default_ts;
    amxd_trans_t trans;

    amxc_var_init(&param);
    amxd_trans_init(&trans);

    when_failed_trace(amxd_object_get_param(object, "LeaseDuration", &param), exit, INFO, "Object[%s] doesn't have parameter LeaseDuration", OBJECT_NAMED);
    duration = GET_UINT32(&param, NULL);

    when_failed_trace(amxd_object_get_param(object, "EndTime", &param), exit, INFO, "Object[%s] doesn't have parameter EndTime", OBJECT_NAMED);
    endtime = amxc_var_constcast(amxc_ts_t, &param); // don't call another amxd_object_get_param with param or endtime will change

    if(force_update_endtime ||
       ((amxc_ts_parse(&default_ts, "0001-01-01T00:00:00Z", 20) == 0) && (amxc_ts_compare(endtime, &default_ts) == 0))) {
        amxc_ts_t now;
        int rv = 0;

        if(duration > 0) {
            int sec = 0;
            when_false_trace(amxc_ts_now(&now) == 0, exit, ERROR, "Object[%s] could not get time", OBJECT_NAMED);
            sec = now.sec;
            now.sec += duration;
            while(!amxc_ts_is_valid(&now) && (now.sec >= sec)) {
                now.sec -= 5;
            }
            when_false_trace(amxc_ts_is_valid(&now), exit, ERROR, "Object[%s] could not set EndTime", OBJECT_NAMED);
        } else {
            when_false_trace(amxc_ts_parse(&now, "9999-12-31T23:59:59Z", 20) == 0, exit, ERROR, "Object[%s] could not set EndTime", OBJECT_NAMED);
        }

        amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
        amxd_trans_select_object(&trans, object);
        amxd_trans_set_value(amxc_ts_t, &trans, "EndTime", &now);
        rv = amxd_trans_apply(&trans, fw_get_dm());
        when_failed_trace(rv, exit, ERROR, "Object[%s] transaction failed: %d", OBJECT_NAMED, rv);
    }
exit:
    amxc_var_clean(&param);
    amxd_trans_clean(&trans);
}

void set_lease_duration(amxd_object_t** object, amxp_timer_t* timer, uint32_t* current_duration, bool force_update_endtime) {
    FW_PROFILING_IN(TRACE_INTERNAL);
    amxc_var_t param;
    uint32_t duration = 0;
    const char* name = amxd_object_get_name(*object, AMXD_OBJECT_NAMED);

    amxc_var_init(&param);

    when_failed_trace(amxd_object_get_param(*object, "LeaseDuration", &param), exit, INFO, "Object[%s] doesn't have parameter LeaseDuration", name);
    duration = GET_UINT32(&param, NULL);

    when_true(duration == *current_duration, exit);
    *current_duration = duration;

    set_endtime(*object, force_update_endtime);

    if(duration > 0) {
        amxc_ts_t now;
        const amxc_ts_t* endtime = NULL;
        int rv = 0;

        when_failed_trace(amxd_object_get_param(*object, "EndTime", &param), exit, INFO, "Object[%s] doesn't have parameter EndTime", name);
        endtime = amxc_var_constcast(amxc_ts_t, &param);
        when_null_trace(endtime, exit, ERROR, "Object[%s] can't convert EndTime", name);

        amxc_ts_now(&now);

        rv = amxc_ts_compare(endtime, &now);
        if(rv != 1) {
            amxd_trans_t trans;
            amxd_trans_init(&trans);
            amxd_trans_select_object(&trans, amxd_object_get_parent(*object));
            amxd_trans_del_inst(&trans, amxd_object_get_index(*object), NULL);
            SAH_TRACEZ_INFO(ME, "Object[%s] will be removed because of EndTime", name);
            rv = amxd_trans_apply(&trans, fw_get_dm());
            if(rv != amxd_status_ok) {
                SAH_TRACEZ_ERROR(ME, "Object[%s] transaction failed: %d", name, rv);
            }
            amxd_trans_clean(&trans);
            // avoid these dangling pointers after object was deleted
            *object = NULL;
            name = NULL;
            timer = NULL;
            current_duration = NULL;
        } else {
            uint32_t remaining = 0;
            when_false_trace(endtime->sec > now.sec, exit, ERROR, "Object[%s] failed to calculate remaining LeaseDuration", name);
            remaining = endtime->sec - now.sec;

            amxp_timer_start(timer, remaining * 1000);
            SAH_TRACEZ_INFO(ME, "Object[%s] remaining LeaseDuration %dsec", name, remaining);
        }
    } else {
        amxp_timer_stop(timer);
    }

exit:
    amxc_var_clean(&param);
    FW_PROFILING_OUT();
}