%define {
    select NAT {
        /**
         * NAT settings for an associated IP Interface on which NAT is enabled.
         * @version 1.0
         */
        %persistent object InterfaceSetting[] {
            counted with InterfaceSettingNumberOfEntries;
            on action destroy call interface_setting_instance_cleanup;

            /**
             * A non-volatile unique key used to reference this instance.
             * @version 1.0
             */
            %persistent %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * Enables or disables the InterfaceSetting entry, indicating if NAT is enabled for the referenced IP Interface instance.
             * @version 1.0
             */
            %persistent bool Enable = false;

            /**
             * The status of this entry. Enumeration of:
             * - Disabled
             * - Enabled
             * - Enabled_NATForcedDisabled (NAT enabled but forced by a third party to be operationally disabled)
             * - Enabled_PortMappingDisabled (NAT enabled but port mapping has been operationally disabled by a third party)
             * - Error_Misconfigured (Indicates that a necessary configuration value is undefined or invalid)
             * - Error
             * @version 1.0
             */
            %read-only string Status = "Disabled" {
                on action validate call check_enum ["Disabled", "Enabled", "Enabled_NATForcedDisabled", "Enabled_PortMappingDisabled", "Error_Misconfigured", "Error"];
            }

            /**
             * Reference to a row in any of the following interface tables representing a WAN interface:
             * - Device.IP.Interface.
             * - IP.Interface.
             * - Device.Logical.Interface.
             * - Logical.Interface.
             * @version 1.0
             */
            %persistent %unique string Interface {
                on action validate call check_maximum_length 256;
                on action validate call matches_regexp "${intf_regex}";
                default "";
            }

            /**
             * List of (LAN) interfaces on which NAT is applied.
             * @version 1.0
             */
            %persistent object '${prefix_}NATInterface'[] {
                counted with '${prefix_}NATInterfaceNumberOfEntries';
                on action destroy call nat_interface_instance_cleanup;

                /**
                 * A non-volatile unique key used to reference this instance.
                 * @version 1.0
                 */
                %persistent %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * The value of the Interface can be a reference to an IP Interface:
                 * In this case all the possible address ranges of the interface must be configured for NAT.
                 * Or the value of the Interface can be Device.IP.Interface.i.IPv4Address.i.
                 * In this case, only the ip address range of the matching IPv4Address instance must use NAT. Other traffic form that interface must pass un NAT'd.
                 * @version 1.0
                 */
                %persistent %unique string Interface {
                    on action validate call check_maximum_length 256;
                    on action validate call matches_regexp "${intf_regex_ipv4}";
                    default "";
                }
            }
        }
    }
}

%populate {
    on event "dm:object-changed" call interface_setting_changed
        filter 'path matches "^NAT\.InterfaceSetting\.[0-9]+\.$"';

    on event "dm:instance-added" call interface_setting_added
        filter 'path == "NAT.InterfaceSetting."';

    on event "dm:instance-added" call natinterface_added
        filter 'path matches "^NAT\.InterfaceSetting\.[0-9]+\.[a-zA-Z0-9_-]*NATInterface\.$"';

    on event "dm:object-changed" call natinterface_changed
        filter 'path matches "^NAT\.InterfaceSetting\.[0-9]+\.[a-zA-Z0-9_-]*NATInterface\.[0-9]+\.$"';
}