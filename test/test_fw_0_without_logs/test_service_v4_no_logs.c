/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "firewall.h"
#include "service/service.h"

#include <amxc/amxc.h>
#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_service_no_logs.h"

static void a_populate_filter_rule(fw_rule_t* filter_rule1) {
    assert_int_equal(fw_rule_set_table(filter_rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_rule1, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(filter_rule1, true), 0);
    assert_int_equal(fw_rule_set_in_interface(filter_rule1, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(filter_rule1, 6), 0);
    assert_int_equal(fw_rule_set_source(filter_rule1, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(filter_rule1, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(filter_rule1, true), 0);
}

static void a_populate_nat_rule(fw_rule_t* nat_rule1) {
    assert_int_equal(fw_rule_set_table(nat_rule1, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(nat_rule1, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(nat_rule1, true), 0);
    assert_int_equal(fw_rule_set_in_interface(nat_rule1, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(nat_rule1, 6), 0);
    assert_int_equal(fw_rule_set_source(nat_rule1, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(nat_rule1, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(nat_rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(nat_rule1, true), 0);
}

static void b_populate_filter_rule(fw_rule_t* filter_rule2) {
    assert_int_equal(fw_rule_set_table(filter_rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(filter_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(filter_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(filter_rule2, 6), 0);
    assert_int_equal(fw_rule_set_source(filter_rule2, "10.0.2.100/32"), 0);
    assert_int_equal(fw_rule_set_destination_port(filter_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(filter_rule2, true), 0);
}

static void b_populate_nat_rule(fw_rule_t* nat_rule2) {
    assert_int_equal(fw_rule_set_table(nat_rule2, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(nat_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(nat_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(nat_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(nat_rule2, 6), 0);
    assert_int_equal(fw_rule_set_source(nat_rule2, "10.0.2.100/32"), 0);
    assert_int_equal(fw_rule_set_destination_port(nat_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(nat_rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(nat_rule2, true), 0);
}

static void c_populate_filter_rule(fw_rule_t* filter_rule1) {
    assert_int_equal(fw_rule_set_table(filter_rule1, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_rule1, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(filter_rule1, true), 0);
    assert_int_equal(fw_rule_set_in_interface(filter_rule1, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(filter_rule1, 6), 0);
    assert_int_equal(fw_rule_set_source(filter_rule1, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(filter_rule1, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(filter_rule1, true), 0);
}

static void c_populate_nat_rule(fw_rule_t* nat_rule1) {
    assert_int_equal(fw_rule_set_table(nat_rule1, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(nat_rule1, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(nat_rule1, true), 0);
    assert_int_equal(fw_rule_set_in_interface(nat_rule1, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(nat_rule1, 6), 0);
    assert_int_equal(fw_rule_set_source(nat_rule1, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(nat_rule1, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(nat_rule1, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(nat_rule1, true), 0);
}

static void d_populate_filter_rule(fw_rule_t* filter_rule2) {
    assert_int_equal(fw_rule_set_table(filter_rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(filter_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(filter_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(filter_rule2, 17), 0);
    assert_int_equal(fw_rule_set_source(filter_rule2, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(filter_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(filter_rule2, true), 0);
}

static void d_populate_nat_rule(fw_rule_t* nat_rule2) {
    assert_int_equal(fw_rule_set_table(nat_rule2, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(nat_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(nat_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(nat_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(nat_rule2, 17), 0);
    assert_int_equal(fw_rule_set_source(nat_rule2, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(nat_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(nat_rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(nat_rule2, true), 0);
}

static void f_populate_filter_rule(fw_rule_t* filter_rule2) {
    assert_int_equal(fw_rule_set_table(filter_rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(filter_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(filter_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(filter_rule2, 6), 0);
    assert_int_equal(fw_rule_set_source(filter_rule2, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(filter_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_rule2, FW_RULE_POLICY_REJECT), 0);
    assert_int_equal(fw_rule_set_enabled(filter_rule2, true), 0);
}

static void f_populate_nat_rule(fw_rule_t* nat_rule2) {
    assert_int_equal(fw_rule_set_table(nat_rule2, TEST_TABLE_NAT), 0);
    assert_int_equal(fw_rule_set_chain(nat_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(nat_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(nat_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_protocol(nat_rule2, 6), 0);
    assert_int_equal(fw_rule_set_source(nat_rule2, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_port(nat_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(nat_rule2, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(nat_rule2, true), 0);
}

static void g_populate_filter_rule(fw_rule_t* filter_rule2) {
    assert_int_equal(fw_rule_set_table(filter_rule2, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(filter_rule2, "INPUT_Services"), 0);
    assert_int_equal(fw_rule_set_ipv4(filter_rule2, true), 0);
    assert_int_equal(fw_rule_set_in_interface(filter_rule2, "eth0"), 0);
    assert_int_equal(fw_rule_set_source(filter_rule2, "10.0.1.0/24"), 0);
    assert_int_equal(fw_rule_set_protocol(filter_rule2, 6), 0);
    assert_int_equal(fw_rule_set_destination_port(filter_rule2, 8000), 0);
    assert_int_equal(fw_rule_set_target_policy(filter_rule2, FW_RULE_POLICY_DROP), 0);
    assert_int_equal(fw_rule_set_enabled(filter_rule2, true), 0);
}

void test_add_service_v4_with_double_prefix(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add service\n");
    amxd_trans_select_pathf(&trans, "Firewall.Service.");
    amxd_trans_add_inst(&trans, 0, "service-1");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.1.0/24,10.0.2.100");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "8000");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule(a_populate_filter_rule, 1);
    expect_fw_replace_rule(b_populate_filter_rule, 2);
    expect_fw_replace_rule(a_populate_nat_rule, 1);
    expect_fw_replace_rule(b_populate_nat_rule, 2);
    handle_events();

    service = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_non_null(service);
    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_remove_service_v4_with_double_prefix(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove service\n");
    amxd_trans_select_pathf(&trans, "Firewall.Service.");
    amxd_trans_del_inst(&trans, 0, "service-1");

    expect_fw_delete_rule(a_populate_filter_rule, 1);
    expect_fw_delete_rule(b_populate_filter_rule, 1);
    expect_fw_delete_rule(a_populate_nat_rule, 1);
    expect_fw_delete_rule(b_populate_nat_rule, 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    handle_events();

    service = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_null(service);

    amxc_var_clean(&params);
}

void test_add_service_v4(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service = NULL;

    expect_query_wan_netdevname();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add service\n");
    amxd_trans_select_pathf(&trans, "Firewall.Service.");
    amxd_trans_add_inst(&trans, 0, "service-1");
    amxd_trans_set_value(cstring_t, &trans, "SourcePrefixes", "10.0.1.0/24");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.2."); // wan
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    amxd_trans_set_value(cstring_t, &trans, "DestPort", "8000");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule(c_populate_filter_rule, 1);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    handle_events();

    service = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_non_null(service);
    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_service_v4_change_protocol(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    service = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_non_null(service);

    print_message("change Protocol to 6,17\n");
    amxd_trans_select_object(&trans, service);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6,17");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(c_populate_filter_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_filter_rule, 1);
    expect_fw_replace_rule(d_populate_filter_rule, 2);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(d_populate_nat_rule, 2);
    handle_events();

    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change Protocol to 6\n");
    amxd_trans_select_object(&trans, service);
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "6");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(c_populate_filter_rule, 1);
    expect_fw_delete_rule(d_populate_filter_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_delete_rule(d_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_filter_rule, 1);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_service_v4_change_ipversion(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service_obj = NULL;

    expect_query_wan_netdevname();

    service_obj = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_non_null(service_obj);

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("change IPVersion to 6\n");
    amxd_trans_select_object(&trans, service_obj);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule(c_populate_filter_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    expect_query_wan_netdevname();

    print_message("change IPVersion to 4\n");
    amxd_trans_select_object(&trans, service_obj);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(c_populate_filter_rule, 1);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service_obj, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_service_v4_change_action(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    service = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_non_null(service);

    print_message("change Action to Reject\n");
    amxd_trans_select_object(&trans, service);
    amxd_trans_set_value(cstring_t, &trans, "Action", "Reject");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(c_populate_filter_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(f_populate_filter_rule, 1);
    expect_fw_replace_rule(f_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change Action to Accept\n");
    amxd_trans_select_object(&trans, service);
    amxd_trans_set_value(cstring_t, &trans, "Action", "Accept");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(f_populate_filter_rule, 1);
    expect_fw_delete_rule(f_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_filter_rule, 1);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change Action to Drop\n");
    amxd_trans_select_object(&trans, service);
    amxd_trans_set_value(cstring_t, &trans, "Action", "Drop");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(c_populate_filter_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    expect_fw_replace_rule(g_populate_filter_rule, 1);
    expect_fw_replace_rule(f_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("change Action to Accept\n");
    amxd_trans_select_object(&trans, service);
    amxd_trans_set_value(cstring_t, &trans, "Action", "Accept");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(g_populate_filter_rule, 1);
    expect_fw_delete_rule(f_populate_nat_rule, 1);
    expect_fw_replace_rule(c_populate_filter_rule, 1);
    expect_fw_replace_rule(c_populate_nat_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(service, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_remove_service_v4(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* service = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove service\n");
    amxd_trans_select_pathf(&trans, "Firewall.Service.");
    amxd_trans_del_inst(&trans, 0, "service-1");
    expect_fw_delete_rule(c_populate_filter_rule, 1);
    expect_fw_delete_rule(c_populate_nat_rule, 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    service = amxd_dm_findf(dm, "Firewall.Service.service-1.");
    assert_null(service);

    amxc_var_clean(&params);
}