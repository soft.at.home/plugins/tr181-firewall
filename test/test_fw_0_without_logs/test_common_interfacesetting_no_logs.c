/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <math.h>

#include "firewall.h"

#include <amxc/amxc.h>
#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include <fwinterface/interface.h>
#include "interfacesetting/interfacesetting.h"
#include "interfacesetting/spoofing_protection.h"
#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/dm_spoofing_protection.h"
#include "test_common_interfacesetting_no_logs.h"

static uint32_t max_instance_index = 0;

amxo_parser_t* get_parser(void) {
    return amxut_bus_parser();
}

amxd_dm_t* get_dm(void) {
    return amxut_bus_dm();
}

typedef struct {
    const char* interface;
    const char* out_interface;
    bool excluded_interface;
    const char* table;
    const char* chain;
    bool ipv4;
    int protocol;
    int icmp_type;
    const char* address;
    bool excluded_address;
    const char* netmask;
    fw_rule_policy_t policy;
    int port;
    int port_range_max;
} ut_ifsetting_t;

static ut_ifsetting_t ut_ifsetting;

void ut_ifsetting_populate_rule(fw_rule_t* rule) {
    const char* rule_interface = NULL;

    if(ut_ifsetting.interface == NULL) {
        rule_interface = "";
    } else {
        rule_interface = ut_ifsetting.interface;
    }

    assert_int_equal(fw_rule_set_table(rule, ut_ifsetting.table), 0);
    assert_int_equal(fw_rule_set_chain(rule, ut_ifsetting.chain), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, ut_ifsetting.ipv4), 0);
    if(ut_ifsetting.ipv4) {
        assert_int_equal(fw_rule_set_ipv6(rule, false), 0);
    } else {
        assert_int_equal(fw_rule_set_ipv6(rule, true), 0);
    }
    assert_int_equal(fw_rule_set_ipv4(rule, ut_ifsetting.ipv4), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, rule_interface), 0);
    assert_int_equal(fw_rule_set_in_interface_excluded(rule, ut_ifsetting.excluded_interface), 0);
    if(!STRING_EMPTY(ut_ifsetting.out_interface)) {
        assert_int_equal(fw_rule_set_out_interface(rule, ut_ifsetting.out_interface), 0);
    }
    if(ut_ifsetting.protocol >= 0) {
        assert_int_equal(fw_rule_set_protocol(rule, ut_ifsetting.protocol), 0);
    }
    if(ut_ifsetting.port >= 0) {
        assert_int_equal(fw_rule_set_destination_port(rule, ut_ifsetting.port), 0);
    }
    if(ut_ifsetting.port_range_max >= 0) {
        assert_int_equal(fw_rule_set_destination_port_range_max(rule, ut_ifsetting.port_range_max), 0);
    }
    if(ut_ifsetting.icmp_type >= 0) {
        if(ut_ifsetting.ipv4) {
            assert_int_equal(fw_rule_set_icmp_type(rule, ut_ifsetting.icmp_type), 0);
        } else {
            assert_int_equal(fw_rule_set_icmpv6_type(rule, ut_ifsetting.icmp_type), 0);
        }
    }
    if(!STRING_EMPTY(ut_ifsetting.address)) {
        assert_int_equal(fw_rule_set_source(rule, ut_ifsetting.address), 0);
        assert_int_equal(fw_rule_set_source_excluded(rule, ut_ifsetting.excluded_address), 0);
    }
    if(!STRING_EMPTY(ut_ifsetting.netmask)) {
        assert_int_equal(fw_rule_set_source_mask(rule, ut_ifsetting.netmask), 0);
    }
    if(ut_ifsetting.policy != FW_RULE_POLICY_LAST) {
        assert_int_equal(fw_rule_set_target_policy(rule, ut_ifsetting.policy), 0);
    }
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

void populate_create_rule(const char* interface, const char* out_interface, bool excluded_interface,
                          const char* table, const char* chain, bool ipv4, int protocol, int icmp_type,
                          const char* address, bool excluded_address, const char* netmask, fw_rule_policy_t policy) {
    memset(&ut_ifsetting, 0, sizeof(ut_ifsetting_t));
    ut_ifsetting.interface = interface;
    ut_ifsetting.out_interface = out_interface;
    ut_ifsetting.excluded_interface = excluded_interface;
    ut_ifsetting.table = table;
    ut_ifsetting.chain = chain;
    ut_ifsetting.ipv4 = ipv4;
    ut_ifsetting.icmp_type = icmp_type;
    ut_ifsetting.address = address;
    ut_ifsetting.excluded_address = excluded_address;
    ut_ifsetting.netmask = netmask;
    ut_ifsetting.policy = policy;
    if((protocol == -1) && ((strcmp(IFSETTING_CHAIN_IPV4, chain) == 0) || (strcmp(IFSETTING_CHAIN_IPV6, chain) == 0))) {
        ut_ifsetting.protocol = 17;
        ut_ifsetting.port = 33434;
        ut_ifsetting.port_range_max = 33523;
    } else {
        ut_ifsetting.protocol = protocol;
    }
}

void populate_delete_rule(const char* interface, const char* out_interface, bool excluded_interface,
                          const char* table, const char* chain, bool ipv4, int protocol, int icmp_type,
                          const char* address, bool excluded_address, const char* netmask, fw_rule_policy_t policy) {
    memset(&ut_ifsetting, 0, sizeof(ut_ifsetting_t));
    ut_ifsetting.interface = interface;
    ut_ifsetting.out_interface = out_interface;
    ut_ifsetting.excluded_interface = excluded_interface;
    ut_ifsetting.table = table;
    ut_ifsetting.chain = chain;
    ut_ifsetting.ipv4 = ipv4;
    ut_ifsetting.icmp_type = icmp_type;
    ut_ifsetting.address = address;
    ut_ifsetting.excluded_address = excluded_address;
    ut_ifsetting.netmask = netmask;
    ut_ifsetting.policy = policy;
    if((protocol == -1) && ((strcmp(IFSETTING_CHAIN_IPV4, chain) == 0) || (strcmp(IFSETTING_CHAIN_IPV6, chain) == 0))) {
        ut_ifsetting.protocol = 17;
        ut_ifsetting.port = 33434;
        ut_ifsetting.port_range_max = 33523;
    } else {
        ut_ifsetting.protocol = protocol;
    }
}

int add_ifsetting(const char* alias, const char* interface, const char* icmp_req, const char* spoofing, bool stealth_mode, bool accept_udp_tr, bool passthrough_icmpv6) {
    int index = 0;
    amxd_trans_t trans;

    assert_non_null(interface);

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.");
    amxd_trans_add_inst(&trans, 0, alias);
    amxd_trans_set_value(cstring_t, &trans, "Interface", interface);
    amxd_trans_set_value(cstring_t, &trans, "AcceptICMPEchoRequest", icmp_req);
    amxd_trans_set_value(bool, &trans, "StealthMode", stealth_mode);
    amxd_trans_set_value(bool, &trans, "AcceptUDPTraceroute", accept_udp_tr);
    amxd_trans_set_value(bool, &trans, "PassthroughICMPv6EchoRequest", passthrough_icmpv6);
    amxd_trans_set_value(cstring_t, &trans, "SpoofingProtection", spoofing);
    if(amxd_trans_apply(&trans, get_dm()) != 0) {
        goto exit;
    }
    handle_events();
    max_instance_index++;
    index = max_instance_index;
exit:
    amxd_trans_clean(&trans);
    return index;
}

int modify_ifsetting(uint32_t instance_index, const char* icmp_req, const char* spoofing, bool stealth_mode, bool accept_udp_tr, bool passthrough_icmpv6) {
    int rv = -1;
    amxd_trans_t trans;
    amxd_object_t* ifsetting_obj = NULL;
    amxd_object_t* ifsetting_instance = NULL;

    ifsetting_obj = amxd_dm_findf(get_dm(), "Firewall.InterfaceSetting.");
    assert_non_null(ifsetting_obj);

    ifsetting_instance = amxd_object_get_instance(ifsetting_obj, NULL, instance_index);
    assert_non_null(ifsetting_instance);

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_select_object(&trans, ifsetting_instance);
    amxd_trans_set_value(cstring_t, &trans, "AcceptICMPEchoRequest", icmp_req);
    amxd_trans_set_value(bool, &trans, "StealthMode", stealth_mode);
    amxd_trans_set_value(bool, &trans, "AcceptUDPTraceroute", accept_udp_tr);
    amxd_trans_set_value(bool, &trans, "PassthroughICMPv6EchoRequest", passthrough_icmpv6);
    amxd_trans_set_value(cstring_t, &trans, "SpoofingProtection", spoofing);
    if(amxd_trans_apply(&trans, get_dm()) != 0) {
        goto exit;
    }
    handle_events();

    rv = 0;
exit:
    amxd_trans_clean(&trans);
    return rv;
}

void delete_ifsetting(const char* alias) {
    amxd_trans_t trans;

    assert_int_equal(amxd_trans_init(&trans), 0);
    amxd_trans_select_pathf(&trans, "Firewall.InterfaceSetting.");
    amxd_trans_del_inst(&trans, 0, alias);
    assert_int_equal(amxd_trans_apply(&trans, get_dm()), 0);
    amxd_trans_clean(&trans);
    handle_events();
}

int modify_netmodel_query(const char* query_path, const char* ip) {
    amxc_var_t result;
    amxc_string_t result_str;

    amxc_string_init(&result_str, 0);
    amxc_string_setf(&result_str, "[{\"Address\":\"%s\",\"Family\":\"ipv4\",\"NetDevName\":\"br-lan\",\"PrefixLen\":\"24\",\"Scope\":\"global\"}"
                     ",{\"Address\":\"192.168.3.1\",\"Family\":\"ipv4\",\"NetDevName\":\"br-lan\",\"PrefixLen\":\"24\",\"Scope\":\"global\"}]", ip);

    amxc_var_init(&result);
    amxc_var_set(jstring_t, &result, amxc_string_get(&result_str, 0));
    assert_int_equal(amxc_var_cast(&result, AMXC_VAR_ID_LIST), 0);

    mocknm_change_query_result(query_path, &result);
    handle_events();

    amxc_var_clean(&result);
    amxc_string_clean(&result_str);
    return 0;
}

void common_test_teardown(void) {
    max_instance_index = 0;
}
