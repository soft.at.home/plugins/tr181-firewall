/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#include "fwrules_mock.h"

extern int __real_fw_commit(fw_callback_t cb);

static int counter = 0;

typedef struct {
    amxc_llist_it_t it;
    int counter_index;
} expected_fw_commit_t;

amxc_llist_t* expected_to_fail = NULL;

static void commit_llist_delete(amxc_llist_it_t* it) {
    expected_fw_commit_t* failed_commit = amxc_container_of(it, expected_fw_commit_t, it);
    free(failed_commit);
}

void reset_fw_commit_counter(void) {
    counter = 0;
}

void reset_fw_commit_expected(void) {
    amxc_llist_delete(&expected_to_fail, commit_llist_delete);
    expected_to_fail = NULL;
}

void add_expected_fw_commit(int counter_index) {
    expected_fw_commit_t* failed_commit = NULL;

    failed_commit = calloc(1, sizeof(*failed_commit));
    failed_commit->counter_index = counter_index;

    if(expected_to_fail == NULL) {
        amxc_llist_new(&expected_to_fail);
    }
    amxc_llist_append(expected_to_fail, &failed_commit->it);
}

int __wrap_fw_commit(fw_callback_t cb) {
    expected_fw_commit_t* failed_commit = NULL;
    int ret = 1;

    counter++;
    when_null(expected_to_fail, performs_commit);
    failed_commit = amxc_container_of(amxc_llist_get_first(expected_to_fail), expected_fw_commit_t, it);
    assert_non_null(failed_commit); //If this fails it means the reset_fw_commit_expected() was probably not called at the right place.

    if(counter == failed_commit->counter_index) {
        failed_commit = amxc_container_of(amxc_llist_take_first(expected_to_fail), expected_fw_commit_t, it);
        free(failed_commit);
        goto exit;
    }

performs_commit:
    ret = __real_fw_commit(cb);
exit:
    if(amxc_llist_size(expected_to_fail) == 0) {
        reset_fw_commit_expected();
    }
    return ret;
}

int get_amount_of_expected_commits(void) {
    return amxc_llist_size(expected_to_fail);
}