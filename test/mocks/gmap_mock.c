/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include "mock.h"
#include "gmap_mock.h"

amxc_llist_t* query_list = NULL;

/**
   @brief
   Copy the data from the object to the variant.

   @param[in] object Object.
   @param[out] var Variant.
 */
static void obj_data_to_var(amxd_object_t* object, amxc_var_t* var) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    uint32_t index = 1;
    uint32_t inst_count = 0;
    amxc_string_t* str_index;
    amxd_object_type_t obj_type;

    amxc_string_new(&str_index, 0);

    assert_non_null(object);
    assert_non_null(var);

    obj_type = amxd_object_get_type(object);
    if(obj_type == amxd_object_template) {
        inst_count = amxd_object_get_instance_count(object);
        if(inst_count > 0) {
            amxc_llist_for_each(it, (&object->instances)) {
                tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
                index = amxd_object_get_index(tmp_obj);
                amxc_string_setf(str_index, "%u", index);
                tmp_var = amxc_var_add_key(amxc_htable_t, var, str_index->buffer, NULL);
                obj_data_to_var(tmp_obj, tmp_var);
                tmp_obj = NULL;
            }
        }
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            if((amxc_var_type_of(tmp_var) != AMXC_VAR_ID_HTABLE) && \
               (amxc_var_type_of(tmp_var) != AMXC_VAR_ID_LIST)) {
                amxc_var_set_pathf(var, tmp_var, AMXC_VAR_FLAG_AUTO_ADD | AMXC_VAR_FLAG_COPY, "%s", param_name);
            }

            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            tmp_var = amxc_var_add_key(amxc_htable_t, var, obj_name, NULL);
            obj_data_to_var(tmp_obj, tmp_var);
            tmp_obj = NULL;
        }
    }
    amxc_string_delete(&str_index);
}

static void call_function(gmap_query_t* gmap_query, amxd_object_t* obj_device, gmap_query_action_t action) {
    char* key = NULL;
    amxc_var_t* device = NULL;

    assert_non_null(obj_device);
    key = amxd_object_get_value(cstring_t, obj_device, "Alias", NULL);
    amxc_var_new(&device);
    amxc_var_set_type(device, AMXC_VAR_ID_HTABLE);
    obj_data_to_var(obj_device, device);
    gmap_query->fn(gmap_query, key, device, action);
    amxc_var_delete(&device);
    free(key);
}

static amxc_string_t* get_token(amxc_string_t* expression, amxc_string_t* parameter) {
    int pos = amxc_string_search(expression, " ", 0);
    amxc_string_t* token = NULL;
    amxc_string_t* buffer = NULL;

    amxc_string_new(&buffer, 0);

    when_true(pos <= 0, exit);

    amxc_string_copy(buffer, expression);
    amxc_string_remove_at(buffer, pos, UINT32_MAX);
    amxc_string_replace(buffer, " ", "", UINT32_MAX);
    amxc_string_replace(buffer, "\"", "", UINT32_MAX);
    amxc_string_remove_at(expression, 0, pos + 1);

    amxc_string_copy(parameter, buffer);
    amxc_string_replace(parameter, " ", "", UINT32_MAX);
    amxc_string_replace(parameter, "\"", "", UINT32_MAX);
    when_str_empty(amxc_string_get(parameter, 0), exit);

    if(amxc_string_search(parameter, "not", 0) >= 0) {
        pos = amxc_string_search(expression, " ", 0);
        when_true(pos == 0, exit);
        if(pos < 0) {
            pos = amxc_string_text_length(expression);
        }

        amxc_string_copy(buffer, expression);
        amxc_string_remove_at(buffer, pos, UINT32_MAX);
        amxc_string_replace(buffer, " ", "", UINT32_MAX);
        amxc_string_replace(buffer, "\"", "", UINT32_MAX);
        amxc_string_remove_at(expression, 0, pos + 1);

        if((amxc_string_search(buffer, "not", 0) >= 0) || (amxc_string_search(buffer, ".", 0) >= 0) || (amxc_string_search(buffer, "matches", 0) >= 0)) {
            goto exit;
        }

        amxc_string_new(&token, 0);
        amxc_string_copy(token, buffer);

    } else if(amxc_string_search(parameter, ".", 0) >= 0) {
        amxc_string_replace(parameter, ".", "", UINT32_MAX);
        pos = amxc_string_search(expression, " ", 0);
        when_true(pos <= 0, exit);

        amxc_string_copy(buffer, expression);
        amxc_string_remove_at(buffer, pos, UINT32_MAX);
        amxc_string_replace(buffer, " ", "", UINT32_MAX);
        amxc_string_replace(buffer, "\"", "", UINT32_MAX);
        amxc_string_remove_at(expression, 0, pos + 1);

        if((amxc_string_search(buffer, "not", 0) >= 0) || (amxc_string_search(buffer, ".", 0) >= 0)) {
            goto exit;
        }

        if(amxc_string_search(buffer, "matches", 0) < 0) {
            goto exit;
        }

        pos = amxc_string_search(expression, " ", 0);
        when_true(pos == 0, exit);
        if(pos < 0) {
            pos = amxc_string_text_length(expression);
        }

        amxc_string_copy(buffer, expression);
        amxc_string_remove_at(buffer, pos, UINT32_MAX);
        amxc_string_replace(buffer, " ", "", UINT32_MAX);
        amxc_string_replace(buffer, "\"", "", UINT32_MAX);
        amxc_string_remove_at(expression, 0, pos + 1);

        if((amxc_string_search(buffer, "not", 0) >= 0) || (amxc_string_search(buffer, ".", 0) >= 0) || (amxc_string_search(buffer, "matches", 0) >= 0)) {
            goto exit;
        }

        amxc_string_new(&token, 0);
        amxc_string_copy(token, buffer);

    } else {
        amxc_string_new(&token, 0);
        amxc_string_copy(token, parameter);
        amxc_string_clean(parameter);
    }

exit:
    amxc_string_delete(&buffer);
    return token;
}

static void parse_expression(amxc_string_t* expression, amxc_llist_t* tags_in, amxc_llist_t* tags_not, amxc_var_t* htable) {
    amxc_string_t* token = NULL;
    amxc_string_t* parameter = NULL;

    amxc_string_new(&parameter, 0);
    amxc_var_set_type(htable, AMXC_VAR_ID_HTABLE);

    //this mock only supports "and" in the expression
    amxc_string_replace(expression, " and", "", UINT32_MAX);

    token = get_token(expression, parameter);
    while(token != NULL) {
        if(amxc_string_is_empty(parameter)) {
            amxc_llist_append(tags_in, &token->it);
        } else if(amxc_string_search(parameter, "not", 0) >= 0) {
            amxc_llist_append(tags_not, &token->it);
        } else {
            amxc_var_add_key(cstring_t, htable, amxc_string_get(parameter, 0), amxc_string_get(token, 0));
            amxc_string_delete(&token);
        }
        token = get_token(expression, parameter);
    }

    amxc_string_delete(&parameter);
}

static void check_all_devices(gmap_query_t* gmap_query, amxc_string_t* expression) {
    amxd_object_t* templ_device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.");
    amxd_object_t* obj_device = NULL;
    amxc_llist_t* tags_in = NULL;
    amxc_llist_t* tags_not = NULL;
    amxc_var_t* htable = NULL;

    amxc_llist_new(&tags_in);
    amxc_llist_new(&tags_not);
    amxc_var_new(&htable);

    parse_expression(expression, tags_in, tags_not, htable);

    amxd_object_for_each(instance, it, templ_device) {
        amxc_string_t* list_string = NULL;
        char* tags = NULL;
        bool matched = true;

        obj_device = amxc_container_of(it, amxd_object_t, it);
        tags = amxd_object_get_value(cstring_t, obj_device, "Tags", NULL);

        amxc_llist_for_each(it, tags_in) {
            list_string = amxc_container_of(it, amxc_string_t, it);
            if(strstr(tags, amxc_string_get(list_string, 0)) == NULL) {
                matched = false;
            }
        }

        amxc_llist_for_each(it, tags_not) {
            list_string = amxc_container_of(it, amxc_string_t, it);
            if(strstr(tags, amxc_string_get(list_string, 0)) != NULL) {
                matched = false;
            }
        }

        amxc_var_for_each(var, htable) {
            const char* key = amxc_var_key(var);
            const char* match_value = GET_CHAR(var, NULL);
            char* value = amxd_object_get_value(cstring_t, obj_device, key, NULL);
            if(strcmp(match_value, value) != 0) {
                matched = false;
            }
            free(value);
        }

        if(!matched) {
            free(tags);
            continue;
        }

        call_function(gmap_query, obj_device, gmap_query_expression_start_matching);
        free(tags);
    }

    amxc_llist_delete(&tags_in, amxc_string_list_it_free);
    amxc_llist_delete(&tags_not, amxc_string_list_it_free);
    amxc_var_delete(&htable);
}

gmap_query_t* gmap_mock_get_latest_query(void) {
    amxc_llist_it_t* it = NULL;
    it = amxc_llist_get_last(query_list);
    return amxc_llist_it_get_data(it, gmap_query_t, it);
}

gmap_query_t* gmap_query_open_ext(const char* expression, UNUSED const char* name, gmap_query_cb_t fn, void* user_data) {
    gmap_query_t* gmap_query = NULL;
    amxc_string_t expression_str;

    gmap_query = (gmap_query_t*) calloc(1, sizeof(gmap_query_t));
    gmap_query->expression = strdup(expression);
    gmap_query->data = user_data;
    gmap_query->fn = fn;

    if(query_list == NULL) {
        amxc_llist_new(&query_list);
    }
    amxc_llist_append(query_list, &gmap_query->it);

    if((strstr(expression, "VendorClassID") != NULL) ||
       (strstr(expression, "mac") != NULL)) {
        amxc_var_t* device_params = mock_type(amxc_var_t*);
        amxc_var_t* alias = amxc_var_take_key(device_params, "Alias");
        assert_non_null(alias);

        gmap_query->fn(gmap_query, GET_CHAR(alias, NULL), device_params, gmap_query_expression_start_matching);
        amxc_var_delete(&device_params);
        amxc_var_delete(&alias);
        goto exit;
    }

    amxc_string_init(&expression_str, 0);
    amxc_string_setf(&expression_str, "%s", expression);

    check_all_devices(gmap_query, &expression_str);

    amxc_string_clean(&expression_str);
exit:
    return gmap_query;
}

void gmap_query_close(gmap_query_t* query) {
    when_null(query, exit);

    amxc_llist_it_take(&query->it);
    if(amxc_llist_is_empty(query_list)) {
        amxc_llist_delete(&query_list, NULL);
    }

    free((char*) query->expression);
    free(query);

exit:
    return;
}

static bool is_query_match(gmap_query_t* query, const char* tags, const char* mac, const char* hostname) {
    amxc_string_t expression_str;
    amxc_llist_t* tags_in = NULL;
    amxc_llist_t* tags_not = NULL;
    amxc_var_t* htable = NULL;
    bool matched = true;
    amxc_string_t* list_string = NULL;
    const char* mac_from_exp = NULL;
    const char* hostname_from_exp = NULL;

    amxc_llist_new(&tags_in);
    amxc_llist_new(&tags_not);
    amxc_var_new(&htable);
    amxc_string_init(&expression_str, 0);
    amxc_string_setf(&expression_str, "%s", query->expression);

    parse_expression(&expression_str, tags_in, tags_not, htable);

    amxc_llist_for_each(it, tags_in) {
        list_string = amxc_container_of(it, amxc_string_t, it);
        if(strstr(tags, amxc_string_get(list_string, 0)) == NULL) {
            matched = false;
        }
    }

    amxc_llist_for_each(it, tags_not) {
        list_string = amxc_container_of(it, amxc_string_t, it);
        if(strstr(tags, amxc_string_get(list_string, 0)) != NULL) {
            matched = false;
        }
    }

    mac_from_exp = GET_CHAR(htable, "PhysAddress");
    if(!STRING_EMPTY(mac_from_exp)) {
        if(strcmp(mac_from_exp, mac) != 0) {
            matched = false;
        }
    }

    hostname_from_exp = GET_CHAR(htable, "Name");
    if(!STRING_EMPTY(hostname_from_exp)) {
        if(strcmp(hostname_from_exp, hostname) != 0) {
            matched = false;
        }
    }

    amxc_llist_delete(&tags_in, amxc_string_list_it_free);
    amxc_llist_delete(&tags_not, amxc_string_list_it_free);
    amxc_var_delete(&htable);
    amxc_string_clean(&expression_str);

    return matched;
}

void _gmap_mock_query(UNUSED const char* const sig_name,
                      const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    amxd_object_t* obj_device = amxd_dm_signal_get_object(amxut_bus_dm(), event_data);
    const amxc_var_t* parameters = NULL;
    const amxc_var_t* tmp_var = NULL;
    const char* old_tags = NULL;
    const char* new_tags = NULL;
    const char* old_name = NULL;
    const char* new_name = NULL;
    const char* old_mac = NULL;
    const char* new_mac = NULL;
    char* tags = NULL;
    char* name = NULL;
    char* mac = NULL;
    bool old_matched = false;
    bool new_matched = false;
    gmap_query_action_t action = gmap_query_device_updated;

    assert_non_null(event_data);
    assert_non_null(obj_device);
    parameters = GET_ARG(event_data, "parameters");
    assert_non_null(parameters);

    tmp_var = GET_ARG(parameters, "Tags");
    if(tmp_var != NULL) {
        new_tags = GET_CHAR(tmp_var, "to");
        old_tags = GET_CHAR(tmp_var, "from");
    } else {
        tags = amxd_object_get_value(cstring_t, obj_device, "Tags", NULL);
        new_tags = tags;
        old_tags = tags;
    }

    tmp_var = GET_ARG(parameters, "PhysAddress");
    if(tmp_var != NULL) {
        new_mac = GET_CHAR(tmp_var, "to");
        old_mac = GET_CHAR(tmp_var, "from");
    } else {
        mac = amxd_object_get_value(cstring_t, obj_device, "PhysAddress", NULL);
        new_mac = mac;
        old_mac = mac;
    }

    tmp_var = GET_ARG(parameters, "Name");
    if(tmp_var != NULL) {
        new_name = GET_CHAR(tmp_var, "to");
        old_name = GET_CHAR(tmp_var, "from");
    } else {
        name = amxd_object_get_value(cstring_t, obj_device, "Name", NULL);
        new_name = name;
        old_name = name;
    }

    amxc_llist_for_each(it, query_list) {
        gmap_query_t* gmap_query = amxc_container_of(it, gmap_query_t, it);
        old_matched = is_query_match(gmap_query, old_tags, old_mac, old_name);
        new_matched = is_query_match(gmap_query, new_tags, new_mac, new_name);

        if(!old_matched && new_matched) {
            action = gmap_query_expression_start_matching;
        } else if(old_matched && !new_matched) {
            action = gmap_query_expression_stop_matching;
        }

        call_function(gmap_query, obj_device, action);
    }

    free(tags);
    free(mac);
    free(name);

    return;
}