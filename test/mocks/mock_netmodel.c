/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxo/amxo.h>
#include <amxb/amxb_be_intf.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb.h>

#include "mock_netmodel.h"

static amxc_set_t mocknm_iface_flags; // maybe track flags per object?
static amxc_htable_t* mocknm_queries = NULL;

typedef struct {
    amxc_htable_it_t it;
    amxc_llist_t subscribers;
} mocknm_query_t;

typedef struct {
    amxc_llist_it_t it;
    netmodel_callback_t handler;
    void* userdata;
} mocknm_query_handler_t;

amxc_set_t* mocknm_get_current_flags(void) {
    return &mocknm_iface_flags;
}

static void mocknm_query_handler_it_delete(amxc_llist_it_t* it) {
    mocknm_query_handler_t* handler = amxc_llist_it_get_data(it, mocknm_query_handler_t, it);
    free(handler);
}

static char* get_description(const char* path, amxc_var_t* args) {
    const char* class = GET_CHAR(args, "class");
    const char* flag = GET_CHAR(args, "flag");
    const char* traverse = GET_CHAR(args, "traverse");
    const char* name = GET_CHAR(args, "name"); // could be NULL if not in args (depends on class)
    amxc_string_t description;
    char* buf = NULL;

    amxc_string_init(&description, 0);

    if(strcmp(class, "getAddrs") == 0) {
        amxc_string_setf(&description, "%sgetAddrs(flag=\"%s\", traverse=\"%s\")", path, flag, traverse);
    } else if((strcmp(class, "getFirstParameter") == 0) || (strcmp(class, "getParameters") == 0)) {
        amxc_string_setf(&description, "%s%s(flag=\"%s\", name=\"%s\", traverse=\"%s\")", path, class, flag, name, traverse);
    } else {
        assert_string_equal(class, ""); // assert failed? add class to previous if
    }

    buf = amxc_string_take_buffer(&description);
    assert_non_null(buf);

    amxc_string_clean(&description);
    return buf;
}

static netmodel_query_t* insert_handler(const char* interface, netmodel_callback_t nm_handler, void* userdata, amxc_var_t* args) {
    mocknm_query_handler_t* handler = NULL;
    char* key = NULL;
    amxc_htable_it_t* query_it = NULL;
    mocknm_query_t* query = NULL;

    key = get_description(interface, args);
    assert_non_null(key);

    query_it = amxc_htable_get(mocknm_queries, key);
    if(query_it == NULL) {
        query = (mocknm_query_t*) calloc(1, sizeof(mocknm_query_t));
        assert_non_null(query);
        amxc_llist_init(&query->subscribers);
        assert_int_equal(amxc_htable_insert(mocknm_queries, key, &query->it), 0);
    } else {
        query = amxc_htable_it_get_data(query_it, mocknm_query_t, it);
    }

    handler = (mocknm_query_handler_t*) calloc(1, sizeof(mocknm_query_handler_t));
    assert_non_null(handler);
    handler->handler = nm_handler;
    handler->userdata = userdata;
    assert_int_equal(amxc_llist_append(&query->subscribers, &handler->it), 0);

    free(key);

    return (netmodel_query_t*) handler;
}

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* interface,
                                                     UNUSED const char* subscriber,
                                                     const char* flag,
                                                     const char* traverse,
                                                     netmodel_callback_t handler,
                                                     void* userdata) {
    amxc_var_t* result = mock_type(amxc_var_t*);
    amxc_var_t args;
    char buf[256] = {0};
    netmodel_query_t* query = NULL;

    check_expected(interface);

    handler("", result, userdata);
    amxc_var_delete(&result);

    amxc_var_init(&args);
    snprintf(buf, sizeof(buf) - 1, "{\"class\":\"getAddrs\",\"flag\":\"%s\",\"traverse\":\"%s\"}", flag, traverse);
    amxc_var_set(jstring_t, &args, buf);
    assert_int_equal(amxc_var_cast(&args, AMXC_VAR_ID_HTABLE), 0);

    query = insert_handler(interface, handler, userdata, &args);

    amxc_var_clean(&args);

    return query;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* const interface,
                                                              UNUSED const char* const subscriber,
                                                              const char* const name,
                                                              const char* const flag,
                                                              const char* const traverse,
                                                              netmodel_callback_t handler,
                                                              void* const userdata) {
    amxc_var_t* result = mock_type(amxc_var_t*);
    amxc_var_t args;
    char buf[256] = {0};
    netmodel_query_t* query = NULL;

    check_expected(interface);

    handler("", result, userdata);
    amxc_var_delete(&result);

    amxc_var_init(&args);
    snprintf(buf, sizeof(buf) - 1, "{\"class\":\"getFirstParameter\",\"flag\":\"%s\",\"name\":\"%s\",\"traverse\":\"%s\"}", flag, name, traverse);
    amxc_var_set(jstring_t, &args, buf);
    assert_int_equal(amxc_var_cast(&args, AMXC_VAR_ID_HTABLE), 0);

    query = insert_handler(interface, handler, userdata, &args);

    amxc_var_clean(&args);

    return query;
}

netmodel_query_t* __wrap_netmodel_openQuery_getParameters(const char* const interface,
                                                          UNUSED const char* const subscriber,
                                                          const char* const name,
                                                          const char* const flag,
                                                          const char* const traverse,
                                                          netmodel_callback_t handler,
                                                          void* const userdata) {
    amxc_var_t* result = mock_type(amxc_var_t*);
    amxc_var_t args;
    char buf[256] = {0};
    netmodel_query_t* query = NULL;

    check_expected(interface);

    handler("", result, userdata);
    amxc_var_delete(&result);

    amxc_var_init(&args);
    snprintf(buf, sizeof(buf) - 1, "{\"class\":\"getParameters\",\"flag\":\"%s\",\"name\":\"%s\",\"traverse\":\"%s\"}", flag, name, traverse);
    amxc_var_set(jstring_t, &args, buf);
    assert_int_equal(amxc_var_cast(&args, AMXC_VAR_ID_HTABLE), 0);

    query = insert_handler(interface, handler, userdata, &args);

    amxc_var_clean(&args);

    return query;
}

void __wrap_netmodel_setFlag(UNUSED const char* intf, const char* flag, UNUSED const char* condition, UNUSED const char* traverse) {
    amxc_set_t set;
    amxc_set_init(&set, false);

    amxc_set_parse(&set, flag);
    amxc_set_union(&mocknm_iface_flags, &set);

    amxc_set_clean(&set);
}

void __wrap_netmodel_clearFlag(UNUSED const char* intf, const char* flag, UNUSED const char* condition, UNUSED const char* traverse) {
    amxc_set_t set;
    amxc_set_init(&set, false);

    amxc_set_parse(&set, flag);
    amxc_set_subtract(&mocknm_iface_flags, &set);

    amxc_set_clean(&set);
}

void __wrap_netmodel_closeQuery(netmodel_query_t* const netmodel_query) {
    mocknm_query_handler_t* handler = (mocknm_query_handler_t*) netmodel_query;
    if(mocknm_queries == NULL) {
        return; // mocknm_clean has already been called
    }
    assert_non_null(handler);
    amxc_llist_it_take(&handler->it);
    mocknm_query_handler_it_delete(&handler->it);
}

void mocknm_expect_openQuery_getAddrs(const char* iface_path, const char* result) {
    amxc_var_t* var = NULL;

    amxc_var_new(&var);
    amxc_var_set(jstring_t, var, result);
    assert_int_equal(amxc_var_cast(var, AMXC_VAR_ID_LIST), 0);

    expect_string(__wrap_netmodel_openQuery_getAddrs, interface, iface_path);
    will_return(__wrap_netmodel_openQuery_getAddrs, var);
}

void mocknm_expect_openQuery_getParameters(const char* iface_path, const char* result) {
    amxc_var_t* var = NULL;

    amxc_var_new(&var);
    amxc_var_set(jstring_t, var, result);
    assert_int_equal(amxc_var_cast(var, AMXC_VAR_ID_HTABLE), 0);

    expect_string(__wrap_netmodel_openQuery_getParameters, interface, iface_path);
    will_return(__wrap_netmodel_openQuery_getParameters, var);
}

void mocknm_expect_openQuery_getFirstParameter(const char* iface_path, const char* result) {
    amxc_var_t* var = NULL;

    amxc_var_new(&var);
    amxc_var_set(cstring_t, var, result);

    expect_string(__wrap_netmodel_openQuery_getFirstParameter, interface, iface_path);
    will_return(__wrap_netmodel_openQuery_getFirstParameter, var);
}

void mocknm_change_query_result(const char* key, amxc_var_t* result) {
    amxc_htable_it_t* query_it = NULL;
    mocknm_query_t* query = NULL;

    query_it = amxc_htable_get(mocknm_queries, key);
    assert_non_null(query_it);
    query = amxc_htable_it_get_data(query_it, mocknm_query_t, it);
    amxc_llist_for_each(it, &query->subscribers) {
        mocknm_query_handler_t* handler = amxc_llist_it_get_data(it, mocknm_query_handler_t, it);
        handler->handler("", result, handler->userdata);
    }
}

void mocknm_init(UNUSED amxo_parser_t* parser, UNUSED amxd_dm_t* dm, UNUSED const char* nm_odl) {
    amxc_set_init(&mocknm_iface_flags, false);
    amxc_htable_new(&mocknm_queries, 20);
}

static void mocknm_query_it_delete(UNUSED const char* key, amxc_htable_it_t* it) {
    mocknm_query_t* query = amxc_htable_it_get_data(it, mocknm_query_t, it);
    amxc_llist_clean(&query->subscribers, mocknm_query_handler_it_delete);
    free(query);
}

void mocknm_clean(void) {
    amxc_set_clean(&mocknm_iface_flags);
    amxc_htable_delete(&mocknm_queries, mocknm_query_it_delete);
}