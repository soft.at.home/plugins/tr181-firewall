/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <dlfcn.h>

#include "firewall.h"
#include "mock.h"

#include <amxd/amxd_path.h>

#define LIBAMXB "libamxb.so"

// remarks:
// 1. I didn't use the linker's "-Wl,--wrap=" capability because I want to also count calls done by libs like libnetmodel (for which the linker doesn't wrap the symbol). So I used the compiler and libdl.
// 2. Unit tests only have local objects, but it is probably the remote calls that have the biggest performance impact.

static bool is_local_object(const char* object) {
    bool is_local = false;

    for(int i = 0; ut_local_objects[i] != NULL; i++) {
        if(strncmp(object, ut_local_objects[i], strlen(ut_local_objects[i])) == 0) {
            is_local = true;
            goto exit;
        }
    }

exit:
    return is_local;
}

int amxb_call(amxb_bus_ctx_t* const bus_ctx,
              const char* object,
              const char* method,
              amxc_var_t* args,
              amxc_var_t* ret,
              int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, const char* method, amxc_var_t* args, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, method, args, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_CALL_LOCAL]++;
    } else {
        ut_count[UT_AMXB_CALL_REMOTE]++;
    }
    ut_count[UT_AMXB_CALL]++;

    return rv;
}

amxb_request_t* amxb_async_call(amxb_bus_ctx_t* const bus_ctx,
                                const char* object,
                                const char* method,
                                amxc_var_t* args,
                                amxb_be_done_cb_fn_t done_fn,
                                void* priv) {
    amxb_request_t* rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    amxb_request_t* (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, const char* method, amxc_var_t* args, amxb_be_done_cb_fn_t done_fn, void* priv);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, method, args, done_fn, priv);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_ASYNC_CALL_LOCAL]++;
    } else {
        ut_count[UT_AMXB_ASYNC_CALL_REMOTE]++;
    }
    ut_count[UT_AMXB_ASYNC_CALL]++;

    return rv;
}

int amxb_get(amxb_bus_ctx_t* const bus_ctx,
             const char* object,
             int32_t depth,
             amxc_var_t* ret,
             int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, int32_t depth, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, depth, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_GET_LOCAL]++;
    } else {
        ut_count[UT_AMXB_GET_REMOTE]++;
    }
    ut_count[UT_AMXB_GET]++;

    return rv;
}

int amxb_get_multiple(amxb_bus_ctx_t* const bus_ctx,
                      amxc_var_t* req_paths,
                      int32_t depth,
                      amxc_var_t* ret,
                      int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, amxc_var_t* req_paths, int32_t depth, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, req_paths, depth, ret, timeout);
    dlclose(handle);

    ut_count[UT_AMXB_GET_MULTIPLE]++; // count get_multiple this unconditionally? (it calls amxb_get in for loop)

    return rv;
}

int amxb_set(amxb_bus_ctx_t* const bus_ctx,
             const char* object,
             amxc_var_t* values,
             amxc_var_t* ret,
             int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, amxc_var_t* values, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, values, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_SET_LOCAL]++;
    } else {
        ut_count[UT_AMXB_SET_REMOTE]++;
    }
    ut_count[UT_AMXB_SET]++;

    return rv;
}

int amxb_set_multiple(amxb_bus_ctx_t* const bus_ctx,
                      uint32_t flags,
                      amxc_var_t* req_paths,
                      amxc_var_t* ret,
                      int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, uint32_t flags, amxc_var_t* req_paths, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, flags, req_paths, ret, timeout);
    dlclose(handle);

    ut_count[UT_AMXB_SET_MULTIPLE]++; // count set_multiple this unconditionally? (it calls amxb_set in for loop)

    return rv;
}

int amxb_add(amxb_bus_ctx_t* const bus_ctx,
             const char* object,
             uint32_t index,
             const char* name,
             amxc_var_t* values,
             amxc_var_t* ret,
             int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, uint32_t index, const char* name, amxc_var_t* values, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, index, name, values, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_ADD_LOCAL]++;
    } else {
        ut_count[UT_AMXB_ADD_REMOTE]++;
    }
    ut_count[UT_AMXB_ADD]++;

    return rv;
}

int amxb_del(amxb_bus_ctx_t* const bus_ctx,
             const char* object,
             uint32_t index,
             const char* name,
             amxc_var_t* ret,
             int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, uint32_t index, const char* name, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, index, name, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_DEL_LOCAL]++;
    } else {
        ut_count[UT_AMXB_DEL_REMOTE]++;
    }
    ut_count[UT_AMXB_DEL]++;

    return rv;
}

int amxb_get_supported(amxb_bus_ctx_t* const bus_ctx,
                       const char* object,
                       uint32_t flags,
                       amxc_var_t* ret,
                       int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, uint32_t flags, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, flags, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_GET_SUPPORTED_LOCAL]++;
    } else {
        ut_count[UT_AMXB_GET_SUPPORTED_REMOTE]++;
    }
    ut_count[UT_AMXB_GET_SUPPORTED]++;

    return rv;
}

int amxb_describe(amxb_bus_ctx_t* const bus_ctx,
                  const char* object,
                  uint32_t flags,
                  amxc_var_t* ret,
                  int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, uint32_t flags, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, flags, ret, timeout);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_DESCRIBE_LOCAL]++;
    } else {
        ut_count[UT_AMXB_DESCRIBE_REMOTE]++;
    }
    ut_count[UT_AMXB_DESCRIBE]++;

    return rv;
}

int amxb_list(amxb_bus_ctx_t* const bus_ctx,
              const char* object,
              uint32_t flags,
              amxb_be_cb_fn_t fn,
              void* priv) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* object, uint32_t flags, amxb_be_cb_fn_t fn, void* priv);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, object, flags, fn, priv);
    dlclose(handle);

    if(is_local_object(object)) {
        ut_count[UT_AMXB_LIST_LOCAL]++;
    } else {
        ut_count[UT_AMXB_LIST_REMOTE]++;
    }
    ut_count[UT_AMXB_LIST]++;

    return rv;
}

int amxb_resolve(amxb_bus_ctx_t* bus_ctx,
                 amxd_path_t* obj_path,
                 amxc_var_t* ret_val) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, amxd_path_t* obj_path, amxc_var_t* ret_val);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, obj_path, ret_val);
    dlclose(handle);

    if(is_local_object(amxd_path_get(obj_path, AMXD_OBJECT_TERMINATE))) {
        ut_count[UT_AMXB_RESOLVE_LOCAL]++;
    } else {
        ut_count[UT_AMXB_RESOLVE_REMOTE]++;
    }
    ut_count[UT_AMXB_RESOLVE]++;

    return rv;
}

int amxb_get_instances(amxb_bus_ctx_t* const bus_ctx,
                       const char* search_path,
                       int32_t depth,
                       amxc_var_t* ret,
                       int timeout) {
    int rv;
    void* handle = dlopen(LIBAMXB, RTLD_LAZY);
    int (* real_call)(amxb_bus_ctx_t* const bus_ctx, const char* search_path, int32_t depth, amxc_var_t* ret, int timeout);

    dlerror();
    real_call = dlsym(handle, __func__);
    rv = real_call(bus_ctx, search_path, depth, ret, timeout);
    dlclose(handle);

    if(is_local_object(search_path)) {
        ut_count[UT_AMXB_GET_INSTANCES_LOCAL]++;
    } else {
        ut_count[UT_AMXB_GET_INSTANCES_REMOTE]++;
    }
    ut_count[UT_AMXB_GET_INSTANCES]++;

    return rv;
}