/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "mock.h"
#include "mock_netmodel.h"
#include "firewall.h"
#include "firewall/dm_firewall.h"
#include "dmz/dm_dmz.h"
#include "interfacesetting/dm_interfacesetting.h"
#include "interfacesetting/dm_spoofing_protection.h"
#include "interfacesetting/dm_icmpv6_passthrough.h"
#include "interfacesetting/dm_icmp_echo_request.h"
#include "interfacesetting/dm_stealth_mode.h"
#include "interfacesetting/dm_accept_udp_traceroute.h"
#include "interfacesetting/dm_log_in_drop.h"
#include "interfacesetting/dm_log_out_drop.h"
#include "interfacesetting/dm_log_out_accept.h"
#include "policy/dm_policy.h"
#include "pinhole/dm_pinhole.h"
#include "porttrigger/dm_porttrigger.h"
#include "service/dm_service.h"
#include "ipset/dm_ipset.h"
#include "connectiontracking/dm_connectiontracking.h"
#include "nat/dm_nat.h"
#include "nat/staticnat.h"
#include "gmap_mock.h"
#include "rules.h"
#include "dmz/dmz.h"
#include "validator.h"

#include <amxd/amxd_action.h>

static amxd_dm_t* dm = NULL;
static amxo_parser_t* parser = NULL;

static amxd_status_t _dummy_validator(UNUSED amxd_object_t* object,
                                      UNUSED amxd_param_t* param,
                                      UNUSED amxd_action_t reason,
                                      UNUSED const amxc_var_t* const args,
                                      UNUSED amxc_var_t* const retval,
                                      UNUSED void* priv) {
    return amxd_status_ok;
}

static void resolve_functions(amxo_parser_t* parser) {
    // validators
    amxo_resolver_ftab_add(parser, "fw_list_check_range", AMXO_FUNC(_fw_list_check_range));
    amxo_resolver_ftab_add(parser, "rule_lease_onread", AMXO_FUNC(_RemainingLeaseTime_rule_lease_onread));
    // Firewall.
    amxo_resolver_ftab_add(parser, "app_start", AMXO_FUNC(_app_start));
    amxo_resolver_ftab_add(parser, "firewall_cleanup", AMXO_FUNC(_firewall_cleanup));
    amxo_resolver_ftab_add(parser, "firewall_changed", AMXO_FUNC(_firewall_changed));
    amxo_resolver_ftab_add(parser, "firewall_enable_changed", AMXO_FUNC(_firewall_enable_changed));
    amxo_resolver_ftab_add(parser, "update_last_changed", AMXO_FUNC(_update_last_changed));
    // Firewall.Level.{i}.
    amxo_resolver_ftab_add(parser, "level_changed", AMXO_FUNC(_level_changed));
    amxo_resolver_ftab_add(parser, "level_instance_cleanup", AMXO_FUNC(_level_instance_cleanup));
    // Firewall.Chain.{i}.Rule.{i}.
    amxo_resolver_ftab_add(parser, "chain_instance_cleanup", AMXO_FUNC(_chain_instance_cleanup));
    amxo_resolver_ftab_add(parser, "rule_instance_cleanup", AMXO_FUNC(_rule_instance_cleanup));
    amxo_resolver_ftab_add(parser, "log_instance_cleanup", AMXO_FUNC(_log_instance_cleanup));
    amxo_resolver_ftab_add(parser, "firewall_chain_added", AMXO_FUNC(_firewall_chain_added));
    amxo_resolver_ftab_add(parser, "firewall_chain_changed", AMXO_FUNC(_firewall_chain_changed));
    amxo_resolver_ftab_add(parser, "firewall_rule_added", AMXO_FUNC(_firewall_rule_added));
    amxo_resolver_ftab_add(parser, "firewall_rule_changed", AMXO_FUNC(_firewall_rule_changed));
    amxo_resolver_ftab_add(parser, "rule_lease_time_changed", AMXO_FUNC(_rule_lease_time_changed));
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    amxo_resolver_ftab_add(parser, "firewall_log_added", AMXO_FUNC(_firewall_log_added));
    amxo_resolver_ftab_add(parser, "firewall_log_changed", AMXO_FUNC(_firewall_log_changed));
    amxo_resolver_ftab_add(parser, "icmp_logs_changed", AMXO_FUNC(_icmp_logs_changed));
#endif //FIREWALL_LOGS
    // Firewall.{i}.ConnectionTracking.
    amxo_resolver_ftab_add(parser, "connectiontracking_changed", AMXO_FUNC(_connectiontracking_changed));
    amxo_resolver_ftab_add(parser, "sip_cleanup", AMXO_FUNC(_sip_cleanup));
    // Firewall.{i}.Policy.{i}.
    amxo_resolver_ftab_add(parser, "policy_instance_cleanup", AMXO_FUNC(_policy_instance_cleanup));
    amxo_resolver_ftab_add(parser, "policy_added", AMXO_FUNC(_policy_added));
    amxo_resolver_ftab_add(parser, "policy_changed", AMXO_FUNC(_policy_changed));
    amxo_resolver_ftab_add(parser, "getPolicy", AMXO_FUNC(_getPolicy));
    amxo_resolver_ftab_add(parser, "setPolicy", AMXO_FUNC(_setPolicy));
    amxo_resolver_ftab_add(parser, "deletePolicy", AMXO_FUNC(_deletePolicy));
    // Firewall.DMZ.{i}.
    amxo_resolver_ftab_add(parser, "dmz_instance_cleanup", AMXO_FUNC(_dmz_instance_cleanup));
    amxo_resolver_ftab_add(parser, "dmz_added", AMXO_FUNC(_dmz_added));
    amxo_resolver_ftab_add(parser, "dmz_changed", AMXO_FUNC(_dmz_changed));
    amxo_resolver_ftab_add(parser, "dmz_lease_onread", AMXO_FUNC(_RemainingLeaseTime_dmz_lease_onread));
    amxo_resolver_ftab_add(parser, "dmz_lease_time_changed", AMXO_FUNC(_dmz_lease_time_changed));
    // Firewall.InterfaceSetting.{i}.
    amxo_resolver_ftab_add(parser, "ifsetting_instance_cleanup", AMXO_FUNC(_ifsetting_instance_cleanup));
    amxo_resolver_ftab_add(parser, "ifsetting_added", AMXO_FUNC(_ifsetting_added));
    amxo_resolver_ftab_add(parser, "spoofing_changed", AMXO_FUNC(_spoofing_changed));
    amxo_resolver_ftab_add(parser, "icmpv6_passthrough_changed", AMXO_FUNC(_icmpv6_passthrough_changed));
    amxo_resolver_ftab_add(parser, "icmp_echo_request_changed", AMXO_FUNC(_icmp_echo_request_changed));
    amxo_resolver_ftab_add(parser, "stealth_mode_changed", AMXO_FUNC(_stealth_mode_changed));
    amxo_resolver_ftab_add(parser, "accept_udp_traceroute_changed", AMXO_FUNC(_accept_udp_traceroute_changed));
    amxo_resolver_ftab_add(parser, "isolate_iface_changed", AMXO_FUNC(_isolate_iface_changed));
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    amxo_resolver_ftab_add(parser, "log_in_drop_changed", AMXO_FUNC(_log_in_drop_changed));
    amxo_resolver_ftab_add(parser, "log_out_drop_changed", AMXO_FUNC(_log_out_drop_changed));
    amxo_resolver_ftab_add(parser, "log_out_accept_changed", AMXO_FUNC(_log_out_accept_changed));
#endif //FIREWALL_LOGS
    // Firewall.{i}.Pinhole.{i}.
    amxo_resolver_ftab_add(parser, "pinhole_added", AMXO_FUNC(_pinhole_added));
    amxo_resolver_ftab_add(parser, "pinhole_changed", AMXO_FUNC(_pinhole_changed));
    amxo_resolver_ftab_add(parser, "pinhole_schedule_changed", AMXO_FUNC(_pinhole_schedule_changed));
    amxo_resolver_ftab_add(parser, "setPinhole", AMXO_FUNC(_setPinhole));
    amxo_resolver_ftab_add(parser, "updatePinhole", AMXO_FUNC(_updatePinhole));
    amxo_resolver_ftab_add(parser, "getPinhole", AMXO_FUNC(_getPinhole));
    amxo_resolver_ftab_add(parser, "deletePinhole", AMXO_FUNC(_deletePinhole));
    // Firewall.{i}.Service.{i}.
    amxo_resolver_ftab_add(parser, "service_instance_cleanup", AMXO_FUNC(_service_instance_cleanup));
    amxo_resolver_ftab_add(parser, "service_added", AMXO_FUNC(_service_added));
    amxo_resolver_ftab_add(parser, "service_changed", AMXO_FUNC(_service_changed));
    amxo_resolver_ftab_add(parser, "getService", AMXO_FUNC(_getService));
    amxo_resolver_ftab_add(parser, "setService", AMXO_FUNC(_setService));
    amxo_resolver_ftab_add(parser, "deleteService", AMXO_FUNC(_deleteService));
    // Firewall.Set.{i}.
    amxo_resolver_ftab_add(parser, "fw_match_type_set", AMXO_FUNC(_fw_match_type_set));
    amxo_resolver_ftab_add(parser, "fw_match_ip_version", AMXO_FUNC(_fw_match_ip_version));
    amxo_resolver_ftab_add(parser, "fw_is_valid_port_range", AMXO_FUNC(_fw_is_valid_port_range));
    amxo_resolver_ftab_add(parser, "set_type_changed", AMXO_FUNC(_set_type_changed));
    amxo_resolver_ftab_add(parser, "set_enable_changed", AMXO_FUNC(_set_enable_changed));
    amxo_resolver_ftab_add(parser, "set_added", AMXO_FUNC(_set_added));
    amxo_resolver_ftab_add(parser, "set_instance_delete", AMXO_FUNC(_set_instance_delete));
    amxo_resolver_ftab_add(parser, "set_instance_cleanup", AMXO_FUNC(_set_instance_cleanup));
    amxo_resolver_ftab_add(parser, "set_rule_added", AMXO_FUNC(_set_rule_added));
    amxo_resolver_ftab_add(parser, "set_rule_changed", AMXO_FUNC(_set_rule_changed));
    amxo_resolver_ftab_add(parser, "set_rule_instance_delete", AMXO_FUNC(_set_rule_instance_delete));
    amxo_resolver_ftab_add(parser, "fw_is_exclude_supported", AMXO_FUNC(_fw_is_exclude_supported));
    amxo_resolver_ftab_add(parser, "fw_has_no_references", AMXO_FUNC(_fw_has_no_references));


    // NAT.
    amxo_resolver_ftab_add(parser, "nat_changed", AMXO_FUNC(_nat_changed));
    amxo_resolver_ftab_add(parser, "updatePortMapping", AMXO_FUNC(_updatePortMapping));
    // NAT.PortTrigger.{i}.
    amxo_resolver_ftab_add(parser, "porttrigger_instance_cleanup", AMXO_FUNC(_porttrigger_instance_cleanup));
    amxo_resolver_ftab_add(parser, "porttrigger_added", AMXO_FUNC(_porttrigger_added));
    amxo_resolver_ftab_add(parser, "porttrigger_changed", AMXO_FUNC(_porttrigger_changed));
    amxo_resolver_ftab_add(parser, "porttrigger_schedule_changed", AMXO_FUNC(_porttrigger_schedule_changed));
    amxo_resolver_ftab_add(parser, "porttrigger_rule_added", AMXO_FUNC(_porttrigger_rule_added));
    amxo_resolver_ftab_add(parser, "porttrigger_rule_changed", AMXO_FUNC(_porttrigger_rule_changed));
    amxo_resolver_ftab_add(parser, "max_instances_check", AMXO_FUNC(_max_instances_check));
    // NAT.InterfaceSetting.{i}.
    amxo_resolver_ftab_add(parser, "interface_setting_instance_cleanup", AMXO_FUNC(_interface_setting_instance_cleanup));
    amxo_resolver_ftab_add(parser, "nat_interface_instance_cleanup", AMXO_FUNC(_nat_interface_instance_cleanup));
    amxo_resolver_ftab_add(parser, "interface_setting_changed", AMXO_FUNC(_interface_setting_changed));
    amxo_resolver_ftab_add(parser, "interface_setting_added", AMXO_FUNC(_interface_setting_added));
    amxo_resolver_ftab_add(parser, "natinterface_added", AMXO_FUNC(_natinterface_added));
    amxo_resolver_ftab_add(parser, "natinterface_changed", AMXO_FUNC(_natinterface_changed));
    // NAT.PortMapping.{i}.
    amxo_resolver_ftab_add(parser, "nat_portmap_changed", AMXO_FUNC(_nat_portmap_changed));
    amxo_resolver_ftab_add(parser, "nat_portmap_schedule_changed", AMXO_FUNC(_nat_portmap_schedule_changed));
    amxo_resolver_ftab_add(parser, "nat_portmap_added", AMXO_FUNC(_nat_portmap_added));
    amxo_resolver_ftab_add(parser, "nat_portmap_changed_allowed_origins", AMXO_FUNC(_nat_portmap_changed_allowed_origins));
    // NAT.StaticNAT
    amxo_resolver_ftab_add(parser, "statnat_host_destroy", AMXO_FUNC(_statnat_host_destroy));
    amxo_resolver_ftab_add(parser, "statnat_host_added", AMXO_FUNC(_statnat_host_added));
    amxo_resolver_ftab_add(parser, "statnat_host_changed", AMXO_FUNC(_statnat_host_changed));
    amxo_resolver_ftab_add(parser, "statnat_portmap_destroy", AMXO_FUNC(_statnat_portmap_destroy));
    amxo_resolver_ftab_add(parser, "statnat_portmap_added", AMXO_FUNC(_statnat_portmap_added));
    amxo_resolver_ftab_add(parser, "statnat_portmap_changed", AMXO_FUNC(_statnat_portmap_changed));

    //GMAP
    amxo_resolver_ftab_add(parser, "gmap_mock_query", AMXO_FUNC(_gmap_mock_query));

    // to ignore import warnings of mod-dmext and such third parties
    amxo_resolver_ftab_add(parser, "is_valid_ip", AMXO_FUNC(_dummy_validator));

    //validators
    amxo_resolver_ftab_add(parser, "fw_is_valid_prefix", AMXO_FUNC(_fw_is_valid_prefix));
    amxo_resolver_ftab_add(parser, "fw_is_valid_ipv4_prefix", AMXO_FUNC(_fw_is_valid_ipv4_prefix));
}

void mock_init(void** state, const char* default_odl) {
    amxd_object_t* root = NULL;
    int retval = -1;

    disable_check = true; // so that tests can load default odls without worrying about expect_check

    amxut_bus_setup(state);

    parser = amxut_bus_parser();
    dm = amxut_bus_dm();
    root = amxd_dm_get_root(dm);

    mocknm_init(parser, dm, NULL);

    // for DMZ
    expect_query_lan_ipv4_addresses();

    assert_int_equal(amxo_resolver_import_open(parser, "/usr/lib/amx/modules/mod-dmext.so", "mod-dmext", 0), 0);

    resolve_functions(parser);

    retval = amxo_parser_parse_file(parser, "mock.odl", root);
    assert_int_equal(retval, 0);

    handle_events();

    print_message("call entry point start\n");
    assert_int_equal(_fw_main(0, dm, parser), 0);

    if(default_odl != NULL) {
        print_message("load default odl file\n");
        assert_int_equal(amxo_parser_parse_file(parser, default_odl, root), 0);
        handle_events();
    }

    print_message("emit signal app:start\n");
    amxp_sigmngr_emit_signal(&dm->sigmngr, "app:start", NULL);
    handle_events();

    disable_check = false;
}

void mock_cleanup(void** state) {
    disable_check = true; // so that tests can teardown without worrying about expect_check

    amxo_resolver_import_close_all();

    amxp_sigmngr_emit_signal(&amxut_bus_dm()->sigmngr, "app:stop", NULL);
    handle_events();
    assert_int_equal(_fw_main(1, amxut_bus_dm(), amxut_bus_parser()), 0);

    handle_events();

    mocknm_clean();

    dm = NULL;
    parser = NULL;
    amxut_bus_teardown(state);
}

void dump_object(amxd_object_t* object, int index) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    cstring_t param_value = NULL;
    index++;

    if(object == NULL) {
        printf("The object is NULL.\n");
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            param_value = amxc_var_dyncast(cstring_t, tmp_var);
            printf("param_name[%d]: %s = %s\n", index, param_name, param_value);
            free(param_value);
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("obj_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
        amxc_llist_for_each(it, (&object->instances)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("instance_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
    }
    fflush(stdout);
}