/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __TEST_MOCK_H__
#define __TEST_MOCK_H__

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>
#include <amxut/amxut_dm.h>

#include "mock_netmodel.h"
#include "fwrules_mock.h"

#define TEST_TABLE_FILTER "filter"
#define TEST_TABLE_NAT "nat"
#define TEST_TABLE_RAW "raw"

#define STRING_EMPTY(TEXT) ((TEXT == NULL) || (*TEXT == 0))

void mock_init(void** state, const char* default_odl);
void mock_cleanup(void** state);
int ut_rule_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data);

void expect_query_wan_ipv4_addresses(void);
void expect_query_wan_ipv4_addresses_no_device(void);
void expect_query_wan_ipv4_addresses_logical(void);
void expect_query_wan_ipv4_addresses_logical_no_device(void);

void expect_query_wan_ipv6_addresses(void);
void expect_query_wan_ipv6_addresses_no_device(void);
void expect_query_wan_ipv6_addresses_logical(void);
void expect_query_wan_ipv6_addresses_logical_no_device(void);

void expect_query_wan_addresses(void);
void expect_query_wan_addresses_no_device(void);
void expect_query_wan_addresses_logical(void);
void expect_query_wan_addresses_logical_no_device(void);

void expect_query_wan_netdevname(void);
void expect_query_wan_netdevname_no_device(void);
void expect_query_wan_netdevname_logical(void);
void expect_query_wan_netdevname_logical_no_device(void);

void expect_query_wan_netdevnames(void);
void expect_query_wan_netdevnames_no_device(void);
void expect_query_wan_netdevnames_logical(void);
void expect_query_wan_netdevnames_logical_no_device(void);

void expect_query_lan_ipv4_addresses(void);
void expect_query_lan_ipv6_addresses(void);
void expect_query_lan_netdevname(void);
void expect_query_lan_netdevnames(void);
void expect_query_guest_ipv4_addresses(void);
void expect_query_guest_ipv6_addresses(void);
void expect_query_guest_netdevname(void);
void expect_query_guest_netdevnames(void);

/**
 * Hack to be able to remove the iterator from the global list of rules.
 * Must be in sync with the struct from libfwrules. todo use amxc_var_t instead of libfwrule's opaque struct
 */
#include <fwrules/fw_features.h>
#include <fwrules/fw_rule.h>

typedef struct _fw_rule {
    amxc_llist_it_t it;
    amxc_llist_it_t g_it;
    fw_rule_flag_t flag;
    bool enabled;
    int feature_marks;
    amxc_var_t* ht;
    fw_feature_t current_feature;
    int traversed;
    struct {
        char old_table[FW_RULE_TABLE_LEN];
        char old_chain[FW_RULE_CHAIN_LEN];
        char table[FW_RULE_TABLE_LEN];
        char chain[FW_RULE_CHAIN_LEN];
    } params;
} fw_rule_t;

int ut_rule_new(fw_rule_t** rule);
fw_rule_t* json_to_fw_rule(const char* json);

// use macro to get line in test on failure
#define expect_fw_replace_rule(populate_rule, ut_index) do { \
        fw_rule_t* ut_rule = NULL; \
        assert_int_equal(ut_rule_new(&ut_rule), 0); /* freed at expect_check */ \
        populate_rule(ut_rule); \
        expect_check(fw_replace_rule, rule, ut_rule_equal_check, ut_rule); \
        expect_value(fw_replace_rule, index, ut_index); \
        reset_fw_commit_counter(); \
} while(0)

// use macro to get line in test on failure
#define expect_fw_replace_rule_json(json, ut_index) do { \
        fw_rule_t* ut_rule = NULL; \
        ut_rule = json_to_fw_rule(json); \
        expect_check(fw_replace_rule, rule, ut_rule_equal_check, ut_rule); \
        expect_value(fw_replace_rule, index, ut_index); \
        reset_fw_commit_counter(); \
} while(0)

// use macro to get line in test on failure
#define expect_fw_delete_rule(populate_rule, ut_index) do { \
        fw_rule_t* ut_rule = NULL; \
        assert_int_equal(ut_rule_new(&ut_rule), 0); /* freed at expect_check */ \
        populate_rule(ut_rule); \
        expect_check(fw_delete_rule, rule, ut_rule_equal_check, ut_rule); \
        expect_value(fw_delete_rule, index, ut_index); \
        reset_fw_commit_counter(); \
} while(0)

// use macro to get line in test on failure
#define expect_fw_delete_rule_json(json, ut_index) do { \
        fw_rule_t* ut_rule = NULL; \
        ut_rule = json_to_fw_rule(json); \
        expect_check(fw_delete_rule, rule, ut_rule_equal_check, ut_rule); \
        expect_value(fw_delete_rule, index, ut_index); \
        reset_fw_commit_counter(); \
} while(0)

// use macro to get line in test on failure
#define expect_fw_commit_failed(commit_count) do { \
        add_expected_fw_commit(commit_count); \
        reset_fw_commit_counter(); \
} while(0)

#define expect_gmap_open_query_ext(json) do { \
        amxc_var_t* _var = NULL; \
        amxc_var_new(&_var); \
        assert_non_null(&_var); \
        amxc_var_set(jstring_t, _var, json); \
        assert_int_equal(amxc_var_cast(_var, AMXC_VAR_ID_HTABLE), 0); \
        will_return(gmap_query_open_ext, _var); \
} while(0)

#define handle_events() do { \
        amxut_bus_handle_events(); \
        assert_int_equal(get_amount_of_expected_commits(), 0); \
        reset_fw_commit_counter(); \
        reset_fw_commit_expected(); \
} while(0)

#define assert_EndTime_equals(OBJECT_PATH, ENDTIME) { \
        char* _endtime = NULL; \
        amxc_var_t _param; \
        amxd_object_t* _obj = NULL; \
        amxc_var_init(&_param); \
        _obj = amxd_dm_findf(dm, "%s", OBJECT_PATH); \
        assert_non_null(_obj); \
        assert_int_equal(amxd_object_get_param(_obj, "EndTime", &_param), 0); \
        _endtime = amxc_var_dyncast(cstring_t, &_param); \
        assert_string_equal(_endtime, ENDTIME); \
        amxc_var_clean(&_param); \
        free(_endtime); \
}

#define assert_EndTime_now_or_later(OBJECT_PATH) { \
        const amxc_ts_t* _endtime = NULL; \
        amxc_ts_t _now; \
        amxc_var_t _param; \
        amxd_object_t* _obj = NULL; \
        amxc_var_init(&_param); \
        assert_int_equal(amxc_ts_now(&_now), 0); \
        _obj = amxd_dm_findf(dm, "%s", OBJECT_PATH); \
        assert_non_null(_obj); \
        assert_int_equal(amxd_object_get_param(_obj, "EndTime", &_param), 0); \
        _endtime = amxc_var_constcast(amxc_ts_t, &_param); \
        assert_non_null(_endtime); \
        assert_int_equal(amxc_ts_compare(_endtime, &_now), 1); \
        amxc_var_clean(&_param); \
}

void duplicate_fw_rule(fw_rule_t* dest, const fw_rule_t* src);

void dump_object(amxd_object_t* object, int index);

extern bool rule_is_being_deleted;
extern bool disable_check;
extern int ut_count[];
extern const char* ut_local_objects[];

typedef enum {
    UT_AMXB_CALL,
    UT_AMXB_CALL_LOCAL,
    UT_AMXB_CALL_REMOTE,
    UT_AMXB_ASYNC_CALL,
    UT_AMXB_ASYNC_CALL_LOCAL,
    UT_AMXB_ASYNC_CALL_REMOTE,
    UT_AMXB_GET,
    UT_AMXB_GET_LOCAL,
    UT_AMXB_GET_REMOTE,
    UT_AMXB_GET_MULTIPLE,
    UT_AMXB_SET,
    UT_AMXB_SET_LOCAL,
    UT_AMXB_SET_REMOTE,
    UT_AMXB_SET_MULTIPLE,
    UT_AMXB_ADD,
    UT_AMXB_ADD_LOCAL,
    UT_AMXB_ADD_REMOTE,
    UT_AMXB_DEL,
    UT_AMXB_DEL_LOCAL,
    UT_AMXB_DEL_REMOTE,
    UT_AMXB_GET_SUPPORTED,
    UT_AMXB_GET_SUPPORTED_LOCAL,
    UT_AMXB_GET_SUPPORTED_REMOTE,
    UT_AMXB_DESCRIBE,
    UT_AMXB_DESCRIBE_LOCAL,
    UT_AMXB_DESCRIBE_REMOTE,
    UT_AMXB_LIST,
    UT_AMXB_LIST_LOCAL,
    UT_AMXB_LIST_REMOTE,
    UT_AMXB_RESOLVE,
    UT_AMXB_RESOLVE_LOCAL,
    UT_AMXB_RESOLVE_REMOTE,
    UT_AMXB_GET_INSTANCES,
    UT_AMXB_GET_INSTANCES_LOCAL,
    UT_AMXB_GET_INSTANCES_REMOTE,
    UT_FWINTERFACE_FW_ADD_CHAIN,
    UT_FWINTERFACE_FW_ADD_RULE,
    UT_FWINTERFACE_FW_REPLACE_RULE,
    UT_FWINTERFACE_FW_DELETE_RULE,
    UT_FWINTERFACE_FW_APPLY,
    UT_MAX
} ut_counter_index_t;

#endif
