/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <netdb.h>

#include <libmnl/libmnl.h>
#include <libnetfilter_queue/libnetfilter_queue.h>

#include "mock.h"

struct nfq_handle {
    int foo;
};

struct nfq_q_handle {
    int foo;
};

struct nfqnl_msg_packet_hdr* nfq_get_msg_packet_hdr(struct nfq_data* nfad) {
    struct nfqnl_msg_packet_hdr* hdr = calloc(1, sizeof(struct nfqnl_msg_packet_hdr));
    assert_non_null(hdr);
    assert_non_null(nfad);
    return hdr;
}

int nfq_get_payload(struct nfq_data* nfad, unsigned char** data) {
    assert_non_null(nfad);
    assert_non_null(data);
    return 0;
}

int nfq_handle_packet(struct nfq_handle* h, char* buf, int len) {
    assert_non_null(h);
    assert_non_null(buf);
    assert_true(len > 0);
    return 0;
}

int nfq_set_verdict(struct nfq_q_handle* qh, UNUSED uint32_t id, UNUSED uint32_t verdict, UNUSED uint32_t data_len, UNUSED const unsigned char* buf) {
    assert_non_null(qh);
    return 0;
}

struct nfq_handle* nfq_open(void) {
    struct nfq_handle* h = calloc(1, sizeof(struct nfq_handle));
    assert_non_null(h);
    return h;
}

int nfq_close(struct nfq_handle* h) {
    assert_non_null(h);
    free(h);
    return 0;
}

int nfq_bind_pf(struct nfq_handle* h, UNUSED uint16_t pf) {
    assert_non_null(h);
    return 0;
}

int nfq_unbind_pf(struct nfq_handle* h, UNUSED uint16_t pf) {
    assert_non_null(h);
    return 0;
}

int nfq_set_mode(struct nfq_q_handle* qh, UNUSED uint8_t mode, UNUSED unsigned int len) {
    assert_non_null(qh);
    return 0;
}

int nfq_fd(struct nfq_handle* h) {
    assert_non_null(h);
    return 10;
}

struct nfq_q_handle* nfq_create_queue(struct nfq_handle* h, UNUSED uint16_t num, nfq_callback* cb, UNUSED void* data) {
    struct nfq_q_handle* qh = calloc(1, sizeof(struct nfq_q_handle));
    assert_non_null(qh);
    assert_non_null(h);
    assert_non_null(cb);
    return qh;
}

int nfq_destroy_queue(struct nfq_q_handle* qh) {
    assert_non_null(qh);
    free(qh);
    return 0;
}
