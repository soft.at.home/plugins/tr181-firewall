/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"
#include "firewall/dm_firewall.h"
#include "porttrigger/dm_porttrigger.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_porttrigger.h"
#include "porttrigger/porttrigger.h"

#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX_REVERSE "[FW][ACCPT_ALL]:"
#define LOG_PREFIX_FORWARD "[FW][ACCPT_ALL]:"
#define SRC "source"
#define DEST "destination"

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};

static const char* jstring_nfq_rule(int protocol, int start, int end) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_PortTrigger\","
             "\"enable\":true,"
             "\"ipv6\":false,"
             "\"destination_port\":%d,"
             "\"destination_port_range_max\":%d,"
             "\"protocol\":%d,"
             "\"in_interface\":\"br-lan\","
             "\"target\":11,"
             "\"target_option\":{\"flags\":0, \"qnum\":1, \"qtotal\":1}"
             "}",
             start, end,
             protocol
             );
    return _buf;
}

static const char* _jstring_filter_rule(bool log, bool reverse, int protocol, const char* dest, int start, int end) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"FORWARD_PortTrigger\","
             "\"enable\":true,"
             "\"ipv6\":false,"
             "\"%s\":\"%s\","
             "\"%s_port\":%d,"
             "\"%s_port_range_max\":%d,"
             "\"protocol\":%d,"
             "\"target\":%s"
             "}",
             reverse ? SRC : DEST, dest,
             reverse ? SRC : DEST, start,
             reverse ? SRC : DEST, end,
             protocol,
             log ? "13, \"target_option\":{\"level\":7, \"logflags\":0, \"prefix\":\"[FW][ACCPT_ALL]:\"}, \"conntrack_state\":\"NEW\"" : "6, \"target_option\":1"
             );
    return _buf;
}

static const char* jstring_filter_rule_log(int protocol, const char* dest, int start, int end) {
    return _jstring_filter_rule(true, false, protocol, dest, start, end);
}

static const char* jstring_filter_rule(int protocol, const char* dest, int start, int end) {
    return _jstring_filter_rule(false, false, protocol, dest, start, end);
}

static const char* jstring_filter_reverse_log(int protocol, const char* dest, int start, int end) {
    return _jstring_filter_rule(true, true, protocol, dest, start, end);
}

static const char* jstring_filter_reverse(int protocol, const char* dest, int start, int end) {
    return _jstring_filter_rule(false, true, protocol, dest, start, end);
}

static const char* jstring_dnat_rule(int protocol, int start, int end, const char* to_dest) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"nat\","
             "\"chain\":\"PREROUTING_PortTrigger\","
             "\"enable\":true,"
             "\"ipv6\":false,"
             "\"destination_port\":%d,"
             "\"destination_port_range_max\":%d,"
             "\"protocol\":%d,"
             "\"target\":8,"
             "\"target_option\":{\"ip\":\"%s\", \"max_port\":0, \"min_port\":0}"
             "}",
             start, end,
             protocol,
             to_dest
             );
    return _buf;
}

int test_porttrigger_setup(void** state) {
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs_all_allowed.odl");
    dm = amxut_bus_dm();
    return 0;
}

int test_porttrigger_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_add_porttrigger(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_trans_t trans2;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans2);
    amxd_trans_select_pathf(&trans2, "NAT.");
    amxd_trans_set_value(uint32_t, &trans2, "MaxPortTriggerNumberOfEntries", 10);
    assert_int_equal(amxd_trans_apply(&trans2, dm), 0);
    amxd_trans_clean(&trans2);
    handle_events();

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    expect_query_lan_ipv4_addresses();

    print_message("add NAT.PortTrigger\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.");
    amxd_trans_add_inst(&trans, 0, "porttrigger-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3."); // LAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    amxd_trans_set_value(uint32_t, &trans, "Port", 6000);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 6001);
    amxd_trans_set_value(uint32_t, &trans, "AutoDisableDuration", 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(object);
    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
}

void test_change_MaxPortTriggerNumber(UNUSED void** state) {
    amxd_trans_t trans;

    expect_query_lan_ipv4_addresses();

    amxd_trans_init(&trans);
    print_message("add NAT.PortTrigger\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.");
    amxd_trans_add_inst(&trans, 0, "porttrigger-1");
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3."); // LAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    amxd_trans_set_value(uint32_t, &trans, "Port", 6000);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 6001);
    amxd_trans_set_value(uint32_t, &trans, "AutoDisableDuration", 10);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(uint32_t, &trans, "MaxPortTriggerNumberOfEntries", 0);
    assert_int_not_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(uint32_t, &trans, "MaxPortTriggerNumberOfEntries", 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.");
    amxd_trans_set_value(uint32_t, &trans, "MaxPortTriggerNumberOfEntries", 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.");
    amxd_trans_del_inst(&trans, 0, "porttrigger-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}


void test_add_porttrigger_rule(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(object);

    amxd_trans_init(&trans);

    print_message("add NAT.PortTrigger.{i}.Rule instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.Rule.");
    amxd_trans_add_inst(&trans, 0, "rule-1");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP");
    amxd_trans_set_value(uint32_t, &trans, "Port", 8000);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 8001);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.Rule.rule-1.");
    assert_non_null(object);
}

void test_enable_porttrigger(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* object = NULL;

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(object);

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("enable porttrigger\n");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);

    assert_int_equal(amxd_object_get_params(object, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_swap_to_inactive_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("swap to Inactive ScheduleRef -> Status should be Inactive\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Inactive");

    amxc_var_clean(&params);
}

void test_add_active_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);

    print_message("add Active ScheduleRef, we have an Active and Inactive schedule inside the Ref resulting in a global Active Status -> Status should be Enabled\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1,Schedules.Schedule.2");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    amxc_var_init(&params);
    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    amxc_var_clean(&params);
}

void test_remove_inactive_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove Inactive ScheduleRef, we only have an Active schedule inside the Ref resulting in a global Active Status -> Status should be Enabled\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_add_disabled_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("add Disabled ScheduleRef, we have an Active and Disabled schedule inside the Ref resulting in a global Active Status -> Status should be Enabled\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1,Schedules.Schedule.3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_remove_active_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("remove Active ScheduleRef, we only have a Disabled schedule inside the Ref resulting in a global Disabled Status -> Status should be Disabled\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.3");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
}

void test_swap_to_active_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);

    print_message("swap to Active ScheduleRef, we only have an Active schedule inside the Ref resulting in a global Active Status -> Status should be Enabled\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "Schedules.Schedule.1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}

void test_clear_schedule_ref(UNUSED void** state) {
    amxc_var_t params;
    amxd_trans_t trans;
    amxd_object_t* porttrigger = NULL;

    amxd_trans_init(&trans);
    amxc_var_init(&params);
    print_message("clear ScheduleRef, the status is now based on the Enable field -> Status should be Enabled\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.");
    amxd_trans_set_value(cstring_t, &trans, "ScheduleRef", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);

    porttrigger = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(porttrigger);

    assert_int_equal(amxd_object_get_params(porttrigger, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    amxc_var_clean(&params);
}


void test_porttrigger_change_destination_port(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(object);

    amxd_trans_init(&trans);

    print_message("change NAT.PortTrigger.{i}.PortEndRange\n");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 6002);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6002), 1);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change NAT.PortTrigger.{i}.PortEndRange\n");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 6001);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6002), 1);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_active_rule(UNUSED void** state) {
    print_message("this test skips the netlink read handler and calls the callback directly, expect an update of Stats object and extra rules to be added\n");
    expect_fw_replace_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_replace_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 3);
    expect_fw_replace_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 4);
    expect_fw_replace_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 5);
    expect_fw_replace_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
    porttrigger_handler("172.17.0.2", 6, 6000);
    handle_events();
}

void test_activate_rule_with_wrong_protocol(UNUSED void** state) {
    print_message("call the callback with some other protocol, nothing should happen because there is no port trigger for this port/destination combination\n");
    porttrigger_handler("172.17.0.2", 17, 6000);
    handle_events();
}

void test_activate_rule_with_wrong_port(UNUSED void** state) {
    print_message("call the callback with some other port, nothing should happen because there is no port trigger for this port/destination combination\n");
    porttrigger_handler("172.17.0.2", 6, 6010);
    handle_events();
}

void test_activate_rule_with_wrong_host(UNUSED void** state) {
    print_message("call the callback with some other source ip address, nothing should happen because another device already uses the port trigger\n");
    porttrigger_handler("172.17.0.3", 6, 6000);
    handle_events();
}

void test_rule_change_destination_port(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.Rule.rule-1.");
    assert_non_null(object);

    amxd_trans_init(&trans);

    print_message("change NAT.PortTrigger.{i}.Rule.{i}.PortEndRange\n");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 8002);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
    expect_fw_replace_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8002), 2);
    expect_fw_replace_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8002), 3);
    expect_fw_replace_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8002), 4);
    expect_fw_replace_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8002), 5);
    expect_fw_replace_rule_json(jstring_dnat_rule(17, 8000, 8002, "172.17.0.2/32"), 1);
    handle_events();
    amxd_trans_clean(&trans);

    print_message("change NAT.PortTrigger.{i}.Rule.{i}.PortEndRange\n");
    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 8001);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_delete_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8002), 2);
    expect_fw_delete_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8002), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8002), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8002), 2);
    expect_fw_delete_rule_json(jstring_dnat_rule(17, 8000, 8002, "172.17.0.2/32"), 1);
    expect_fw_replace_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_replace_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 3);
    expect_fw_replace_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 4);
    expect_fw_replace_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 5);
    expect_fw_replace_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
    handle_events();
    amxd_trans_clean(&trans);
}

void test_add_porttrigger_rule_2(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);

    print_message("add another NAT.PortTrigger.{i}.Rule instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.Rule.");
    amxd_trans_add_inst(&trans, 0, "rule-2");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    amxd_trans_set_value(uint32_t, &trans, "Port", 9000);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 0);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    expect_fw_replace_rule_json(jstring_filter_rule_log(6, "172.17.0.2/32", 9000, 0), 6);
    expect_fw_replace_rule_json(jstring_filter_rule(6, "172.17.0.2/32", 9000, 0), 7);
    expect_fw_replace_rule_json(jstring_filter_reverse_log(6, "172.17.0.2/32", 9000, 0), 8);
    expect_fw_replace_rule_json(jstring_filter_reverse(6, "172.17.0.2/32", 9000, 0), 9);
    expect_fw_replace_rule_json(jstring_dnat_rule(6, 9000, 0, "172.17.0.2/32"), 2);
    handle_events();
    amxd_trans_clean(&trans);

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.Rule.rule-2.");
    assert_non_null(object);
}

void test_remove_porttrigger_rule_2(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    amxd_trans_init(&trans);

    print_message("remove the another NAT.PortTrigger.{i}.Rule instance\n");
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.porttrigger-1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-2");
    expect_fw_delete_rule_json(jstring_filter_rule_log(6, "172.17.0.2/32", 9000, 0), 6);
    expect_fw_delete_rule_json(jstring_filter_rule(6, "172.17.0.2/32", 9000, 0), 6);
    expect_fw_delete_rule_json(jstring_filter_reverse_log(6, "172.17.0.2/32", 9000, 0), 6);
    expect_fw_delete_rule_json(jstring_filter_reverse(6, "172.17.0.2/32", 9000, 0), 6);
    expect_fw_delete_rule_json(jstring_dnat_rule(6, 9000, 0, "172.17.0.2/32"), 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.Rule.rule-2.");
    assert_null(object);
}

void test_wait_until_expired(UNUSED void** state) {
    print_message("wait until port trigger expires, expect the rules to be removed");
    expect_fw_delete_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
    amxut_timer_go_to_future_ms(10001); // todo how long to wait??
}

void test_active_rule_with_different_host(UNUSED void** state) {
    print_message("calls the callback again with a different source ip address, expect an update of Stats object and extra rules to be added\n");
    expect_fw_replace_rule_json(jstring_filter_rule_log(17, "172.17.0.3/32", 8000, 8001), 2);
    expect_fw_replace_rule_json(jstring_filter_rule(17, "172.17.0.3/32", 8000, 8001), 3);
    expect_fw_replace_rule_json(jstring_filter_reverse_log(17, "172.17.0.3/32", 8000, 8001), 4);
    expect_fw_replace_rule_json(jstring_filter_reverse(17, "172.17.0.3/32", 8000, 8001), 5);
    expect_fw_replace_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.3/32"), 1);
    porttrigger_handler("172.17.0.3", 6, 6000);
    handle_events();
}

void test_delete_porttrigger(UNUSED void** state) {
    amxd_trans_t trans;
    amxd_object_t* object = NULL;

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_non_null(object);

    amxd_trans_init(&trans);

    print_message("delete instance, allocated memory should be freed\n");
    expect_fw_delete_rule_json(jstring_filter_rule_log(17, "172.17.0.3/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_rule(17, "172.17.0.3/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse_log(17, "172.17.0.3/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_filter_reverse(17, "172.17.0.3/32", 8000, 8001), 2);
    expect_fw_delete_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.3/32"), 1);
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.");
    amxd_trans_del_inst(&trans, 0, "porttrigger-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    handle_events();
    amxd_trans_clean(&trans);

    object = amxd_dm_findf(dm, "NAT.PortTrigger.porttrigger-1.");
    assert_null(object);
}

void test_porttrigger_csv_protocol(UNUSED void** state) {
    amxd_trans_t trans;
    int protocol[] = {6, 17};

    amxd_trans_init(&trans);

    expect_query_lan_ipv4_addresses();

    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.");
    amxd_trans_add_inst(&trans, 0, "pt-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Interface", "Device.IP.Interface.3."); // LAN
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP,UDP");
    amxd_trans_set_value(uint32_t, &trans, "Port", 6000);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 6001);
    amxd_trans_set_value(uint32_t, &trans, "AutoDisableDuration", 1);
    amxd_trans_select_pathf(&trans, ".Rule.");
    amxd_trans_add_inst(&trans, 0, "rule-1");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP");
    amxd_trans_set_value(uint32_t, &trans, "Port", 8000);
    amxd_trans_set_value(uint32_t, &trans, "PortEndRange", 8001);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    expect_fw_replace_rule_json(jstring_nfq_rule(17, 6000, 6001), 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxut_dm_param_equals(cstring_t, "NAT.PortTrigger.pt-99", "Status", "Enabled");

    for(size_t i = 0; i < sizeof(protocol) / sizeof(protocol[i]); i++) {
        expect_fw_replace_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_replace_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 4);
        expect_fw_replace_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 5);
        expect_fw_replace_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 6);
        expect_fw_replace_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
        porttrigger_handler("172.17.0.2", protocol[i], 6000);
        handle_events();

        expect_fw_delete_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
        amxut_timer_go_to_future_ms(10001);
    }

    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.pt-99.Rule.rule-1");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP,UDP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    for(size_t i = 0; i < sizeof(protocol) / sizeof(protocol[i]); i++) {
        expect_fw_replace_rule_json(jstring_filter_rule_log(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_replace_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 4);
        expect_fw_replace_rule_json(jstring_filter_rule(6, "172.17.0.2/32", 8000, 8001), 5);
        expect_fw_replace_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 6);
        expect_fw_replace_rule_json(jstring_filter_reverse_log(6, "172.17.0.2/32", 8000, 8001), 7);
        expect_fw_replace_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 8);
        expect_fw_replace_rule_json(jstring_filter_reverse(6, "172.17.0.2/32", 8000, 8001), 9);
        expect_fw_replace_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 10);
        expect_fw_replace_rule_json(jstring_dnat_rule(6, 8000, 8001, "172.17.0.2/32"), 1);
        expect_fw_replace_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 2);
        porttrigger_handler("172.17.0.2", protocol[i], 6000);
        handle_events();

        expect_fw_delete_rule_json(jstring_filter_rule_log(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_rule_log(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_rule(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_rule(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse_log(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse_log(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse(17, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_dnat_rule(6, 8000, 8001, "172.17.0.2/32"), 1);
        expect_fw_delete_rule_json(jstring_dnat_rule(17, 8000, 8001, "172.17.0.2/32"), 1);
        amxut_timer_go_to_future_ms(10001);
    }

    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.pt-99.Rule.rule-1");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    for(size_t i = 0; i < sizeof(protocol) / sizeof(protocol[i]); i++) {
        expect_fw_replace_rule_json(jstring_filter_rule_log(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_replace_rule_json(jstring_filter_rule(6, "172.17.0.2/32", 8000, 8001), 4);
        expect_fw_replace_rule_json(jstring_filter_reverse_log(6, "172.17.0.2/32", 8000, 8001), 5);
        expect_fw_replace_rule_json(jstring_filter_reverse(6, "172.17.0.2/32", 8000, 8001), 6);
        expect_fw_replace_rule_json(jstring_dnat_rule(6, 8000, 8001, "172.17.0.2/32"), 1);
        porttrigger_handler("172.17.0.2", protocol[i], 6000);
        handle_events();

        expect_fw_delete_rule_json(jstring_filter_rule_log(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_rule(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse_log(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_filter_reverse(6, "172.17.0.2/32", 8000, 8001), 3);
        expect_fw_delete_rule_json(jstring_dnat_rule(6, 8000, 8001, "172.17.0.2/32"), 1);
        amxut_timer_go_to_future_ms(10001);
    }

    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.pt-99");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "UDP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    expect_fw_delete_rule_json(jstring_nfq_rule(17, 6000, 6001), 1);
    expect_fw_replace_rule_json(jstring_nfq_rule(17, 6000, 6001), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.pt-99");
    amxd_trans_set_value(cstring_t, &trans, "Protocol", "TCP,UDP");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_nfq_rule(17, 6000, 6001), 1);
    expect_fw_replace_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    expect_fw_replace_rule_json(jstring_nfq_rule(17, 6000, 6001), 2);
    handle_events();

    amxd_trans_select_pathf(&trans, "NAT.PortTrigger.");
    amxd_trans_del_inst(&trans, 0, "pt-99");
    expect_fw_delete_rule_json(jstring_nfq_rule(6, 6000, 6001), 1);
    expect_fw_delete_rule_json(jstring_nfq_rule(17, 6000, 6001), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}
