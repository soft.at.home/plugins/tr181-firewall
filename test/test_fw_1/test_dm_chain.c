/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "gmap_mock.h"
#include "test_dm_chain.h"
#include <string.h>

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};

#define LOG_LEVEL (7)
#define LOG_FLAGS (0)
#define LOG_PREFIX "[FW][Rule,All]:"

static const char* jstring_filter_log(const char* src, const char* dest, const char* in, const char* out, const char* log_prefix) {
    int len = 0;
    bool ipv6 = false;

    if(src != NULL) {
        ipv6 = (strchr(src, ':') != NULL);
    } else if(dest != NULL) {
        ipv6 = (strchr(dest, ':') != NULL);
    }

    len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "{\"table\":\"filter\","
                    "\"chain\":\"FORWARD%s_L_Low\","
                    "\"enable\":true,"
                    "\"ipv6\":%s,",
                    ipv6 ? "6" : "",
                    ipv6 ? "true" : "false");

    if(in != NULL) {
        len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"in_interface\":\"%s\",",
                        in);
    }

    if(out != NULL) {
        len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"out_interface\":\"%s\",",
                        out);
    }

    if(dest != NULL) {
        len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"destination\":\"%s\","
                        "\"destination_exclude\":false,",
                        dest);
    }

    if(src != NULL) {
        len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"source\":\"%s\","
                        "\"source_exclude\":false,",
                        src);
    }

    if(log_prefix != NULL) {
        len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"target\":13,"
                        "\"target_option\":{\"level\":7, \"logflags\":0, \"prefix\":\"%s\"},"
                        "\"conntrack_state\":\"NEW\"",
                        log_prefix);
    } else {
        len += snprintf(&_buf[len], sizeof(_buf) - 1 - len, "\"target\":6,"
                        "\"target_option\":1");
    }

    snprintf(&_buf[len], sizeof(_buf) - 1 - len, "}");
    return _buf;
}

static const char* jstring_filter_log_allow_inbound(const char* src, const char* dest) {
    return jstring_filter_log(src, dest, "eth0", NULL, "[FW][ACCPT_CONN_ATMPT]:");
}

static const char* jstring_filter_log_allow_outbound(const char* src, const char* dest) {
    return jstring_filter_log(src, dest, NULL, "eth0", "[FW][ACCPT_OUT_CONN]:");
}

static const char* jstring_filter_rule_log(const char* src, const char* dest) {
    return jstring_filter_log(src, dest, NULL, NULL, "[FW][Rule,All]:");
}

static const char* jstring_filter_rule(const char* src, const char* dest) {
    return jstring_filter_log(src, dest, NULL, NULL, NULL);
}

static void a_populate_filter_rule_log(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_source(rule, "10.0.0.1/32"), 0);
    assert_int_equal(fw_rule_set_source_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_source_port(rule, 7000), 0);
    assert_int_equal(fw_rule_set_source_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_in_interface_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination(rule, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_destination_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_dscp_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
}

static void a_populate_filter_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_source(rule, "10.0.0.1/32"), 0);
    assert_int_equal(fw_rule_set_source_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_source_port(rule, 7000), 0);
    assert_int_equal(fw_rule_set_source_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_in_interface_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "br-lan"), 0);
    assert_int_equal(fw_rule_set_out_interface_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination(rule, "192.168.1.0/24"), 0);
    assert_int_equal(fw_rule_set_destination_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_destination_port_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_dscp_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void b_populate_filter_rule_log_allowed_inbound(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_source_mac_address(rule, "aa:bb:cc:dd:ee:ff"), 0);
    assert_int_equal(fw_rule_set_source_mac_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][ACCPT_CONN_ATMPT]:"), 0);
}

static void b_populate_filter_rule_log_allowed_outbound(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_source_mac_address(rule, "aa:bb:cc:dd:ee:ff"), 0);
    assert_int_equal(fw_rule_set_source_mac_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][ACCPT_OUT_CONN]:"), 0);
}

static void b_populate_filter_rule_log(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_source_mac_address(rule, "aa:bb:cc:dd:ee:ff"), 0);
    assert_int_equal(fw_rule_set_source_mac_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
}

static void b_populate_filter_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_source_mac_address(rule, "aa:bb:cc:dd:ee:ff"), 0);
    assert_int_equal(fw_rule_set_source_mac_excluded(rule, true), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void c_populate_filter_rule_log_denied_inbound(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][BLK_CONN_ATMPT]:"), 0);
}

static void c_populate_filter_rule_log_denied_outbound(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][BLK_OUT_ATMPT]:"), 0);
}

static void c_populate_filter_rule_log_allowed_inbound(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_in_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][ACCPT_CONN_ATMPT]:"), 0);
}

static void c_populate_filter_rule_log_allowed_outbound(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_out_interface(rule, "eth0"), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, "[FW][ACCPT_OUT_CONN]:"), 0);
}

static void c_populate_filter_rule_log(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
    assert_int_equal(fw_rule_set_conntrack_state(rule, "NEW"), 0);
    assert_int_equal(fw_rule_set_target_log_options(rule, LOG_LEVEL, LOG_FLAGS, LOG_PREFIX), 0);
}

static void c_populate_filter_rule_accept(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

static void c_populate_filter_rule(fw_rule_t* rule) {
    assert_int_equal(fw_rule_set_table(rule, TEST_TABLE_FILTER), 0);
    assert_int_equal(fw_rule_set_chain(rule, "FORWARD_L_Low"), 0);
    assert_int_equal(fw_rule_set_ipv4(rule, true), 0);
    assert_int_equal(fw_rule_set_protocol(rule, 6), 0);
    assert_int_equal(fw_rule_set_destination_port(rule, 8000), 0);
    assert_int_equal(fw_rule_set_destination_port_range_max(rule, 8001), 0);
    assert_int_equal(fw_rule_set_target_policy(rule, FW_RULE_POLICY_DROP), 0);
    assert_int_equal(fw_rule_set_dscp(rule, 40), 0);
    assert_int_equal(fw_rule_set_enabled(rule, true), 0);
}

int test_chain_setup(void** state) {
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    expect_query_wan_netdevname(); // for logs
    mock_init(state, "logs.odl");
    dm = amxut_bus_dm();
    return 0;
}

int test_chain_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

void test_chain_status(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;
    amxd_object_t* chain = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    expect_query_lan_netdevname();
    expect_query_wan_netdevname();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "L_Low"); // determines which chain is used for the rules: FORWARD_L_Low
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // 1. enable rule, Status == Disabled because chain is not enabled
    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.1");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.1.0/24");
    amxd_trans_set_value(bool, &trans, "DestIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(bool, &trans, "DestPortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "10.0.0.1");
    amxd_trans_set_value(bool, &trans, "SourceIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", 7000);
    amxd_trans_set_value(bool, &trans, "SourcePortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "DestInterface", "Device.IP.Interface.3.");   // LAN
    amxd_trans_set_value(bool, &trans, "DestInterfaceExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(bool, &trans, "SourceInterfaceExclude", true);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(bool, &trans, "DSCPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    // 2. enable chain, rule Status == Enabled
    chain = amxd_dm_findf(dm, "Firewall.Chain.1.");
    assert_non_null(chain);
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, chain);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_rule_log, 1);
    expect_fw_replace_rule(a_populate_filter_rule, 2);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    // 3. disable when Status == Enabled
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_rule_log, 1);
    expect_fw_delete_rule(a_populate_filter_rule, 1);

    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_source_mac(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    print_message("add Firewall.Chain.1.Rule.2 with SourceMAC aa:bb:cc:dd:ee:ff, expect status to be enabled\n");
    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.2");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMAC", "aa:bb:cc:dd:ee:ff");
    amxd_trans_set_value(bool, &trans, "SourceMACExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule(b_populate_filter_rule_log_allowed_inbound, 1);
    expect_fw_replace_rule(b_populate_filter_rule_log_allowed_outbound, 2);
    expect_fw_replace_rule(b_populate_filter_rule_log, 3);
    expect_fw_replace_rule(b_populate_filter_rule, 4);
    handle_events();
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    print_message("delete the instance, expect the rule to be removed\n");
    expect_fw_delete_rule(b_populate_filter_rule_log_allowed_inbound, 1);
    expect_fw_delete_rule(b_populate_filter_rule_log_allowed_outbound, 1);
    expect_fw_delete_rule(b_populate_filter_rule_log, 1);
    expect_fw_delete_rule(b_populate_filter_rule, 1);
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 2, NULL);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxc_var_clean(&params);
}

void test_rule_ip_or_mask(UNUSED void** state) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceIP", "192.168.1.2/32");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "192.168.1.0/24");
    amxd_trans_set_value(cstring_t, &trans, "DestIP", "192.168.2.1/32");
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.2.0/24");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", "192.168.2.0/24"), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.0/24", "192.168.2.0/24"), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 4);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.2/32", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.2/32", "192.168.2.0/24"), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.2/32", "192.168.2.0/24"), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.0/24"), 4);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.2/32", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.2/32", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.2/32", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.2/32", "192.168.2.1/32"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.2/32", "192.168.2.1/32"), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.2/32", "192.168.2.1/32"), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.1/32"), 4);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "192.168.1.0/24");
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.2.0/24");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.2/32", "192.168.2.1/32"), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.2/32", "192.168.2.1/32"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.2/32", "192.168.2.1/32"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.2/32", "192.168.2.1/32"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", "192.168.2.0/24"), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.0/24", "192.168.2.0/24"), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 4);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.0/24", "192.168.2.0/24"), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", "192.168.2.0/24"), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
}

void test_chain_add_log_rule(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    expect_query_lan_netdevname();
    expect_query_wan_netdevname();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.rule-99");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "DestMask", "192.168.1.0/24");
    amxd_trans_set_value(bool, &trans, "DestIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(bool, &trans, "DestPortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "10.0.0.1");
    amxd_trans_set_value(bool, &trans, "SourceIPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "SourcePort", 7000);
    amxd_trans_set_value(bool, &trans, "SourcePortExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "DestInterface", "Device.IP.Interface.3.");   // LAN
    amxd_trans_set_value(bool, &trans, "DestInterfaceExclude", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceInterface", "Device.IP.Interface.2."); // WAN
    amxd_trans_set_value(bool, &trans, "SourceInterfaceExclude", true);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(bool, &trans, "DSCPExclude", true);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(cstring_t, &trans, "Target", "Log");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_replace_rule(a_populate_filter_rule_log, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    // 3. disable when Status == Enabled
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(a_populate_filter_rule_log, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_chain_rule_with_log_features_denied(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;
    amxd_object_t* log_object = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-100");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.rule-100");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule(c_populate_filter_rule_log_denied_inbound, 1);
    expect_fw_replace_rule(c_populate_filter_rule_log_denied_outbound, 2);
    expect_fw_replace_rule(c_populate_filter_rule_log, 3);
    expect_fw_replace_rule(c_populate_filter_rule, 4);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    //Disable the Firewall.Log.DeniedOutbound
    log_object = amxd_dm_findf(dm, "Firewall.Log.DeniedOutbound.");
    assert_non_null(log_object);
    print_message("change Log to disable\n");
    amxd_trans_select_object(&trans, log_object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(c_populate_filter_rule_log_denied_inbound, 1);
    expect_fw_delete_rule(c_populate_filter_rule_log_denied_outbound, 1);
    expect_fw_delete_rule(c_populate_filter_rule_log, 1);
    expect_fw_delete_rule(c_populate_filter_rule, 1);

    expect_fw_replace_rule(c_populate_filter_rule_log_denied_inbound, 1);
    expect_fw_replace_rule(c_populate_filter_rule_log, 2);
    expect_fw_replace_rule(c_populate_filter_rule, 3);
    handle_events();

    // 3. disable when Status == Enabled
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(c_populate_filter_rule_log_denied_inbound, 1);
    expect_fw_delete_rule(c_populate_filter_rule_log, 1);
    expect_fw_delete_rule(c_populate_filter_rule, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_chain_rule_with_log_features_allowed(UNUSED void** state) {
    amxd_trans_t trans;
    amxc_var_t params;
    amxd_object_t* instance = NULL;
    amxd_object_t* log_object = NULL;

    amxc_var_init(&params);
    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-101");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    instance = amxd_dm_findf(dm, "Firewall.Chain.1.Rule.rule-101");
    assert_non_null(instance);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DestPort", 8000);
    amxd_trans_set_value(int32_t, &trans, "DestPortRangeMax", 8001);
    amxd_trans_set_value(int32_t, &trans, "Protocol", 6);
    amxd_trans_set_value(int32_t, &trans, "DSCP", 40);
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_replace_rule(c_populate_filter_rule_log_allowed_inbound, 1);
    expect_fw_replace_rule(c_populate_filter_rule_log_allowed_outbound, 2);
    expect_fw_replace_rule(c_populate_filter_rule_log, 3);
    expect_fw_replace_rule(c_populate_filter_rule_accept, 4);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");

    //Disable the Firewall.Log.DeniedOutbound
    log_object = amxd_dm_findf(dm, "Firewall.Log.AllowedOutbound.");
    assert_non_null(log_object);
    print_message("change Log to disable\n");
    amxd_trans_select_object(&trans, log_object);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);

    expect_fw_delete_rule(c_populate_filter_rule_log_allowed_inbound, 1);
    expect_fw_delete_rule(c_populate_filter_rule_log_allowed_outbound, 1);
    expect_fw_delete_rule(c_populate_filter_rule_log, 1);
    expect_fw_delete_rule(c_populate_filter_rule_accept, 1);

    expect_fw_replace_rule(c_populate_filter_rule_log_allowed_inbound, 1);
    expect_fw_replace_rule(c_populate_filter_rule_log, 2);
    expect_fw_replace_rule(c_populate_filter_rule_accept, 3);
    handle_events();

    // 3. disable when Status == Enabled
    amxd_trans_clean(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, "Enable", false);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);

    expect_fw_delete_rule(c_populate_filter_rule_log_allowed_inbound, 1);
    expect_fw_delete_rule(c_populate_filter_rule_log, 1);
    expect_fw_delete_rule(c_populate_filter_rule_accept, 1);
    handle_events();

    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Disabled");

    amxc_var_clean(&params);
    amxd_trans_clean(&trans);
}

void test_rule_vendorclassid_and_ipversion(UNUSED void** state) {
    amxd_trans_t trans;
    gmap_query_t* query = NULL;
    amxc_var_t device_params;

    amxd_trans_init(&trans);
    amxc_var_init(&device_params);

    expect_gmap_open_query_ext("{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[{\"Address\":\"fe80::50\"}]}");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "VendorClassID", "IP-STB");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    handle_events();

    query = gmap_mock_get_latest_query();
    assert_non_null(query);

    // ip address change
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.51\"}],\"IPv6Address\":[{\"Address\":\"fe80::51\"}]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.51/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.51/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.51/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.51/32", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);

    // device gone
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.51\"}],\"IPv6Address\":[{\"Address\":\"fe80::51\"}]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.51/32", NULL), 1);
    query->fn(query, "device-99", &device_params, gmap_query_expression_stop_matching);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // device back ipv4 only
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_expression_start_matching);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 0);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // ipv6 address change
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[{\"Address\":\"fe80::51\"}]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // ipv6 address only
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[],\"IPv6Address\":[{\"Address\":\"fe80::51\"}]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 0);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // ipv4 address only
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 0);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // ipv4 and ipv6 address change
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[{\"Address\":\"fe80::51\"}]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 0);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("fe80::51/128", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("fe80::51/128", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("fe80::51/128", NULL), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxc_var_clean(&device_params);
}

// test SourceIP/SourceMask and vendor class id
void test_rule_vendorclassid_and_source(UNUSED void** state) {
    amxd_trans_t trans;
    gmap_query_t* query = NULL;
    amxc_var_t device_params;

    amxd_trans_init(&trans);
    amxc_var_init(&device_params);

    expect_gmap_open_query_ext("{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[{\"Address\":\"fe80::50\"}]}");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "VendorClassID", "IP-STB");
    amxd_trans_set_value(cstring_t, &trans, "SourceIP", "192.168.1.12");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.12/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.12/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.12/32", NULL), 5);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 6);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.12/32", NULL), 7);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 8);
    handle_events();

    query = gmap_mock_get_latest_query();
    assert_non_null(query);

    // ipv4 address change
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.51\"}],\"IPv6Address\":[]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.12/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.51/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.12/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.51/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.12/32", NULL), 5);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.51/32", NULL), 6);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.12/32", NULL), 7);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.51/32", NULL), 8);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // ipv4 address gone
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[],\"IPv6Address\":[{\"Address\":\"fe80::50\"}]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.51/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.12/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.12/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.12/32", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.12/32", NULL), 4);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceIP", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.12/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.12/32", NULL), 1);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMask", "192.168.1.0/24");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    // ipv4 address change
    amxc_var_set(jstring_t, &device_params, "{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.51\"}],\"IPv6Address\":[]}");
    assert_int_equal(amxc_var_cast(&device_params, AMXC_VAR_ID_HTABLE), 0);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.51/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.51/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 5);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.51/32", NULL), 6);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 7);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.51/32", NULL), 8);
    query->fn(query, "device-99", &device_params, gmap_query_device_updated);
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "VendorClassID", "");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.51/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.51/32", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 4);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");
    query = NULL;

    expect_gmap_open_query_ext("{\"Alias\":\"device-99\",\"IPv4Address\":[{\"Address\":\"192.168.1.50\"}],\"IPv6Address\":[{\"Address\":\"fe80::50\"}]}");
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "VendorClassID", "IP-STB");
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_replace_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 2);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 3);
    expect_fw_replace_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 4);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 5);
    expect_fw_replace_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 6);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 7);
    expect_fw_replace_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 8);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();
    amxut_dm_param_equals(cstring_t, "Firewall.Chain.1.Rule.rule-99", "Status", "Enabled");

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_inbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_log_allow_outbound("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule_log("192.168.1.50/32", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.0/24", NULL), 1);
    expect_fw_delete_rule_json(jstring_filter_rule("192.168.1.50/32", NULL), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxc_var_clean(&device_params);
}
