/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "firewall.h"
#include "firewall/dm_firewall.h"
#include "service/dm_service.h"

#include <amxc/amxc.h>
#include <amxd/amxd_object_expression.h>

#include "mock.h"
#include "test_dm_service.h"

#define SERVICE_NOT_FOUND 2

void test_method_setService(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("if no instance exists with alias, a new instance should be added\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "protocol", "6");
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");

    assert_int_equal(amxd_object_invoke_function(template, "setService", &args, &retval), 0);
    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_HTABLE);

    print_message("if an instance exists with alias, the instance should be modified\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "protocol", "17");
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");

    assert_int_equal(amxd_object_invoke_function(template, "setService", &args, &retval), 0);
    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_HTABLE);
    assert_string_equal(GET_CHAR(&retval, "Protocol"), "17");

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_list_one_service(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;
    int i = 0;
    struct test_data {
        const char* alias;
        int result;
    } td[] = {
        {"toberemoved", 0},
        {"doesNotExist", SERVICE_NOT_FOUND},
        {}
    };

    amxc_var_init(&args);
    amxc_var_init(&retval);

    for(i = 0; td[i].alias; i++) {
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "alias", td[i].alias);

        assert_int_equal(amxd_object_invoke_function(template, "getService", &args, &retval), td[i].result);

        if(td[i].result == 0) {
            assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_LIST);
            assert_int_equal(amxc_llist_size(amxc_var_constcast(amxc_llist_t, &retval)), 1);
        }
    }

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_list_all_services(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    assert_int_equal(amxd_object_invoke_function(template, "getService", &args, &retval), 0);

    assert_int_equal(amxc_var_type_of(&retval), AMXC_VAR_ID_LIST);
    assert_int_equal((amxc_llist_size(amxc_var_constcast(amxc_llist_t, &retval)) > 1), 1);

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}

void test_method_deleteService(UNUSED void** state) {
    amxd_object_t* template = amxd_dm_findf(dm, "Firewall.");
    amxc_var_t retval;
    amxc_var_t args;

    amxc_var_init(&args);
    amxc_var_init(&retval);

    print_message("delete an existing instance\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    assert_int_equal(amxd_object_invoke_function(template, "deleteService", &args, &retval), 0);

    print_message("delete of a non existing instance should be silently ignored\n");
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", "toberemoved");
    assert_int_equal(amxd_object_invoke_function(template, "deleteService", &args, &retval), 0);

    handle_events();

    amxc_var_clean(&args);
    amxc_var_clean(&retval);
}
