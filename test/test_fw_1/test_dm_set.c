/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include "firewall.h"

#include "mock.h"
#include "mock_ipset.h"
#include "test_dm_set.h"

static amxd_dm_t* dm = NULL;
static char _buf[1024] = {0};

int test_set_setup(void** state) {
    mock_init(state, NULL);
    dm = amxut_bus_dm();
    return 0;
}

int test_set_teardown(void** state) {
    mock_cleanup(state);
    return 0;
}

static amxd_status_t create_set(const char* name, const char* type, bool ipv4) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Set.");
    amxd_trans_add_inst(&trans, 0, name);
    amxd_trans_set_value(cstring_t, &trans, "Type", type);
    amxd_trans_set_value(uint32_t, &trans, "IPVersion", ipv4 ? 4 : 6);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);
    handle_events();
    return status;
}

static amxd_status_t update_set(const char* name, const char* type, bool ipv4, bool enable) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Set.%s.", name);
    amxd_trans_set_value(cstring_t, &trans, "Type", type);
    amxd_trans_set_value(uint32_t, &trans, "IPVersion", ipv4 ? 4 : 6);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);
    handle_events();
    return status;
}

static amxd_status_t delete_set(const char* name) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Set.");
    amxd_trans_del_inst(&trans, 0, name);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);
    handle_events();
    return status;
}

static amxd_status_t create_rule_set(const char* set_name, const char* rule_alias, const char* type, const char* item) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Set.%s.Rule.", set_name);
    amxd_trans_add_inst(&trans, 0, rule_alias);
    amxd_trans_set_value(cstring_t, &trans, type, item);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);
    handle_events();
    return status;
}

static amxd_status_t update_rule_set(const char* set_name, const char* rule_alias, const char* type, const char* item) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Set.%s.Rule.%s.", set_name, rule_alias);
    amxd_trans_set_value(cstring_t, &trans, type, item);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);
    handle_events();
    return status;
}

static amxd_status_t remove_rule_set(const char* set_name, const char* rule_alias) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, "Firewall.Set.%s.Rule.", set_name);
    amxd_trans_del_inst(&trans, 0, rule_alias);
    status = amxd_trans_apply(&trans, dm);
    amxd_trans_clean(&trans);
    handle_events();
    return status;
}

void test_ipv4_set(UNUSED void** state) {
    const char* set_name_ipv4 = "set-ipv4";
    const char* rule_alias_1 = "rule-1";
    const char* rule_ip_1 = "192.168.1.25";
    const char* rule_ip_1_updated = "192.168.1.70";
    const char* rule_alias_2 = "rule-2";
    const char* rule_ip_2 = "10.168.1.30";

    // Create ipv4 set
    expect_ipset_create(set_name_ipv4, SET_TYPE_HASH_IPV4);
    assert_int_equal(create_set(set_name_ipv4, "IPAddresses", true), amxd_status_ok);

    // Create two rules in set
    expect_ipset_add_entry(set_name_ipv4, rule_ip_1, false);
    assert_int_equal(create_rule_set(set_name_ipv4, rule_alias_1, "IPAddressList", rule_ip_1), amxd_status_ok);
    expect_ipset_add_entry(set_name_ipv4, rule_ip_2, false);
    assert_int_equal(create_rule_set(set_name_ipv4, rule_alias_2, "IPAddressList", rule_ip_2), amxd_status_ok);

    // Update the first rule, the set is flushed and repopulated with all the rules
    expect_string(__wrap_ipset_flush_set, set, set_name_ipv4);
    expect_ipset_add_entry(set_name_ipv4, rule_ip_1_updated, false);
    expect_ipset_add_entry(set_name_ipv4, rule_ip_2, false);
    assert_int_equal(update_rule_set(set_name_ipv4, rule_alias_1, "IPAddressList", rule_ip_1_updated), amxd_status_ok);

    // Remove the first rule, the set is flushed and repopulated with the remaining rules
    expect_string(__wrap_ipset_flush_set, set, set_name_ipv4);
    expect_ipset_add_entry(set_name_ipv4, rule_ip_2, false);
    assert_int_equal(remove_rule_set(set_name_ipv4, rule_alias_1), amxd_status_ok);

    // Delete set
    expect_string(__wrap_ipset_destroy, set, set_name_ipv4);
    assert_int_equal(delete_set(set_name_ipv4), amxd_status_ok);
}

void test_ipv6_set(UNUSED void** state) {
    const char* set_name_ipv6 = "set-ipv6";
    const char* rule_alias_1 = "rule-1";
    const char* rule_ip_1 = "9d47:a27d:fe72:97ff:ed78:a2c4:d77b:0001";
    const char* rule_ip_1_updated = "9d47:a27d:fe72:97ff:ed78:a2c4:d77b:9596";
    const char* rule_alias_2 = "rule-2";
    const char* rule_ip_2 = "b913:0485:56c9:99e8:4207:3a20:3b8c:a9d9";

    // Create ipv6 set
    expect_ipset_create(set_name_ipv6, SET_TYPE_HASH_IPV6);
    assert_int_equal(create_set(set_name_ipv6, "IPAddresses", false), amxd_status_ok);

    // Create two rules in set
    expect_ipset_add_entry(set_name_ipv6, rule_ip_1, false);
    assert_int_equal(create_rule_set(set_name_ipv6, rule_alias_1, "IPAddressList", rule_ip_1), amxd_status_ok);
    expect_ipset_add_entry(set_name_ipv6, rule_ip_2, false);
    assert_int_equal(create_rule_set(set_name_ipv6, rule_alias_2, "IPAddressList", rule_ip_2), amxd_status_ok);

    // Update the first rule, the set is flushed and repopulated with all the rules
    expect_string(__wrap_ipset_flush_set, set, set_name_ipv6);
    expect_ipset_add_entry(set_name_ipv6, rule_ip_1_updated, false);
    expect_ipset_add_entry(set_name_ipv6, rule_ip_2, false);
    assert_int_equal(update_rule_set(set_name_ipv6, rule_alias_1, "IPAddressList", rule_ip_1_updated), amxd_status_ok);

    // Remove the first rule, the set is flushed and repopulated with the remaining rules
    expect_string(__wrap_ipset_flush_set, set, set_name_ipv6);
    expect_ipset_add_entry(set_name_ipv6, rule_ip_2, false);
    assert_int_equal(remove_rule_set(set_name_ipv6, rule_alias_1), amxd_status_ok);

    // Delete set
    expect_string(__wrap_ipset_destroy, set, set_name_ipv6);
    assert_int_equal(delete_set(set_name_ipv6), amxd_status_ok);
}

void test_mac_set(UNUSED void** state) {
    const char* set_name_mac = "set-mac";
    const char* rule_alias_1 = "rule-1";
    const char* rule_mac_1 = "9e:52:f2:67:27:e3";
    const char* rule_mac_1_updated = "9e:52:f2:67:27:00";
    const char* rule_alias_2 = "rule-2";
    const char* rule_mac_2 = "2f:58:55:ac:59:43";

    // Create MAC set
    expect_ipset_create(set_name_mac, SET_TYPE_HASH_MAC);
    assert_int_equal(create_set(set_name_mac, "MACAddresses", false), amxd_status_ok);

    // Create two rules in set
    expect_ipset_add_entry(set_name_mac, rule_mac_1, false);
    assert_int_equal(create_rule_set(set_name_mac, rule_alias_1, "MACAddressList", rule_mac_1), amxd_status_ok);
    expect_ipset_add_entry(set_name_mac, rule_mac_2, false);
    assert_int_equal(create_rule_set(set_name_mac, rule_alias_2, "MACAddressList", rule_mac_2), amxd_status_ok);

    // Update the first rule, the set is flushed and repopulated with all the rules
    expect_string(__wrap_ipset_flush_set, set, set_name_mac);
    expect_ipset_add_entry(set_name_mac, rule_mac_1_updated, false);
    expect_ipset_add_entry(set_name_mac, rule_mac_2, false);
    assert_int_equal(update_rule_set(set_name_mac, rule_alias_1, "MACAddressList", rule_mac_1_updated), amxd_status_ok);

    // Remove the first rule, the set is flushed and repopulated with the remaining rules
    expect_string(__wrap_ipset_flush_set, set, set_name_mac);
    expect_ipset_add_entry(set_name_mac, rule_mac_2, false);
    assert_int_equal(remove_rule_set(set_name_mac, rule_alias_1), amxd_status_ok);

    // Delete set
    expect_string(__wrap_ipset_destroy, set, set_name_mac);
    assert_int_equal(delete_set(set_name_mac), amxd_status_ok);
}

void test_port_set(UNUSED void** state) {
    const char* set_name_port = "set-port";
    const char* rule_alias_1 = "rule-1";
    const char* rule_port_1 = "368";
    const char* rule_port_1_updated = "370";
    const char* rule_alias_2 = "rule-2";
    const char* rule_port_2 = "11000";

    // Create port set
    expect_ipset_create(set_name_port, SET_TYPE_HASH_PORT);
    assert_int_equal(create_set(set_name_port, "Ports", false), amxd_status_ok);

    // Create two rules in set
    expect_ipset_add_entry(set_name_port, rule_port_1, false);
    assert_int_equal(create_rule_set(set_name_port, rule_alias_1, "PortList", rule_port_1), amxd_status_ok);
    expect_ipset_add_entry(set_name_port, rule_port_2, false);
    assert_int_equal(create_rule_set(set_name_port, rule_alias_2, "PortList", rule_port_2), amxd_status_ok);

    // Update the first rule, the set is flushed and repopulated with all the rules
    expect_string(__wrap_ipset_flush_set, set, set_name_port);
    expect_ipset_add_entry(set_name_port, rule_port_1_updated, false);
    expect_ipset_add_entry(set_name_port, rule_port_2, false);
    assert_int_equal(update_rule_set(set_name_port, rule_alias_1, "PortList", rule_port_1_updated), amxd_status_ok);

    // Remove the first rule, the set is flushed and repopulated with the remaining rules
    expect_string(__wrap_ipset_flush_set, set, set_name_port);
    expect_ipset_add_entry(set_name_port, rule_port_2, false);
    assert_int_equal(remove_rule_set(set_name_port, rule_alias_1), amxd_status_ok);

    // Delete set
    expect_string(__wrap_ipset_destroy, set, set_name_port);
    assert_int_equal(delete_set(set_name_port), amxd_status_ok);
}

void test_incompatible_rule_type(UNUSED void** state) {
    const char* set_alias = "set-1";
    const char* rule_alias = "rule-1";
    const char* rule_value[3] = {"192.168.1.4", "2f:58:55:ac:59:49", "537"};
    const char* set_type[3] = {"IPAddresses", "MACAddresses", "Ports"};
    const set_type_t set_type_enum[3] = {SET_TYPE_HASH_IPV4, SET_TYPE_HASH_MAC, SET_TYPE_HASH_PORT};
    const char* rule_param[3] = {"IPAddressList", "MACAddressList", "PortList"};

    for(int i = 0; i < 3; i++) {
        // Create set
        expect_ipset_create(set_alias, set_type_enum[i]);
        assert_int_equal(create_set(set_alias, set_type[i], true), amxd_status_ok);
        for(int j = 0; j < 3; j++) {
            if(j == i) {
                // Set type is compatible
                expect_ipset_add_entry(set_alias, rule_value[j], false);
                assert_int_equal(create_rule_set(set_alias, rule_alias, rule_param[j], rule_value[j]), amxd_status_ok);
                expect_string(__wrap_ipset_flush_set, set, set_alias);
                assert_int_equal(remove_rule_set(set_alias, rule_alias), amxd_status_ok);
            } else {
                // Set type is incompatible
                assert_int_equal(create_rule_set(set_alias, rule_alias, rule_param[j], rule_value[j]), amxd_status_invalid_value);
            }
        }
        expect_string(__wrap_ipset_destroy, set, set_alias);
        assert_int_equal(delete_set(set_alias), amxd_status_ok);
    }
}

void test_set_type_update(UNUSED void** state) {
    const char* set_alias = "set-1";
    const char* rule_alias = "rule-1";
    const char* rule_ip = "192.168.1.25";

    // Create set with type IPv4
    expect_ipset_create(set_alias, SET_TYPE_HASH_IPV4);
    assert_int_equal(create_set(set_alias, "IPAddresses", true), amxd_status_ok);

    // Create rule set
    expect_ipset_add_entry(set_alias, rule_ip, false);
    assert_int_equal(create_rule_set(set_alias, rule_alias, "IPAddressList", rule_ip), amxd_status_ok);

    // Update set type to MAC, ipset is recreated with the new type
    expect_string(__wrap_ipset_flush_set, set, set_alias);
    expect_ipset_update(set_alias, SET_TYPE_HASH_MAC);
    assert_int_equal(update_set(set_alias, "MACAddresses", true, true), amxd_status_ok);

    // Delete set
    expect_string(__wrap_ipset_destroy, set, set_alias);
    assert_int_equal(delete_set(set_alias), amxd_status_ok);
}


static const char* jstring_filter_rule(const char* source_set, bool source_set_excluded, const char* destination_set, bool destination_set_excluded, bool ipv6) {
    snprintf(_buf, sizeof(_buf) - 1, "{\"table\":\"filter\","
             "\"chain\":\"%s\","
             "\"enable\":true,"
             "\"ipv6\":%s,"
             "\"source_ipset\":\"%s\","
             "\"source_ipset_excluded\":%s,"
             "\"destination_ipset\":\"%s\","
             "\"destination_ipset_excluded\":%s,"
             "\"target\":6,"
             "\"target_option\":1"
             "}",
             ipv6 ? "FORWARD6_L_Low" : "FORWARD_L_Low",
             ipv6 ? "true" : "false",
             source_set,
             source_set_excluded ? "true" : "false",
             destination_set,
             destination_set_excluded ? "true" : "false"
             );
    return _buf;
}

void test_rule_set(UNUSED void** state) {
    const char* set_alias = "set-1";
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_ipset_create(set_alias, SET_TYPE_HASH_IPV4);
    assert_int_equal(create_set(set_alias, "IPAddresses", true), amxd_status_ok);

    amxd_trans_select_pathf(&trans, "Firewall.Chain.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Name", "L_Low");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-1");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_replace_rule_json(jstring_filter_rule(set_alias, false, "", false, false), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSetExclude", "Firewall.Set.set-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_rule(set_alias, false, "", false, false), 1);
    expect_fw_replace_rule_json(jstring_filter_rule(set_alias, true, "", false, false), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSetExclude", "");
    amxd_trans_set_value(cstring_t, &trans, "DestMatchSet", "Firewall.Set.set-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_rule(set_alias, true, "", false, false), 1);
    expect_fw_replace_rule_json(jstring_filter_rule("", false, set_alias, false, false), 1);
    handle_events();

    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "DestMatchSet", "");
    amxd_trans_set_value(cstring_t, &trans, "DestMatchSetExclude", "Firewall.Set.set-1");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    expect_fw_delete_rule_json(jstring_filter_rule("", false, set_alias, false, false), 1);
    expect_fw_replace_rule_json(jstring_filter_rule("", false, set_alias, true, false), 1);
    handle_events();

    // Cleanup
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_rule("", false, set_alias, true, false), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    expect_string(__wrap_ipset_destroy, set, set_alias);
    assert_int_equal(delete_set(set_alias), amxd_status_ok);
}

void test_rule_set_ipversion(UNUSED void** state) {
    const char* set_alias_ipv4 = "set-ipv4";
    const char* set_alias_ipv6 = "set-ipv6";
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_ipset_create(set_alias_ipv4, SET_TYPE_HASH_IPV4);
    assert_int_equal(create_set(set_alias_ipv4, "IPAddresses", true), amxd_status_ok);
    expect_ipset_create(set_alias_ipv6, SET_TYPE_HASH_IPV6);
    assert_int_equal(create_set(set_alias_ipv6, "IPAddresses", false), amxd_status_ok);

    // Use an IPv6 SET in a IPv4 Rule object : NOK (no rule created)
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-ipv6");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Use an IPv6 SET in a IPv6 Rule object : OK
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-ipv6");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    expect_fw_replace_rule_json(jstring_filter_rule(set_alias_ipv6, false, "", false, true), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Use an IPv4 SET in a IPv6 Rule object : NOK (no rule created)
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-ipv4");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 6);
    expect_fw_delete_rule_json(jstring_filter_rule(set_alias_ipv6, false, "", false, true), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Use the IPv4 SET in a IPv4 Rule object : OK
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-99");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-ipv4");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    expect_fw_replace_rule_json(jstring_filter_rule(set_alias_ipv4, false, "", false, false), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Cleanup
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_rule(set_alias_ipv4, false, "", false, false), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    expect_string(__wrap_ipset_destroy, set, set_alias_ipv4);
    assert_int_equal(delete_set(set_alias_ipv4), amxd_status_ok);
    expect_string(__wrap_ipset_destroy, set, set_alias_ipv6);
    assert_int_equal(delete_set(set_alias_ipv6), amxd_status_ok);
}


void test_rule_set_disabled(UNUSED void** state) {
    const char* set_alias = "set-1";
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_ipset_create(set_alias, SET_TYPE_HASH_IPV4);
    assert_int_equal(create_set(set_alias, "IPAddresses", true), amxd_status_ok);

    // Disable set-1
    expect_string(__wrap_ipset_destroy, set, set_alias);
    assert_int_equal(update_set(set_alias, "IPAddresses", true, false), amxd_status_ok);

    // Try to use disabled set-1 in Rule object : NOK
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-1");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Cleanup
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    assert_int_equal(delete_set(set_alias), amxd_status_ok);
}

void test_rule_set_references(UNUSED void** state) {
    const char* set_alias = "set-1";
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_ipset_create(set_alias, SET_TYPE_HASH_IPV4);
    assert_int_equal(create_set(set_alias, "IPAddresses", true), amxd_status_ok);

    // Reference set-1 in two differents Rule objects
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-99");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-1");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule");
    amxd_trans_add_inst(&trans, 0, "rule-98");
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "Firewall.Set.set-1");
    amxd_trans_set_value(cstring_t, &trans, "Target", "Accept");
    amxd_trans_set_value(int32_t, &trans, "IPVersion", 4);
    expect_fw_replace_rule_json(jstring_filter_rule(set_alias, false, "", false, false), 1);
    expect_fw_replace_rule_json(jstring_filter_rule(set_alias, false, "", false, false), 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Try to disable set-1 : NOK
    assert_int_equal(update_set(set_alias, "IPAddresses", true, false), amxd_status_invalid_action);
    // Try to update set-1 IPVersion : NOK
    assert_int_equal(update_set(set_alias, "IPAddresses", false, true), amxd_status_invalid_action);
    // Try to update set-1 type : NOK
    assert_int_equal(update_set(set_alias, "MACAddresses", true, true), amxd_status_invalid_action);

    // Remove one of the reference by emptying SourceMatchSet parameter
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.rule-98");
    amxd_trans_set_value(cstring_t, &trans, "SourceMatchSet", "");
    expect_fw_delete_rule_json(jstring_filter_rule(set_alias, false, "", false, false), 2);
    expect_fw_replace_rule_json(jstring_filter_rule("", false, "", false, false), 2);
    assert_int_equal(amxd_trans_apply(&trans, dm), amxd_status_ok);
    amxd_trans_clean(&trans);
    handle_events();

    // Try to disable set-1 : NOK
    assert_int_equal(update_set(set_alias, "IPAddresses", true, false), amxd_status_invalid_action);
    // Try to update set-1 IPVersion : NOK
    assert_int_equal(update_set(set_alias, "IPAddresses", false, true), amxd_status_invalid_action);
    // Try to update set-1 type : NOK
    assert_int_equal(update_set(set_alias, "MACAddresses", true, true), amxd_status_invalid_action);

    // Remove the last reference by directly deleting the Rule object
    amxd_trans_select_pathf(&trans, "Firewall.Chain.1.Rule.");
    amxd_trans_del_inst(&trans, 0, "rule-99");
    expect_fw_delete_rule_json(jstring_filter_rule(set_alias, false, "", false, false), 1);
    assert_int_equal(amxd_trans_apply(&trans, dm), 0);
    amxd_trans_clean(&trans);
    handle_events();

    // Try to update set-1 IPVersion : OK
    expect_ipset_update(set_alias, SET_TYPE_HASH_IPV6);
    assert_int_equal(update_set(set_alias, "IPAddresses", false, true), amxd_status_ok);
    // Try to update set-1 type : OK
    expect_ipset_update(set_alias, SET_TYPE_HASH_MAC);
    assert_int_equal(update_set(set_alias, "MACAddresses", true, true), amxd_status_ok);
    // Try to disable set-1 : OK
    expect_string(__wrap_ipset_destroy, set, set_alias);
    assert_int_equal(update_set(set_alias, "MACAddresses", true, false), amxd_status_ok);
    assert_int_equal(delete_set(set_alias), amxd_status_ok);
}