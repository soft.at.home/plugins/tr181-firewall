MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)

HEADERS = $(wildcard $(INCDIR)/$(TARGET)/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) \
		  $(wildcard $(SRCDIR)/connectiontracking/*.c) \
		  $(wildcard $(SRCDIR)/firewall/*.c) \
		  $(wildcard $(SRCDIR)/nat/*.c) \
		  $(wildcard $(SRCDIR)/service/*.c) \
		  $(wildcard $(SRCDIR)/policy/*.c) \
		  $(wildcard $(SRCDIR)/dmz/*.c) \
		  $(wildcard $(SRCDIR)/interfacesetting/*.c) \
		  $(wildcard $(SRCDIR)/pinhole/*.c) \
		  $(wildcard $(SRCDIR)/porttrigger/*.c) \
		  $(wildcard $(SRCDIR)/gmap/*.c) \
		  $(wildcard $(SRCDIR)/ipset/*.c)

WRAP_FUNC = -Wl,--wrap=
MOCK_WRAP = netmodel_openQuery_getAddrs \
			netmodel_openQuery_getFirstParameter \
			netmodel_openQuery_getParameters \
			netmodel_setFlag \
			netmodel_clearFlag \
			netmodel_closeQuery \
			fw_commit \
			amxp_subproc_vstart \
			amxp_subproc_wait \
			amxp_subproc_get_exitstatus \
			ipset_initialize \
			ipset_cleanup \
			ipset_create \
			ipset_destroy \
			ipset_add_entry \
			ipset_flush_set

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
		  --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../mocks \
		  -fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		  -fprofile-arcs -ftest-coverage \
		   -DWITH_GMAP -DPORTMAPPING_CONNTRACK_INACTIVITY_CHECK

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxb -lamxo -lamxut -lamxj \
		   -ldl -lm -lpthread -lsahtrace -lfwrules -lnetmodel -lipat -ltr181-schedules -lipset\
		   $(addprefix $(WRAP_FUNC),$(MOCK_WRAP)) \
		   -lgcov \
		    -lgmap-client -lamx-conntrack
