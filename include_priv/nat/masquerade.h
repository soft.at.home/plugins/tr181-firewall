/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__MASQUERADE_H__)
#define __MASQUERADE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "netmodel.h" // for INTERFACE_LEN
#include <fwrules/fw_folder.h>
#include <netmodel/common_api.h>

typedef enum {
    NAT_INIT,
    NAT_INACTIVE,
    NAT_ACTIVE,
    NAT_ERROR,
    NAT_ERROR_MISCONF,
} nat_status_t;

typedef struct {
    bool enable;
    nat_status_t status;
    fw_folder_t* folder;
    const char* table;
    const char* chain;
} ft_t;

typedef struct {
    int flags;
    ft_t nat;
    firewall_interface_t fwiface;
    firewall_interface_t address_fwiface;
    amxc_llist_t lanifaces;
    char* nm_intf_path;               // Path to the netmodel interface which provides the netdev name
    amxd_object_t* object;            // Pointer to the datamodel object
} natiface_wan_t;

typedef struct {
    amxc_llist_it_t it;
    amxd_object_t* object;
    firewall_interface_t fwiface;
    amxb_subscription_t* ipv4address_subscription;
    natiface_wan_t* waniface;
} natiface_lan_t;

bool interface_implement_changes(natiface_wan_t* waniface);
natiface_wan_t* interface_create(amxd_object_t* object);
natiface_lan_t* lan_interface_create(amxd_object_t* object, natiface_wan_t* waniface);
void lan_interface_delete(natiface_lan_t* laniface);
bool is_nat_enabled_interface(const char* netmodel_interface);
char* get_nm_intf_by_netdev_name(const char* netdev_name);
int toggle_nat_flags_nm(natiface_wan_t* waniface, const char* flags, bool set);

firewall_interface_t* waniface_get_addresses(amxd_object_t* object);

#ifdef __cplusplus
}
#endif

#endif // __MASQUERADE_H__
