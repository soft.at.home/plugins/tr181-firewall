/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _FIREWALL_NETMODEL_H_
#define _FIREWALL_NETMODEL_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "ip_address.h"
#include <netmodel/common_api.h>

#define INTERFACE_LEN 16
#define IP_LEN 48

/* example:
 +-----------------------------------------fwiface---------------------------------------------------+
 | netmodel_interface = "Device.Logical.Interface.[Alias=='wan']."                                   |
 | query = NetModel.intf.wan.getParameters(name="NetDevName", flag="netdev-bound && (ipv4 || ipv6)") |
 | default_netdevname = eth0                                                                         |
 | linifaces                                                                                         |
 |                                                                                                   |
 +---------------------------------------------------------------------------------------------------+

   linifaces
 |
 +-->+-------------liniface----------------+
 |   | netmodel_interface = ethIntf-ETH0   |
 |   | netdevname = eth0                   |
 |   +-------------------------------------+
 |
 +-->+-------------liniface------------+
 | netmodel_interface = dslite-dslite0 |
 | netdevname = dslite0                |
 +-------------------------------------+
 */

typedef struct {
    amxc_llist_it_t it;
    amxc_string_t netmodel_interface;
    char netdevname[INTERFACE_LEN];
    bool marked;
} linux_interface_t;

typedef struct firewall_interface firewall_interface_t;

struct firewall_interface {
    amxc_string_t netmodel_interface;
    char default_netdevname[INTERFACE_LEN];
    address_t address;
    ipversion_t ipversion;
    amxc_llist_t linifaces;
    bool netdev_required;
    bool addr_required;
    netmodel_query_t* query;
    const char* query_flags;
    void (* query_result_pre_change)(firewall_interface_t* fwiface);
    void (* query_result_changed)(firewall_interface_t* fwiface);
    void* query_priv;
};

void init_firewall_interface(firewall_interface_t* fwiface);
void clean_firewall_interface(firewall_interface_t* fwiface);

bool to_linux_interface(const char* netmodel_interface, ipversion_t ipversion, firewall_interface_t* fwiface);
bool to_linux_interface_dual_query(const char* netmodel_interface, ipversion_t ipversion, firewall_interface_t* fwiface, firewall_interface_t* v6_fwiface);

bool open_ip_address_query(const char* netmodel_interface, ipversion_t ipversion, const char* aflags, firewall_interface_t* fwiface);
bool open_netdev_name_query(const char* netmodel_interface, ipversion_t ipversion, const char* aflags, firewall_interface_t* fwiface);

void _netmodel_closeQuery(netmodel_query_t** query);

#ifdef __cplusplus
}
#endif

#endif // _FIREWALL_NETMODEL_H_
