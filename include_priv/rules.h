/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef _RULES_H_
#define _RULES_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "fwrule_helpers.h"
#include "chains.h"
#include "firewall_utilities.h" // for status_t
#ifdef WITH_GMAP
#include <gmap/gmap.h>
#endif

#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
#include <amx_conntrack/query.h>
#endif

typedef enum {
    FIREWALL_FORWARD,
    FIREWALL_PORTFW,
    FIREWALL_PORTTRIGGER,
    FIREWALL_PINHOLE,
} class_t;

typedef struct {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    folder_wrapper_t log_in_denied;
    folder_wrapper_t log_in_allowed;
    folder_wrapper_t log_out_denied;
    folder_wrapper_t log_out_allowed;
#else //FIREWALL_LOGS
    char data; //use the least amount of memory as it's not used.
#endif //FIREWALL_LOGS
} log_feature_t;

typedef struct {
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
    log_feature_t log_features;
    log_feature_t log_features_reverse;
#endif //FIREWALL_LOGS
    common_t common;
    folder_container_t reverse_folders;
    target_t target;
    target_t log_target;
    class_t class_type;
    amxd_object_t* chain;
    fw_folder_t* dnat_folder;
    fw_folder_t* snat_folder;
    amxc_llist_t src_ports;
    amxc_llist_t dst_ports;
    bool reverse;
    address_t source;
    address_t destination;
    amxp_timer_t* lease_timer;
    uint32_t lease_duration;
    amxp_timer_t* inactivity_timer;
    uint32_t inactivity_timeout;

#ifdef WITH_GMAP
    void* destination_query;
    void* source_query;
#endif

#ifdef PORTMAPPING_CONNTRACK_INACTIVITY_CHECK
    uint32_t ct_count;
    ct_query_t* ct_query;
#endif
} rule_t;

rule_t* rule_create(amxd_object_t* object, bool reverse);
rule_t* rule_init(amxd_object_t* object);
void rule_delete(rule_t** rule);
bool rule_deactivate(rule_t* rule);
bool rule_activate(rule_t* rule);
void rule_update(rule_t* chain);
void rule_suspend(rule_t* rule, bool suspend);

void rules_allowed_origins_changed(const amxc_var_t* from, const amxc_var_t* to, amxd_object_t* template);

// these are helpers for rule_t
void rule_set_target(rule_t* rule, const char* target);
void rule_set_log_target(rule_t* rule, const char* target);
void rule_set_protocol(rule_t* rule, int protocol);
void rule_set_protocols(rule_t* rule, const char* protocols);
void rule_set_src_port(rule_t* rule, int start, int end);
void rule_set_dst_port(rule_t* rule, int start, int end);
void rule_set_source(rule_t* rule, const char* address);
void rule_set_destination(rule_t* rule, const char* address);
void rule_set_enable(rule_t* rule, bool enable);
void rule_set_lease_duration(rule_t** rule, bool force_update_endtime);
bool commit_filter_rules(common_t* common, folder_container_t* reverse_folders, bool ipv4);
#ifdef FIREWALL_LOGS //only compile this piece of code if the logging configuration is enabled
void init_rule_logs(amxd_object_t* object, rule_t* rule, firewall_interface_t* src_interface, firewall_interface_t* dest_interface,
                    bool use_excluded_parameter, amxp_slot_fn_t slot_cb);
bool update_rule_logs(rule_t* p_rule, amxd_object_t* object, const amxc_var_t* const data_rule, const amxc_var_t* const data_log, log_t* log, bool use_excluded_parameter);
#endif //FIREWALL_LOGS

amxd_status_t _RemainingLeaseTime_rule_lease_onread(amxd_object_t* const object,
                                                    amxd_param_t* const param,
                                                    amxd_action_t reason,
                                                    const amxc_var_t* const args,
                                                    amxc_var_t* const retval,
                                                    void* priv);

void _rule_lease_time_changed(const char* const sig_name,
                              const amxc_var_t* const data,
                              void* const priv);

void fw_callback_lease_timer(amxp_timer_t* timer, void* data);

#ifdef __cplusplus
}
#endif

#endif // _RULES_H_
